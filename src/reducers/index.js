import { combineReducers } from 'redux';

import settings from './settings';
import marker from './marker';

const reducers = combineReducers({
  settings,
  marker
});
export default reducers;

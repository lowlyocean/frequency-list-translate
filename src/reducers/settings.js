import { SET_BOLD } from '../actions/settings';

const initialState = {
  bold: true,
};

export default function settings(state = initialState, action) {
  const {type, data} = action;
  switch (type) {
    case SET_BOLD:
      return {
        ...state,
        bold: Boolean(data)
      };
    default:
      return state;
  }
}

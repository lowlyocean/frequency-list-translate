import {
  SET_LEX_COVERAGE, SET_ENABLED
} from '../actions/marker';
import { rankThresh, lexCoverageSliderVals } from '../api/rank_thresh';

const initialState = {
  enabled: true,
  lexCoverageIdx: 0,
  lexCoverage: lexCoverageSliderVals[0],
  maxRank: rankThresh(lexCoverageSliderVals[0]),
};

export default function marker(state = initialState, action) {
  const {type, data} = action;
  switch (type) {
    case SET_LEX_COVERAGE:
      return {
        ...state,
        lexCoverageIdx: data,
        lexCoverage: lexCoverageSliderVals[data],
        maxRank: rankThresh(lexCoverageSliderVals[data])
      };
      case SET_ENABLED:
        return {
          ...state,
          enabled: Boolean(data)
        };
      default:
        return state;
  }
}

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setLexCoverage, setEnabled } from '../../actions/marker';
import {
  Container, Segment, Button, Checkbox, Header, Label,
} from 'semantic-ui-react';
import { Slider } from "react-semantic-ui-range";
import './App.css';

class App extends Component {
  onSettings = (e) => {
    e.preventDefault();
    chrome.runtime.openOptionsPage && chrome.runtime.openOptionsPage();
  }

  onLogout = (e) => {
    e.preventDefault();
    const { accountLogout } = this.props;
    accountLogout();
  }

  onCheck = (e, { checked }) => {
    e.preventDefault();
    const { setEnabled } = this.props;
    setEnabled(checked);
  }

  render() {
    const { enabled, lexCoverage, lexCoverageIdx, maxRank, setLexCoverage } = this.props;
    return (
      <div className='App'>
        <Header as='h3' attached='top' textAlign='center' inverted color='teal'>
          Frequency List Translate
        </Header>
        <div className='App-view'>
          <div>
            <Container textAlign='center'>
              <Button floated='left' circular icon='cog' onClick={this.onSettings} />
              <Checkbox toggle defaultChecked={Boolean(enabled)} onChange={this.onCheck} />
            </Container>
            <Segment textAlign='center'>
                <Slider
                  color="red"
                  inverted={false}
                  settings={{
                    start: lexCoverageIdx,
                    min: 0,
                    max: 8,
                    step: 1,
                    onChange: value => setLexCoverage(value)
                  }}
                />
                <Label color="red">{`Translate ${maxRank} most frequent words (~${lexCoverage * 100}% of text)`}</Label>
            </Segment>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => Object.assign(
  {}, state.marker
);

const mapDispatchToProps = dispatch => ({
  setEnabled: data => {
    dispatch(setEnabled(data));
  },
  setLexCoverage: data => {
    dispatch(setLexCoverage(data));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

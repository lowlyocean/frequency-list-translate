import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form, Checkbox } from 'semantic-ui-react';
import { setBold } from '../../actions/settings';
import './App.css';

class App extends Component {
  onCheck = (e, { name, checked }) => {
    const {setBold} = this.props;
    const map = {bold: setBold};
    map[name] && map[name](checked);
  }

  render() {
    const { bold } = this.props;
    return (
      <div className='App'>
        <h2>Frequency List Translate: Settings</h2>
        <Form>
          <Form.Group inline>
            <Form.Field>
              <Checkbox toggle name='bold' label='Bold' defaultChecked={Boolean(bold)} onChange={this.onCheck} />
            </Form.Field>
          </Form.Group>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => state.settings;

const mapDispatchToProps = dispatch => ({
  setBold: data => {
    dispatch(setBold(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

import { createStore } from 'redux';
import storeCreatorFactory from 'reduxed-chrome-storage';
import reducers from './reducers';
import { mark, unmark } from './mark';

let toUnmark = false;

const render = (store, tabHidden) => {
  const o = store.getState();
  const { marker, settings} = o;
  const toMark = marker.enabled;
  if (toUnmark) {
    unmark();
  }
  if (tabHidden)
    return false;
  if (!toMark)
    return false;
  toUnmark = toMark;
  const {maxRank} = marker;
  const { bold } = settings;
  mark(maxRank, bold);
  return true;
};

// Taken from https://davidwalsh.name/javascript-debounce-function
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
const debounce = (func, wait, immediate) => {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

const delay = 2.e3; //milliseconds between mutations before translating

(async () => {
  const store = await storeCreatorFactory({createStore})(reducers);
  store.subscribe(() => {
    render(store, document.hidden);
  });
  
  const observerConfig = {childList: true, subtree: true, characterData: false, attributes: false };

  const debouncedMutationCallback = (_mutationList, _observer) => debounce((_mutationList, _observer) => {
    _observer.disconnect();
    render(store);
    _observer.observe(document.body,observerConfig);
  }, delay);

  const observer = new MutationObserver(debouncedMutationCallback());
  observer.observe(document.body,observerConfig);
})();


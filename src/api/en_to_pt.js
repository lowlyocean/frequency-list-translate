const lookup = [
    {
        "Rank": 1,
        "Port": "o",
        "Eng": "the"
    },
    {
        "Rank": 2,
        "Port": "de",
        "Eng": "of"
    },
    {
        "Rank": 2,
        "Port": "de",
        "Eng": "from"
    },
    {
        "Rank": 3,
        "Port": "em",
        "Eng": "in"
    },
    {
        "Rank": 3,
        "Port": "em",
        "Eng": "on"
    },
    {
        "Rank": 4,
        "Port": "e",
        "Eng": "and"
    },
    {
        "Rank": 5,
        "Port": "que",
        "Eng": "that"
    },
    {
        "Rank": 5,
        "Port": "que",
        "Eng": "than"
    },
    {
        "Rank": 5,
        "Port": "que",
        "Eng": "what"
    },
    {
        "Rank": 6,
        "Port": "ser",
        "Eng": "be"
    },
    {
        "Rank": 7,
        "Port": "um",
        "Eng": "a"
    },
    {
        "Rank": 7,
        "Port": "um",
        "Eng": "one"
    },
    {
        "Rank": 8,
        "Port": "por",
        "Eng": "by"
    },
    {
        "Rank": 8,
        "Port": "por",
        "Eng": "through"
    },
    {
        "Rank": 8,
        "Port": "por",
        "Eng": "for"
    },
    {
        "Rank": 9,
        "Port": "para",
        "Eng": "to"
    },
    {
        "Rank": 9,
        "Port": "para",
        "Eng": "for"
    },
    {
        "Rank": 9,
        "Port": "para",
        "Eng": "in order to"
    },
    {
        "Rank": 10,
        "Port": "a",
        "Eng": "to"
    },
    {
        "Rank": 10,
        "Port": "a",
        "Eng": "at"
    },
    {
        "Rank": 11,
        "Port": "não",
        "Eng": "no"
    },
    {
        "Rank": 11,
        "Port": "não",
        "Eng": "not"
    },
    {
        "Rank": 12,
        "Port": "com",
        "Eng": "with"
    },
    {
        "Rank": 13,
        "Port": "ter",
        "Eng": "have"
    },
    {
        "Rank": 14,
        "Port": "se",
        "Eng": "myself"
    },
    {
        "Rank": 14,
        "Port": "se",
        "Eng": "yourself"
    },
    {
        "Rank": 14,
        "Port": "se",
        "Eng": "himself"
    },
    {
        "Rank": 14,
        "Port": "se",
        "Eng": "herself"
    },
    {
        "Rank": 14,
        "Port": "se",
        "Eng": "itself"
    },
    {
        "Rank": 14,
        "Port": "se",
        "Eng": "ourselves"
    },
    {
        "Rank": 14,
        "Port": "se",
        "Eng": "yourselves"
    },
    {
        "Rank": 14,
        "Port": "se",
        "Eng": "themselves"
    },
    {
        "Rank": 15,
        "Port": "o",
        "Eng": "it"
    },
    {
        "Rank": 15,
        "Port": "o",
        "Eng": "him"
    },
    {
        "Rank": 15,
        "Port": "o",
        "Eng": "her"
    },
    {
        "Rank": 15,
        "Port": "o",
        "Eng": "them"
    },
    {
        "Rank": 15,
        "Port": "o",
        "Eng": "you"
    },
    {
        "Rank": 16,
        "Port": "seu",
        "Eng": "his"
    },
    {
        "Rank": 16,
        "Port": "seu",
        "Eng": "her"
    },
    {
        "Rank": 17,
        "Port": "como",
        "Eng": "how"
    },
    {
        "Rank": 17,
        "Port": "como",
        "Eng": "like"
    },
    {
        "Rank": 17,
        "Port": "como",
        "Eng": "as"
    },
    {
        "Rank": 18,
        "Port": "estar",
        "Eng": "be"
    },
    {
        "Rank": 19,
        "Port": "mais",
        "Eng": "more"
    },
    {
        "Rank": 19,
        "Port": "mais",
        "Eng": "most"
    },
    {
        "Rank": 20,
        "Port": "mas",
        "Eng": "but"
    },
    {
        "Rank": 21,
        "Port": "fazer",
        "Eng": "do"
    },
    {
        "Rank": 21,
        "Port": "fazer",
        "Eng": "make"
    },
    {
        "Rank": 22,
        "Port": "poder",
        "Eng": "can"
    },
    {
        "Rank": 22,
        "Port": "poder",
        "Eng": "be able to"
    },
    {
        "Rank": 23,
        "Port": "este",
        "Eng": "this"
    },
    {
        "Rank": 24,
        "Port": "ou",
        "Eng": "or"
    },
    {
        "Rank": 24,
        "Port": "ou",
        "Eng": "either"
    },
    {
        "Rank": 25,
        "Port": "ele",
        "Eng": "he"
    },
    {
        "Rank": 25,
        "Port": "ele",
        "Eng": "it"
    },
    {
        "Rank": 26,
        "Port": "esse",
        "Eng": "that"
    },
    {
        "Rank": 27,
        "Port": "outro",
        "Eng": "other"
    },
    {
        "Rank": 27,
        "Port": "outro",
        "Eng": "another"
    },
    {
        "Rank": 28,
        "Port": "muito",
        "Eng": "very"
    },
    {
        "Rank": 28,
        "Port": "muito",
        "Eng": "much"
    },
    {
        "Rank": 28,
        "Port": "muito",
        "Eng": "many"
    },
    {
        "Rank": 29,
        "Port": "haver",
        "Eng": "there is"
    },
    {
        "Rank": 29,
        "Port": "haver",
        "Eng": "have"
    },
    {
        "Rank": 30,
        "Port": "ir",
        "Eng": "go"
    },
    {
        "Rank": 31,
        "Port": "todo",
        "Eng": "all"
    },
    {
        "Rank": 31,
        "Port": "todo",
        "Eng": "every"
    },
    {
        "Rank": 32,
        "Port": "eu",
        "Eng": "I"
    },
    {
        "Rank": 33,
        "Port": "já",
        "Eng": "already"
    },
    {
        "Rank": 33,
        "Port": "já",
        "Eng": "now"
    },
    {
        "Rank": 34,
        "Port": "dizer",
        "Eng": "tell"
    },
    {
        "Rank": 34,
        "Port": "dizer",
        "Eng": "say"
    },
    {
        "Rank": 35,
        "Port": "ano",
        "Eng": "year"
    },
    {
        "Rank": 36,
        "Port": "dar",
        "Eng": "give"
    },
    {
        "Rank": 37,
        "Port": "também",
        "Eng": "also"
    },
    {
        "Rank": 37,
        "Port": "também",
        "Eng": "too"
    },
    {
        "Rank": 37,
        "Port": "também",
        "Eng": "as well"
    },
    {
        "Rank": 38,
        "Port": "quando",
        "Eng": "when"
    },
    {
        "Rank": 39,
        "Port": "mesmo",
        "Eng": "same"
    },
    {
        "Rank": 40,
        "Port": "ver",
        "Eng": "see"
    },
    {
        "Rank": 41,
        "Port": "até",
        "Eng": "until"
    },
    {
        "Rank": 41,
        "Port": "até",
        "Eng": "even"
    },
    {
        "Rank": 41,
        "Port": "até",
        "Eng": "up to"
    },
    {
        "Rank": 42,
        "Port": "dois",
        "Eng": "two"
    },
    {
        "Rank": 43,
        "Port": "ainda",
        "Eng": "still"
    },
    {
        "Rank": 43,
        "Port": "ainda",
        "Eng": "yet"
    },
    {
        "Rank": 44,
        "Port": "isso",
        "Eng": "that"
    },
    {
        "Rank": 45,
        "Port": "grande",
        "Eng": "big"
    },
    {
        "Rank": 45,
        "Port": "grande",
        "Eng": "grand"
    },
    {
        "Rank": 45,
        "Port": "grande",
        "Eng": "great"
    },
    {
        "Rank": 46,
        "Port": "vez",
        "Eng": "each time"
    },
    {
        "Rank": 46,
        "Port": "vez",
        "Eng": "each turn"
    },
    {
        "Rank": 46,
        "Port": "vez",
        "Eng": "iteration"
    },
    {
        "Rank": 47,
        "Port": "algum",
        "Eng": "some"
    },
    {
        "Rank": 48,
        "Port": "ela",
        "Eng": "she"
    },
    {
        "Rank": 48,
        "Port": "ela",
        "Eng": "it"
    },
    {
        "Rank": 49,
        "Port": "depois",
        "Eng": "after"
    },
    {
        "Rank": 50,
        "Port": "entre",
        "Eng": "between"
    },
    {
        "Rank": 50,
        "Port": "entre",
        "Eng": "among"
    },
    {
        "Rank": 51,
        "Port": "dia",
        "Eng": "day"
    },
    {
        "Rank": 52,
        "Port": "só",
        "Eng": "only"
    },
    {
        "Rank": 52,
        "Port": "só",
        "Eng": "just"
    },
    {
        "Rank": 53,
        "Port": "aquele",
        "Eng": "that"
    },
    {
        "Rank": 54,
        "Port": "sobre",
        "Eng": "about"
    },
    {
        "Rank": 54,
        "Port": "sobre",
        "Eng": "over"
    },
    {
        "Rank": 54,
        "Port": "sobre",
        "Eng": "above"
    },
    {
        "Rank": 54,
        "Port": "sobre",
        "Eng": "upon"
    },
    {
        "Rank": 55,
        "Port": "primeiro",
        "Eng": "first"
    },
    {
        "Rank": 56,
        "Port": "ficar",
        "Eng": "stay"
    },
    {
        "Rank": 56,
        "Port": "ficar",
        "Eng": "be located"
    },
    {
        "Rank": 56,
        "Port": "ficar",
        "Eng": "get"
    },
    {
        "Rank": 57,
        "Port": "dever",
        "Eng": "must"
    },
    {
        "Rank": 57,
        "Port": "dever",
        "Eng": "should"
    },
    {
        "Rank": 57,
        "Port": "dever",
        "Eng": "owe"
    },
    {
        "Rank": 58,
        "Port": "passar",
        "Eng": "go through"
    },
    {
        "Rank": 58,
        "Port": "passar",
        "Eng": "spend"
    },
    {
        "Rank": 59,
        "Port": "saber",
        "Eng": "know"
    },
    {
        "Rank": 60,
        "Port": "assim",
        "Eng": "thus"
    },
    {
        "Rank": 60,
        "Port": "assim",
        "Eng": "so"
    },
    {
        "Rank": 60,
        "Port": "assim",
        "Eng": "like this"
    },
    {
        "Rank": 61,
        "Port": "querer",
        "Eng": "want"
    },
    {
        "Rank": 62,
        "Port": "onde",
        "Eng": "where"
    },
    {
        "Rank": 63,
        "Port": "novo",
        "Eng": "new"
    },
    {
        "Rank": 64,
        "Port": "sem",
        "Eng": "without"
    },
    {
        "Rank": 65,
        "Port": "vir",
        "Eng": "come"
    },
    {
        "Rank": 66,
        "Port": "tempo",
        "Eng": "time"
    },
    {
        "Rank": 66,
        "Port": "tempo",
        "Eng": "weather"
    },
    {
        "Rank": 67,
        "Port": "bem",
        "Eng": "well"
    },
    {
        "Rank": 67,
        "Port": "bem",
        "Eng": "very"
    },
    {
        "Rank": 68,
        "Port": "porque",
        "Eng": "because"
    },
    {
        "Rank": 69,
        "Port": "meu",
        "Eng": "my"
    },
    {
        "Rank": 69,
        "Port": "meu",
        "Eng": "mine"
    },
    {
        "Rank": 70,
        "Port": "pessoa",
        "Eng": "person"
    },
    {
        "Rank": 71,
        "Port": "coisa",
        "Eng": "thing"
    },
    {
        "Rank": 72,
        "Port": "então",
        "Eng": "then"
    },
    {
        "Rank": 72,
        "Port": "então",
        "Eng": "so"
    },
    {
        "Rank": 73,
        "Port": "quem",
        "Eng": "whom"
    },
    {
        "Rank": 73,
        "Port": "quem",
        "Eng": "who"
    },
    {
        "Rank": 74,
        "Port": "sempre",
        "Eng": "always"
    },
    {
        "Rank": 75,
        "Port": "qual",
        "Eng": "which"
    },
    {
        "Rank": 76,
        "Port": "chegar",
        "Eng": "arrive"
    },
    {
        "Rank": 77,
        "Port": "vida",
        "Eng": "life"
    },
    {
        "Rank": 78,
        "Port": "pouco",
        "Eng": "a little"
    },
    {
        "Rank": 79,
        "Port": "homem",
        "Eng": "man"
    },
    {
        "Rank": 80,
        "Port": "parte",
        "Eng": "part"
    },
    {
        "Rank": 81,
        "Port": "tudo",
        "Eng": "everything"
    },
    {
        "Rank": 81,
        "Port": "tudo",
        "Eng": "all"
    },
    {
        "Rank": 82,
        "Port": "casa",
        "Eng": "house"
    },
    {
        "Rank": 82,
        "Port": "casa",
        "Eng": "home"
    },
    {
        "Rank": 83,
        "Port": "agora",
        "Eng": "now"
    },
    {
        "Rank": 84,
        "Port": "lhe",
        "Eng": "you"
    },
    {
        "Rank": 84,
        "Port": "lhe",
        "Eng": "him"
    },
    {
        "Rank": 84,
        "Port": "lhe",
        "Eng": "her"
    },
    {
        "Rank": 85,
        "Port": "trabalho",
        "Eng": "work"
    },
    {
        "Rank": 86,
        "Port": "nosso",
        "Eng": "our"
    },
    {
        "Rank": 87,
        "Port": "levar",
        "Eng": "carry"
    },
    {
        "Rank": 88,
        "Port": "pois",
        "Eng": "for"
    },
    {
        "Rank": 88,
        "Port": "pois",
        "Eng": "because"
    },
    {
        "Rank": 88,
        "Port": "pois",
        "Eng": "whereas"
    },
    {
        "Rank": 89,
        "Port": "deixar",
        "Eng": "leave"
    },
    {
        "Rank": 89,
        "Port": "deixar",
        "Eng": "allow"
    },
    {
        "Rank": 90,
        "Port": "bom",
        "Eng": "good"
    },
    {
        "Rank": 91,
        "Port": "começar",
        "Eng": "begin"
    },
    {
        "Rank": 91,
        "Port": "começar",
        "Eng": "start"
    },
    {
        "Rank": 92,
        "Port": "próprio",
        "Eng": "own"
    },
    {
        "Rank": 92,
        "Port": "próprio",
        "Eng": "very own"
    },
    {
        "Rank": 93,
        "Port": "maior",
        "Eng": "greater"
    },
    {
        "Rank": 93,
        "Port": "maior",
        "Eng": "larger"
    },
    {
        "Rank": 94,
        "Port": "caso",
        "Eng": "case"
    },
    {
        "Rank": 95,
        "Port": "falar",
        "Eng": "speak"
    },
    {
        "Rank": 95,
        "Port": "falar",
        "Eng": "talk"
    },
    {
        "Rank": 96,
        "Port": "país",
        "Eng": "country"
    },
    {
        "Rank": 97,
        "Port": "forma",
        "Eng": "form"
    },
    {
        "Rank": 97,
        "Port": "forma",
        "Eng": "way"
    },
    {
        "Rank": 98,
        "Port": "cada",
        "Eng": "each"
    },
    {
        "Rank": 98,
        "Port": "cada",
        "Eng": "every"
    },
    {
        "Rank": 99,
        "Port": "hoje",
        "Eng": "today"
    },
    {
        "Rank": 100,
        "Port": "nem",
        "Eng": "neither"
    },
    {
        "Rank": 100,
        "Port": "nem",
        "Eng": "not"
    },
    {
        "Rank": 100,
        "Port": "nem",
        "Eng": "nor"
    },
    {
        "Rank": 101,
        "Port": "três",
        "Eng": "three"
    },
    {
        "Rank": 102,
        "Port": "se",
        "Eng": "if"
    },
    {
        "Rank": 103,
        "Port": "encontrar",
        "Eng": "find"
    },
    {
        "Rank": 103,
        "Port": "encontrar",
        "Eng": "meet"
    },
    {
        "Rank": 104,
        "Port": "meio",
        "Eng": "means"
    },
    {
        "Rank": 104,
        "Port": "meio",
        "Eng": "way"
    },
    {
        "Rank": 104,
        "Port": "meio",
        "Eng": "half-"
    },
    {
        "Rank": 104,
        "Port": "meio",
        "Eng": "middle"
    },
    {
        "Rank": 105,
        "Port": "aqui",
        "Eng": "here"
    },
    {
        "Rank": 106,
        "Port": "mundo",
        "Eng": "world"
    },
    {
        "Rank": 107,
        "Port": "apenas",
        "Eng": "only"
    },
    {
        "Rank": 107,
        "Port": "apenas",
        "Eng": "just"
    },
    {
        "Rank": 108,
        "Port": "estado",
        "Eng": "state"
    },
    {
        "Rank": 108,
        "Port": "estado",
        "Eng": "condition"
    },
    {
        "Rank": 109,
        "Port": "segundo",
        "Eng": "second"
    },
    {
        "Rank": 109,
        "Port": "segundo",
        "Eng": "according to"
    },
    {
        "Rank": 110,
        "Port": "qualquer",
        "Eng": "any"
    },
    {
        "Rank": 111,
        "Port": "cidade",
        "Eng": "city"
    },
    {
        "Rank": 112,
        "Port": "menos",
        "Eng": "less"
    },
    {
        "Rank": 112,
        "Port": "menos",
        "Eng": "fewer"
    },
    {
        "Rank": 113,
        "Port": "governo",
        "Eng": "government"
    },
    {
        "Rank": 114,
        "Port": "partir",
        "Eng": "starting at N"
    },
    {
        "Rank": 115,
        "Port": "conseguir",
        "Eng": "succeed in"
    },
    {
        "Rank": 115,
        "Port": "conseguir",
        "Eng": "be able to"
    },
    {
        "Rank": 116,
        "Port": "tanto",
        "Eng": "so much"
    },
    {
        "Rank": 116,
        "Port": "tanto",
        "Eng": "enough"
    },
    {
        "Rank": 117,
        "Port": "lado",
        "Eng": "side"
    },
    {
        "Rank": 118,
        "Port": "chamar",
        "Eng": "call"
    },
    {
        "Rank": 119,
        "Port": "melhor",
        "Eng": "better"
    },
    {
        "Rank": 119,
        "Port": "melhor",
        "Eng": "best"
    },
    {
        "Rank": 120,
        "Port": "pensar",
        "Eng": "think"
    },
    {
        "Rank": 121,
        "Port": "nome",
        "Eng": "name"
    },
    {
        "Rank": 122,
        "Port": "isto",
        "Eng": "this"
    },
    {
        "Rank": 123,
        "Port": "certo",
        "Eng": "certain"
    },
    {
        "Rank": 123,
        "Port": "certo",
        "Eng": "right"
    },
    {
        "Rank": 123,
        "Port": "certo",
        "Eng": "sure"
    },
    {
        "Rank": 124,
        "Port": "mulher",
        "Eng": "woman"
    },
    {
        "Rank": 124,
        "Port": "mulher",
        "Eng": "wife"
    },
    {
        "Rank": 125,
        "Port": "conhecer",
        "Eng": "know"
    },
    {
        "Rank": 126,
        "Port": "exemplo",
        "Eng": "example"
    },
    {
        "Rank": 127,
        "Port": "existir",
        "Eng": "exist"
    },
    {
        "Rank": 128,
        "Port": "antes",
        "Eng": "before"
    },
    {
        "Rank": 129,
        "Port": "tal",
        "Eng": "such"
    },
    {
        "Rank": 130,
        "Port": "você",
        "Eng": "you"
    },
    {
        "Rank": 131,
        "Port": "lá",
        "Eng": "there"
    },
    {
        "Rank": 131,
        "Port": "lá",
        "Eng": "over there"
    },
    {
        "Rank": 132,
        "Port": "durante",
        "Eng": "during"
    },
    {
        "Rank": 132,
        "Port": "durante",
        "Eng": "for"
    },
    {
        "Rank": 133,
        "Port": "terra",
        "Eng": "land"
    },
    {
        "Rank": 133,
        "Port": "terra",
        "Eng": "earth"
    },
    {
        "Rank": 134,
        "Port": "último",
        "Eng": "last"
    },
    {
        "Rank": 135,
        "Port": "desde",
        "Eng": "since"
    },
    {
        "Rank": 136,
        "Port": "contra",
        "Eng": "against"
    },
    {
        "Rank": 137,
        "Port": "aí",
        "Eng": "there"
    },
    {
        "Rank": 138,
        "Port": "parecer",
        "Eng": "seem"
    },
    {
        "Rank": 139,
        "Port": "pequeno",
        "Eng": "small"
    },
    {
        "Rank": 140,
        "Port": "quanto",
        "Eng": "how much"
    },
    {
        "Rank": 141,
        "Port": "nada",
        "Eng": "nothing"
    },
    {
        "Rank": 142,
        "Port": "português",
        "Eng": "Portuguese"
    },
    {
        "Rank": 143,
        "Port": "filho",
        "Eng": "son"
    },
    {
        "Rank": 143,
        "Port": "filho",
        "Eng": "children"
    },
    {
        "Rank": 144,
        "Port": "tornar",
        "Eng": "become"
    },
    {
        "Rank": 144,
        "Port": "tornar",
        "Eng": "turn into"
    },
    {
        "Rank": 145,
        "Port": "água",
        "Eng": "water"
    },
    {
        "Rank": 146,
        "Port": "direito",
        "Eng": "right"
    },
    {
        "Rank": 146,
        "Port": "direito",
        "Eng": "law"
    },
    {
        "Rank": 147,
        "Port": "público",
        "Eng": "public"
    },
    {
        "Rank": 148,
        "Port": "entrar",
        "Eng": "come in"
    },
    {
        "Rank": 148,
        "Port": "entrar",
        "Eng": "enter"
    },
    {
        "Rank": 149,
        "Port": "problema",
        "Eng": "problem"
    },
    {
        "Rank": 150,
        "Port": "viver",
        "Eng": "live"
    },
    {
        "Rank": 151,
        "Port": "além",
        "Eng": "beyond"
    },
    {
        "Rank": 151,
        "Port": "além",
        "Eng": "in addition to"
    },
    {
        "Rank": 152,
        "Port": "pôr",
        "Eng": "put"
    },
    {
        "Rank": 152,
        "Port": "pôr",
        "Eng": "place"
    },
    {
        "Rank": 153,
        "Port": "história",
        "Eng": "story"
    },
    {
        "Rank": 153,
        "Port": "história",
        "Eng": "history"
    },
    {
        "Rank": 154,
        "Port": "grupo",
        "Eng": "group"
    },
    {
        "Rank": 155,
        "Port": "hora",
        "Eng": "hour"
    },
    {
        "Rank": 156,
        "Port": "sair",
        "Eng": "leave"
    },
    {
        "Rank": 157,
        "Port": "acabar",
        "Eng": "finish"
    },
    {
        "Rank": 157,
        "Port": "acabar",
        "Eng": "end up"
    },
    {
        "Rank": 158,
        "Port": "continuar",
        "Eng": "continue"
    },
    {
        "Rank": 159,
        "Port": "tão",
        "Eng": "so"
    },
    {
        "Rank": 159,
        "Port": "tão",
        "Eng": "as"
    },
    {
        "Rank": 160,
        "Port": "nunca",
        "Eng": "never"
    },
    {
        "Rank": 161,
        "Port": "dentro",
        "Eng": "within"
    },
    {
        "Rank": 161,
        "Port": "dentro",
        "Eng": "in"
    },
    {
        "Rank": 161,
        "Port": "dentro",
        "Eng": "inside"
    },
    {
        "Rank": 162,
        "Port": "voltar",
        "Eng": "return"
    },
    {
        "Rank": 163,
        "Port": "tomar",
        "Eng": "take"
    },
    {
        "Rank": 163,
        "Port": "tomar",
        "Eng": "drink"
    },
    {
        "Rank": 164,
        "Port": "obra",
        "Eng": "work"
    },
    {
        "Rank": 164,
        "Port": "obra",
        "Eng": "project"
    },
    {
        "Rank": 165,
        "Port": "facto",
        "Eng": "fact"
    },
    {
        "Rank": 166,
        "Port": "ponto",
        "Eng": "point"
    },
    {
        "Rank": 166,
        "Port": "ponto",
        "Eng": "dot"
    },
    {
        "Rank": 166,
        "Port": "ponto",
        "Eng": "period"
    },
    {
        "Rank": 167,
        "Port": "trabalhar",
        "Eng": "work"
    },
    {
        "Rank": 168,
        "Port": "fim",
        "Eng": "purpose"
    },
    {
        "Rank": 168,
        "Port": "fim",
        "Eng": "end"
    },
    {
        "Rank": 169,
        "Port": "quase",
        "Eng": "almost"
    },
    {
        "Rank": 170,
        "Port": "pai",
        "Eng": "father"
    },
    {
        "Rank": 170,
        "Port": "pai",
        "Eng": "parents"
    },
    {
        "Rank": 171,
        "Port": "apresentar",
        "Eng": "introduce"
    },
    {
        "Rank": 171,
        "Port": "apresentar",
        "Eng": "present"
    },
    {
        "Rank": 172,
        "Port": "relação",
        "Eng": "relation"
    },
    {
        "Rank": 173,
        "Port": "criar",
        "Eng": "create"
    },
    {
        "Rank": 174,
        "Port": "considerar",
        "Eng": "consider"
    },
    {
        "Rank": 175,
        "Port": "momento",
        "Eng": "moment"
    },
    {
        "Rank": 176,
        "Port": "receber",
        "Eng": "receive"
    },
    {
        "Rank": 177,
        "Port": "ideia",
        "Eng": "idea"
    },
    {
        "Rank": 178,
        "Port": "política",
        "Eng": "politics"
    },
    {
        "Rank": 179,
        "Port": "vários",
        "Eng": "various"
    },
    {
        "Rank": 179,
        "Port": "vários",
        "Eng": "many"
    },
    {
        "Rank": 180,
        "Port": "lugar",
        "Eng": "place"
    },
    {
        "Rank": 181,
        "Port": "sentir",
        "Eng": "feel"
    },
    {
        "Rank": 182,
        "Port": "livro",
        "Eng": "book"
    },
    {
        "Rank": 183,
        "Port": "nós",
        "Eng": "we"
    },
    {
        "Rank": 183,
        "Port": "nós",
        "Eng": "us"
    },
    {
        "Rank": 184,
        "Port": "mês",
        "Eng": "month"
    },
    {
        "Rank": 185,
        "Port": "alto",
        "Eng": "tall"
    },
    {
        "Rank": 185,
        "Port": "alto",
        "Eng": "high"
    },
    {
        "Rank": 185,
        "Port": "alto",
        "Eng": "top"
    },
    {
        "Rank": 186,
        "Port": "força",
        "Eng": "force"
    },
    {
        "Rank": 186,
        "Port": "força",
        "Eng": "power"
    },
    {
        "Rank": 186,
        "Port": "força",
        "Eng": "strength"
    },
    {
        "Rank": 187,
        "Port": "acontecer",
        "Eng": "happen"
    },
    {
        "Rank": 187,
        "Port": "acontecer",
        "Eng": "occur"
    },
    {
        "Rank": 188,
        "Port": "família",
        "Eng": "family"
    },
    {
        "Rank": 189,
        "Port": "tipo",
        "Eng": "type"
    },
    {
        "Rank": 189,
        "Port": "tipo",
        "Eng": "like"
    },
    {
        "Rank": 190,
        "Port": "presidente",
        "Eng": "president"
    },
    {
        "Rank": 191,
        "Port": "mil",
        "Eng": "thousand"
    },
    {
        "Rank": 192,
        "Port": "tratar",
        "Eng": "treat"
    },
    {
        "Rank": 192,
        "Port": "tratar",
        "Eng": "deal with"
    },
    {
        "Rank": 193,
        "Port": "enquanto",
        "Eng": "while"
    },
    {
        "Rank": 194,
        "Port": "perder",
        "Eng": "lose"
    },
    {
        "Rank": 195,
        "Port": "achar",
        "Eng": "find"
    },
    {
        "Rank": 195,
        "Port": "achar",
        "Eng": "think"
    },
    {
        "Rank": 195,
        "Port": "achar",
        "Eng": "suppose"
    },
    {
        "Rank": 196,
        "Port": "escrever",
        "Eng": "write"
    },
    {
        "Rank": 197,
        "Port": "quatro",
        "Eng": "four"
    },
    {
        "Rank": 198,
        "Port": "usar",
        "Eng": "use"
    },
    {
        "Rank": 199,
        "Port": "único",
        "Eng": "only"
    },
    {
        "Rank": 199,
        "Port": "único",
        "Eng": "unique"
    },
    {
        "Rank": 200,
        "Port": "nenhum",
        "Eng": "none"
    },
    {
        "Rank": 200,
        "Port": "nenhum",
        "Eng": "not a single one"
    },
    {
        "Rank": 201,
        "Port": "contar",
        "Eng": "tell"
    },
    {
        "Rank": 201,
        "Port": "contar",
        "Eng": "count"
    },
    {
        "Rank": 202,
        "Port": "real",
        "Eng": "real"
    },
    {
        "Rank": 202,
        "Port": "real",
        "Eng": "royal"
    },
    {
        "Rank": 202,
        "Port": "real",
        "Eng": "Brazilian currency"
    },
    {
        "Rank": 203,
        "Port": "palavra",
        "Eng": "word"
    },
    {
        "Rank": 204,
        "Port": "embora",
        "Eng": "although"
    },
    {
        "Rank": 204,
        "Port": "embora",
        "Eng": "even though"
    },
    {
        "Rank": 205,
        "Port": "diferente",
        "Eng": "different"
    },
    {
        "Rank": 206,
        "Port": "possível",
        "Eng": "possible"
    },
    {
        "Rank": 207,
        "Port": "importante",
        "Eng": "important"
    },
    {
        "Rank": 208,
        "Port": "mostrar",
        "Eng": "show"
    },
    {
        "Rank": 209,
        "Port": "social",
        "Eng": "social"
    },
    {
        "Rank": 210,
        "Port": "ali",
        "Eng": "there"
    },
    {
        "Rank": 211,
        "Port": "claro",
        "Eng": "clear"
    },
    {
        "Rank": 211,
        "Port": "claro",
        "Eng": "light"
    },
    {
        "Rank": 212,
        "Port": "mão",
        "Eng": "hand"
    },
    {
        "Rank": 213,
        "Port": "logo",
        "Eng": "soon"
    },
    {
        "Rank": 213,
        "Port": "logo",
        "Eng": "quickly"
    },
    {
        "Rank": 213,
        "Port": "logo",
        "Eng": "as soon as"
    },
    {
        "Rank": 214,
        "Port": "rio",
        "Eng": "river"
    },
    {
        "Rank": 215,
        "Port": "seguir",
        "Eng": "follow"
    },
    {
        "Rank": 216,
        "Port": "situação",
        "Eng": "situation"
    },
    {
        "Rank": 217,
        "Port": "questão",
        "Eng": "question"
    },
    {
        "Rank": 217,
        "Port": "questão",
        "Eng": "issue"
    },
    {
        "Rank": 217,
        "Port": "questão",
        "Eng": "point"
    },
    {
        "Rank": 218,
        "Port": "procurar",
        "Eng": "seek"
    },
    {
        "Rank": 218,
        "Port": "procurar",
        "Eng": "look for"
    },
    {
        "Rank": 219,
        "Port": "campo",
        "Eng": "field"
    },
    {
        "Rank": 220,
        "Port": "através",
        "Eng": "by way of"
    },
    {
        "Rank": 220,
        "Port": "através",
        "Eng": "through"
    },
    {
        "Rank": 221,
        "Port": "brasileiro",
        "Eng": "Brazilian"
    },
    {
        "Rank": 222,
        "Port": "tentar",
        "Eng": "try"
    },
    {
        "Rank": 222,
        "Port": "tentar",
        "Eng": "attempt"
    },
    {
        "Rank": 223,
        "Port": "serviço",
        "Eng": "service"
    },
    {
        "Rank": 224,
        "Port": "lei",
        "Eng": "law"
    },
    {
        "Rank": 225,
        "Port": "criança",
        "Eng": "child"
    },
    {
        "Rank": 226,
        "Port": "próximo",
        "Eng": "next"
    },
    {
        "Rank": 226,
        "Port": "próximo",
        "Eng": "close"
    },
    {
        "Rank": 226,
        "Port": "próximo",
        "Eng": "near"
    },
    {
        "Rank": 227,
        "Port": "nacional",
        "Eng": "national"
    },
    {
        "Rank": 228,
        "Port": "trazer",
        "Eng": "bring"
    },
    {
        "Rank": 229,
        "Port": "geral",
        "Eng": "general"
    },
    {
        "Rank": 230,
        "Port": "frente",
        "Eng": "front"
    },
    {
        "Rank": 231,
        "Port": "aparecer",
        "Eng": "appear"
    },
    {
        "Rank": 232,
        "Port": "manter",
        "Eng": "maintain"
    },
    {
        "Rank": 233,
        "Port": "colocar",
        "Eng": "place"
    },
    {
        "Rank": 233,
        "Port": "colocar",
        "Eng": "put"
    },
    {
        "Rank": 234,
        "Port": "conta",
        "Eng": "account"
    },
    {
        "Rank": 234,
        "Port": "conta",
        "Eng": "bill"
    },
    {
        "Rank": 235,
        "Port": "pedir",
        "Eng": "ask for"
    },
    {
        "Rank": 235,
        "Port": "pedir",
        "Eng": "request"
    },
    {
        "Rank": 236,
        "Port": "cinco",
        "Eng": "five"
    },
    {
        "Rank": 237,
        "Port": "escola",
        "Eng": "school"
    },
    {
        "Rank": 238,
        "Port": "verdade",
        "Eng": "truth"
    },
    {
        "Rank": 239,
        "Port": "corpo",
        "Eng": "body"
    },
    {
        "Rank": 240,
        "Port": "morrer",
        "Eng": "die"
    },
    {
        "Rank": 241,
        "Port": "guerra",
        "Eng": "war"
    },
    {
        "Rank": 242,
        "Port": "música",
        "Eng": "music"
    },
    {
        "Rank": 243,
        "Port": "região",
        "Eng": "region"
    },
    {
        "Rank": 244,
        "Port": "baixo",
        "Eng": "low"
    },
    {
        "Rank": 244,
        "Port": "baixo",
        "Eng": "short"
    },
    {
        "Rank": 245,
        "Port": "professor",
        "Eng": "teacher"
    },
    {
        "Rank": 245,
        "Port": "professor",
        "Eng": "professor"
    },
    {
        "Rank": 246,
        "Port": "longo",
        "Eng": "long"
    },
    {
        "Rank": 247,
        "Port": "acção",
        "Eng": "action"
    },
    {
        "Rank": 248,
        "Port": "entender",
        "Eng": "understand"
    },
    {
        "Rank": 249,
        "Port": "movimento",
        "Eng": "movement"
    },
    {
        "Rank": 250,
        "Port": "branco",
        "Eng": "white"
    },
    {
        "Rank": 251,
        "Port": "processo",
        "Eng": "process"
    },
    {
        "Rank": 252,
        "Port": "ganhar",
        "Eng": "win"
    },
    {
        "Rank": 252,
        "Port": "ganhar",
        "Eng": "earn"
    },
    {
        "Rank": 252,
        "Port": "ganhar",
        "Eng": "gain"
    },
    {
        "Rank": 253,
        "Port": "arte",
        "Eng": "art"
    },
    {
        "Rank": 254,
        "Port": "papel",
        "Eng": "paper"
    },
    {
        "Rank": 254,
        "Port": "papel",
        "Eng": "role"
    },
    {
        "Rank": 255,
        "Port": "sim",
        "Eng": "yes"
    },
    {
        "Rank": 256,
        "Port": "esperar",
        "Eng": "wait"
    },
    {
        "Rank": 256,
        "Port": "esperar",
        "Eng": "hope"
    },
    {
        "Rank": 256,
        "Port": "esperar",
        "Eng": "expect"
    },
    {
        "Rank": 257,
        "Port": "fundo",
        "Eng": "bottom"
    },
    {
        "Rank": 257,
        "Port": "fundo",
        "Eng": "rear"
    },
    {
        "Rank": 257,
        "Port": "fundo",
        "Eng": "fund"
    },
    {
        "Rank": 258,
        "Port": "senhor",
        "Eng": "lord"
    },
    {
        "Rank": 258,
        "Port": "senhor",
        "Eng": "sir"
    },
    {
        "Rank": 258,
        "Port": "senhor",
        "Eng": "mister"
    },
    {
        "Rank": 259,
        "Port": "número",
        "Eng": "number"
    },
    {
        "Rank": 260,
        "Port": "definir",
        "Eng": "define"
    },
    {
        "Rank": 261,
        "Port": "tarde",
        "Eng": "late"
    },
    {
        "Rank": 261,
        "Port": "tarde",
        "Eng": "afternoon"
    },
    {
        "Rank": 262,
        "Port": "abrir",
        "Eng": "open"
    },
    {
        "Rank": 263,
        "Port": "sociedade",
        "Eng": "society"
    },
    {
        "Rank": 264,
        "Port": "povo",
        "Eng": "people"
    },
    {
        "Rank": 265,
        "Port": "forte",
        "Eng": "strong"
    },
    {
        "Rank": 265,
        "Port": "forte",
        "Eng": "stronghold"
    },
    {
        "Rank": 266,
        "Port": "cabeça",
        "Eng": "head"
    },
    {
        "Rank": 267,
        "Port": "altura",
        "Eng": "height"
    },
    {
        "Rank": 267,
        "Port": "altura",
        "Eng": "time period"
    },
    {
        "Rank": 268,
        "Port": "volta",
        "Eng": "return"
    },
    {
        "Rank": 268,
        "Port": "volta",
        "Eng": "turn"
    },
    {
        "Rank": 269,
        "Port": "condição",
        "Eng": "condition"
    },
    {
        "Rank": 270,
        "Port": "apesar",
        "Eng": "despite"
    },
    {
        "Rank": 270,
        "Port": "apesar",
        "Eng": "even though"
    },
    {
        "Rank": 271,
        "Port": "valor",
        "Eng": "value"
    },
    {
        "Rank": 271,
        "Port": "valor",
        "Eng": "worth"
    },
    {
        "Rank": 272,
        "Port": "mãe",
        "Eng": "mother"
    },
    {
        "Rank": 273,
        "Port": "servir",
        "Eng": "serve"
    },
    {
        "Rank": 274,
        "Port": "pagar",
        "Eng": "pay"
    },
    {
        "Rank": 275,
        "Port": "causa",
        "Eng": "cause"
    },
    {
        "Rank": 276,
        "Port": "antigo",
        "Eng": "ancient"
    },
    {
        "Rank": 276,
        "Port": "antigo",
        "Eng": "old"
    },
    {
        "Rank": 276,
        "Port": "antigo",
        "Eng": "former"
    },
    {
        "Rank": 277,
        "Port": "maneira",
        "Eng": "way"
    },
    {
        "Rank": 277,
        "Port": "maneira",
        "Eng": "manner"
    },
    {
        "Rank": 278,
        "Port": "humano",
        "Eng": "human"
    },
    {
        "Rank": 279,
        "Port": "sentido",
        "Eng": "sense"
    },
    {
        "Rank": 279,
        "Port": "sentido",
        "Eng": "meaning"
    },
    {
        "Rank": 279,
        "Port": "sentido",
        "Eng": "feeling"
    },
    {
        "Rank": 280,
        "Port": "permitir",
        "Eng": "permit"
    },
    {
        "Rank": 280,
        "Port": "permitir",
        "Eng": "allow"
    },
    {
        "Rank": 281,
        "Port": "deus",
        "Eng": "god"
    },
    {
        "Rank": 282,
        "Port": "modo",
        "Eng": "manner"
    },
    {
        "Rank": 282,
        "Port": "modo",
        "Eng": "way"
    },
    {
        "Rank": 282,
        "Port": "modo",
        "Eng": "style"
    },
    {
        "Rank": 283,
        "Port": "gente",
        "Eng": "people"
    },
    {
        "Rank": 283,
        "Port": "gente",
        "Eng": "we"
    },
    {
        "Rank": 283,
        "Port": "gente",
        "Eng": "us"
    },
    {
        "Rank": 284,
        "Port": "imagem",
        "Eng": "image"
    },
    {
        "Rank": 285,
        "Port": "época",
        "Eng": "time period"
    },
    {
        "Rank": 285,
        "Port": "época",
        "Eng": "epoch"
    },
    {
        "Rank": 286,
        "Port": "noite",
        "Eng": "night"
    },
    {
        "Rank": 287,
        "Port": "velho",
        "Eng": "old"
    },
    {
        "Rank": 288,
        "Port": "cair",
        "Eng": "fall"
    },
    {
        "Rank": 289,
        "Port": "aquilo",
        "Eng": "that"
    },
    {
        "Rank": 290,
        "Port": "projecto",
        "Eng": "project"
    },
    {
        "Rank": 291,
        "Port": "final",
        "Eng": "ending"
    },
    {
        "Rank": 291,
        "Port": "final",
        "Eng": "end"
    },
    {
        "Rank": 291,
        "Port": "final",
        "Eng": "final"
    },
    {
        "Rank": 292,
        "Port": "acreditar",
        "Eng": "believe"
    },
    {
        "Rank": 293,
        "Port": "jornal",
        "Eng": "newspaper"
    },
    {
        "Rank": 294,
        "Port": "razão",
        "Eng": "reason"
    },
    {
        "Rank": 295,
        "Port": "espécie",
        "Eng": "type"
    },
    {
        "Rank": 295,
        "Port": "espécie",
        "Eng": "species"
    },
    {
        "Rank": 295,
        "Port": "espécie",
        "Eng": "kind"
    },
    {
        "Rank": 296,
        "Port": "junto",
        "Eng": "together"
    },
    {
        "Rank": 297,
        "Port": "preciso",
        "Eng": "necessary"
    },
    {
        "Rank": 297,
        "Port": "preciso",
        "Eng": "precise"
    },
    {
        "Rank": 298,
        "Port": "século",
        "Eng": "century"
    },
    {
        "Rank": 299,
        "Port": "precisar",
        "Eng": "need"
    },
    {
        "Rank": 300,
        "Port": "ler",
        "Eng": "read"
    },
    {
        "Rank": 301,
        "Port": "dinheiro",
        "Eng": "money"
    },
    {
        "Rank": 302,
        "Port": "talvez",
        "Eng": "maybe"
    },
    {
        "Rank": 303,
        "Port": "plano",
        "Eng": "plan"
    },
    {
        "Rank": 303,
        "Port": "plano",
        "Eng": "flat"
    },
    {
        "Rank": 303,
        "Port": "plano",
        "Eng": "smooth"
    },
    {
        "Rank": 304,
        "Port": "nascer",
        "Eng": "be born"
    },
    {
        "Rank": 305,
        "Port": "centro",
        "Eng": "center"
    },
    {
        "Rank": 305,
        "Port": "centro",
        "Eng": "downtown"
    },
    {
        "Rank": 306,
        "Port": "partido",
        "Eng": "party"
    },
    {
        "Rank": 307,
        "Port": "descobrir",
        "Eng": "discover"
    },
    {
        "Rank": 308,
        "Port": "ouvir",
        "Eng": "hear"
    },
    {
        "Rank": 309,
        "Port": "ligar",
        "Eng": "connect"
    },
    {
        "Rank": 309,
        "Port": "ligar",
        "Eng": "turn on"
    },
    {
        "Rank": 310,
        "Port": "interesse",
        "Eng": "interest"
    },
    {
        "Rank": 311,
        "Port": "amigo",
        "Eng": "friend"
    },
    {
        "Rank": 312,
        "Port": "seguinte",
        "Eng": "following"
    },
    {
        "Rank": 313,
        "Port": "termo",
        "Eng": "term"
    },
    {
        "Rank": 314,
        "Port": "mudar",
        "Eng": "change"
    },
    {
        "Rank": 315,
        "Port": "linha",
        "Eng": "line"
    },
    {
        "Rank": 316,
        "Port": "medida",
        "Eng": "measure"
    },
    {
        "Rank": 317,
        "Port": "teatro",
        "Eng": "theater"
    },
    {
        "Rank": 318,
        "Port": "espaço",
        "Eng": "space"
    },
    {
        "Rank": 318,
        "Port": "espaço",
        "Eng": "room"
    },
    {
        "Rank": 319,
        "Port": "animal",
        "Eng": "animal"
    },
    {
        "Rank": 320,
        "Port": "santo",
        "Eng": "saint"
    },
    {
        "Rank": 320,
        "Port": "santo",
        "Eng": "holy"
    },
    {
        "Rank": 321,
        "Port": "acordo",
        "Eng": "agreement"
    },
    {
        "Rank": 322,
        "Port": "olhar",
        "Eng": "look"
    },
    {
        "Rank": 323,
        "Port": "necessário",
        "Eng": "necessary"
    },
    {
        "Rank": 324,
        "Port": "jovem",
        "Eng": "young"
    },
    {
        "Rank": 325,
        "Port": "futuro",
        "Eng": "future"
    },
    {
        "Rank": 326,
        "Port": "local",
        "Eng": "place"
    },
    {
        "Rank": 326,
        "Port": "local",
        "Eng": "location"
    },
    {
        "Rank": 327,
        "Port": "falta",
        "Eng": "lack"
    },
    {
        "Rank": 328,
        "Port": "morte",
        "Eng": "death"
    },
    {
        "Rank": 329,
        "Port": "político",
        "Eng": "political"
    },
    {
        "Rank": 329,
        "Port": "político",
        "Eng": "politician"
    },
    {
        "Rank": 330,
        "Port": "banco",
        "Eng": "bank"
    },
    {
        "Rank": 330,
        "Port": "banco",
        "Eng": "bench"
    },
    {
        "Rank": 331,
        "Port": "posição",
        "Eng": "position"
    },
    {
        "Rank": 332,
        "Port": "rua",
        "Eng": "street"
    },
    {
        "Rank": 333,
        "Port": "difícil",
        "Eng": "difficult"
    },
    {
        "Rank": 334,
        "Port": "mercado",
        "Eng": "market"
    },
    {
        "Rank": 335,
        "Port": "resolver",
        "Eng": "resolve"
    },
    {
        "Rank": 335,
        "Port": "resolver",
        "Eng": "decide"
    },
    {
        "Rank": 336,
        "Port": "caminho",
        "Eng": "path"
    },
    {
        "Rank": 336,
        "Port": "caminho",
        "Eng": "way"
    },
    {
        "Rank": 337,
        "Port": "jogo",
        "Eng": "game"
    },
    {
        "Rank": 338,
        "Port": "estudar",
        "Eng": "study"
    },
    {
        "Rank": 339,
        "Port": "igreja",
        "Eng": "church"
    },
    {
        "Rank": 340,
        "Port": "formar",
        "Eng": "create"
    },
    {
        "Rank": 340,
        "Port": "formar",
        "Eng": "form"
    },
    {
        "Rank": 340,
        "Port": "formar",
        "Eng": "graduate"
    },
    {
        "Rank": 341,
        "Port": "surgir",
        "Eng": "appear"
    },
    {
        "Rank": 341,
        "Port": "surgir",
        "Eng": "arise"
    },
    {
        "Rank": 341,
        "Port": "surgir",
        "Eng": "emerge"
    },
    {
        "Rank": 342,
        "Port": "lembrar",
        "Eng": "remember"
    },
    {
        "Rank": 342,
        "Port": "lembrar",
        "Eng": "remind"
    },
    {
        "Rank": 343,
        "Port": "representar",
        "Eng": "represent"
    },
    {
        "Rank": 344,
        "Port": "negócio",
        "Eng": "business"
    },
    {
        "Rank": 344,
        "Port": "negócio",
        "Eng": "deal"
    },
    {
        "Rank": 344,
        "Port": "negócio",
        "Eng": "thing"
    },
    {
        "Rank": 345,
        "Port": "via",
        "Eng": "way"
    },
    {
        "Rank": 345,
        "Port": "via",
        "Eng": "road"
    },
    {
        "Rank": 346,
        "Port": "semana",
        "Eng": "week"
    },
    {
        "Rank": 347,
        "Port": "luz",
        "Eng": "light"
    },
    {
        "Rank": 348,
        "Port": "contrário",
        "Eng": "contrary"
    },
    {
        "Rank": 348,
        "Port": "contrário",
        "Eng": "opposite"
    },
    {
        "Rank": 348,
        "Port": "contrário",
        "Eng": "enemy"
    },
    {
        "Rank": 349,
        "Port": "bastante",
        "Eng": "a lot"
    },
    {
        "Rank": 349,
        "Port": "bastante",
        "Eng": "enough"
    },
    {
        "Rank": 350,
        "Port": "pessoal",
        "Eng": "personal"
    },
    {
        "Rank": 350,
        "Port": "pessoal",
        "Eng": "personnel"
    },
    {
        "Rank": 351,
        "Port": "realidade",
        "Eng": "reality"
    },
    {
        "Rank": 351,
        "Port": "realidade",
        "Eng": "real life"
    },
    {
        "Rank": 352,
        "Port": "explicar",
        "Eng": "explain"
    },
    {
        "Rank": 353,
        "Port": "mal",
        "Eng": "poorly"
    },
    {
        "Rank": 353,
        "Port": "mal",
        "Eng": "hardly"
    },
    {
        "Rank": 354,
        "Port": "militar",
        "Eng": "military"
    },
    {
        "Rank": 354,
        "Port": "militar",
        "Eng": "soldier"
    },
    {
        "Rank": 355,
        "Port": "seis",
        "Eng": "six"
    },
    {
        "Rank": 356,
        "Port": "empresa",
        "Eng": "company"
    },
    {
        "Rank": 356,
        "Port": "empresa",
        "Eng": "firm"
    },
    {
        "Rank": 356,
        "Port": "empresa",
        "Eng": "business"
    },
    {
        "Rank": 357,
        "Port": "pé",
        "Eng": "foot"
    },
    {
        "Rank": 357,
        "Port": "pé",
        "Eng": "tree shoot"
    },
    {
        "Rank": 358,
        "Port": "perceber",
        "Eng": "understand"
    },
    {
        "Rank": 358,
        "Port": "perceber",
        "Eng": "perceive"
    },
    {
        "Rank": 359,
        "Port": "ajudar",
        "Eng": "help"
    },
    {
        "Rank": 360,
        "Port": "ordem",
        "Eng": "order"
    },
    {
        "Rank": 361,
        "Port": "defender",
        "Eng": "defend"
    },
    {
        "Rank": 362,
        "Port": "ocorrer",
        "Eng": "occur"
    },
    {
        "Rank": 363,
        "Port": "principal",
        "Eng": "principal"
    },
    {
        "Rank": 363,
        "Port": "principal",
        "Eng": "main"
    },
    {
        "Rank": 364,
        "Port": "assunto",
        "Eng": "subject"
    },
    {
        "Rank": 364,
        "Port": "assunto",
        "Eng": "topic"
    },
    {
        "Rank": 365,
        "Port": "passo",
        "Eng": "step"
    },
    {
        "Rank": 366,
        "Port": "portanto",
        "Eng": "therefore"
    },
    {
        "Rank": 367,
        "Port": "actividade",
        "Eng": "activity"
    },
    {
        "Rank": 368,
        "Port": "aceitar",
        "Eng": "accept"
    },
    {
        "Rank": 369,
        "Port": "direcção",
        "Eng": "direction"
    },
    {
        "Rank": 370,
        "Port": "necessidade",
        "Eng": "necessity"
    },
    {
        "Rank": 371,
        "Port": "dez",
        "Eng": "ten"
    },
    {
        "Rank": 372,
        "Port": "ministro",
        "Eng": "minister"
    },
    {
        "Rank": 373,
        "Port": "qualidade",
        "Eng": "quality"
    },
    {
        "Rank": 374,
        "Port": "realizar",
        "Eng": "fulfill"
    },
    {
        "Rank": 374,
        "Port": "realizar",
        "Eng": "make happen"
    },
    {
        "Rank": 375,
        "Port": "função",
        "Eng": "function"
    },
    {
        "Rank": 376,
        "Port": "olho",
        "Eng": "eye"
    },
    {
        "Rank": 377,
        "Port": "respeito",
        "Eng": "respect"
    },
    {
        "Rank": 378,
        "Port": "tirar",
        "Eng": "take out"
    },
    {
        "Rank": 378,
        "Port": "tirar",
        "Eng": "remove"
    },
    {
        "Rank": 379,
        "Port": "cima",
        "Eng": "top"
    },
    {
        "Rank": 380,
        "Port": "mar",
        "Eng": "sea"
    },
    {
        "Rank": 381,
        "Port": "natural",
        "Eng": "natural"
    },
    {
        "Rank": 382,
        "Port": "costa",
        "Eng": "coast"
    },
    {
        "Rank": 382,
        "Port": "costa",
        "Eng": "back"
    },
    {
        "Rank": 383,
        "Port": "peça",
        "Eng": "piece"
    },
    {
        "Rank": 383,
        "Port": "peça",
        "Eng": "spare part"
    },
    {
        "Rank": 383,
        "Port": "peça",
        "Eng": "play"
    },
    {
        "Rank": 384,
        "Port": "área",
        "Eng": "area"
    },
    {
        "Rank": 385,
        "Port": "lançar",
        "Eng": "throw"
    },
    {
        "Rank": 385,
        "Port": "lançar",
        "Eng": "send out"
    },
    {
        "Rank": 386,
        "Port": "preço",
        "Eng": "price"
    },
    {
        "Rank": 387,
        "Port": "correr",
        "Eng": "run"
    },
    {
        "Rank": 388,
        "Port": "experiência",
        "Eng": "experience"
    },
    {
        "Rank": 389,
        "Port": "norte",
        "Eng": "north"
    },
    {
        "Rank": 390,
        "Port": "princípio",
        "Eng": "principle"
    },
    {
        "Rank": 390,
        "Port": "princípio",
        "Eng": "start"
    },
    {
        "Rank": 390,
        "Port": "princípio",
        "Eng": "beginning"
    },
    {
        "Rank": 391,
        "Port": "autor",
        "Eng": "author"
    },
    {
        "Rank": 392,
        "Port": "curso",
        "Eng": "course"
    },
    {
        "Rank": 392,
        "Port": "curso",
        "Eng": "college major"
    },
    {
        "Rank": 393,
        "Port": "crescer",
        "Eng": "grow"
    },
    {
        "Rank": 394,
        "Port": "aumentar",
        "Eng": "increase"
    },
    {
        "Rank": 394,
        "Port": "aumentar",
        "Eng": "augment"
    },
    {
        "Rank": 395,
        "Port": "decisão",
        "Eng": "decision"
    },
    {
        "Rank": 396,
        "Port": "câmara",
        "Eng": "city council"
    },
    {
        "Rank": 396,
        "Port": "câmara",
        "Eng": "chamber"
    },
    {
        "Rank": 397,
        "Port": "andar",
        "Eng": "walk"
    },
    {
        "Rank": 397,
        "Port": "andar",
        "Eng": "go"
    },
    {
        "Rank": 397,
        "Port": "andar",
        "Eng": "ride"
    },
    {
        "Rank": 398,
        "Port": "sistema",
        "Eng": "system"
    },
    {
        "Rank": 399,
        "Port": "vontade",
        "Eng": "desire"
    },
    {
        "Rank": 399,
        "Port": "vontade",
        "Eng": "will"
    },
    {
        "Rank": 400,
        "Port": "maioria",
        "Eng": "majority"
    },
    {
        "Rank": 401,
        "Port": "acompanhar",
        "Eng": "go with"
    },
    {
        "Rank": 401,
        "Port": "acompanhar",
        "Eng": "keep company"
    },
    {
        "Rank": 402,
        "Port": "viagem",
        "Eng": "trip"
    },
    {
        "Rank": 402,
        "Port": "viagem",
        "Eng": "journey"
    },
    {
        "Rank": 402,
        "Port": "viagem",
        "Eng": "voyage"
    },
    {
        "Rank": 403,
        "Port": "senhora",
        "Eng": "lady"
    },
    {
        "Rank": 404,
        "Port": "aspecto",
        "Eng": "aspect"
    },
    {
        "Rank": 405,
        "Port": "artista",
        "Eng": "artist"
    },
    {
        "Rank": 406,
        "Port": "idade",
        "Eng": "age"
    },
    {
        "Rank": 407,
        "Port": "aberto",
        "Eng": "open"
    },
    {
        "Rank": 408,
        "Port": "conto",
        "Eng": "short story"
    },
    {
        "Rank": 408,
        "Port": "conto",
        "Eng": "monetary value"
    },
    {
        "Rank": 409,
        "Port": "médico",
        "Eng": "medical doctor"
    },
    {
        "Rank": 409,
        "Port": "médico",
        "Eng": "medical"
    },
    {
        "Rank": 410,
        "Port": "capital",
        "Eng": "capital"
    },
    {
        "Rank": 411,
        "Port": "sobretudo",
        "Eng": "above all"
    },
    {
        "Rank": 411,
        "Port": "sobretudo",
        "Eng": "mainly"
    },
    {
        "Rank": 412,
        "Port": "entanto",
        "Eng": "however"
    },
    {
        "Rank": 412,
        "Port": "entanto",
        "Eng": "even though"
    },
    {
        "Rank": 413,
        "Port": "cultura",
        "Eng": "culture"
    },
    {
        "Rank": 414,
        "Port": "escolher",
        "Eng": "choose"
    },
    {
        "Rank": 415,
        "Port": "conhecimento",
        "Eng": "knowledge"
    },
    {
        "Rank": 415,
        "Port": "conhecimento",
        "Eng": "understanding"
    },
    {
        "Rank": 416,
        "Port": "responder",
        "Eng": "respond"
    },
    {
        "Rank": 416,
        "Port": "responder",
        "Eng": "answer"
    },
    {
        "Rank": 417,
        "Port": "pretender",
        "Eng": "plan to"
    },
    {
        "Rank": 417,
        "Port": "pretender",
        "Eng": "intend"
    },
    {
        "Rank": 418,
        "Port": "população",
        "Eng": "population"
    },
    {
        "Rank": 419,
        "Port": "estudo",
        "Eng": "study"
    },
    {
        "Rank": 420,
        "Port": "resposta",
        "Eng": "answer"
    },
    {
        "Rank": 420,
        "Port": "resposta",
        "Eng": "response"
    },
    {
        "Rank": 421,
        "Port": "informação",
        "Eng": "information"
    },
    {
        "Rank": 422,
        "Port": "comum",
        "Eng": "common"
    },
    {
        "Rank": 423,
        "Port": "gostar",
        "Eng": "like"
    },
    {
        "Rank": 424,
        "Port": "superior",
        "Eng": "greater"
    },
    {
        "Rank": 424,
        "Port": "superior",
        "Eng": "higher"
    },
    {
        "Rank": 424,
        "Port": "superior",
        "Eng": "superior"
    },
    {
        "Rank": 425,
        "Port": "saúde",
        "Eng": "health"
    },
    {
        "Rank": 426,
        "Port": "capaz",
        "Eng": "capable"
    },
    {
        "Rank": 427,
        "Port": "porta",
        "Eng": "door"
    },
    {
        "Rank": 428,
        "Port": "terceiro",
        "Eng": "third"
    },
    {
        "Rank": 429,
        "Port": "vender",
        "Eng": "sell"
    },
    {
        "Rank": 430,
        "Port": "república",
        "Eng": "republic"
    },
    {
        "Rank": 431,
        "Port": "matar",
        "Eng": "kill"
    },
    {
        "Rank": 432,
        "Port": "menor",
        "Eng": "smaller"
    },
    {
        "Rank": 432,
        "Port": "menor",
        "Eng": "younger"
    },
    {
        "Rank": 432,
        "Port": "menor",
        "Eng": "less"
    },
    {
        "Rank": 432,
        "Port": "menor",
        "Eng": "least"
    },
    {
        "Rank": 433,
        "Port": "especial",
        "Eng": "special"
    },
    {
        "Rank": 434,
        "Port": "sul",
        "Eng": "south"
    },
    {
        "Rank": 435,
        "Port": "cinema",
        "Eng": "movie"
    },
    {
        "Rank": 435,
        "Port": "cinema",
        "Eng": "movie theater"
    },
    {
        "Rank": 436,
        "Port": "pena",
        "Eng": "penalty"
    },
    {
        "Rank": 436,
        "Port": "pena",
        "Eng": "shame"
    },
    {
        "Rank": 437,
        "Port": "cor",
        "Eng": "color"
    },
    {
        "Rank": 438,
        "Port": "sofrer",
        "Eng": "suffer"
    },
    {
        "Rank": 439,
        "Port": "estrangeiro",
        "Eng": "foreigner"
    },
    {
        "Rank": 439,
        "Port": "estrangeiro",
        "Eng": "stranger"
    },
    {
        "Rank": 439,
        "Port": "estrangeiro",
        "Eng": "foreign"
    },
    {
        "Rank": 440,
        "Port": "ar",
        "Eng": "air"
    },
    {
        "Rank": 441,
        "Port": "carro",
        "Eng": "car"
    },
    {
        "Rank": 441,
        "Port": "carro",
        "Eng": "cart"
    },
    {
        "Rank": 441,
        "Port": "carro",
        "Eng": "buggy"
    },
    {
        "Rank": 442,
        "Port": "igual",
        "Eng": "equal"
    },
    {
        "Rank": 443,
        "Port": "figura",
        "Eng": "figure"
    },
    {
        "Rank": 443,
        "Port": "figura",
        "Eng": "chart"
    },
    {
        "Rank": 443,
        "Port": "figura",
        "Eng": "character"
    },
    {
        "Rank": 444,
        "Port": "interior",
        "Eng": "interior"
    },
    {
        "Rank": 444,
        "Port": "interior",
        "Eng": "inland"
    },
    {
        "Rank": 444,
        "Port": "interior",
        "Eng": "inside"
    },
    {
        "Rank": 445,
        "Port": "dificuldade",
        "Eng": "difficulty"
    },
    {
        "Rank": 446,
        "Port": "decidir",
        "Eng": "decide"
    },
    {
        "Rank": 447,
        "Port": "negro",
        "Eng": "black"
    },
    {
        "Rank": 447,
        "Port": "negro",
        "Eng": "dark"
    },
    {
        "Rank": 448,
        "Port": "compreender",
        "Eng": "comprehend"
    },
    {
        "Rank": 448,
        "Port": "compreender",
        "Eng": "understand"
    },
    {
        "Rank": 449,
        "Port": "milhão",
        "Eng": "million"
    },
    {
        "Rank": 450,
        "Port": "importância",
        "Eng": "importance"
    },
    {
        "Rank": 451,
        "Port": "produzir",
        "Eng": "produce"
    },
    {
        "Rank": 452,
        "Port": "livre",
        "Eng": "free"
    },
    {
        "Rank": 453,
        "Port": "rede",
        "Eng": "network"
    },
    {
        "Rank": 453,
        "Port": "rede",
        "Eng": "net"
    },
    {
        "Rank": 454,
        "Port": "efeito",
        "Eng": "effect"
    },
    {
        "Rank": 455,
        "Port": "fechar",
        "Eng": "shut"
    },
    {
        "Rank": 455,
        "Port": "fechar",
        "Eng": "close"
    },
    {
        "Rank": 456,
        "Port": "possibilidade",
        "Eng": "possibility"
    },
    {
        "Rank": 457,
        "Port": "oito",
        "Eng": "eight"
    },
    {
        "Rank": 458,
        "Port": "principalmente",
        "Eng": "especially"
    },
    {
        "Rank": 458,
        "Port": "principalmente",
        "Eng": "mainly"
    },
    {
        "Rank": 459,
        "Port": "quadro",
        "Eng": "painting"
    },
    {
        "Rank": 459,
        "Port": "quadro",
        "Eng": "panel"
    },
    {
        "Rank": 460,
        "Port": "espírito",
        "Eng": "spirit"
    },
    {
        "Rank": 461,
        "Port": "elemento",
        "Eng": "element"
    },
    {
        "Rank": 462,
        "Port": "base",
        "Eng": "basis"
    },
    {
        "Rank": 462,
        "Port": "base",
        "Eng": "base"
    },
    {
        "Rank": 462,
        "Port": "base",
        "Eng": "foundation"
    },
    {
        "Rank": 463,
        "Port": "cujo",
        "Eng": "whose"
    },
    {
        "Rank": 464,
        "Port": "transformar",
        "Eng": "transform"
    },
    {
        "Rank": 465,
        "Port": "comprar",
        "Eng": "buy"
    },
    {
        "Rank": 466,
        "Port": "tu",
        "Eng": "you"
    },
    {
        "Rank": 467,
        "Port": "após",
        "Eng": "after"
    },
    {
        "Rank": 468,
        "Port": "filha",
        "Eng": "daughter"
    },
    {
        "Rank": 469,
        "Port": "sob",
        "Eng": "below"
    },
    {
        "Rank": 469,
        "Port": "sob",
        "Eng": "under"
    },
    {
        "Rank": 469,
        "Port": "sob",
        "Eng": "underneath"
    },
    {
        "Rank": 470,
        "Port": "referir",
        "Eng": "refer to"
    },
    {
        "Rank": 471,
        "Port": "pedra",
        "Eng": "stone"
    },
    {
        "Rank": 472,
        "Port": "filme",
        "Eng": "movie"
    },
    {
        "Rank": 472,
        "Port": "filme",
        "Eng": "film"
    },
    {
        "Rank": 473,
        "Port": "madeira",
        "Eng": "wood"
    },
    {
        "Rank": 474,
        "Port": "natureza",
        "Eng": "nature"
    },
    {
        "Rank": 475,
        "Port": "período",
        "Eng": "period"
    },
    {
        "Rank": 476,
        "Port": "presente",
        "Eng": "present"
    },
    {
        "Rank": 476,
        "Port": "presente",
        "Eng": "present time"
    },
    {
        "Rank": 476,
        "Port": "presente",
        "Eng": "gift"
    },
    {
        "Rank": 477,
        "Port": "sete",
        "Eng": "seven"
    },
    {
        "Rank": 478,
        "Port": "recurso",
        "Eng": "resource"
    },
    {
        "Rank": 479,
        "Port": "levantar",
        "Eng": "raise"
    },
    {
        "Rank": 479,
        "Port": "levantar",
        "Eng": "stand up"
    },
    {
        "Rank": 480,
        "Port": "mandar",
        "Eng": "send"
    },
    {
        "Rank": 480,
        "Port": "mandar",
        "Eng": "command"
    },
    {
        "Rank": 480,
        "Port": "mandar",
        "Eng": "order"
    },
    {
        "Rank": 481,
        "Port": "unido",
        "Eng": "united"
    },
    {
        "Rank": 482,
        "Port": "resultado",
        "Eng": "result"
    },
    {
        "Rank": 483,
        "Port": "preso",
        "Eng": "captive"
    },
    {
        "Rank": 483,
        "Port": "preso",
        "Eng": "imprisoned"
    },
    {
        "Rank": 484,
        "Port": "reconhecer",
        "Eng": "recognize"
    },
    {
        "Rank": 485,
        "Port": "dirigir",
        "Eng": "direct"
    },
    {
        "Rank": 485,
        "Port": "dirigir",
        "Eng": "drive"
    },
    {
        "Rank": 485,
        "Port": "dirigir",
        "Eng": "conduct"
    },
    {
        "Rank": 486,
        "Port": "tocar",
        "Eng": "touch"
    },
    {
        "Rank": 486,
        "Port": "tocar",
        "Eng": "play"
    },
    {
        "Rank": 487,
        "Port": "director",
        "Eng": "director"
    },
    {
        "Rank": 488,
        "Port": "irmão",
        "Eng": "brother"
    },
    {
        "Rank": 489,
        "Port": "ninguém",
        "Eng": "no one"
    },
    {
        "Rank": 490,
        "Port": "mau",
        "Eng": "bad"
    },
    {
        "Rank": 490,
        "Port": "mau",
        "Eng": "evil"
    },
    {
        "Rank": 491,
        "Port": "simples",
        "Eng": "simple"
    },
    {
        "Rank": 492,
        "Port": "programa",
        "Eng": "program"
    },
    {
        "Rank": 493,
        "Port": "construir",
        "Eng": "construct"
    },
    {
        "Rank": 494,
        "Port": "diverso",
        "Eng": "diverse"
    },
    {
        "Rank": 494,
        "Port": "diverso",
        "Eng": "several"
    },
    {
        "Rank": 495,
        "Port": "cerca",
        "Eng": "about"
    },
    {
        "Rank": 495,
        "Port": "cerca",
        "Eng": "near"
    },
    {
        "Rank": 495,
        "Port": "cerca",
        "Eng": "close by"
    },
    {
        "Rank": 496,
        "Port": "classe",
        "Eng": "class"
    },
    {
        "Rank": 496,
        "Port": "classe",
        "Eng": "type"
    },
    {
        "Rank": 497,
        "Port": "conselho",
        "Eng": "advice"
    },
    {
        "Rank": 497,
        "Port": "conselho",
        "Eng": "counsel"
    },
    {
        "Rank": 497,
        "Port": "conselho",
        "Eng": "council"
    },
    {
        "Rank": 498,
        "Port": "amor",
        "Eng": "love"
    },
    {
        "Rank": 499,
        "Port": "televisão",
        "Eng": "television"
    },
    {
        "Rank": 500,
        "Port": "valer",
        "Eng": "be worth"
    },
    {
        "Rank": 501,
        "Port": "banda",
        "Eng": "band"
    },
    {
        "Rank": 502,
        "Port": "carta",
        "Eng": "letter"
    },
    {
        "Rank": 503,
        "Port": "segurança",
        "Eng": "security"
    },
    {
        "Rank": 503,
        "Port": "segurança",
        "Eng": "safety"
    },
    {
        "Rank": 504,
        "Port": "vinte",
        "Eng": "twenty"
    },
    {
        "Rank": 505,
        "Port": "quarto",
        "Eng": "room"
    },
    {
        "Rank": 505,
        "Port": "quarto",
        "Eng": "bedroom"
    },
    {
        "Rank": 505,
        "Port": "quarto",
        "Eng": "fourth"
    },
    {
        "Rank": 506,
        "Port": "diferença",
        "Eng": "difference"
    },
    {
        "Rank": 507,
        "Port": "perto",
        "Eng": "close"
    },
    {
        "Rank": 508,
        "Port": "opinião",
        "Eng": "opinion"
    },
    {
        "Rank": 509,
        "Port": "título",
        "Eng": "title"
    },
    {
        "Rank": 510,
        "Port": "luta",
        "Eng": "struggle"
    },
    {
        "Rank": 510,
        "Port": "luta",
        "Eng": "fight"
    },
    {
        "Rank": 510,
        "Port": "luta",
        "Eng": "conflict"
    },
    {
        "Rank": 511,
        "Port": "afirmar",
        "Eng": "affirm"
    },
    {
        "Rank": 512,
        "Port": "terminar",
        "Eng": "end"
    },
    {
        "Rank": 512,
        "Port": "terminar",
        "Eng": "finish"
    },
    {
        "Rank": 513,
        "Port": "algo",
        "Eng": "something"
    },
    {
        "Rank": 514,
        "Port": "peixe",
        "Eng": "fish"
    },
    {
        "Rank": 515,
        "Port": "atrás",
        "Eng": "behind"
    },
    {
        "Rank": 515,
        "Port": "atrás",
        "Eng": "back"
    },
    {
        "Rank": 515,
        "Port": "atrás",
        "Eng": "ago"
    },
    {
        "Rank": 516,
        "Port": "atingir",
        "Eng": "reach"
    },
    {
        "Rank": 516,
        "Port": "atingir",
        "Eng": "attain"
    },
    {
        "Rank": 517,
        "Port": "interessar",
        "Eng": "interest"
    },
    {
        "Rank": 517,
        "Port": "interessar",
        "Eng": "concern"
    },
    {
        "Rank": 518,
        "Port": "discutir",
        "Eng": "discuss"
    },
    {
        "Rank": 518,
        "Port": "discutir",
        "Eng": "dispute"
    },
    {
        "Rank": 519,
        "Port": "voz",
        "Eng": "voice"
    },
    {
        "Rank": 520,
        "Port": "companhia",
        "Eng": "company"
    },
    {
        "Rank": 521,
        "Port": "significar",
        "Eng": "mean"
    },
    {
        "Rank": 521,
        "Port": "significar",
        "Eng": "signify"
    },
    {
        "Rank": 522,
        "Port": "objecto",
        "Eng": "object"
    },
    {
        "Rank": 523,
        "Port": "alguém",
        "Eng": "someone"
    },
    {
        "Rank": 524,
        "Port": "anterior",
        "Eng": "previous"
    },
    {
        "Rank": 524,
        "Port": "anterior",
        "Eng": "anterior"
    },
    {
        "Rank": 525,
        "Port": "buscar",
        "Eng": "look for"
    },
    {
        "Rank": 525,
        "Port": "buscar",
        "Eng": "search for"
    },
    {
        "Rank": 526,
        "Port": "dúvida",
        "Eng": "doubt"
    },
    {
        "Rank": 527,
        "Port": "início",
        "Eng": "beginning"
    },
    {
        "Rank": 527,
        "Port": "início",
        "Eng": "start"
    },
    {
        "Rank": 528,
        "Port": "matéria",
        "Eng": "material"
    },
    {
        "Rank": 528,
        "Port": "matéria",
        "Eng": " matter"
    },
    {
        "Rank": 529,
        "Port": "graça",
        "Eng": "thanks"
    },
    {
        "Rank": 529,
        "Port": "graça",
        "Eng": "grace"
    },
    {
        "Rank": 530,
        "Port": "língua",
        "Eng": "language"
    },
    {
        "Rank": 530,
        "Port": "língua",
        "Eng": "tongue"
    },
    {
        "Rank": 531,
        "Port": "enorme",
        "Eng": "enormous"
    },
    {
        "Rank": 532,
        "Port": "americano",
        "Eng": "American"
    },
    {
        "Rank": 533,
        "Port": "doutor",
        "Eng": "doctor"
    },
    {
        "Rank": 534,
        "Port": "julgar",
        "Eng": "judge"
    },
    {
        "Rank": 535,
        "Port": "francês",
        "Eng": "French"
    },
    {
        "Rank": 536,
        "Port": "verde",
        "Eng": "green"
    },
    {
        "Rank": 536,
        "Port": "verde",
        "Eng": "unripe"
    },
    {
        "Rank": 537,
        "Port": "atenção",
        "Eng": "attention"
    },
    {
        "Rank": 538,
        "Port": "nível",
        "Eng": "level"
    },
    {
        "Rank": 539,
        "Port": "criação",
        "Eng": "creation"
    },
    {
        "Rank": 540,
        "Port": "verdadeiro",
        "Eng": "true"
    },
    {
        "Rank": 541,
        "Port": "justiça",
        "Eng": "justice"
    },
    {
        "Rank": 542,
        "Port": "vivo",
        "Eng": "alive"
    },
    {
        "Rank": 543,
        "Port": "educação",
        "Eng": "education"
    },
    {
        "Rank": 544,
        "Port": "passado",
        "Eng": "past"
    },
    {
        "Rank": 544,
        "Port": "passado",
        "Eng": "last"
    },
    {
        "Rank": 544,
        "Port": "passado",
        "Eng": "previous"
    },
    {
        "Rank": 545,
        "Port": "evitar",
        "Eng": "avoid"
    },
    {
        "Rank": 546,
        "Port": "ambiente",
        "Eng": "environment"
    },
    {
        "Rank": 546,
        "Port": "ambiente",
        "Eng": "surroundings"
    },
    {
        "Rank": 547,
        "Port": "guarda",
        "Eng": "guard"
    },
    {
        "Rank": 547,
        "Port": "guarda",
        "Eng": "care"
    },
    {
        "Rank": 548,
        "Port": "económico",
        "Eng": "economic"
    },
    {
        "Rank": 549,
        "Port": "casar",
        "Eng": "marry"
    },
    {
        "Rank": 550,
        "Port": "participar",
        "Eng": "participate"
    },
    {
        "Rank": 551,
        "Port": "rico",
        "Eng": "rich"
    },
    {
        "Rank": 552,
        "Port": "pobre",
        "Eng": "poor"
    },
    {
        "Rank": 553,
        "Port": "cabo",
        "Eng": "cape"
    },
    {
        "Rank": 553,
        "Port": "cabo",
        "Eng": "cable"
    },
    {
        "Rank": 553,
        "Port": "cabo",
        "Eng": "end"
    },
    {
        "Rank": 554,
        "Port": "eleição",
        "Eng": "election"
    },
    {
        "Rank": 555,
        "Port": "apoio",
        "Eng": "support"
    },
    {
        "Rank": 556,
        "Port": "inglês",
        "Eng": "English"
    },
    {
        "Rank": 557,
        "Port": "solução",
        "Eng": "solution"
    },
    {
        "Rank": 558,
        "Port": "demais",
        "Eng": "too much"
    },
    {
        "Rank": 559,
        "Port": "máquina",
        "Eng": "machine"
    },
    {
        "Rank": 559,
        "Port": "máquina",
        "Eng": "device"
    },
    {
        "Rank": 560,
        "Port": "trabalhador",
        "Eng": "worker"
    },
    {
        "Rank": 561,
        "Port": "produto",
        "Eng": "product"
    },
    {
        "Rank": 562,
        "Port": "envolver",
        "Eng": "involve"
    },
    {
        "Rank": 563,
        "Port": "doença",
        "Eng": "illness"
    },
    {
        "Rank": 564,
        "Port": "resto",
        "Eng": "rest"
    },
    {
        "Rank": 564,
        "Port": "resto",
        "Eng": "remaining part"
    },
    {
        "Rank": 565,
        "Port": "ciência",
        "Eng": "science"
    },
    {
        "Rank": 566,
        "Port": "marcar",
        "Eng": "mark"
    },
    {
        "Rank": 566,
        "Port": "marcar",
        "Eng": "set"
    },
    {
        "Rank": 567,
        "Port": "cumprir",
        "Eng": "fulfill"
    },
    {
        "Rank": 567,
        "Port": "cumprir",
        "Eng": "obey"
    },
    {
        "Rank": 568,
        "Port": "contacto",
        "Eng": "contact"
    },
    {
        "Rank": 569,
        "Port": "realmente",
        "Eng": "really"
    },
    {
        "Rank": 569,
        "Port": "realmente",
        "Eng": "truly"
    },
    {
        "Rank": 570,
        "Port": "morar",
        "Eng": "live"
    },
    {
        "Rank": 570,
        "Port": "morar",
        "Eng": "dwell"
    },
    {
        "Rank": 571,
        "Port": "rápido",
        "Eng": "fast"
    },
    {
        "Rank": 571,
        "Port": "rápido",
        "Eng": "rapid"
    },
    {
        "Rank": 572,
        "Port": "literatura",
        "Eng": "literature"
    },
    {
        "Rank": 573,
        "Port": "fácil",
        "Eng": "easy"
    },
    {
        "Rank": 574,
        "Port": "actual",
        "Eng": "current"
    },
    {
        "Rank": 574,
        "Port": "actual",
        "Eng": "up-to-date"
    },
    {
        "Rank": 575,
        "Port": "morto",
        "Eng": "dead"
    },
    {
        "Rank": 576,
        "Port": "exigir",
        "Eng": "require"
    },
    {
        "Rank": 576,
        "Port": "exigir",
        "Eng": "demand"
    },
    {
        "Rank": 577,
        "Port": "diante",
        "Eng": "from then on; in front of"
    },
    {
        "Rank": 578,
        "Port": "liberdade",
        "Eng": "liberty"
    },
    {
        "Rank": 578,
        "Port": "liberdade",
        "Eng": "freedom"
    },
    {
        "Rank": 579,
        "Port": "zona",
        "Eng": "zone"
    },
    {
        "Rank": 580,
        "Port": "terreno",
        "Eng": "land"
    },
    {
        "Rank": 580,
        "Port": "terreno",
        "Eng": "terrain"
    },
    {
        "Rank": 580,
        "Port": "terreno",
        "Eng": "ground"
    },
    {
        "Rank": 581,
        "Port": "provocar",
        "Eng": "provoke"
    },
    {
        "Rank": 582,
        "Port": "garantir",
        "Eng": "guarantee"
    },
    {
        "Rank": 583,
        "Port": "jogar",
        "Eng": "play"
    },
    {
        "Rank": 583,
        "Port": "jogar",
        "Eng": "throw"
    },
    {
        "Rank": 584,
        "Port": "modelo",
        "Eng": "model"
    },
    {
        "Rank": 584,
        "Port": "modelo",
        "Eng": "example"
    },
    {
        "Rank": 585,
        "Port": "assumir",
        "Eng": "assume"
    },
    {
        "Rank": 586,
        "Port": "oferecer",
        "Eng": "offer"
    },
    {
        "Rank": 586,
        "Port": "oferecer",
        "Eng": "give"
    },
    {
        "Rank": 587,
        "Port": "visão",
        "Eng": "vision"
    },
    {
        "Rank": 587,
        "Port": "visão",
        "Eng": "view"
    },
    {
        "Rank": 588,
        "Port": "preparar",
        "Eng": "prepare"
    },
    {
        "Rank": 589,
        "Port": "pro",
        "Eng": "constitute"
    },
    {
        "Rank": 590,
        "Port": "escritor",
        "Eng": "author"
    },
    {
        "Rank": 590,
        "Port": "escritor",
        "Eng": "writer"
    },
    {
        "Rank": 591,
        "Port": "estrada",
        "Eng": "highway"
    },
    {
        "Rank": 591,
        "Port": "estrada",
        "Eng": "road"
    },
    {
        "Rank": 592,
        "Port": "construção",
        "Eng": "construction"
    },
    {
        "Rank": 593,
        "Port": "polícia",
        "Eng": "police"
    },
    {
        "Rank": 594,
        "Port": "mudança",
        "Eng": "change"
    },
    {
        "Rank": 595,
        "Port": "aprender",
        "Eng": "learn"
    },
    {
        "Rank": 596,
        "Port": "mínimo",
        "Eng": "minimum"
    },
    {
        "Rank": 596,
        "Port": "mínimo",
        "Eng": "least"
    },
    {
        "Rank": 597,
        "Port": "comer",
        "Eng": "eat"
    },
    {
        "Rank": 598,
        "Port": "fugir",
        "Eng": "flee"
    },
    {
        "Rank": 598,
        "Port": "fugir",
        "Eng": "run away"
    },
    {
        "Rank": 599,
        "Port": "motivo",
        "Eng": "reason"
    },
    {
        "Rank": 599,
        "Port": "motivo",
        "Eng": "motive"
    },
    {
        "Rank": 600,
        "Port": "som",
        "Eng": "sound"
    },
    {
        "Rank": 601,
        "Port": "obrigar",
        "Eng": "force"
    },
    {
        "Rank": 601,
        "Port": "obrigar",
        "Eng": "obligate"
    },
    {
        "Rank": 602,
        "Port": "publicar",
        "Eng": "publish"
    },
    {
        "Rank": 603,
        "Port": "sol",
        "Eng": "sun"
    },
    {
        "Rank": 604,
        "Port": "comércio",
        "Eng": "commerce"
    },
    {
        "Rank": 604,
        "Port": "comércio",
        "Eng": "trade"
    },
    {
        "Rank": 605,
        "Port": "ocupar",
        "Eng": "occupy"
    },
    {
        "Rank": 606,
        "Port": "central",
        "Eng": "central"
    },
    {
        "Rank": 606,
        "Port": "central",
        "Eng": "station"
    },
    {
        "Rank": 606,
        "Port": "central",
        "Eng": "office"
    },
    {
        "Rank": 607,
        "Port": "curto",
        "Eng": "short"
    },
    {
        "Rank": 608,
        "Port": "prova",
        "Eng": "proof"
    },
    {
        "Rank": 608,
        "Port": "prova",
        "Eng": "test"
    },
    {
        "Rank": 608,
        "Port": "prova",
        "Eng": "evidence"
    },
    {
        "Rank": 609,
        "Port": "memória",
        "Eng": "memory"
    },
    {
        "Rank": 610,
        "Port": "bater",
        "Eng": "hit"
    },
    {
        "Rank": 610,
        "Port": "bater",
        "Eng": "beat"
    },
    {
        "Rank": 611,
        "Port": "aproveitar",
        "Eng": "make good use of"
    },
    {
        "Rank": 611,
        "Port": "aproveitar",
        "Eng": "use"
    },
    {
        "Rank": 612,
        "Port": "depender",
        "Eng": "depend"
    },
    {
        "Rank": 613,
        "Port": "parar",
        "Eng": "stop"
    },
    {
        "Rank": 614,
        "Port": "metro",
        "Eng": "meter"
    },
    {
        "Rank": 614,
        "Port": "metro",
        "Eng": "subway"
    },
    {
        "Rank": 615,
        "Port": "material",
        "Eng": "material"
    },
    {
        "Rank": 616,
        "Port": "revista",
        "Eng": "magazine"
    },
    {
        "Rank": 616,
        "Port": "revista",
        "Eng": "periodical"
    },
    {
        "Rank": 617,
        "Port": "acima",
        "Eng": "above"
    },
    {
        "Rank": 618,
        "Port": "crítica",
        "Eng": "criticism"
    },
    {
        "Rank": 619,
        "Port": "sinal",
        "Eng": "sign"
    },
    {
        "Rank": 619,
        "Port": "sinal",
        "Eng": "signal"
    },
    {
        "Rank": 620,
        "Port": "medo",
        "Eng": "fear"
    },
    {
        "Rank": 621,
        "Port": "profissional",
        "Eng": "professional"
    },
    {
        "Rank": 622,
        "Port": "objectivo",
        "Eng": "objective"
    },
    {
        "Rank": 623,
        "Port": "inteiro",
        "Eng": "entire"
    },
    {
        "Rank": 624,
        "Port": "venda",
        "Eng": "sale"
    },
    {
        "Rank": 625,
        "Port": "sucesso",
        "Eng": "success"
    },
    {
        "Rank": 626,
        "Port": "carreira",
        "Eng": "career"
    },
    {
        "Rank": 626,
        "Port": "carreira",
        "Eng": "race"
    },
    {
        "Rank": 627,
        "Port": "assistir",
        "Eng": "watch"
    },
    {
        "Rank": 627,
        "Port": "assistir",
        "Eng": "help"
    },
    {
        "Rank": 627,
        "Port": "assistir",
        "Eng": "attend"
    },
    {
        "Rank": 628,
        "Port": "esquerda",
        "Eng": "left"
    },
    {
        "Rank": 629,
        "Port": "cortar",
        "Eng": "cut"
    },
    {
        "Rank": 630,
        "Port": "influência",
        "Eng": "influence"
    },
    {
        "Rank": 631,
        "Port": "pertencer",
        "Eng": "belong to"
    },
    {
        "Rank": 632,
        "Port": "personagem",
        "Eng": "character"
    },
    {
        "Rank": 632,
        "Port": "personagem",
        "Eng": "personage"
    },
    {
        "Rank": 633,
        "Port": "obter",
        "Eng": "get"
    },
    {
        "Rank": 633,
        "Port": "obter",
        "Eng": "obtain"
    },
    {
        "Rank": 634,
        "Port": "apoiar",
        "Eng": "support"
    },
    {
        "Rank": 634,
        "Port": "apoiar",
        "Eng": "uphold"
    },
    {
        "Rank": 634,
        "Port": "apoiar",
        "Eng": "sustain"
    },
    {
        "Rank": 635,
        "Port": "funcionar",
        "Eng": "function"
    },
    {
        "Rank": 636,
        "Port": "prática",
        "Eng": "practice"
    },
    {
        "Rank": 637,
        "Port": "formação",
        "Eng": "formation"
    },
    {
        "Rank": 637,
        "Port": "formação",
        "Eng": "graduation"
    },
    {
        "Rank": 638,
        "Port": "europeu",
        "Eng": "European"
    },
    {
        "Rank": 639,
        "Port": "conforme",
        "Eng": "according to"
    },
    {
        "Rank": 640,
        "Port": "preocupação",
        "Eng": "worry"
    },
    {
        "Rank": 640,
        "Port": "preocupação",
        "Eng": "preoccupation"
    },
    {
        "Rank": 641,
        "Port": "estabelecer",
        "Eng": "establish"
    },
    {
        "Rank": 642,
        "Port": "produção",
        "Eng": "production"
    },
    {
        "Rank": 643,
        "Port": "conjunto",
        "Eng": "set"
    },
    {
        "Rank": 643,
        "Port": "conjunto",
        "Eng": "combination"
    },
    {
        "Rank": 643,
        "Port": "conjunto",
        "Eng": "group"
    },
    {
        "Rank": 644,
        "Port": "esforço",
        "Eng": "effort"
    },
    {
        "Rank": 645,
        "Port": "massa",
        "Eng": "mass"
    },
    {
        "Rank": 645,
        "Port": "massa",
        "Eng": "dough"
    },
    {
        "Rank": 646,
        "Port": "comissão",
        "Eng": "commission"
    },
    {
        "Rank": 647,
        "Port": "porém",
        "Eng": "however"
    },
    {
        "Rank": 647,
        "Port": "porém",
        "Eng": "though"
    },
    {
        "Rank": 648,
        "Port": "subir",
        "Eng": "go up"
    },
    {
        "Rank": 648,
        "Port": "subir",
        "Eng": "climb"
    },
    {
        "Rank": 649,
        "Port": "popular",
        "Eng": "popular"
    },
    {
        "Rank": 650,
        "Port": "desenvolver",
        "Eng": "develop"
    },
    {
        "Rank": 651,
        "Port": "rei",
        "Eng": "king"
    },
    {
        "Rank": 652,
        "Port": "rádio",
        "Eng": "radio"
    },
    {
        "Rank": 653,
        "Port": "prestar",
        "Eng": "render"
    },
    {
        "Rank": 653,
        "Port": "prestar",
        "Eng": "be useful"
    },
    {
        "Rank": 654,
        "Port": "dividir",
        "Eng": "divide"
    },
    {
        "Rank": 655,
        "Port": "erro",
        "Eng": "mistake"
    },
    {
        "Rank": 655,
        "Port": "erro",
        "Eng": "error"
    },
    {
        "Rank": 656,
        "Port": "género",
        "Eng": "kind"
    },
    {
        "Rank": 656,
        "Port": "género",
        "Eng": "type"
    },
    {
        "Rank": 656,
        "Port": "género",
        "Eng": "genus"
    },
    {
        "Rank": 657,
        "Port": "favor",
        "Eng": "favor"
    },
    {
        "Rank": 658,
        "Port": "oficial",
        "Eng": "official"
    },
    {
        "Rank": 659,
        "Port": "administração",
        "Eng": "administration"
    },
    {
        "Rank": 660,
        "Port": "reunir",
        "Eng": "gather"
    },
    {
        "Rank": 661,
        "Port": "cento",
        "Eng": "percent"
    },
    {
        "Rank": 662,
        "Port": "sangue",
        "Eng": "blood"
    },
    {
        "Rank": 663,
        "Port": "letra",
        "Eng": "letter"
    },
    {
        "Rank": 663,
        "Port": "letra",
        "Eng": "handwriting"
    },
    {
        "Rank": 663,
        "Port": "letra",
        "Eng": "lyrics"
    },
    {
        "Rank": 664,
        "Port": "série",
        "Eng": "series"
    },
    {
        "Rank": 665,
        "Port": "expressão",
        "Eng": "expression"
    },
    {
        "Rank": 666,
        "Port": "face",
        "Eng": "face"
    },
    {
        "Rank": 666,
        "Port": "face",
        "Eng": "surface"
    },
    {
        "Rank": 667,
        "Port": "supor",
        "Eng": "suppose"
    },
    {
        "Rank": 668,
        "Port": "vila",
        "Eng": "small town"
    },
    {
        "Rank": 668,
        "Port": "vila",
        "Eng": "village"
    },
    {
        "Rank": 669,
        "Port": "determinar",
        "Eng": "determine"
    },
    {
        "Rank": 670,
        "Port": "completamente",
        "Eng": "completely"
    },
    {
        "Rank": 671,
        "Port": "energia",
        "Eng": "energy"
    },
    {
        "Rank": 672,
        "Port": "presença",
        "Eng": "presence"
    },
    {
        "Rank": 673,
        "Port": "árvore",
        "Eng": "tree"
    },
    {
        "Rank": 674,
        "Port": "fase",
        "Eng": "phase"
    },
    {
        "Rank": 675,
        "Port": "encontro",
        "Eng": "encounter"
    },
    {
        "Rank": 675,
        "Port": "encontro",
        "Eng": "meeting"
    },
    {
        "Rank": 675,
        "Port": "encontro",
        "Eng": "date"
    },
    {
        "Rank": 676,
        "Port": "deputado",
        "Eng": "representative"
    },
    {
        "Rank": 676,
        "Port": "deputado",
        "Eng": "deputy"
    },
    {
        "Rank": 677,
        "Port": "emprego",
        "Eng": "job"
    },
    {
        "Rank": 677,
        "Port": "emprego",
        "Eng": "work"
    },
    {
        "Rank": 677,
        "Port": "emprego",
        "Eng": "employment"
    },
    {
        "Rank": 678,
        "Port": "particular",
        "Eng": "private"
    },
    {
        "Rank": 678,
        "Port": "particular",
        "Eng": "particular"
    },
    {
        "Rank": 679,
        "Port": "risco",
        "Eng": "risk"
    },
    {
        "Rank": 680,
        "Port": "belo",
        "Eng": "beautiful"
    },
    {
        "Rank": 681,
        "Port": "cena",
        "Eng": "scene"
    },
    {
        "Rank": 682,
        "Port": "consciência",
        "Eng": "conscience"
    },
    {
        "Rank": 682,
        "Port": "consciência",
        "Eng": "awareness"
    },
    {
        "Rank": 683,
        "Port": "geração",
        "Eng": "generation"
    },
    {
        "Rank": 684,
        "Port": "crime",
        "Eng": "crime"
    },
    {
        "Rank": 685,
        "Port": "somente",
        "Eng": "only"
    },
    {
        "Rank": 685,
        "Port": "somente",
        "Eng": "solely"
    },
    {
        "Rank": 686,
        "Port": "verificar",
        "Eng": "verify"
    },
    {
        "Rank": 686,
        "Port": "verificar",
        "Eng": "check"
    },
    {
        "Rank": 687,
        "Port": "preferir",
        "Eng": "prefer"
    },
    {
        "Rank": 688,
        "Port": "crise",
        "Eng": "crisis"
    },
    {
        "Rank": 689,
        "Port": "ora",
        "Eng": "now"
    },
    {
        "Rank": 689,
        "Port": "ora",
        "Eng": "presently"
    },
    {
        "Rank": 690,
        "Port": "entregar",
        "Eng": "deliver"
    },
    {
        "Rank": 690,
        "Port": "entregar",
        "Eng": "give"
    },
    {
        "Rank": 691,
        "Port": "moderno",
        "Eng": "modern"
    },
    {
        "Rank": 692,
        "Port": "regra",
        "Eng": "rule"
    },
    {
        "Rank": 693,
        "Port": "revelar",
        "Eng": "reveal"
    },
    {
        "Rank": 693,
        "Port": "revelar",
        "Eng": "develop"
    },
    {
        "Rank": 694,
        "Port": "desejar",
        "Eng": "wish"
    },
    {
        "Rank": 694,
        "Port": "desejar",
        "Eng": "desire"
    },
    {
        "Rank": 695,
        "Port": "exactamente",
        "Eng": "exactly"
    },
    {
        "Rank": 696,
        "Port": "fonte",
        "Eng": "source"
    },
    {
        "Rank": 696,
        "Port": "fonte",
        "Eng": "fountain"
    },
    {
        "Rank": 697,
        "Port": "repetir",
        "Eng": "repeat"
    },
    {
        "Rank": 698,
        "Port": "largo",
        "Eng": "wide"
    },
    {
        "Rank": 698,
        "Port": "largo",
        "Eng": "large"
    },
    {
        "Rank": 698,
        "Port": "largo",
        "Eng": "broad"
    },
    {
        "Rank": 699,
        "Port": "estilo",
        "Eng": "style"
    },
    {
        "Rank": 700,
        "Port": "imaginar",
        "Eng": "imagine"
    },
    {
        "Rank": 701,
        "Port": "responsabilidade",
        "Eng": "responsibility"
    },
    {
        "Rank": 702,
        "Port": "economia",
        "Eng": "economy"
    },
    {
        "Rank": 703,
        "Port": "vale",
        "Eng": "valley"
    },
    {
        "Rank": 703,
        "Port": "vale",
        "Eng": "receipt"
    },
    {
        "Rank": 704,
        "Port": "avançar",
        "Eng": "advance"
    },
    {
        "Rank": 705,
        "Port": "observar",
        "Eng": "observe"
    },
    {
        "Rank": 706,
        "Port": "carne",
        "Eng": "meat"
    },
    {
        "Rank": 706,
        "Port": "carne",
        "Eng": "flesh"
    },
    {
        "Rank": 707,
        "Port": "origem",
        "Eng": "origin"
    },
    {
        "Rank": 707,
        "Port": "origem",
        "Eng": "root"
    },
    {
        "Rank": 708,
        "Port": "atitude",
        "Eng": "attitude"
    },
    {
        "Rank": 709,
        "Port": "indivíduo",
        "Eng": "individual"
    },
    {
        "Rank": 710,
        "Port": "inclusive",
        "Eng": "including"
    },
    {
        "Rank": 710,
        "Port": "inclusive",
        "Eng": "even"
    },
    {
        "Rank": 711,
        "Port": "sala",
        "Eng": "room"
    },
    {
        "Rank": 712,
        "Port": "cheio",
        "Eng": "full"
    },
    {
        "Rank": 713,
        "Port": "preto",
        "Eng": "black"
    },
    {
        "Rank": 714,
        "Port": "reduzir",
        "Eng": "reduce"
    },
    {
        "Rank": 715,
        "Port": "defesa",
        "Eng": "defense"
    },
    {
        "Rank": 716,
        "Port": "propor",
        "Eng": "propose"
    },
    {
        "Rank": 717,
        "Port": "civil",
        "Eng": "civil"
    },
    {
        "Rank": 718,
        "Port": "comunicação",
        "Eng": "communication"
    },
    {
        "Rank": 719,
        "Port": "resultar",
        "Eng": "result"
    },
    {
        "Rank": 720,
        "Port": "texto",
        "Eng": "text"
    },
    {
        "Rank": 721,
        "Port": "oportunidade",
        "Eng": "opportunity"
    },
    {
        "Rank": 722,
        "Port": "proposta",
        "Eng": "proposal"
    },
    {
        "Rank": 723,
        "Port": "manhã",
        "Eng": "morning"
    },
    {
        "Rank": 724,
        "Port": "peso",
        "Eng": "weight"
    },
    {
        "Rank": 725,
        "Port": "entrada",
        "Eng": "entrance"
    },
    {
        "Rank": 725,
        "Port": "entrada",
        "Eng": "entryway"
    },
    {
        "Rank": 726,
        "Port": "existência",
        "Eng": "existence"
    },
    {
        "Rank": 727,
        "Port": "moeda",
        "Eng": "currency"
    },
    {
        "Rank": 727,
        "Port": "moeda",
        "Eng": "coin"
    },
    {
        "Rank": 728,
        "Port": "total",
        "Eng": "total"
    },
    {
        "Rank": 729,
        "Port": "clube",
        "Eng": "club"
    },
    {
        "Rank": 730,
        "Port": "ferro",
        "Eng": "iron"
    },
    {
        "Rank": 731,
        "Port": "romance",
        "Eng": "novel"
    },
    {
        "Rank": 731,
        "Port": "romance",
        "Eng": "romance"
    },
    {
        "Rank": 732,
        "Port": "autoridade",
        "Eng": "authority"
    },
    {
        "Rank": 733,
        "Port": "sentimento",
        "Eng": "feeling"
    },
    {
        "Rank": 734,
        "Port": "grave",
        "Eng": "grave"
    },
    {
        "Rank": 734,
        "Port": "grave",
        "Eng": "serious"
    },
    {
        "Rank": 735,
        "Port": "vitória",
        "Eng": "victory"
    },
    {
        "Rank": 736,
        "Port": "reunião",
        "Eng": "meeting"
    },
    {
        "Rank": 736,
        "Port": "reunião",
        "Eng": "reunion"
    },
    {
        "Rank": 737,
        "Port": "marido",
        "Eng": "husband"
    },
    {
        "Rank": 738,
        "Port": "interessante",
        "Eng": "interesting"
    },
    {
        "Rank": 739,
        "Port": "admitir",
        "Eng": "admit"
    },
    {
        "Rank": 740,
        "Port": "sério",
        "Eng": "serious"
    },
    {
        "Rank": 741,
        "Port": "operação",
        "Eng": "operation"
    },
    {
        "Rank": 742,
        "Port": "indicar",
        "Eng": "indicate"
    },
    {
        "Rank": 743,
        "Port": "máximo",
        "Eng": "maximum"
    },
    {
        "Rank": 744,
        "Port": "normal",
        "Eng": "normal"
    },
    {
        "Rank": 744,
        "Port": "normal",
        "Eng": "norm"
    },
    {
        "Rank": 745,
        "Port": "juntar",
        "Eng": "join"
    },
    {
        "Rank": 745,
        "Port": "juntar",
        "Eng": "gather together"
    },
    {
        "Rank": 746,
        "Port": "impedir",
        "Eng": "impede"
    },
    {
        "Rank": 746,
        "Port": "impedir",
        "Eng": "prevent"
    },
    {
        "Rank": 747,
        "Port": "ilha",
        "Eng": "island"
    },
    {
        "Rank": 748,
        "Port": "espectáculo",
        "Eng": "show"
    },
    {
        "Rank": 748,
        "Port": "espectáculo",
        "Eng": "spectacular"
    },
    {
        "Rank": 748,
        "Port": "espectáculo",
        "Eng": "spectacle"
    },
    {
        "Rank": 749,
        "Port": "possuir",
        "Eng": "have"
    },
    {
        "Rank": 749,
        "Port": "possuir",
        "Eng": "possess"
    },
    {
        "Rank": 750,
        "Port": "dado",
        "Eng": "datum"
    },
    {
        "Rank": 750,
        "Port": "dado",
        "Eng": "given"
    },
    {
        "Rank": 751,
        "Port": "certeza",
        "Eng": "certainty"
    },
    {
        "Rank": 752,
        "Port": "torno",
        "Eng": "around"
    },
    {
        "Rank": 752,
        "Port": "torno",
        "Eng": "about"
    },
    {
        "Rank": 753,
        "Port": "frio",
        "Eng": "cold"
    },
    {
        "Rank": 754,
        "Port": "histórico",
        "Eng": "historic"
    },
    {
        "Rank": 754,
        "Port": "histórico",
        "Eng": "historical"
    },
    {
        "Rank": 755,
        "Port": "faltar",
        "Eng": "lack"
    },
    {
        "Rank": 755,
        "Port": "faltar",
        "Eng": "miss"
    },
    {
        "Rank": 755,
        "Port": "faltar",
        "Eng": "not be present"
    },
    {
        "Rank": 756,
        "Port": "iniciar",
        "Eng": "initiate"
    },
    {
        "Rank": 756,
        "Port": "iniciar",
        "Eng": "begin"
    },
    {
        "Rank": 757,
        "Port": "dedicar",
        "Eng": "dedicate"
    },
    {
        "Rank": 758,
        "Port": "aliás",
        "Eng": "or rather"
    },
    {
        "Rank": 758,
        "Port": "aliás",
        "Eng": "besides"
    },
    {
        "Rank": 759,
        "Port": "universidade",
        "Eng": "university"
    },
    {
        "Rank": 760,
        "Port": "intenção",
        "Eng": "intention"
    },
    {
        "Rank": 761,
        "Port": "discurso",
        "Eng": "speech"
    },
    {
        "Rank": 761,
        "Port": "discurso",
        "Eng": "discourse"
    },
    {
        "Rank": 762,
        "Port": "trinta",
        "Eng": "thirty"
    },
    {
        "Rank": 763,
        "Port": "apontar",
        "Eng": "point out"
    },
    {
        "Rank": 763,
        "Port": "apontar",
        "Eng": "indicate"
    },
    {
        "Rank": 764,
        "Port": "passagem",
        "Eng": "ticket"
    },
    {
        "Rank": 764,
        "Port": "passagem",
        "Eng": "fare"
    },
    {
        "Rank": 764,
        "Port": "passagem",
        "Eng": "way"
    },
    {
        "Rank": 764,
        "Port": "passagem",
        "Eng": "passage"
    },
    {
        "Rank": 765,
        "Port": "tema",
        "Eng": "subject"
    },
    {
        "Rank": 765,
        "Port": "tema",
        "Eng": "theme"
    },
    {
        "Rank": 765,
        "Port": "tema",
        "Eng": "topic"
    },
    {
        "Rank": 766,
        "Port": "abandonar",
        "Eng": "leave"
    },
    {
        "Rank": 766,
        "Port": "abandonar",
        "Eng": "abandon"
    },
    {
        "Rank": 767,
        "Port": "notar",
        "Eng": "note"
    },
    {
        "Rank": 767,
        "Port": "notar",
        "Eng": "notice"
    },
    {
        "Rank": 768,
        "Port": "coração",
        "Eng": "heart"
    },
    {
        "Rank": 769,
        "Port": "doente",
        "Eng": "sick"
    },
    {
        "Rank": 770,
        "Port": "concluir",
        "Eng": "conclude"
    },
    {
        "Rank": 771,
        "Port": "leite",
        "Eng": "milk"
    },
    {
        "Rank": 772,
        "Port": "paz",
        "Eng": "peace"
    },
    {
        "Rank": 773,
        "Port": "margem",
        "Eng": "margin"
    },
    {
        "Rank": 773,
        "Port": "margem",
        "Eng": "border"
    },
    {
        "Rank": 773,
        "Port": "margem",
        "Eng": "riverbank"
    },
    {
        "Rank": 774,
        "Port": "ambos",
        "Eng": "both"
    },
    {
        "Rank": 775,
        "Port": "retirar",
        "Eng": "remove"
    },
    {
        "Rank": 776,
        "Port": "pronto",
        "Eng": "ready"
    },
    {
        "Rank": 777,
        "Port": "vencer",
        "Eng": "win"
    },
    {
        "Rank": 777,
        "Port": "vencer",
        "Eng": "triumph"
    },
    {
        "Rank": 778,
        "Port": "discussão",
        "Eng": "discussion"
    },
    {
        "Rank": 778,
        "Port": "discussão",
        "Eng": "debate"
    },
    {
        "Rank": 779,
        "Port": "hospital",
        "Eng": "hospital"
    },
    {
        "Rank": 780,
        "Port": "instrumento",
        "Eng": "instrument"
    },
    {
        "Rank": 781,
        "Port": "minuto",
        "Eng": "minute"
    },
    {
        "Rank": 782,
        "Port": "monte",
        "Eng": "mount"
    },
    {
        "Rank": 782,
        "Port": "monte",
        "Eng": "mound"
    },
    {
        "Rank": 782,
        "Port": "monte",
        "Eng": "a lot of"
    },
    {
        "Rank": 783,
        "Port": "campanha",
        "Eng": "campaign"
    },
    {
        "Rank": 784,
        "Port": "incluir",
        "Eng": "include"
    },
    {
        "Rank": 785,
        "Port": "prever",
        "Eng": "foresee"
    },
    {
        "Rank": 786,
        "Port": "tarefa",
        "Eng": "assignment"
    },
    {
        "Rank": 786,
        "Port": "tarefa",
        "Eng": "task"
    },
    {
        "Rank": 786,
        "Port": "tarefa",
        "Eng": "homework"
    },
    {
        "Rank": 787,
        "Port": "indústria",
        "Eng": "industry"
    },
    {
        "Rank": 788,
        "Port": "viajar",
        "Eng": "travel"
    },
    {
        "Rank": 789,
        "Port": "reforma",
        "Eng": "reform"
    },
    {
        "Rank": 790,
        "Port": "vento",
        "Eng": "wind"
    },
    {
        "Rank": 791,
        "Port": "especializar",
        "Eng": "specialize in"
    },
    {
        "Rank": 792,
        "Port": "saída",
        "Eng": "exit"
    },
    {
        "Rank": 793,
        "Port": "esquecer",
        "Eng": "forget"
    },
    {
        "Rank": 794,
        "Port": "adquirir",
        "Eng": "acquire"
    },
    {
        "Rank": 795,
        "Port": "pergunta",
        "Eng": "question"
    },
    {
        "Rank": 796,
        "Port": "boca",
        "Eng": "mouth"
    },
    {
        "Rank": 797,
        "Port": "organização",
        "Eng": "organization"
    },
    {
        "Rank": 798,
        "Port": "poeta",
        "Eng": "poet"
    },
    {
        "Rank": 799,
        "Port": "cargo",
        "Eng": "position"
    },
    {
        "Rank": 799,
        "Port": "cargo",
        "Eng": "responsibility"
    },
    {
        "Rank": 800,
        "Port": "capacidade",
        "Eng": "capacity"
    },
    {
        "Rank": 801,
        "Port": "cantar",
        "Eng": "sing"
    },
    {
        "Rank": 802,
        "Port": "chefe",
        "Eng": "chief"
    },
    {
        "Rank": 802,
        "Port": "chefe",
        "Eng": "boss"
    },
    {
        "Rank": 803,
        "Port": "exército",
        "Eng": "army"
    },
    {
        "Rank": 804,
        "Port": "corrente",
        "Eng": "current"
    },
    {
        "Rank": 804,
        "Port": "corrente",
        "Eng": "chain"
    },
    {
        "Rank": 805,
        "Port": "festa",
        "Eng": "party"
    },
    {
        "Rank": 805,
        "Port": "festa",
        "Eng": "celebration"
    },
    {
        "Rank": 806,
        "Port": "melhorar",
        "Eng": "improve"
    },
    {
        "Rank": 806,
        "Port": "melhorar",
        "Eng": "make better"
    },
    {
        "Rank": 807,
        "Port": "pensamento",
        "Eng": "thought"
    },
    {
        "Rank": 808,
        "Port": "limite",
        "Eng": "limit"
    },
    {
        "Rank": 809,
        "Port": "atender",
        "Eng": "help"
    },
    {
        "Rank": 809,
        "Port": "atender",
        "Eng": "receive"
    },
    {
        "Rank": 809,
        "Port": "atender",
        "Eng": "give attention"
    },
    {
        "Rank": 810,
        "Port": "profundo",
        "Eng": "deep"
    },
    {
        "Rank": 810,
        "Port": "profundo",
        "Eng": "profound"
    },
    {
        "Rank": 811,
        "Port": "aula",
        "Eng": "class"
    },
    {
        "Rank": 811,
        "Port": "aula",
        "Eng": "lesson"
    },
    {
        "Rank": 812,
        "Port": "pesar",
        "Eng": "weigh"
    },
    {
        "Rank": 813,
        "Port": "ministério",
        "Eng": "ministry"
    },
    {
        "Rank": 814,
        "Port": "longe",
        "Eng": "far"
    },
    {
        "Rank": 815,
        "Port": "estrutura",
        "Eng": "structure"
    },
    {
        "Rank": 816,
        "Port": "aplicar",
        "Eng": "apply"
    },
    {
        "Rank": 817,
        "Port": "uso",
        "Eng": "use"
    },
    {
        "Rank": 818,
        "Port": "conduzir",
        "Eng": "lead"
    },
    {
        "Rank": 818,
        "Port": "conduzir",
        "Eng": "conduct"
    },
    {
        "Rank": 819,
        "Port": "vermelho",
        "Eng": "red"
    },
    {
        "Rank": 820,
        "Port": "pintura",
        "Eng": "painting"
    },
    {
        "Rank": 821,
        "Port": "ramo",
        "Eng": "branch"
    },
    {
        "Rank": 822,
        "Port": "cavalo",
        "Eng": "horse"
    },
    {
        "Rank": 823,
        "Port": "índio",
        "Eng": "Indian"
    },
    {
        "Rank": 823,
        "Port": "índio",
        "Eng": "Native American"
    },
    {
        "Rank": 824,
        "Port": "comercial",
        "Eng": "commercial"
    },
    {
        "Rank": 825,
        "Port": "técnico",
        "Eng": "technical"
    },
    {
        "Rank": 825,
        "Port": "técnico",
        "Eng": "coach"
    },
    {
        "Rank": 825,
        "Port": "técnico",
        "Eng": "technician"
    },
    {
        "Rank": 826,
        "Port": "disco",
        "Eng": "record"
    },
    {
        "Rank": 826,
        "Port": "disco",
        "Eng": "disc"
    },
    {
        "Rank": 827,
        "Port": "sujeito",
        "Eng": "subject"
    },
    {
        "Rank": 828,
        "Port": "preocupar",
        "Eng": "worry about"
    },
    {
        "Rank": 829,
        "Port": "afastar",
        "Eng": "walk away"
    },
    {
        "Rank": 829,
        "Port": "afastar",
        "Eng": "withdraw"
    },
    {
        "Rank": 830,
        "Port": "metade",
        "Eng": "half"
    },
    {
        "Rank": 831,
        "Port": "directo",
        "Eng": "direct"
    },
    {
        "Rank": 832,
        "Port": "respeitar",
        "Eng": "respect"
    },
    {
        "Rank": 833,
        "Port": "linguagem",
        "Eng": "language"
    },
    {
        "Rank": 834,
        "Port": "união",
        "Eng": "union"
    },
    {
        "Rank": 835,
        "Port": "parede",
        "Eng": "wall"
    },
    {
        "Rank": 836,
        "Port": "fogo",
        "Eng": "fire"
    },
    {
        "Rank": 837,
        "Port": "arma",
        "Eng": "weapon"
    },
    {
        "Rank": 837,
        "Port": "arma",
        "Eng": "arm"
    },
    {
        "Rank": 838,
        "Port": "advogado",
        "Eng": "lawyer"
    },
    {
        "Rank": 838,
        "Port": "advogado",
        "Eng": "attorney"
    },
    {
        "Rank": 839,
        "Port": "cuidar",
        "Eng": "take care"
    },
    {
        "Rank": 840,
        "Port": "caixa",
        "Eng": "box"
    },
    {
        "Rank": 840,
        "Port": "caixa",
        "Eng": "cash register"
    },
    {
        "Rank": 841,
        "Port": "familiar",
        "Eng": "familiar"
    },
    {
        "Rank": 841,
        "Port": "familiar",
        "Eng": "of the family"
    },
    {
        "Rank": 841,
        "Port": "familiar",
        "Eng": "family member"
    },
    {
        "Rank": 842,
        "Port": "completar",
        "Eng": "complete"
    },
    {
        "Rank": 843,
        "Port": "aluno",
        "Eng": "student"
    },
    {
        "Rank": 843,
        "Port": "aluno",
        "Eng": "pupil"
    },
    {
        "Rank": 844,
        "Port": "entretanto",
        "Eng": "meanwhile"
    },
    {
        "Rank": 844,
        "Port": "entretanto",
        "Eng": "however"
    },
    {
        "Rank": 845,
        "Port": "revolução",
        "Eng": "revolution"
    },
    {
        "Rank": 846,
        "Port": "nota",
        "Eng": "note"
    },
    {
        "Rank": 846,
        "Port": "nota",
        "Eng": "grade"
    },
    {
        "Rank": 846,
        "Port": "nota",
        "Eng": "mark"
    },
    {
        "Rank": 847,
        "Port": "causar",
        "Eng": "cause"
    },
    {
        "Rank": 848,
        "Port": "armado",
        "Eng": "armed"
    },
    {
        "Rank": 848,
        "Port": "armado",
        "Eng": "military"
    },
    {
        "Rank": 849,
        "Port": "custo",
        "Eng": "cost"
    },
    {
        "Rank": 850,
        "Port": "procura",
        "Eng": "search"
    },
    {
        "Rank": 851,
        "Port": "escolha",
        "Eng": "choice"
    },
    {
        "Rank": 852,
        "Port": "literário",
        "Eng": "literary"
    },
    {
        "Rank": 853,
        "Port": "responsável",
        "Eng": "responsible"
    },
    {
        "Rank": 853,
        "Port": "responsável",
        "Eng": "person in charge"
    },
    {
        "Rank": 854,
        "Port": "mesa",
        "Eng": "table"
    },
    {
        "Rank": 855,
        "Port": "físico",
        "Eng": "physical"
    },
    {
        "Rank": 855,
        "Port": "físico",
        "Eng": "physicist"
    },
    {
        "Rank": 856,
        "Port": "tamanho",
        "Eng": "size"
    },
    {
        "Rank": 857,
        "Port": "leitura",
        "Eng": "reading"
    },
    {
        "Rank": 858,
        "Port": "suficiente",
        "Eng": "sufficient"
    },
    {
        "Rank": 859,
        "Port": "aproximar",
        "Eng": "approach"
    },
    {
        "Rank": 859,
        "Port": "aproximar",
        "Eng": "move closer"
    },
    {
        "Rank": 860,
        "Port": "data",
        "Eng": "date"
    },
    {
        "Rank": 861,
        "Port": "pegar",
        "Eng": "get"
    },
    {
        "Rank": 861,
        "Port": "pegar",
        "Eng": "grab"
    },
    {
        "Rank": 861,
        "Port": "pegar",
        "Eng": "catch"
    },
    {
        "Rank": 862,
        "Port": "artigo",
        "Eng": "article"
    },
    {
        "Rank": 863,
        "Port": "oposição",
        "Eng": "opposition"
    },
    {
        "Rank": 864,
        "Port": "ponte",
        "Eng": "bridge"
    },
    {
        "Rank": 865,
        "Port": "menino",
        "Eng": "young boy"
    },
    {
        "Rank": 866,
        "Port": "fixar",
        "Eng": "establish"
    },
    {
        "Rank": 866,
        "Port": "fixar",
        "Eng": "fix"
    },
    {
        "Rank": 867,
        "Port": "automóvel",
        "Eng": "car"
    },
    {
        "Rank": 867,
        "Port": "automóvel",
        "Eng": "automobile"
    },
    {
        "Rank": 868,
        "Port": "absoluto",
        "Eng": "absolute"
    },
    {
        "Rank": 869,
        "Port": "cara",
        "Eng": "face"
    },
    {
        "Rank": 870,
        "Port": "consequência",
        "Eng": "consequence"
    },
    {
        "Rank": 871,
        "Port": "planta",
        "Eng": "plant"
    },
    {
        "Rank": 872,
        "Port": "importar",
        "Eng": "be interested in"
    },
    {
        "Rank": 872,
        "Port": "importar",
        "Eng": "care"
    },
    {
        "Rank": 872,
        "Port": "importar",
        "Eng": "import"
    },
    {
        "Rank": 873,
        "Port": "caber",
        "Eng": "fit"
    },
    {
        "Rank": 873,
        "Port": "caber",
        "Eng": "have capacity for"
    },
    {
        "Rank": 874,
        "Port": "perna",
        "Eng": "leg"
    },
    {
        "Rank": 875,
        "Port": "perspectiva",
        "Eng": "perspective"
    },
    {
        "Rank": 876,
        "Port": "religioso",
        "Eng": "religious"
    },
    {
        "Rank": 877,
        "Port": "nove",
        "Eng": "nine"
    },
    {
        "Rank": 878,
        "Port": "concordar",
        "Eng": "agree"
    },
    {
        "Rank": 879,
        "Port": "impor",
        "Eng": "impose"
    },
    {
        "Rank": 879,
        "Port": "impor",
        "Eng": "enforce"
    },
    {
        "Rank": 880,
        "Port": "exposição",
        "Eng": "exposition"
    },
    {
        "Rank": 880,
        "Port": "exposição",
        "Eng": "display"
    },
    {
        "Rank": 881,
        "Port": "interno",
        "Eng": "internal"
    },
    {
        "Rank": 882,
        "Port": "sector",
        "Eng": "sector"
    },
    {
        "Rank": 883,
        "Port": "lutar",
        "Eng": "fight"
    },
    {
        "Rank": 884,
        "Port": "ideal",
        "Eng": "ideal"
    },
    {
        "Rank": 885,
        "Port": "substituir",
        "Eng": "substitute"
    },
    {
        "Rank": 886,
        "Port": "tendência",
        "Eng": "tendency"
    },
    {
        "Rank": 887,
        "Port": "governador",
        "Eng": "governor"
    },
    {
        "Rank": 888,
        "Port": "funcionário",
        "Eng": "employee"
    },
    {
        "Rank": 888,
        "Port": "funcionário",
        "Eng": "worker"
    },
    {
        "Rank": 889,
        "Port": "roda",
        "Eng": "wheel"
    },
    {
        "Rank": 890,
        "Port": "candidato",
        "Eng": "candidate"
    },
    {
        "Rank": 891,
        "Port": "compor",
        "Eng": "compose"
    },
    {
        "Rank": 891,
        "Port": "compor",
        "Eng": "consist of"
    },
    {
        "Rank": 892,
        "Port": "costumar",
        "Eng": "tend to"
    },
    {
        "Rank": 892,
        "Port": "costumar",
        "Eng": "have the habit of"
    },
    {
        "Rank": 893,
        "Port": "impressão",
        "Eng": "impression"
    },
    {
        "Rank": 893,
        "Port": "impressão",
        "Eng": "printing"
    },
    {
        "Rank": 894,
        "Port": "hipótese",
        "Eng": "hypothesis"
    },
    {
        "Rank": 895,
        "Port": "verão",
        "Eng": "summer"
    },
    {
        "Rank": 896,
        "Port": "cobrir",
        "Eng": "cover"
    },
    {
        "Rank": 897,
        "Port": "reflectir",
        "Eng": "reflect"
    },
    {
        "Rank": 898,
        "Port": "anunciar",
        "Eng": "announce"
    },
    {
        "Rank": 899,
        "Port": "dispor",
        "Eng": "possess"
    },
    {
        "Rank": 899,
        "Port": "dispor",
        "Eng": "have"
    },
    {
        "Rank": 899,
        "Port": "dispor",
        "Eng": "use"
    },
    {
        "Rank": 900,
        "Port": "violência",
        "Eng": "violence"
    },
    {
        "Rank": 901,
        "Port": "virar",
        "Eng": "turn"
    },
    {
        "Rank": 901,
        "Port": "virar",
        "Eng": "become"
    },
    {
        "Rank": 902,
        "Port": "seguro",
        "Eng": "secure"
    },
    {
        "Rank": 902,
        "Port": "seguro",
        "Eng": "safe"
    },
    {
        "Rank": 902,
        "Port": "seguro",
        "Eng": "insurance"
    },
    {
        "Rank": 903,
        "Port": "instituição",
        "Eng": "institution"
    },
    {
        "Rank": 904,
        "Port": "exercer",
        "Eng": "exert"
    },
    {
        "Rank": 904,
        "Port": "exercer",
        "Eng": "exercise"
    },
    {
        "Rank": 905,
        "Port": "durar",
        "Eng": "last"
    },
    {
        "Rank": 906,
        "Port": "perguntar",
        "Eng": "ask"
    },
    {
        "Rank": 907,
        "Port": "café",
        "Eng": "coffee"
    },
    {
        "Rank": 908,
        "Port": "financeiro",
        "Eng": "financial"
    },
    {
        "Rank": 909,
        "Port": "abaixo",
        "Eng": "below"
    },
    {
        "Rank": 909,
        "Port": "abaixo",
        "Eng": "beneath"
    },
    {
        "Rank": 909,
        "Port": "abaixo",
        "Eng": "under"
    },
    {
        "Rank": 910,
        "Port": "fotografia",
        "Eng": "photograph"
    },
    {
        "Rank": 910,
        "Port": "fotografia",
        "Eng": "photography"
    },
    {
        "Rank": 911,
        "Port": "conhecido",
        "Eng": "known"
    },
    {
        "Rank": 912,
        "Port": "utilizar",
        "Eng": "utilize"
    },
    {
        "Rank": 913,
        "Port": "vantagem",
        "Eng": "advantage"
    },
    {
        "Rank": 914,
        "Port": "regime",
        "Eng": "regime"
    },
    {
        "Rank": 915,
        "Port": "sítio",
        "Eng": "site"
    },
    {
        "Rank": 915,
        "Port": "sítio",
        "Eng": "place"
    },
    {
        "Rank": 915,
        "Port": "sítio",
        "Eng": "small farm"
    },
    {
        "Rank": 916,
        "Port": "citar",
        "Eng": "cite"
    },
    {
        "Rank": 916,
        "Port": "citar",
        "Eng": "quote"
    },
    {
        "Rank": 917,
        "Port": "tradição",
        "Eng": "tradition"
    },
    {
        "Rank": 918,
        "Port": "alcançar",
        "Eng": "reach"
    },
    {
        "Rank": 918,
        "Port": "alcançar",
        "Eng": "attain"
    },
    {
        "Rank": 919,
        "Port": "desenvolvimento",
        "Eng": "development"
    },
    {
        "Rank": 920,
        "Port": "sonho",
        "Eng": "dream"
    },
    {
        "Rank": 921,
        "Port": "jardim",
        "Eng": "garden"
    },
    {
        "Rank": 922,
        "Port": "assembleia",
        "Eng": "assembly"
    },
    {
        "Rank": 923,
        "Port": "corte",
        "Eng": "cut"
    },
    {
        "Rank": 924,
        "Port": "estrela",
        "Eng": "star"
    },
    {
        "Rank": 925,
        "Port": "impossível",
        "Eng": "impossible"
    },
    {
        "Rank": 926,
        "Port": "determinado",
        "Eng": "determined"
    },
    {
        "Rank": 926,
        "Port": "determinado",
        "Eng": "certain"
    },
    {
        "Rank": 927,
        "Port": "faculdade",
        "Eng": "college"
    },
    {
        "Rank": 927,
        "Port": "faculdade",
        "Eng": "faculty"
    },
    {
        "Rank": 928,
        "Port": "voto",
        "Eng": "vote"
    },
    {
        "Rank": 928,
        "Port": "voto",
        "Eng": "vow"
    },
    {
        "Rank": 929,
        "Port": "estudante",
        "Eng": "student"
    },
    {
        "Rank": 930,
        "Port": "padre",
        "Eng": "priest"
    },
    {
        "Rank": 930,
        "Port": "padre",
        "Eng": "father"
    },
    {
        "Rank": 931,
        "Port": "média",
        "Eng": "average"
    },
    {
        "Rank": 931,
        "Port": "média",
        "Eng": "middle"
    },
    {
        "Rank": 932,
        "Port": "exterior",
        "Eng": "outside"
    },
    {
        "Rank": 932,
        "Port": "exterior",
        "Eng": "exterior"
    },
    {
        "Rank": 933,
        "Port": "associação",
        "Eng": "association"
    },
    {
        "Rank": 933,
        "Port": "associação",
        "Eng": "organization"
    },
    {
        "Rank": 934,
        "Port": "comunidade",
        "Eng": "community"
    },
    {
        "Rank": 935,
        "Port": "simplesmente",
        "Eng": "simply"
    },
    {
        "Rank": 936,
        "Port": "notícia",
        "Eng": "news"
    },
    {
        "Rank": 937,
        "Port": "desaparecer",
        "Eng": "disappear"
    },
    {
        "Rank": 938,
        "Port": "cão",
        "Eng": "dog"
    },
    {
        "Rank": 939,
        "Port": "sede",
        "Eng": "headquarters"
    },
    {
        "Rank": 939,
        "Port": "sede",
        "Eng": "thirst"
    },
    {
        "Rank": 940,
        "Port": "transporte",
        "Eng": "transportation"
    },
    {
        "Rank": 941,
        "Port": "cedo",
        "Eng": "early"
    },
    {
        "Rank": 941,
        "Port": "cedo",
        "Eng": "soon"
    },
    {
        "Rank": 942,
        "Port": "ensinar",
        "Eng": "teach"
    },
    {
        "Rank": 943,
        "Port": "perante",
        "Eng": "before"
    },
    {
        "Rank": 944,
        "Port": "renda",
        "Eng": "income"
    },
    {
        "Rank": 945,
        "Port": "casamento",
        "Eng": "marriage"
    },
    {
        "Rank": 946,
        "Port": "semelhante",
        "Eng": "similar"
    },
    {
        "Rank": 947,
        "Port": "amarelo",
        "Eng": "yellow"
    },
    {
        "Rank": 948,
        "Port": "enfrentar",
        "Eng": "face"
    },
    {
        "Rank": 948,
        "Port": "enfrentar",
        "Eng": "confront"
    },
    {
        "Rank": 949,
        "Port": "moral",
        "Eng": "moral"
    },
    {
        "Rank": 949,
        "Port": "moral",
        "Eng": "ethics"
    },
    {
        "Rank": 949,
        "Port": "moral",
        "Eng": "morale"
    },
    {
        "Rank": 950,
        "Port": "cruz",
        "Eng": "cross"
    },
    {
        "Rank": 951,
        "Port": "convidar",
        "Eng": "invite"
    },
    {
        "Rank": 952,
        "Port": "distância",
        "Eng": "distance"
    },
    {
        "Rank": 953,
        "Port": "carácter",
        "Eng": "personality"
    },
    {
        "Rank": 953,
        "Port": "carácter",
        "Eng": "character"
    },
    {
        "Rank": 954,
        "Port": "nação",
        "Eng": "nation"
    },
    {
        "Rank": 955,
        "Port": "prazo",
        "Eng": "deadline"
    },
    {
        "Rank": 955,
        "Port": "prazo",
        "Eng": "term"
    },
    {
        "Rank": 955,
        "Port": "prazo",
        "Eng": "amount of time"
    },
    {
        "Rank": 956,
        "Port": "separar",
        "Eng": "separate"
    },
    {
        "Rank": 957,
        "Port": "pior",
        "Eng": "worse"
    },
    {
        "Rank": 957,
        "Port": "pior",
        "Eng": "worst"
    },
    {
        "Rank": 958,
        "Port": "rapaz",
        "Eng": "young man"
    },
    {
        "Rank": 958,
        "Port": "rapaz",
        "Eng": "kid"
    },
    {
        "Rank": 959,
        "Port": "braço",
        "Eng": "arm"
    },
    {
        "Rank": 960,
        "Port": "prémio",
        "Eng": "prize"
    },
    {
        "Rank": 961,
        "Port": "atravessar",
        "Eng": "cross"
    },
    {
        "Rank": 961,
        "Port": "atravessar",
        "Eng": "pass"
    },
    {
        "Rank": 962,
        "Port": "batalha",
        "Eng": "battle"
    },
    {
        "Rank": 963,
        "Port": "reacção",
        "Eng": "reaction"
    },
    {
        "Rank": 964,
        "Port": "acesso",
        "Eng": "access"
    },
    {
        "Rank": 965,
        "Port": "tratamento",
        "Eng": "treatment"
    },
    {
        "Rank": 966,
        "Port": "salvar",
        "Eng": "save"
    },
    {
        "Rank": 967,
        "Port": "membro",
        "Eng": "member"
    },
    {
        "Rank": 968,
        "Port": "gosto",
        "Eng": "taste"
    },
    {
        "Rank": 968,
        "Port": "gosto",
        "Eng": "preference"
    },
    {
        "Rank": 969,
        "Port": "atrair",
        "Eng": "attract"
    },
    {
        "Rank": 970,
        "Port": "profissão",
        "Eng": "profession"
    },
    {
        "Rank": 971,
        "Port": "poesia",
        "Eng": "poetry"
    },
    {
        "Rank": 972,
        "Port": "busca",
        "Eng": "search"
    },
    {
        "Rank": 973,
        "Port": "actor",
        "Eng": "actor"
    },
    {
        "Rank": 974,
        "Port": "limitar",
        "Eng": "limit"
    },
    {
        "Rank": 975,
        "Port": "novamente",
        "Eng": "again"
    },
    {
        "Rank": 975,
        "Port": "novamente",
        "Eng": "newly"
    },
    {
        "Rank": 975,
        "Port": "novamente",
        "Eng": "recently"
    },
    {
        "Rank": 976,
        "Port": "página",
        "Eng": "page"
    },
    {
        "Rank": 977,
        "Port": "permanecer",
        "Eng": "stay"
    },
    {
        "Rank": 977,
        "Port": "permanecer",
        "Eng": "remain"
    },
    {
        "Rank": 978,
        "Port": "desejo",
        "Eng": "desire"
    },
    {
        "Rank": 979,
        "Port": "destino",
        "Eng": "destination"
    },
    {
        "Rank": 980,
        "Port": "espanhol",
        "Eng": "Spanish"
    },
    {
        "Rank": 981,
        "Port": "marca",
        "Eng": "brand name"
    },
    {
        "Rank": 981,
        "Port": "marca",
        "Eng": "mark"
    },
    {
        "Rank": 982,
        "Port": "conter",
        "Eng": "contain"
    },
    {
        "Rank": 983,
        "Port": "vinho",
        "Eng": "wine"
    },
    {
        "Rank": 984,
        "Port": "quente",
        "Eng": "hot"
    },
    {
        "Rank": 985,
        "Port": "trás",
        "Eng": "back"
    },
    {
        "Rank": 985,
        "Port": "trás",
        "Eng": "behind"
    },
    {
        "Rank": 986,
        "Port": "acto",
        "Eng": "act"
    },
    {
        "Rank": 987,
        "Port": "ligação",
        "Eng": "connection"
    },
    {
        "Rank": 987,
        "Port": "ligação",
        "Eng": "phone call"
    },
    {
        "Rank": 988,
        "Port": "intelectual",
        "Eng": "intellectual"
    },
    {
        "Rank": 989,
        "Port": "tom",
        "Eng": "tone"
    },
    {
        "Rank": 989,
        "Port": "tom",
        "Eng": "sound"
    },
    {
        "Rank": 990,
        "Port": "estender",
        "Eng": "extend"
    },
    {
        "Rank": 990,
        "Port": "estender",
        "Eng": "stretch"
    },
    {
        "Rank": 991,
        "Port": "visitar",
        "Eng": "visit"
    },
    {
        "Rank": 992,
        "Port": "bastar",
        "Eng": "be enough"
    },
    {
        "Rank": 992,
        "Port": "bastar",
        "Eng": "suffice"
    },
    {
        "Rank": 993,
        "Port": "cidadão",
        "Eng": "citizen"
    },
    {
        "Rank": 994,
        "Port": "companheiro",
        "Eng": "companion"
    },
    {
        "Rank": 994,
        "Port": "companheiro",
        "Eng": "colleague"
    },
    {
        "Rank": 995,
        "Port": "crer",
        "Eng": "believe"
    },
    {
        "Rank": 996,
        "Port": "ouro",
        "Eng": "gold"
    },
    {
        "Rank": 997,
        "Port": "eléctrico",
        "Eng": "electric"
    },
    {
        "Rank": 998,
        "Port": "seco",
        "Eng": "dry"
    },
    {
        "Rank": 999,
        "Port": "fábrica",
        "Eng": "factory"
    },
    {
        "Rank": 1000,
        "Port": "acrescentar",
        "Eng": "add to"
    },
    {
        "Rank": 1001,
        "Port": "juiz",
        "Eng": "judge"
    },
    {
        "Rank": 1001,
        "Port": "juiz",
        "Eng": "referee"
    },
    {
        "Rank": 1002,
        "Port": "original",
        "Eng": "original"
    },
    {
        "Rank": 1003,
        "Port": "dívida",
        "Eng": "debt"
    },
    {
        "Rank": 1004,
        "Port": "chão",
        "Eng": "ground"
    },
    {
        "Rank": 1004,
        "Port": "chão",
        "Eng": "floor"
    },
    {
        "Rank": 1005,
        "Port": "eleger",
        "Eng": "elect"
    },
    {
        "Rank": 1005,
        "Port": "eleger",
        "Eng": "choose"
    },
    {
        "Rank": 1006,
        "Port": "tentativa",
        "Eng": "attempt"
    },
    {
        "Rank": 1007,
        "Port": "alemão",
        "Eng": "German"
    },
    {
        "Rank": 1008,
        "Port": "baixar",
        "Eng": "lower"
    },
    {
        "Rank": 1008,
        "Port": "baixar",
        "Eng": "go down"
    },
    {
        "Rank": 1009,
        "Port": "praia",
        "Eng": "beach"
    },
    {
        "Rank": 1010,
        "Port": "ajuda",
        "Eng": "help"
    },
    {
        "Rank": 1011,
        "Port": "navio",
        "Eng": "ship"
    },
    {
        "Rank": 1012,
        "Port": "lista",
        "Eng": "list"
    },
    {
        "Rank": 1013,
        "Port": "torre",
        "Eng": "tower"
    },
    {
        "Rank": 1014,
        "Port": "pele",
        "Eng": "skin"
    },
    {
        "Rank": 1015,
        "Port": "perigo",
        "Eng": "danger"
    },
    {
        "Rank": 1016,
        "Port": "céu",
        "Eng": "sky"
    },
    {
        "Rank": 1016,
        "Port": "céu",
        "Eng": "heaven"
    },
    {
        "Rank": 1017,
        "Port": "diário",
        "Eng": "diary"
    },
    {
        "Rank": 1017,
        "Port": "diário",
        "Eng": "journal"
    },
    {
        "Rank": 1017,
        "Port": "diário",
        "Eng": "daily"
    },
    {
        "Rank": 1018,
        "Port": "juro",
        "Eng": "interest"
    },
    {
        "Rank": 1019,
        "Port": "comparar",
        "Eng": "compare"
    },
    {
        "Rank": 1020,
        "Port": "cuidado",
        "Eng": "caution"
    },
    {
        "Rank": 1020,
        "Port": "cuidado",
        "Eng": "care"
    },
    {
        "Rank": 1021,
        "Port": "imposto",
        "Eng": "tax"
    },
    {
        "Rank": 1021,
        "Port": "imposto",
        "Eng": "imposed"
    },
    {
        "Rank": 1022,
        "Port": "recusar",
        "Eng": "refuse"
    },
    {
        "Rank": 1023,
        "Port": "bola",
        "Eng": "ball"
    },
    {
        "Rank": 1024,
        "Port": "demonstrar",
        "Eng": "demonstrate"
    },
    {
        "Rank": 1025,
        "Port": "regressar",
        "Eng": "return"
    },
    {
        "Rank": 1026,
        "Port": "técnica",
        "Eng": "technique"
    },
    {
        "Rank": 1027,
        "Port": "volume",
        "Eng": "volume"
    },
    {
        "Rank": 1028,
        "Port": "desenho",
        "Eng": "drawing"
    },
    {
        "Rank": 1029,
        "Port": "domínio",
        "Eng": "dominion"
    },
    {
        "Rank": 1029,
        "Port": "domínio",
        "Eng": "domain"
    },
    {
        "Rank": 1029,
        "Port": "domínio",
        "Eng": "dominance"
    },
    {
        "Rank": 1030,
        "Port": "dominar",
        "Eng": "dominate"
    },
    {
        "Rank": 1031,
        "Port": "congresso",
        "Eng": "congress"
    },
    {
        "Rank": 1032,
        "Port": "secretário",
        "Eng": "secretary"
    },
    {
        "Rank": 1033,
        "Port": "roupa",
        "Eng": "clothing"
    },
    {
        "Rank": 1033,
        "Port": "roupa",
        "Eng": "clothes"
    },
    {
        "Rank": 1034,
        "Port": "documento",
        "Eng": "document"
    },
    {
        "Rank": 1035,
        "Port": "acusar",
        "Eng": "accuse"
    },
    {
        "Rank": 1036,
        "Port": "puro",
        "Eng": "pure"
    },
    {
        "Rank": 1037,
        "Port": "russisch",
        "Eng": "deny"
    },
    {
        "Rank": 1038,
        "Port": "identificar",
        "Eng": "identify"
    },
    {
        "Rank": 1039,
        "Port": "totalmente",
        "Eng": "totally"
    },
    {
        "Rank": 1040,
        "Port": "clássico",
        "Eng": "classic"
    },
    {
        "Rank": 1040,
        "Port": "clássico",
        "Eng": "classical"
    },
    {
        "Rank": 1041,
        "Port": "transmitir",
        "Eng": "transmit"
    },
    {
        "Rank": 1042,
        "Port": "ritmo",
        "Eng": "rhythm"
    },
    {
        "Rank": 1043,
        "Port": "edifício",
        "Eng": "building"
    },
    {
        "Rank": 1043,
        "Port": "edifício",
        "Eng": "edifice"
    },
    {
        "Rank": 1044,
        "Port": "corresponder",
        "Eng": "correspond"
    },
    {
        "Rank": 1045,
        "Port": "concepção",
        "Eng": "concept"
    },
    {
        "Rank": 1045,
        "Port": "concepção",
        "Eng": "conception"
    },
    {
        "Rank": 1046,
        "Port": "barco",
        "Eng": "boat"
    },
    {
        "Rank": 1046,
        "Port": "barco",
        "Eng": "ship"
    },
    {
        "Rank": 1047,
        "Port": "prazer",
        "Eng": "pleasure"
    },
    {
        "Rank": 1048,
        "Port": "propriedade",
        "Eng": "property"
    },
    {
        "Rank": 1049,
        "Port": "estação",
        "Eng": "season"
    },
    {
        "Rank": 1049,
        "Port": "estação",
        "Eng": "station"
    },
    {
        "Rank": 1050,
        "Port": "fundamental",
        "Eng": "fundamental"
    },
    {
        "Rank": 1050,
        "Port": "fundamental",
        "Eng": "basic"
    },
    {
        "Rank": 1051,
        "Port": "fazenda",
        "Eng": "farm"
    },
    {
        "Rank": 1051,
        "Port": "fazenda",
        "Eng": "fabric"
    },
    {
        "Rank": 1052,
        "Port": "conceito",
        "Eng": "concept"
    },
    {
        "Rank": 1053,
        "Port": "alma",
        "Eng": "soul"
    },
    {
        "Rank": 1054,
        "Port": "ensino",
        "Eng": "education"
    },
    {
        "Rank": 1054,
        "Port": "ensino",
        "Eng": "teaching"
    },
    {
        "Rank": 1055,
        "Port": "imprensa",
        "Eng": "press"
    },
    {
        "Rank": 1056,
        "Port": "confiança",
        "Eng": "confidence"
    },
    {
        "Rank": 1056,
        "Port": "confiança",
        "Eng": "trust"
    },
    {
        "Rank": 1057,
        "Port": "carga",
        "Eng": "load"
    },
    {
        "Rank": 1057,
        "Port": "carga",
        "Eng": "cargo"
    },
    {
        "Rank": 1057,
        "Port": "carga",
        "Eng": "baggage"
    },
    {
        "Rank": 1058,
        "Port": "rapidamente",
        "Eng": "quickly"
    },
    {
        "Rank": 1058,
        "Port": "rapidamente",
        "Eng": "fast"
    },
    {
        "Rank": 1059,
        "Port": "guardar",
        "Eng": "keep"
    },
    {
        "Rank": 1059,
        "Port": "guardar",
        "Eng": "guard"
    },
    {
        "Rank": 1059,
        "Port": "guardar",
        "Eng": "put away"
    },
    {
        "Rank": 1060,
        "Port": "federal",
        "Eng": "federal"
    },
    {
        "Rank": 1061,
        "Port": "grosso",
        "Eng": "thick"
    },
    {
        "Rank": 1061,
        "Port": "grosso",
        "Eng": "coarse"
    },
    {
        "Rank": 1061,
        "Port": "grosso",
        "Eng": "rude"
    },
    {
        "Rank": 1062,
        "Port": "teoria",
        "Eng": "theory"
    },
    {
        "Rank": 1063,
        "Port": "católico",
        "Eng": "catholic"
    },
    {
        "Rank": 1064,
        "Port": "instalar",
        "Eng": "establish"
    },
    {
        "Rank": 1064,
        "Port": "instalar",
        "Eng": "install"
    },
    {
        "Rank": 1065,
        "Port": "par",
        "Eng": "pair"
    },
    {
        "Rank": 1066,
        "Port": "futebol",
        "Eng": "soccer"
    },
    {
        "Rank": 1067,
        "Port": "privado",
        "Eng": "private"
    },
    {
        "Rank": 1067,
        "Port": "privado",
        "Eng": "deprived"
    },
    {
        "Rank": 1068,
        "Port": "ataque",
        "Eng": "attack"
    },
    {
        "Rank": 1069,
        "Port": "gerar",
        "Eng": "create"
    },
    {
        "Rank": 1069,
        "Port": "gerar",
        "Eng": "generate"
    },
    {
        "Rank": 1070,
        "Port": "praticar",
        "Eng": "practice"
    },
    {
        "Rank": 1071,
        "Port": "museu",
        "Eng": "museum"
    },
    {
        "Rank": 1072,
        "Port": "pesquisa",
        "Eng": "study"
    },
    {
        "Rank": 1072,
        "Port": "pesquisa",
        "Eng": "research"
    },
    {
        "Rank": 1073,
        "Port": "finalmente",
        "Eng": "finally"
    },
    {
        "Rank": 1074,
        "Port": "leitor",
        "Eng": "reader"
    },
    {
        "Rank": 1075,
        "Port": "positivo",
        "Eng": "positive"
    },
    {
        "Rank": 1076,
        "Port": "vizinho",
        "Eng": "neighbor"
    },
    {
        "Rank": 1077,
        "Port": "ocasião",
        "Eng": "occasion"
    },
    {
        "Rank": 1078,
        "Port": "quantidade",
        "Eng": "quantity"
    },
    {
        "Rank": 1079,
        "Port": "diminuir",
        "Eng": "go down"
    },
    {
        "Rank": 1079,
        "Port": "diminuir",
        "Eng": "diminish"
    },
    {
        "Rank": 1079,
        "Port": "diminuir",
        "Eng": "reduce"
    },
    {
        "Rank": 1080,
        "Port": "general",
        "Eng": "general"
    },
    {
        "Rank": 1081,
        "Port": "montar",
        "Eng": "assemble"
    },
    {
        "Rank": 1081,
        "Port": "montar",
        "Eng": "ride"
    },
    {
        "Rank": 1082,
        "Port": "alterar",
        "Eng": "alter"
    },
    {
        "Rank": 1083,
        "Port": "científico",
        "Eng": "scientific"
    },
    {
        "Rank": 1084,
        "Port": "mestre",
        "Eng": "master"
    },
    {
        "Rank": 1084,
        "Port": "mestre",
        "Eng": "teacher"
    },
    {
        "Rank": 1085,
        "Port": "missão",
        "Eng": "mission"
    },
    {
        "Rank": 1086,
        "Port": "imediato",
        "Eng": "immediate"
    },
    {
        "Rank": 1087,
        "Port": "contracto",
        "Eng": "contract"
    },
    {
        "Rank": 1088,
        "Port": "frase",
        "Eng": "phrase"
    },
    {
        "Rank": 1089,
        "Port": "independente",
        "Eng": "independent"
    },
    {
        "Rank": 1090,
        "Port": "soldado",
        "Eng": "soldier"
    },
    {
        "Rank": 1091,
        "Port": "pressão",
        "Eng": "pressure"
    },
    {
        "Rank": 1092,
        "Port": "avião",
        "Eng": "airplane"
    },
    {
        "Rank": 1093,
        "Port": "colega",
        "Eng": "colleague"
    },
    {
        "Rank": 1093,
        "Port": "colega",
        "Eng": "friend"
    },
    {
        "Rank": 1093,
        "Port": "colega",
        "Eng": "classmate"
    },
    {
        "Rank": 1094,
        "Port": "órgão",
        "Eng": "institution"
    },
    {
        "Rank": 1094,
        "Port": "órgão",
        "Eng": "organ"
    },
    {
        "Rank": 1095,
        "Port": "equipamento",
        "Eng": "equipment"
    },
    {
        "Rank": 1096,
        "Port": "descer",
        "Eng": "descend"
    },
    {
        "Rank": 1096,
        "Port": "descer",
        "Eng": "go down"
    },
    {
        "Rank": 1097,
        "Port": "jornalista",
        "Eng": "journalist"
    },
    {
        "Rank": 1098,
        "Port": "ponta",
        "Eng": "tip"
    },
    {
        "Rank": 1098,
        "Port": "ponta",
        "Eng": "point"
    },
    {
        "Rank": 1098,
        "Port": "ponta",
        "Eng": "end"
    },
    {
        "Rank": 1099,
        "Port": "propósito",
        "Eng": "purpose"
    },
    {
        "Rank": 1100,
        "Port": "irmã",
        "Eng": "sister"
    },
    {
        "Rank": 1101,
        "Port": "ovo",
        "Eng": "egg"
    },
    {
        "Rank": 1102,
        "Port": "iniciativa",
        "Eng": "initiative"
    },
    {
        "Rank": 1103,
        "Port": "legal",
        "Eng": "legal"
    },
    {
        "Rank": 1104,
        "Port": "dor",
        "Eng": "pain"
    },
    {
        "Rank": 1105,
        "Port": "referência",
        "Eng": "reference"
    },
    {
        "Rank": 1105,
        "Port": "referência",
        "Eng": "referral"
    },
    {
        "Rank": 1106,
        "Port": "imenso",
        "Eng": "immense"
    },
    {
        "Rank": 1107,
        "Port": "destacar",
        "Eng": "stand out"
    },
    {
        "Rank": 1107,
        "Port": "destacar",
        "Eng": "highlight"
    },
    {
        "Rank": 1108,
        "Port": "grau",
        "Eng": "degree"
    },
    {
        "Rank": 1109,
        "Port": "chuva",
        "Eng": "rain"
    },
    {
        "Rank": 1110,
        "Port": "praça",
        "Eng": "square"
    },
    {
        "Rank": 1110,
        "Port": "praça",
        "Eng": "plaza"
    },
    {
        "Rank": 1111,
        "Port": "engenheiro",
        "Eng": "engineer"
    },
    {
        "Rank": 1112,
        "Port": "agir",
        "Eng": "act"
    },
    {
        "Rank": 1113,
        "Port": "proteger",
        "Eng": "protect"
    },
    {
        "Rank": 1114,
        "Port": "atribuir",
        "Eng": "attribute"
    },
    {
        "Rank": 1115,
        "Port": "religião",
        "Eng": "religion"
    },
    {
        "Rank": 1116,
        "Port": "analisar",
        "Eng": "analyze"
    },
    {
        "Rank": 1117,
        "Port": "hábito",
        "Eng": "habit"
    },
    {
        "Rank": 1118,
        "Port": "quinto",
        "Eng": "fifth"
    },
    {
        "Rank": 1119,
        "Port": "destruir",
        "Eng": "destroy"
    },
    {
        "Rank": 1120,
        "Port": "compra",
        "Eng": "purchase"
    },
    {
        "Rank": 1121,
        "Port": "atacar",
        "Eng": "attack"
    },
    {
        "Rank": 1122,
        "Port": "combate",
        "Eng": "fight"
    },
    {
        "Rank": 1122,
        "Port": "combate",
        "Eng": "combat"
    },
    {
        "Rank": 1123,
        "Port": "avaliar",
        "Eng": "evaluate"
    },
    {
        "Rank": 1123,
        "Port": "avaliar",
        "Eng": "assess"
    },
    {
        "Rank": 1124,
        "Port": "dono",
        "Eng": "owner"
    },
    {
        "Rank": 1124,
        "Port": "dono",
        "Eng": "boss"
    },
    {
        "Rank": 1125,
        "Port": "trocar",
        "Eng": "change"
    },
    {
        "Rank": 1125,
        "Port": "trocar",
        "Eng": "exchange"
    },
    {
        "Rank": 1125,
        "Port": "trocar",
        "Eng": "switch"
    },
    {
        "Rank": 1126,
        "Port": "acontecimento",
        "Eng": "event"
    },
    {
        "Rank": 1126,
        "Port": "acontecimento",
        "Eng": "happening"
    },
    {
        "Rank": 1127,
        "Port": "policial",
        "Eng": "police"
    },
    {
        "Rank": 1127,
        "Port": "policial",
        "Eng": "policeman"
    },
    {
        "Rank": 1128,
        "Port": "recente",
        "Eng": "recent"
    },
    {
        "Rank": 1129,
        "Port": "custar",
        "Eng": "cost"
    },
    {
        "Rank": 1130,
        "Port": "enviar",
        "Eng": "send"
    },
    {
        "Rank": 1131,
        "Port": "onda",
        "Eng": "wave"
    },
    {
        "Rank": 1132,
        "Port": "entrevista",
        "Eng": "interview"
    },
    {
        "Rank": 1133,
        "Port": "imediatamente",
        "Eng": "immediately"
    },
    {
        "Rank": 1134,
        "Port": "declarar",
        "Eng": "declare"
    },
    {
        "Rank": 1135,
        "Port": "afinal",
        "Eng": "finally"
    },
    {
        "Rank": 1135,
        "Port": "afinal",
        "Eng": "at last"
    },
    {
        "Rank": 1135,
        "Port": "afinal",
        "Eng": "in the end"
    },
    {
        "Rank": 1136,
        "Port": "beleza",
        "Eng": "beauty"
    },
    {
        "Rank": 1137,
        "Port": "mina",
        "Eng": "mine"
    },
    {
        "Rank": 1138,
        "Port": "bonito",
        "Eng": "beautiful"
    },
    {
        "Rank": 1138,
        "Port": "bonito",
        "Eng": "pretty"
    },
    {
        "Rank": 1138,
        "Port": "bonito",
        "Eng": "handsome"
    },
    {
        "Rank": 1139,
        "Port": "raro",
        "Eng": "rare"
    },
    {
        "Rank": 1140,
        "Port": "evidente",
        "Eng": "evident"
    },
    {
        "Rank": 1141,
        "Port": "esperança",
        "Eng": "hope"
    },
    {
        "Rank": 1142,
        "Port": "aparelho",
        "Eng": "device"
    },
    {
        "Rank": 1142,
        "Port": "aparelho",
        "Eng": "apparatus"
    },
    {
        "Rank": 1143,
        "Port": "diálogo",
        "Eng": "dialogue"
    },
    {
        "Rank": 1144,
        "Port": "conversar",
        "Eng": "talk"
    },
    {
        "Rank": 1144,
        "Port": "conversar",
        "Eng": "converse"
    },
    {
        "Rank": 1145,
        "Port": "inverno",
        "Eng": "winter"
    },
    {
        "Rank": 1146,
        "Port": "vítima",
        "Eng": "victim"
    },
    {
        "Rank": 1147,
        "Port": "cliente",
        "Eng": "customer"
    },
    {
        "Rank": 1147,
        "Port": "cliente",
        "Eng": "client"
    },
    {
        "Rank": 1148,
        "Port": "contribuir",
        "Eng": "contribute"
    },
    {
        "Rank": 1149,
        "Port": "actualmente",
        "Eng": "currently"
    },
    {
        "Rank": 1149,
        "Port": "actualmente",
        "Eng": "nowadays"
    },
    {
        "Rank": 1150,
        "Port": "fio",
        "Eng": "strand"
    },
    {
        "Rank": 1150,
        "Port": "fio",
        "Eng": "wire"
    },
    {
        "Rank": 1151,
        "Port": "tropa",
        "Eng": "army"
    },
    {
        "Rank": 1152,
        "Port": "unidade",
        "Eng": "unit"
    },
    {
        "Rank": 1153,
        "Port": "estranho",
        "Eng": "strange"
    },
    {
        "Rank": 1153,
        "Port": "estranho",
        "Eng": "uncommon"
    },
    {
        "Rank": 1154,
        "Port": "característica",
        "Eng": "characteristic"
    },
    {
        "Rank": 1155,
        "Port": "fronteira",
        "Eng": "border"
    },
    {
        "Rank": 1155,
        "Port": "fronteira",
        "Eng": "frontier"
    },
    {
        "Rank": 1156,
        "Port": "organizar",
        "Eng": "organize"
    },
    {
        "Rank": 1157,
        "Port": "carregar",
        "Eng": "carry"
    },
    {
        "Rank": 1157,
        "Port": "carregar",
        "Eng": "transport"
    },
    {
        "Rank": 1158,
        "Port": "contudo",
        "Eng": "however"
    },
    {
        "Rank": 1158,
        "Port": "contudo",
        "Eng": "although"
    },
    {
        "Rank": 1159,
        "Port": "intervenção",
        "Eng": "intervention"
    },
    {
        "Rank": 1160,
        "Port": "poder",
        "Eng": "power"
    },
    {
        "Rank": 1161,
        "Port": "naturalmente",
        "Eng": "naturally"
    },
    {
        "Rank": 1162,
        "Port": "sensação",
        "Eng": "sensation"
    },
    {
        "Rank": 1163,
        "Port": "doce",
        "Eng": "sweet"
    },
    {
        "Rank": 1163,
        "Port": "doce",
        "Eng": "candy"
    },
    {
        "Rank": 1164,
        "Port": "sugerir",
        "Eng": "suggest"
    },
    {
        "Rank": 1165,
        "Port": "cadeia",
        "Eng": "jail"
    },
    {
        "Rank": 1165,
        "Port": "cadeia",
        "Eng": "chain"
    },
    {
        "Rank": 1165,
        "Port": "cadeia",
        "Eng": "sequence"
    },
    {
        "Rank": 1166,
        "Port": "colégio",
        "Eng": "high school"
    },
    {
        "Rank": 1166,
        "Port": "colégio",
        "Eng": "private school"
    },
    {
        "Rank": 1167,
        "Port": "moda",
        "Eng": "fashion"
    },
    {
        "Rank": 1167,
        "Port": "moda",
        "Eng": "style"
    },
    {
        "Rank": 1168,
        "Port": "gado",
        "Eng": "cattle"
    },
    {
        "Rank": 1169,
        "Port": "conclusão",
        "Eng": "conclusion"
    },
    {
        "Rank": 1170,
        "Port": "sozinho",
        "Eng": "alone"
    },
    {
        "Rank": 1170,
        "Port": "sozinho",
        "Eng": "lonely"
    },
    {
        "Rank": 1171,
        "Port": "acidente",
        "Eng": "accident"
    },
    {
        "Rank": 1172,
        "Port": "bolsa",
        "Eng": "purse"
    },
    {
        "Rank": 1172,
        "Port": "bolsa",
        "Eng": "bag"
    },
    {
        "Rank": 1173,
        "Port": "conflito",
        "Eng": "conflict"
    },
    {
        "Rank": 1174,
        "Port": "aldeia",
        "Eng": "village"
    },
    {
        "Rank": 1175,
        "Port": "chave",
        "Eng": "key"
    },
    {
        "Rank": 1176,
        "Port": "activo",
        "Eng": "active"
    },
    {
        "Rank": 1177,
        "Port": "visita",
        "Eng": "visit"
    },
    {
        "Rank": 1178,
        "Port": "agente",
        "Eng": "agent"
    },
    {
        "Rank": 1179,
        "Port": "alimentar",
        "Eng": "feed"
    },
    {
        "Rank": 1179,
        "Port": "alimentar",
        "Eng": "nourish"
    },
    {
        "Rank": 1180,
        "Port": "prisão",
        "Eng": "prison"
    },
    {
        "Rank": 1181,
        "Port": "voar",
        "Eng": "fly"
    },
    {
        "Rank": 1182,
        "Port": "recuperar",
        "Eng": "recover"
    },
    {
        "Rank": 1182,
        "Port": "recuperar",
        "Eng": "recuperate"
    },
    {
        "Rank": 1183,
        "Port": "bandeira",
        "Eng": "flag"
    },
    {
        "Rank": 1184,
        "Port": "municipal",
        "Eng": "municipal"
    },
    {
        "Rank": 1185,
        "Port": "famoso",
        "Eng": "famous"
    },
    {
        "Rank": 1186,
        "Port": "filosofia",
        "Eng": "philosophy"
    },
    {
        "Rank": 1187,
        "Port": "empregar",
        "Eng": "employ"
    },
    {
        "Rank": 1188,
        "Port": "italiano",
        "Eng": "Italian"
    },
    {
        "Rank": 1189,
        "Port": "justificar",
        "Eng": "justify"
    },
    {
        "Rank": 1190,
        "Port": "aumento",
        "Eng": "increase"
    },
    {
        "Rank": 1190,
        "Port": "aumento",
        "Eng": "growth"
    },
    {
        "Rank": 1191,
        "Port": "queda",
        "Eng": "fall"
    },
    {
        "Rank": 1192,
        "Port": "musical",
        "Eng": "musical"
    },
    {
        "Rank": 1193,
        "Port": "dança",
        "Eng": "dance"
    },
    {
        "Rank": 1194,
        "Port": "canto",
        "Eng": "corner"
    },
    {
        "Rank": 1194,
        "Port": "canto",
        "Eng": "song"
    },
    {
        "Rank": 1195,
        "Port": "apanhar",
        "Eng": "catch"
    },
    {
        "Rank": 1195,
        "Port": "apanhar",
        "Eng": "grab"
    },
    {
        "Rank": 1195,
        "Port": "apanhar",
        "Eng": "lift"
    },
    {
        "Rank": 1196,
        "Port": "tribunal",
        "Eng": "court"
    },
    {
        "Rank": 1196,
        "Port": "tribunal",
        "Eng": "tribunal"
    },
    {
        "Rank": 1197,
        "Port": "resistência",
        "Eng": "resistance"
    },
    {
        "Rank": 1198,
        "Port": "quinze",
        "Eng": "fifteen"
    },
    {
        "Rank": 1199,
        "Port": "fruto",
        "Eng": "fruit"
    },
    {
        "Rank": 1200,
        "Port": "evolução",
        "Eng": "evolution"
    },
    {
        "Rank": 1201,
        "Port": "provavelmente",
        "Eng": "probably"
    },
    {
        "Rank": 1202,
        "Port": "paixão",
        "Eng": "passion"
    },
    {
        "Rank": 1203,
        "Port": "flor",
        "Eng": "flower"
    },
    {
        "Rank": 1204,
        "Port": "personalidade",
        "Eng": "personality"
    },
    {
        "Rank": 1205,
        "Port": "raiz",
        "Eng": "root"
    },
    {
        "Rank": 1206,
        "Port": "conversa",
        "Eng": "conversation"
    },
    {
        "Rank": 1207,
        "Port": "prometer",
        "Eng": "promise"
    },
    {
        "Rank": 1208,
        "Port": "solo",
        "Eng": "soil"
    },
    {
        "Rank": 1209,
        "Port": "duro",
        "Eng": "hard"
    },
    {
        "Rank": 1210,
        "Port": "informar",
        "Eng": "inform"
    },
    {
        "Rank": 1211,
        "Port": "parque",
        "Eng": "park"
    },
    {
        "Rank": 1212,
        "Port": "exame",
        "Eng": "exam"
    },
    {
        "Rank": 1213,
        "Port": "comentar",
        "Eng": "comment"
    },
    {
        "Rank": 1214,
        "Port": "telefone",
        "Eng": "telephone"
    },
    {
        "Rank": 1215,
        "Port": "azul",
        "Eng": "blue"
    },
    {
        "Rank": 1216,
        "Port": "inventar",
        "Eng": "invent"
    },
    {
        "Rank": 1217,
        "Port": "sustentar",
        "Eng": "support"
    },
    {
        "Rank": 1217,
        "Port": "sustentar",
        "Eng": "sustain"
    },
    {
        "Rank": 1218,
        "Port": "quebrar",
        "Eng": "break"
    },
    {
        "Rank": 1219,
        "Port": "fraco",
        "Eng": "weak"
    },
    {
        "Rank": 1220,
        "Port": "fino",
        "Eng": "fine"
    },
    {
        "Rank": 1220,
        "Port": "fino",
        "Eng": "thin"
    },
    {
        "Rank": 1221,
        "Port": "despesa",
        "Eng": "expense"
    },
    {
        "Rank": 1222,
        "Port": "cem",
        "Eng": "hundred"
    },
    {
        "Rank": 1223,
        "Port": "factor",
        "Eng": "factor"
    },
    {
        "Rank": 1224,
        "Port": "encarar",
        "Eng": "face"
    },
    {
        "Rank": 1225,
        "Port": "loja",
        "Eng": "store"
    },
    {
        "Rank": 1226,
        "Port": "menina",
        "Eng": "little girl"
    },
    {
        "Rank": 1227,
        "Port": "sexo",
        "Eng": "gender"
    },
    {
        "Rank": 1227,
        "Port": "sexo",
        "Eng": "sex"
    },
    {
        "Rank": 1228,
        "Port": "culpa",
        "Eng": "fault"
    },
    {
        "Rank": 1228,
        "Port": "culpa",
        "Eng": "guilt"
    },
    {
        "Rank": 1229,
        "Port": "castelo",
        "Eng": "castle"
    },
    {
        "Rank": 1230,
        "Port": "instituto",
        "Eng": "institute"
    },
    {
        "Rank": 1231,
        "Port": "pleno",
        "Eng": "complete"
    },
    {
        "Rank": 1231,
        "Port": "pleno",
        "Eng": "full"
    },
    {
        "Rank": 1232,
        "Port": "merecer",
        "Eng": "deserve"
    },
    {
        "Rank": 1233,
        "Port": "breve",
        "Eng": "brief"
    },
    {
        "Rank": 1234,
        "Port": "directamente",
        "Eng": "directly"
    },
    {
        "Rank": 1235,
        "Port": "perfeito",
        "Eng": "perfect"
    },
    {
        "Rank": 1236,
        "Port": "golpe",
        "Eng": "coup"
    },
    {
        "Rank": 1236,
        "Port": "golpe",
        "Eng": "hit"
    },
    {
        "Rank": 1236,
        "Port": "golpe",
        "Eng": "blow"
    },
    {
        "Rank": 1237,
        "Port": "fenómeno",
        "Eng": "phenomenon"
    },
    {
        "Rank": 1238,
        "Port": "prédio",
        "Eng": "building"
    },
    {
        "Rank": 1239,
        "Port": "provar",
        "Eng": "prove"
    },
    {
        "Rank": 1239,
        "Port": "provar",
        "Eng": "test"
    },
    {
        "Rank": 1239,
        "Port": "provar",
        "Eng": "try"
    },
    {
        "Rank": 1240,
        "Port": "africano",
        "Eng": "African"
    },
    {
        "Rank": 1241,
        "Port": "partida",
        "Eng": "departure"
    },
    {
        "Rank": 1242,
        "Port": "complexo",
        "Eng": "complex"
    },
    {
        "Rank": 1243,
        "Port": "conquistar",
        "Eng": "conquer"
    },
    {
        "Rank": 1243,
        "Port": "conquistar",
        "Eng": "secure"
    },
    {
        "Rank": 1244,
        "Port": "obrigação",
        "Eng": "obligation"
    },
    {
        "Rank": 1245,
        "Port": "especialmente",
        "Eng": "especially"
    },
    {
        "Rank": 1246,
        "Port": "senão",
        "Eng": "or else"
    },
    {
        "Rank": 1246,
        "Port": "senão",
        "Eng": "except"
    },
    {
        "Rank": 1246,
        "Port": "senão",
        "Eng": "if not"
    },
    {
        "Rank": 1247,
        "Port": "cultural",
        "Eng": "cultural"
    },
    {
        "Rank": 1248,
        "Port": "elevado",
        "Eng": "high"
    },
    {
        "Rank": 1248,
        "Port": "elevado",
        "Eng": "elevated"
    },
    {
        "Rank": 1249,
        "Port": "chamada",
        "Eng": "phone call"
    },
    {
        "Rank": 1250,
        "Port": "violento",
        "Eng": "violent"
    },
    {
        "Rank": 1251,
        "Port": "disposição",
        "Eng": "willingness"
    },
    {
        "Rank": 1251,
        "Port": "disposição",
        "Eng": "disposition"
    },
    {
        "Rank": 1252,
        "Port": "análise",
        "Eng": "analysis"
    },
    {
        "Rank": 1253,
        "Port": "pão",
        "Eng": "bread"
    },
    {
        "Rank": 1254,
        "Port": "anormal",
        "Eng": "unusual"
    },
    {
        "Rank": 1254,
        "Port": "anormal",
        "Eng": "abnormal"
    },
    {
        "Rank": 1255,
        "Port": "amplo",
        "Eng": "ample"
    },
    {
        "Rank": 1255,
        "Port": "amplo",
        "Eng": "broad"
    },
    {
        "Rank": 1256,
        "Port": "confirmar",
        "Eng": "confirm"
    },
    {
        "Rank": 1257,
        "Port": "perigoso",
        "Eng": "dangerous"
    },
    {
        "Rank": 1258,
        "Port": "milhar",
        "Eng": "a thousand"
    },
    {
        "Rank": 1259,
        "Port": "pintar",
        "Eng": "paint"
    },
    {
        "Rank": 1260,
        "Port": "cometer",
        "Eng": "commit"
    },
    {
        "Rank": 1261,
        "Port": "apto",
        "Eng": "capable"
    },
    {
        "Rank": 1261,
        "Port": "apto",
        "Eng": "apt"
    },
    {
        "Rank": 1262,
        "Port": "crédito",
        "Eng": "credit"
    },
    {
        "Rank": 1263,
        "Port": "enfim",
        "Eng": "in the end"
    },
    {
        "Rank": 1263,
        "Port": "enfim",
        "Eng": "finally"
    },
    {
        "Rank": 1263,
        "Port": "enfim",
        "Eng": "in short"
    },
    {
        "Rank": 1264,
        "Port": "suceder",
        "Eng": "happen"
    },
    {
        "Rank": 1264,
        "Port": "suceder",
        "Eng": "come next"
    },
    {
        "Rank": 1265,
        "Port": "normalmente",
        "Eng": "normally"
    },
    {
        "Rank": 1265,
        "Port": "normalmente",
        "Eng": "as normal"
    },
    {
        "Rank": 1266,
        "Port": "cá",
        "Eng": "here"
    },
    {
        "Rank": 1267,
        "Port": "abertura",
        "Eng": "opening"
    },
    {
        "Rank": 1268,
        "Port": "extremo",
        "Eng": "extreme"
    },
    {
        "Rank": 1269,
        "Port": "desafio",
        "Eng": "challenge"
    },
    {
        "Rank": 1270,
        "Port": "gastar",
        "Eng": "spend"
    },
    {
        "Rank": 1270,
        "Port": "gastar",
        "Eng": "waste"
    },
    {
        "Rank": 1271,
        "Port": "decorrer",
        "Eng": "happen as a result of"
    },
    {
        "Rank": 1271,
        "Port": "decorrer",
        "Eng": "elapse"
    },
    {
        "Rank": 1272,
        "Port": "essencial",
        "Eng": "essential"
    },
    {
        "Rank": 1273,
        "Port": "ajustar",
        "Eng": "adjust"
    },
    {
        "Rank": 1274,
        "Port": "poema",
        "Eng": "poem"
    },
    {
        "Rank": 1275,
        "Port": "exercício",
        "Eng": "exercise"
    },
    {
        "Rank": 1276,
        "Port": "barro",
        "Eng": "clay"
    },
    {
        "Rank": 1276,
        "Port": "barro",
        "Eng": "mud"
    },
    {
        "Rank": 1277,
        "Port": "mundial",
        "Eng": "world"
    },
    {
        "Rank": 1277,
        "Port": "mundial",
        "Eng": "worldwide"
    },
    {
        "Rank": 1278,
        "Port": "folha",
        "Eng": "sheet"
    },
    {
        "Rank": 1278,
        "Port": "folha",
        "Eng": "page"
    },
    {
        "Rank": 1278,
        "Port": "folha",
        "Eng": "leaf"
    },
    {
        "Rank": 1279,
        "Port": "extraordinário",
        "Eng": "extraordinary"
    },
    {
        "Rank": 1280,
        "Port": "reserva",
        "Eng": "reserve"
    },
    {
        "Rank": 1281,
        "Port": "cenário",
        "Eng": "backdrop"
    },
    {
        "Rank": 1281,
        "Port": "cenário",
        "Eng": "scenery"
    },
    {
        "Rank": 1282,
        "Port": "mensagem",
        "Eng": "message"
    },
    {
        "Rank": 1283,
        "Port": "crescimento",
        "Eng": "growth"
    },
    {
        "Rank": 1284,
        "Port": "reagir",
        "Eng": "react"
    },
    {
        "Rank": 1285,
        "Port": "constituição",
        "Eng": "constitution"
    },
    {
        "Rank": 1286,
        "Port": "explicação",
        "Eng": "explanation"
    },
    {
        "Rank": 1287,
        "Port": "espera",
        "Eng": "wait"
    },
    {
        "Rank": 1287,
        "Port": "espera",
        "Eng": "expectation"
    },
    {
        "Rank": 1288,
        "Port": "votar",
        "Eng": "vote"
    },
    {
        "Rank": 1289,
        "Port": "perda",
        "Eng": "loss"
    },
    {
        "Rank": 1290,
        "Port": "artístico",
        "Eng": "artistic"
    },
    {
        "Rank": 1291,
        "Port": "leve",
        "Eng": "light"
    },
    {
        "Rank": 1292,
        "Port": "clima",
        "Eng": "climate"
    },
    {
        "Rank": 1293,
        "Port": "permanente",
        "Eng": "permanent"
    },
    {
        "Rank": 1294,
        "Port": "quilómetro",
        "Eng": "kilometer"
    },
    {
        "Rank": 1295,
        "Port": "jogador",
        "Eng": "player"
    },
    {
        "Rank": 1296,
        "Port": "troca",
        "Eng": "exchange"
    },
    {
        "Rank": 1296,
        "Port": "troca",
        "Eng": "switch"
    },
    {
        "Rank": 1297,
        "Port": "divisão",
        "Eng": "division"
    },
    {
        "Rank": 1298,
        "Port": "investigação",
        "Eng": "investigation"
    },
    {
        "Rank": 1299,
        "Port": "rainha",
        "Eng": "queen"
    },
    {
        "Rank": 1300,
        "Port": "palácio",
        "Eng": "palace"
    },
    {
        "Rank": 1301,
        "Port": "integrar",
        "Eng": "integrate"
    },
    {
        "Rank": 1301,
        "Port": "integrar",
        "Eng": "be part of"
    },
    {
        "Rank": 1302,
        "Port": "administrativo",
        "Eng": "administrative"
    },
    {
        "Rank": 1303,
        "Port": "útil",
        "Eng": "useful"
    },
    {
        "Rank": 1303,
        "Port": "útil",
        "Eng": "helpful"
    },
    {
        "Rank": 1304,
        "Port": "manifestar",
        "Eng": "manifest"
    },
    {
        "Rank": 1304,
        "Port": "manifestar",
        "Eng": "express"
    },
    {
        "Rank": 1305,
        "Port": "participação",
        "Eng": "participation"
    },
    {
        "Rank": 1306,
        "Port": "comportamento",
        "Eng": "behavior"
    },
    {
        "Rank": 1306,
        "Port": "comportamento",
        "Eng": "conduct"
    },
    {
        "Rank": 1307,
        "Port": "circunstância",
        "Eng": "circumstance"
    },
    {
        "Rank": 1308,
        "Port": "alegre",
        "Eng": "happy"
    },
    {
        "Rank": 1309,
        "Port": "casal",
        "Eng": "married couple"
    },
    {
        "Rank": 1310,
        "Port": "esconder",
        "Eng": "hide"
    },
    {
        "Rank": 1311,
        "Port": "taxa",
        "Eng": "rate"
    },
    {
        "Rank": 1311,
        "Port": "taxa",
        "Eng": "tax"
    },
    {
        "Rank": 1312,
        "Port": "inicial",
        "Eng": "initial"
    },
    {
        "Rank": 1313,
        "Port": "príncipe",
        "Eng": "prince"
    },
    {
        "Rank": 1314,
        "Port": "universo",
        "Eng": "universe"
    },
    {
        "Rank": 1315,
        "Port": "dormir",
        "Eng": "sleep"
    },
    {
        "Rank": 1316,
        "Port": "garantia",
        "Eng": "warranty"
    },
    {
        "Rank": 1316,
        "Port": "garantia",
        "Eng": "guarantee"
    },
    {
        "Rank": 1317,
        "Port": "ave",
        "Eng": "bird"
    },
    {
        "Rank": 1318,
        "Port": "amar",
        "Eng": "love"
    },
    {
        "Rank": 1319,
        "Port": "assinar",
        "Eng": "sign"
    },
    {
        "Rank": 1320,
        "Port": "motor",
        "Eng": "engine"
    },
    {
        "Rank": 1320,
        "Port": "motor",
        "Eng": "motor"
    },
    {
        "Rank": 1321,
        "Port": "gravar",
        "Eng": "record"
    },
    {
        "Rank": 1321,
        "Port": "gravar",
        "Eng": "engrave"
    },
    {
        "Rank": 1322,
        "Port": "fé",
        "Eng": "faith"
    },
    {
        "Rank": 1323,
        "Port": "expor",
        "Eng": "exhibit"
    },
    {
        "Rank": 1323,
        "Port": "expor",
        "Eng": "expose"
    },
    {
        "Rank": 1324,
        "Port": "dedo",
        "Eng": "finger"
    },
    {
        "Rank": 1325,
        "Port": "canção",
        "Eng": "song"
    },
    {
        "Rank": 1326,
        "Port": "negativo",
        "Eng": "negative"
    },
    {
        "Rank": 1327,
        "Port": "industrial",
        "Eng": "industrial"
    },
    {
        "Rank": 1328,
        "Port": "posse",
        "Eng": "possession"
    },
    {
        "Rank": 1329,
        "Port": "lucro",
        "Eng": "profit"
    },
    {
        "Rank": 1330,
        "Port": "expectativa",
        "Eng": "expectation"
    },
    {
        "Rank": 1331,
        "Port": "traduzir",
        "Eng": "translate"
    },
    {
        "Rank": 1332,
        "Port": "libertar",
        "Eng": "free"
    },
    {
        "Rank": 1332,
        "Port": "libertar",
        "Eng": "liberate"
    },
    {
        "Rank": 1333,
        "Port": "manifestação",
        "Eng": "demonstration"
    },
    {
        "Rank": 1333,
        "Port": "manifestação",
        "Eng": "protest"
    },
    {
        "Rank": 1334,
        "Port": "líder",
        "Eng": "leader"
    },
    {
        "Rank": 1335,
        "Port": "horizonte",
        "Eng": "horizon"
    },
    {
        "Rank": 1336,
        "Port": "urbano",
        "Eng": "urban"
    },
    {
        "Rank": 1337,
        "Port": "território",
        "Eng": "territory"
    },
    {
        "Rank": 1338,
        "Port": "compromisso",
        "Eng": "commitment"
    },
    {
        "Rank": 1338,
        "Port": "compromisso",
        "Eng": "appointment"
    },
    {
        "Rank": 1339,
        "Port": "feminino",
        "Eng": "female"
    },
    {
        "Rank": 1339,
        "Port": "feminino",
        "Eng": "feminine"
    },
    {
        "Rank": 1340,
        "Port": "mover",
        "Eng": "move"
    },
    {
        "Rank": 1341,
        "Port": "município",
        "Eng": "municipality"
    },
    {
        "Rank": 1342,
        "Port": "entidade",
        "Eng": "entity"
    },
    {
        "Rank": 1343,
        "Port": "constante",
        "Eng": "constant"
    },
    {
        "Rank": 1344,
        "Port": "pagamento",
        "Eng": "payment"
    },
    {
        "Rank": 1345,
        "Port": "sequer",
        "Eng": "even"
    },
    {
        "Rank": 1346,
        "Port": "promover",
        "Eng": "promote"
    },
    {
        "Rank": 1347,
        "Port": "argumento",
        "Eng": "argument"
    },
    {
        "Rank": 1348,
        "Port": "controlar",
        "Eng": "control"
    },
    {
        "Rank": 1349,
        "Port": "fome",
        "Eng": "hunger"
    },
    {
        "Rank": 1349,
        "Port": "fome",
        "Eng": "famine"
    },
    {
        "Rank": 1350,
        "Port": "pintor",
        "Eng": "painter"
    },
    {
        "Rank": 1351,
        "Port": "adoptar",
        "Eng": "adopt"
    },
    {
        "Rank": 1352,
        "Port": "restar",
        "Eng": "remain"
    },
    {
        "Rank": 1353,
        "Port": "calor",
        "Eng": "heat"
    },
    {
        "Rank": 1353,
        "Port": "calor",
        "Eng": "warmth"
    },
    {
        "Rank": 1354,
        "Port": "excepção",
        "Eng": "exception"
    },
    {
        "Rank": 1355,
        "Port": "ausência",
        "Eng": "absence"
    },
    {
        "Rank": 1356,
        "Port": "externo",
        "Eng": "external"
    },
    {
        "Rank": 1357,
        "Port": "distribuir",
        "Eng": "distribute"
    },
    {
        "Rank": 1358,
        "Port": "método",
        "Eng": "method"
    },
    {
        "Rank": 1359,
        "Port": "crítico",
        "Eng": "critic"
    },
    {
        "Rank": 1359,
        "Port": "crítico",
        "Eng": "critical"
    },
    {
        "Rank": 1360,
        "Port": "inferior",
        "Eng": "lower"
    },
    {
        "Rank": 1360,
        "Port": "inferior",
        "Eng": "less"
    },
    {
        "Rank": 1360,
        "Port": "inferior",
        "Eng": "inferior"
    },
    {
        "Rank": 1361,
        "Port": "completo",
        "Eng": "complete"
    },
    {
        "Rank": 1362,
        "Port": "protecção",
        "Eng": "protection"
    },
    {
        "Rank": 1363,
        "Port": "excelente",
        "Eng": "excellent"
    },
    {
        "Rank": 1364,
        "Port": "calcular",
        "Eng": "calculate"
    },
    {
        "Rank": 1364,
        "Port": "calcular",
        "Eng": "reckon"
    },
    {
        "Rank": 1365,
        "Port": "demorar",
        "Eng": "take"
    },
    {
        "Rank": 1365,
        "Port": "demorar",
        "Eng": "delay"
    },
    {
        "Rank": 1366,
        "Port": "sentar",
        "Eng": "sit"
    },
    {
        "Rank": 1367,
        "Port": "interpretar",
        "Eng": "interpret"
    },
    {
        "Rank": 1367,
        "Port": "interpretar",
        "Eng": "act"
    },
    {
        "Rank": 1368,
        "Port": "sorte",
        "Eng": "luck"
    },
    {
        "Rank": 1368,
        "Port": "sorte",
        "Eng": "lot"
    },
    {
        "Rank": 1369,
        "Port": "secretaria",
        "Eng": "office"
    },
    {
        "Rank": 1370,
        "Port": "recolher",
        "Eng": "collect"
    },
    {
        "Rank": 1370,
        "Port": "recolher",
        "Eng": "put away"
    },
    {
        "Rank": 1370,
        "Port": "recolher",
        "Eng": "remove"
    },
    {
        "Rank": 1371,
        "Port": "deter",
        "Eng": "arrest"
    },
    {
        "Rank": 1371,
        "Port": "deter",
        "Eng": "detain"
    },
    {
        "Rank": 1372,
        "Port": "ceder",
        "Eng": "give in"
    },
    {
        "Rank": 1372,
        "Port": "ceder",
        "Eng": "yield"
    },
    {
        "Rank": 1373,
        "Port": "feliz",
        "Eng": "happy"
    },
    {
        "Rank": 1374,
        "Port": "cadeira",
        "Eng": "chair"
    },
    {
        "Rank": 1374,
        "Port": "cadeira",
        "Eng": "college course"
    },
    {
        "Rank": 1375,
        "Port": "comunista",
        "Eng": "communist"
    },
    {
        "Rank": 1376,
        "Port": "emoção",
        "Eng": "emotion"
    },
    {
        "Rank": 1377,
        "Port": "representante",
        "Eng": "representative"
    },
    {
        "Rank": 1378,
        "Port": "ultrapassar",
        "Eng": "overcome"
    },
    {
        "Rank": 1378,
        "Port": "ultrapassar",
        "Eng": "surpass"
    },
    {
        "Rank": 1378,
        "Port": "ultrapassar",
        "Eng": "pass"
    },
    {
        "Rank": 1379,
        "Port": "comando",
        "Eng": "command"
    },
    {
        "Rank": 1380,
        "Port": "alvo",
        "Eng": "target"
    },
    {
        "Rank": 1380,
        "Port": "alvo",
        "Eng": "aim"
    },
    {
        "Rank": 1380,
        "Port": "alvo",
        "Eng": "white as snow"
    },
    {
        "Rank": 1381,
        "Port": "concentrar",
        "Eng": "concentrate"
    },
    {
        "Rank": 1382,
        "Port": "comunicar",
        "Eng": "convey"
    },
    {
        "Rank": 1382,
        "Port": "comunicar",
        "Eng": "communicate"
    },
    {
        "Rank": 1383,
        "Port": "colectivo",
        "Eng": "collective"
    },
    {
        "Rank": 1383,
        "Port": "colectivo",
        "Eng": "public transportation"
    },
    {
        "Rank": 1384,
        "Port": "jeito",
        "Eng": "way"
    },
    {
        "Rank": 1384,
        "Port": "jeito",
        "Eng": "manner"
    },
    {
        "Rank": 1385,
        "Port": "actuar",
        "Eng": "perform"
    },
    {
        "Rank": 1385,
        "Port": "actuar",
        "Eng": "act"
    },
    {
        "Rank": 1386,
        "Port": "janela",
        "Eng": "window"
    },
    {
        "Rank": 1387,
        "Port": "certamente",
        "Eng": "certainly"
    },
    {
        "Rank": 1388,
        "Port": "estratégia",
        "Eng": "strategy"
    },
    {
        "Rank": 1389,
        "Port": "papa",
        "Eng": "pope"
    },
    {
        "Rank": 1390,
        "Port": "unir",
        "Eng": "unite"
    },
    {
        "Rank": 1391,
        "Port": "comandante",
        "Eng": "commander"
    },
    {
        "Rank": 1392,
        "Port": "inimigo",
        "Eng": "enemy"
    },
    {
        "Rank": 1393,
        "Port": "eleitoral",
        "Eng": "electoral"
    },
    {
        "Rank": 1394,
        "Port": "executivo",
        "Eng": "executive"
    },
    {
        "Rank": 1395,
        "Port": "gás",
        "Eng": "gas"
    },
    {
        "Rank": 1396,
        "Port": "marquês",
        "Eng": "marquis"
    },
    {
        "Rank": 1397,
        "Port": "raça",
        "Eng": "race"
    },
    {
        "Rank": 1398,
        "Port": "palco",
        "Eng": "stage"
    },
    {
        "Rank": 1399,
        "Port": "independência",
        "Eng": "independence"
    },
    {
        "Rank": 1400,
        "Port": "empregado",
        "Eng": "employee"
    },
    {
        "Rank": 1400,
        "Port": "empregado",
        "Eng": "employed"
    },
    {
        "Rank": 1401,
        "Port": "começo",
        "Eng": "beginning"
    },
    {
        "Rank": 1401,
        "Port": "começo",
        "Eng": "start"
    },
    {
        "Rank": 1402,
        "Port": "desenhar",
        "Eng": "design"
    },
    {
        "Rank": 1402,
        "Port": "desenhar",
        "Eng": "draw"
    },
    {
        "Rank": 1403,
        "Port": "descrever",
        "Eng": "describe"
    },
    {
        "Rank": 1404,
        "Port": "receita",
        "Eng": "revenue"
    },
    {
        "Rank": 1404,
        "Port": "receita",
        "Eng": "income"
    },
    {
        "Rank": 1404,
        "Port": "receita",
        "Eng": "recipe"
    },
    {
        "Rank": 1405,
        "Port": "errar",
        "Eng": "err"
    },
    {
        "Rank": 1405,
        "Port": "errar",
        "Eng": "make a mistake"
    },
    {
        "Rank": 1406,
        "Port": "império",
        "Eng": "empire"
    },
    {
        "Rank": 1407,
        "Port": "explorar",
        "Eng": "exploit"
    },
    {
        "Rank": 1407,
        "Port": "explorar",
        "Eng": "explore"
    },
    {
        "Rank": 1408,
        "Port": "critério",
        "Eng": "criterion"
    },
    {
        "Rank": 1409,
        "Port": "deitar",
        "Eng": "lie down"
    },
    {
        "Rank": 1410,
        "Port": "chamado",
        "Eng": "called"
    },
    {
        "Rank": 1410,
        "Port": "chamado",
        "Eng": "so-called"
    },
    {
        "Rank": 1410,
        "Port": "chamado",
        "Eng": "named"
    },
    {
        "Rank": 1411,
        "Port": "detalhe",
        "Eng": "detail"
    },
    {
        "Rank": 1412,
        "Port": "plástico",
        "Eng": "plastic"
    },
    {
        "Rank": 1413,
        "Port": "corda",
        "Eng": "string"
    },
    {
        "Rank": 1413,
        "Port": "corda",
        "Eng": "cord"
    },
    {
        "Rank": 1413,
        "Port": "corda",
        "Eng": "spring"
    },
    {
        "Rank": 1414,
        "Port": "jamais",
        "Eng": "never"
    },
    {
        "Rank": 1415,
        "Port": "juventude",
        "Eng": "youth"
    },
    {
        "Rank": 1416,
        "Port": "areia",
        "Eng": "sand"
    },
    {
        "Rank": 1417,
        "Port": "categoria",
        "Eng": "category"
    },
    {
        "Rank": 1418,
        "Port": "controle",
        "Eng": "control"
    },
    {
        "Rank": 1419,
        "Port": "comida",
        "Eng": "food"
    },
    {
        "Rank": 1420,
        "Port": "sombra",
        "Eng": "shadow"
    },
    {
        "Rank": 1420,
        "Port": "sombra",
        "Eng": "shade"
    },
    {
        "Rank": 1421,
        "Port": "limpar",
        "Eng": "clean"
    },
    {
        "Rank": 1422,
        "Port": "biblioteca",
        "Eng": "library"
    },
    {
        "Rank": 1423,
        "Port": "escuro",
        "Eng": "dark"
    },
    {
        "Rank": 1424,
        "Port": "disposto",
        "Eng": "willing"
    },
    {
        "Rank": 1424,
        "Port": "disposto",
        "Eng": "arranged"
    },
    {
        "Rank": 1425,
        "Port": "rural",
        "Eng": "rural"
    },
    {
        "Rank": 1426,
        "Port": "disciplina",
        "Eng": "discipline"
    },
    {
        "Rank": 1427,
        "Port": "retomar",
        "Eng": "resume"
    },
    {
        "Rank": 1427,
        "Port": "retomar",
        "Eng": "retake"
    },
    {
        "Rank": 1428,
        "Port": "neto",
        "Eng": "grandson"
    },
    {
        "Rank": 1428,
        "Port": "neto",
        "Eng": "grandchildren"
    },
    {
        "Rank": 1429,
        "Port": "justamente",
        "Eng": "exactly"
    },
    {
        "Rank": 1429,
        "Port": "justamente",
        "Eng": "actually"
    },
    {
        "Rank": 1429,
        "Port": "justamente",
        "Eng": "just"
    },
    {
        "Rank": 1430,
        "Port": "salário",
        "Eng": "salary"
    },
    {
        "Rank": 1430,
        "Port": "salário",
        "Eng": "wage"
    },
    {
        "Rank": 1431,
        "Port": "agricultura",
        "Eng": "agriculture"
    },
    {
        "Rank": 1431,
        "Port": "agricultura",
        "Eng": "farming"
    },
    {
        "Rank": 1432,
        "Port": "russo",
        "Eng": "Russian"
    },
    {
        "Rank": 1433,
        "Port": "geralmente",
        "Eng": "generally"
    },
    {
        "Rank": 1433,
        "Port": "geralmente",
        "Eng": "usually"
    },
    {
        "Rank": 1434,
        "Port": "declaração",
        "Eng": "declaration"
    },
    {
        "Rank": 1435,
        "Port": "dimensão",
        "Eng": "dimension"
    },
    {
        "Rank": 1436,
        "Port": "feira",
        "Eng": "fair"
    },
    {
        "Rank": 1436,
        "Port": "feira",
        "Eng": "open-air market"
    },
    {
        "Rank": 1437,
        "Port": "prender",
        "Eng": "apprehend"
    },
    {
        "Rank": 1437,
        "Port": "prender",
        "Eng": "catch"
    },
    {
        "Rank": 1437,
        "Port": "prender",
        "Eng": "fasten"
    },
    {
        "Rank": 1438,
        "Port": "fala",
        "Eng": "speech"
    },
    {
        "Rank": 1439,
        "Port": "correcto",
        "Eng": "correct"
    },
    {
        "Rank": 1440,
        "Port": "medicina",
        "Eng": "medicine"
    },
    {
        "Rank": 1441,
        "Port": "aliança",
        "Eng": "alliance"
    },
    {
        "Rank": 1441,
        "Port": "aliança",
        "Eng": "wedding band"
    },
    {
        "Rank": 1442,
        "Port": "faixa",
        "Eng": "strip"
    },
    {
        "Rank": 1442,
        "Port": "faixa",
        "Eng": "section"
    },
    {
        "Rank": 1442,
        "Port": "faixa",
        "Eng": "band"
    },
    {
        "Rank": 1443,
        "Port": "vazio",
        "Eng": "empty"
    },
    {
        "Rank": 1443,
        "Port": "vazio",
        "Eng": "emptiness"
    },
    {
        "Rank": 1444,
        "Port": "tradicional",
        "Eng": "traditional"
    },
    {
        "Rank": 1445,
        "Port": "infantil",
        "Eng": "childish"
    },
    {
        "Rank": 1445,
        "Port": "infantil",
        "Eng": "infantile"
    },
    {
        "Rank": 1446,
        "Port": "acaso",
        "Eng": "by chance"
    },
    {
        "Rank": 1447,
        "Port": "porto",
        "Eng": "port"
    },
    {
        "Rank": 1448,
        "Port": "sócio",
        "Eng": "partner"
    },
    {
        "Rank": 1448,
        "Port": "sócio",
        "Eng": "associate"
    },
    {
        "Rank": 1449,
        "Port": "assegurar",
        "Eng": "secure"
    },
    {
        "Rank": 1449,
        "Port": "assegurar",
        "Eng": "assure"
    },
    {
        "Rank": 1450,
        "Port": "código",
        "Eng": "code"
    },
    {
        "Rank": 1451,
        "Port": "igualmente",
        "Eng": "also"
    },
    {
        "Rank": 1451,
        "Port": "igualmente",
        "Eng": "equally"
    },
    {
        "Rank": 1451,
        "Port": "igualmente",
        "Eng": "likewise"
    },
    {
        "Rank": 1452,
        "Port": "misturar",
        "Eng": "mix"
    },
    {
        "Rank": 1453,
        "Port": "coluna",
        "Eng": "column"
    },
    {
        "Rank": 1453,
        "Port": "coluna",
        "Eng": "spinal column"
    },
    {
        "Rank": 1454,
        "Port": "versão",
        "Eng": "version"
    },
    {
        "Rank": 1455,
        "Port": "nordeste",
        "Eng": "northeast"
    },
    {
        "Rank": 1456,
        "Port": "canal",
        "Eng": "channel"
    },
    {
        "Rank": 1457,
        "Port": "doze",
        "Eng": "twelve"
    },
    {
        "Rank": 1458,
        "Port": "espalhar",
        "Eng": "spread"
    },
    {
        "Rank": 1459,
        "Port": "ameaçar",
        "Eng": "threaten"
    },
    {
        "Rank": 1460,
        "Port": "relativamente",
        "Eng": "relatively"
    },
    {
        "Rank": 1460,
        "Port": "relativamente",
        "Eng": "pertaining to"
    },
    {
        "Rank": 1461,
        "Port": "quarenta",
        "Eng": "forty"
    },
    {
        "Rank": 1462,
        "Port": "fiscal",
        "Eng": "fiscal"
    },
    {
        "Rank": 1462,
        "Port": "fiscal",
        "Eng": "customs inspector"
    },
    {
        "Rank": 1463,
        "Port": "operar",
        "Eng": "operate"
    },
    {
        "Rank": 1464,
        "Port": "década",
        "Eng": "decade"
    },
    {
        "Rank": 1465,
        "Port": "cinquenta",
        "Eng": "fifty"
    },
    {
        "Rank": 1466,
        "Port": "opor",
        "Eng": "oppose"
    },
    {
        "Rank": 1467,
        "Port": "fornecer",
        "Eng": "provide"
    },
    {
        "Rank": 1467,
        "Port": "fornecer",
        "Eng": "supply"
    },
    {
        "Rank": 1468,
        "Port": "floresta",
        "Eng": "forest"
    },
    {
        "Rank": 1469,
        "Port": "investimento",
        "Eng": "investment"
    },
    {
        "Rank": 1470,
        "Port": "paisagem",
        "Eng": "landscape"
    },
    {
        "Rank": 1470,
        "Port": "paisagem",
        "Eng": "view"
    },
    {
        "Rank": 1470,
        "Port": "paisagem",
        "Eng": "surroundings"
    },
    {
        "Rank": 1471,
        "Port": "alternativa",
        "Eng": "alternative"
    },
    {
        "Rank": 1472,
        "Port": "comentário",
        "Eng": "comment"
    },
    {
        "Rank": 1472,
        "Port": "comentário",
        "Eng": "commentary"
    },
    {
        "Rank": 1473,
        "Port": "resolução",
        "Eng": "resolution"
    },
    {
        "Rank": 1474,
        "Port": "caro",
        "Eng": "expensive"
    },
    {
        "Rank": 1474,
        "Port": "caro",
        "Eng": "esteemed"
    },
    {
        "Rank": 1475,
        "Port": "puxar",
        "Eng": "pull"
    },
    {
        "Rank": 1476,
        "Port": "recorrer",
        "Eng": "appeal"
    },
    {
        "Rank": 1476,
        "Port": "recorrer",
        "Eng": "resort to"
    },
    {
        "Rank": 1477,
        "Port": "bem",
        "Eng": "goods"
    },
    {
        "Rank": 1478,
        "Port": "alteração",
        "Eng": "change"
    },
    {
        "Rank": 1478,
        "Port": "alteração",
        "Eng": "alteration"
    },
    {
        "Rank": 1479,
        "Port": "fita",
        "Eng": "tape"
    },
    {
        "Rank": 1479,
        "Port": "fita",
        "Eng": "ribbon"
    },
    {
        "Rank": 1480,
        "Port": "necessitar",
        "Eng": "need"
    },
    {
        "Rank": 1481,
        "Port": "infância",
        "Eng": "childhood"
    },
    {
        "Rank": 1482,
        "Port": "prejudicar",
        "Eng": "harm"
    },
    {
        "Rank": 1482,
        "Port": "prejudicar",
        "Eng": "endanger"
    },
    {
        "Rank": 1483,
        "Port": "lua",
        "Eng": "moon"
    },
    {
        "Rank": 1484,
        "Port": "distrito",
        "Eng": "district"
    },
    {
        "Rank": 1485,
        "Port": "conceder",
        "Eng": "grant"
    },
    {
        "Rank": 1486,
        "Port": "caminhar",
        "Eng": "walk"
    },
    {
        "Rank": 1486,
        "Port": "caminhar",
        "Eng": "go on foot"
    },
    {
        "Rank": 1487,
        "Port": "vara",
        "Eng": "stick"
    },
    {
        "Rank": 1487,
        "Port": "vara",
        "Eng": "staff"
    },
    {
        "Rank": 1487,
        "Port": "vara",
        "Eng": "rod"
    },
    {
        "Rank": 1488,
        "Port": "executar",
        "Eng": "carry out"
    },
    {
        "Rank": 1488,
        "Port": "executar",
        "Eng": "execute"
    },
    {
        "Rank": 1489,
        "Port": "percorrer",
        "Eng": "cover"
    },
    {
        "Rank": 1489,
        "Port": "percorrer",
        "Eng": "run by"
    },
    {
        "Rank": 1489,
        "Port": "percorrer",
        "Eng": "through"
    },
    {
        "Rank": 1490,
        "Port": "organizado",
        "Eng": "organized"
    },
    {
        "Rank": 1491,
        "Port": "modificar",
        "Eng": "change"
    },
    {
        "Rank": 1491,
        "Port": "modificar",
        "Eng": "modify"
    },
    {
        "Rank": 1492,
        "Port": "agrícola",
        "Eng": "agricultural"
    },
    {
        "Rank": 1493,
        "Port": "adequado",
        "Eng": "adequate"
    },
    {
        "Rank": 1494,
        "Port": "investir",
        "Eng": "invest"
    },
    {
        "Rank": 1495,
        "Port": "satisfazer",
        "Eng": "satisfy"
    },
    {
        "Rank": 1496,
        "Port": "dente",
        "Eng": "tooth"
    },
    {
        "Rank": 1497,
        "Port": "intenso",
        "Eng": "intense"
    },
    {
        "Rank": 1498,
        "Port": "velocidade",
        "Eng": "speed"
    },
    {
        "Rank": 1498,
        "Port": "velocidade",
        "Eng": "velocity"
    },
    {
        "Rank": 1499,
        "Port": "bloco",
        "Eng": "block"
    },
    {
        "Rank": 1499,
        "Port": "bloco",
        "Eng": "bloc"
    },
    {
        "Rank": 1500,
        "Port": "representação",
        "Eng": "representation"
    },
    {
        "Rank": 1501,
        "Port": "mental",
        "Eng": "mental"
    },
    {
        "Rank": 1502,
        "Port": "riqueza",
        "Eng": "wealth"
    },
    {
        "Rank": 1502,
        "Port": "riqueza",
        "Eng": "riches"
    },
    {
        "Rank": 1503,
        "Port": "chegada",
        "Eng": "arrival"
    },
    {
        "Rank": 1504,
        "Port": "escapar",
        "Eng": "escape"
    },
    {
        "Rank": 1505,
        "Port": "absolutamente",
        "Eng": "absolutely"
    },
    {
        "Rank": 1506,
        "Port": "imóvel",
        "Eng": "real-estate"
    },
    {
        "Rank": 1506,
        "Port": "imóvel",
        "Eng": "immobile"
    },
    {
        "Rank": 1507,
        "Port": "província",
        "Eng": "province"
    },
    {
        "Rank": 1508,
        "Port": "habitante",
        "Eng": "inhabitant"
    },
    {
        "Rank": 1509,
        "Port": "alimento",
        "Eng": "food"
    },
    {
        "Rank": 1509,
        "Port": "alimento",
        "Eng": "nourishment"
    },
    {
        "Rank": 1510,
        "Port": "combater",
        "Eng": "fight"
    },
    {
        "Rank": 1510,
        "Port": "combater",
        "Eng": "combat"
    },
    {
        "Rank": 1511,
        "Port": "beber",
        "Eng": "drink"
    },
    {
        "Rank": 1512,
        "Port": "silêncio",
        "Eng": "silence"
    },
    {
        "Rank": 1513,
        "Port": "insistir",
        "Eng": "insist"
    },
    {
        "Rank": 1514,
        "Port": "japonês",
        "Eng": "Japanese"
    },
    {
        "Rank": 1515,
        "Port": "cobrar",
        "Eng": "collect"
    },
    {
        "Rank": 1515,
        "Port": "cobrar",
        "Eng": "charge"
    },
    {
        "Rank": 1516,
        "Port": "cabelo",
        "Eng": "hair"
    },
    {
        "Rank": 1517,
        "Port": "computador",
        "Eng": "computer"
    },
    {
        "Rank": 1518,
        "Port": "invadir",
        "Eng": "invade"
    },
    {
        "Rank": 1519,
        "Port": "convencer",
        "Eng": "convince"
    },
    {
        "Rank": 1520,
        "Port": "móvel",
        "Eng": "piece of furniture"
    },
    {
        "Rank": 1520,
        "Port": "móvel",
        "Eng": "mobile"
    },
    {
        "Rank": 1521,
        "Port": "equilíbrio",
        "Eng": "balance"
    },
    {
        "Rank": 1521,
        "Port": "equilíbrio",
        "Eng": "equilibrium"
    },
    {
        "Rank": 1522,
        "Port": "afectar",
        "Eng": "affect"
    },
    {
        "Rank": 1523,
        "Port": "virtude",
        "Eng": "virtue"
    },
    {
        "Rank": 1524,
        "Port": "revisão",
        "Eng": "revision"
    },
    {
        "Rank": 1525,
        "Port": "gabinete",
        "Eng": "office"
    },
    {
        "Rank": 1525,
        "Port": "gabinete",
        "Eng": "cabinet"
    },
    {
        "Rank": 1526,
        "Port": "democracia",
        "Eng": "democracy"
    },
    {
        "Rank": 1527,
        "Port": "norma",
        "Eng": "norm"
    },
    {
        "Rank": 1527,
        "Port": "norma",
        "Eng": "rule"
    },
    {
        "Rank": 1527,
        "Port": "norma",
        "Eng": "standard"
    },
    {
        "Rank": 1528,
        "Port": "experimentar",
        "Eng": "experiment"
    },
    {
        "Rank": 1528,
        "Port": "experimentar",
        "Eng": "try  out"
    },
    {
        "Rank": 1529,
        "Port": "edição",
        "Eng": "edition"
    },
    {
        "Rank": 1530,
        "Port": "distinguir",
        "Eng": "distinguish"
    },
    {
        "Rank": 1531,
        "Port": "falso",
        "Eng": "false"
    },
    {
        "Rank": 1532,
        "Port": "milho",
        "Eng": "corn"
    },
    {
        "Rank": 1533,
        "Port": "proibir",
        "Eng": "prohibit"
    },
    {
        "Rank": 1534,
        "Port": "exacto",
        "Eng": "exact"
    },
    {
        "Rank": 1535,
        "Port": "estreito",
        "Eng": "straight"
    },
    {
        "Rank": 1535,
        "Port": "estreito",
        "Eng": "narrow"
    },
    {
        "Rank": 1535,
        "Port": "estreito",
        "Eng": "strait"
    },
    {
        "Rank": 1536,
        "Port": "criticar",
        "Eng": "criticize"
    },
    {
        "Rank": 1537,
        "Port": "governar",
        "Eng": "govern"
    },
    {
        "Rank": 1537,
        "Port": "governar",
        "Eng": "rule"
    },
    {
        "Rank": 1538,
        "Port": "corrida",
        "Eng": "race"
    },
    {
        "Rank": 1539,
        "Port": "resistir",
        "Eng": "resist"
    },
    {
        "Rank": 1540,
        "Port": "regional",
        "Eng": "regional"
    },
    {
        "Rank": 1541,
        "Port": "inspirar",
        "Eng": "inspire"
    },
    {
        "Rank": 1542,
        "Port": "curioso",
        "Eng": "curious"
    },
    {
        "Rank": 1542,
        "Port": "curioso",
        "Eng": "strange"
    },
    {
        "Rank": 1543,
        "Port": "acumular",
        "Eng": "accumulate"
    },
    {
        "Rank": 1544,
        "Port": "doméstico",
        "Eng": "domestic"
    },
    {
        "Rank": 1545,
        "Port": "socialista",
        "Eng": "socialist"
    },
    {
        "Rank": 1546,
        "Port": "costume",
        "Eng": "custom"
    },
    {
        "Rank": 1547,
        "Port": "símbolo",
        "Eng": "symbol"
    },
    {
        "Rank": 1548,
        "Port": "deslocar",
        "Eng": "move"
    },
    {
        "Rank": 1548,
        "Port": "deslocar",
        "Eng": "dislocate"
    },
    {
        "Rank": 1549,
        "Port": "poderoso",
        "Eng": "powerful"
    },
    {
        "Rank": 1550,
        "Port": "reino",
        "Eng": "kingdom"
    },
    {
        "Rank": 1551,
        "Port": "medir",
        "Eng": "measure"
    },
    {
        "Rank": 1552,
        "Port": "relatório",
        "Eng": "report"
    },
    {
        "Rank": 1553,
        "Port": "noção",
        "Eng": "notion"
    },
    {
        "Rank": 1554,
        "Port": "droga",
        "Eng": "drug"
    },
    {
        "Rank": 1555,
        "Port": "mata",
        "Eng": "jungle"
    },
    {
        "Rank": 1555,
        "Port": "mata",
        "Eng": "woods"
    },
    {
        "Rank": 1555,
        "Port": "mata",
        "Eng": "forest"
    },
    {
        "Rank": 1556,
        "Port": "círculo",
        "Eng": "circle"
    },
    {
        "Rank": 1557,
        "Port": "observação",
        "Eng": "observation"
    },
    {
        "Rank": 1558,
        "Port": "reclamar",
        "Eng": "complain"
    },
    {
        "Rank": 1559,
        "Port": "convite",
        "Eng": "invitation"
    },
    {
        "Rank": 1560,
        "Port": "definitivo",
        "Eng": "definitive"
    },
    {
        "Rank": 1561,
        "Port": "metal",
        "Eng": "metal"
    },
    {
        "Rank": 1562,
        "Port": "buraco",
        "Eng": "hole"
    },
    {
        "Rank": 1563,
        "Port": "vidro",
        "Eng": "glass"
    },
    {
        "Rank": 1564,
        "Port": "isolado",
        "Eng": "isolated"
    },
    {
        "Rank": 1565,
        "Port": "sal",
        "Eng": "salt"
    },
    {
        "Rank": 1566,
        "Port": "supremo",
        "Eng": "supreme"
    },
    {
        "Rank": 1567,
        "Port": "concreto",
        "Eng": "concrete"
    },
    {
        "Rank": 1568,
        "Port": "individual",
        "Eng": "individual"
    },
    {
        "Rank": 1569,
        "Port": "umgehen",
        "Eng": "harvest"
    },
    {
        "Rank": 1569,
        "Port": "umgehen",
        "Eng": "gather"
    },
    {
        "Rank": 1570,
        "Port": "distante",
        "Eng": "distant"
    },
    {
        "Rank": 1571,
        "Port": "estimar",
        "Eng": "estimate"
    },
    {
        "Rank": 1571,
        "Port": "estimar",
        "Eng": "esteem"
    },
    {
        "Rank": 1572,
        "Port": "acordar",
        "Eng": "wake up"
    },
    {
        "Rank": 1573,
        "Port": "raio",
        "Eng": "ray"
    },
    {
        "Rank": 1574,
        "Port": "suportar",
        "Eng": "endure"
    },
    {
        "Rank": 1574,
        "Port": "suportar",
        "Eng": "support"
    },
    {
        "Rank": 1574,
        "Port": "suportar",
        "Eng": "bear"
    },
    {
        "Rank": 1575,
        "Port": "honra",
        "Eng": "honor"
    },
    {
        "Rank": 1576,
        "Port": "ameaça",
        "Eng": "threat"
    },
    {
        "Rank": 1576,
        "Port": "ameaça",
        "Eng": "threatening"
    },
    {
        "Rank": 1577,
        "Port": "sindicato",
        "Eng": "workers’ union"
    },
    {
        "Rank": 1577,
        "Port": "sindicato",
        "Eng": "syndicate"
    },
    {
        "Rank": 1578,
        "Port": "encher",
        "Eng": "fill"
    },
    {
        "Rank": 1579,
        "Port": "herói",
        "Eng": "hero"
    },
    {
        "Rank": 1580,
        "Port": "precisamente",
        "Eng": "precisely"
    },
    {
        "Rank": 1581,
        "Port": "específico",
        "Eng": "specific"
    },
    {
        "Rank": 1582,
        "Port": "pesado",
        "Eng": "heavy"
    },
    {
        "Rank": 1583,
        "Port": "extremamente",
        "Eng": "extremely"
    },
    {
        "Rank": 1584,
        "Port": "aventura",
        "Eng": "adventure"
    },
    {
        "Rank": 1585,
        "Port": "lógica",
        "Eng": "logic"
    },
    {
        "Rank": 1586,
        "Port": "parecer",
        "Eng": "opinion"
    },
    {
        "Rank": 1586,
        "Port": "parecer",
        "Eng": "appearance"
    },
    {
        "Rank": 1587,
        "Port": "assistência",
        "Eng": "assistance"
    },
    {
        "Rank": 1588,
        "Port": "justo",
        "Eng": "just"
    },
    {
        "Rank": 1588,
        "Port": "justo",
        "Eng": "fair"
    },
    {
        "Rank": 1589,
        "Port": "montanha",
        "Eng": "mountain"
    },
    {
        "Rank": 1590,
        "Port": "regresso",
        "Eng": "return"
    },
    {
        "Rank": 1591,
        "Port": "humanidade",
        "Eng": "humanity"
    },
    {
        "Rank": 1592,
        "Port": "apresentação",
        "Eng": "presentation"
    },
    {
        "Rank": 1593,
        "Port": "academia",
        "Eng": "academy"
    },
    {
        "Rank": 1593,
        "Port": "academia",
        "Eng": "gym"
    },
    {
        "Rank": 1594,
        "Port": "interessado",
        "Eng": "interested"
    },
    {
        "Rank": 1595,
        "Port": "meter",
        "Eng": "put into"
    },
    {
        "Rank": 1595,
        "Port": "meter",
        "Eng": "get involved"
    },
    {
        "Rank": 1596,
        "Port": "ai",
        "Eng": "oh"
    },
    {
        "Rank": 1596,
        "Port": "ai",
        "Eng": "ouch"
    },
    {
        "Rank": 1596,
        "Port": "ai",
        "Eng": "ow"
    },
    {
        "Rank": 1597,
        "Port": "universal",
        "Eng": "universal"
    },
    {
        "Rank": 1598,
        "Port": "inteligência",
        "Eng": "intelligence"
    },
    {
        "Rank": 1599,
        "Port": "recordar",
        "Eng": "recall"
    },
    {
        "Rank": 1599,
        "Port": "recordar",
        "Eng": "remember"
    },
    {
        "Rank": 1600,
        "Port": "caça",
        "Eng": "hunt"
    },
    {
        "Rank": 1601,
        "Port": "capítulo",
        "Eng": "chapter"
    },
    {
        "Rank": 1602,
        "Port": "frequentar",
        "Eng": "attend"
    },
    {
        "Rank": 1603,
        "Port": "prosseguir",
        "Eng": "proceed"
    },
    {
        "Rank": 1604,
        "Port": "choque",
        "Eng": "shock"
    },
    {
        "Rank": 1605,
        "Port": "mulato",
        "Eng": "mulatto"
    },
    {
        "Rank": 1606,
        "Port": "escrita",
        "Eng": "writing"
    },
    {
        "Rank": 1607,
        "Port": "adulto",
        "Eng": "adult"
    },
    {
        "Rank": 1608,
        "Port": "tender",
        "Eng": "tend to"
    },
    {
        "Rank": 1609,
        "Port": "gestão",
        "Eng": "management"
    },
    {
        "Rank": 1609,
        "Port": "gestão",
        "Eng": "administration"
    },
    {
        "Rank": 1610,
        "Port": "rever",
        "Eng": "see again"
    },
    {
        "Rank": 1610,
        "Port": "rever",
        "Eng": "look over"
    },
    {
        "Rank": 1610,
        "Port": "rever",
        "Eng": "examine"
    },
    {
        "Rank": 1611,
        "Port": "serra",
        "Eng": "mountain range"
    },
    {
        "Rank": 1611,
        "Port": "serra",
        "Eng": "saw"
    },
    {
        "Rank": 1612,
        "Port": "forno",
        "Eng": "oven"
    },
    {
        "Rank": 1613,
        "Port": "médio",
        "Eng": "middle"
    },
    {
        "Rank": 1613,
        "Port": "médio",
        "Eng": "average"
    },
    {
        "Rank": 1614,
        "Port": "instalação",
        "Eng": "installation"
    },
    {
        "Rank": 1615,
        "Port": "democrático",
        "Eng": "democratic"
    },
    {
        "Rank": 1616,
        "Port": "proprietário",
        "Eng": "owner"
    },
    {
        "Rank": 1616,
        "Port": "proprietário",
        "Eng": "proprietor"
    },
    {
        "Rank": 1617,
        "Port": "orçamento",
        "Eng": "budget"
    },
    {
        "Rank": 1617,
        "Port": "orçamento",
        "Eng": "financing"
    },
    {
        "Rank": 1618,
        "Port": "temer",
        "Eng": "fear"
    },
    {
        "Rank": 1619,
        "Port": "debate",
        "Eng": "debate"
    },
    {
        "Rank": 1619,
        "Port": "debate",
        "Eng": "discussion"
    },
    {
        "Rank": 1620,
        "Port": "implicar",
        "Eng": "involve"
    },
    {
        "Rank": 1620,
        "Port": "implicar",
        "Eng": "imply"
    },
    {
        "Rank": 1621,
        "Port": "facilitar",
        "Eng": "facilitate"
    },
    {
        "Rank": 1621,
        "Port": "facilitar",
        "Eng": "ease"
    },
    {
        "Rank": 1622,
        "Port": "realização",
        "Eng": "accomplishment"
    },
    {
        "Rank": 1622,
        "Port": "realização",
        "Eng": "fulfillment"
    },
    {
        "Rank": 1623,
        "Port": "disponível",
        "Eng": "available"
    },
    {
        "Rank": 1624,
        "Port": "repousar",
        "Eng": "rest"
    },
    {
        "Rank": 1625,
        "Port": "continente",
        "Eng": "continent"
    },
    {
        "Rank": 1626,
        "Port": "líquido",
        "Eng": "liquid"
    },
    {
        "Rank": 1627,
        "Port": "teste",
        "Eng": "test"
    },
    {
        "Rank": 1627,
        "Port": "teste",
        "Eng": "exam"
    },
    {
        "Rank": 1628,
        "Port": "junta",
        "Eng": "council"
    },
    {
        "Rank": 1628,
        "Port": "junta",
        "Eng": "commission"
    },
    {
        "Rank": 1629,
        "Port": "talento",
        "Eng": "talent"
    },
    {
        "Rank": 1630,
        "Port": "dona",
        "Eng": "Mrs"
    },
    {
        "Rank": 1630,
        "Port": "dona",
        "Eng": "madam"
    },
    {
        "Rank": 1630,
        "Port": "dona",
        "Eng": "owner"
    },
    {
        "Rank": 1631,
        "Port": "variar",
        "Eng": "vary"
    },
    {
        "Rank": 1632,
        "Port": "contemporâneo",
        "Eng": "contemporary"
    },
    {
        "Rank": 1633,
        "Port": "secção",
        "Eng": "department"
    },
    {
        "Rank": 1633,
        "Port": "secção",
        "Eng": "section"
    },
    {
        "Rank": 1634,
        "Port": "escritório",
        "Eng": "office"
    },
    {
        "Rank": 1635,
        "Port": "barato",
        "Eng": "cheap"
    },
    {
        "Rank": 1635,
        "Port": "barato",
        "Eng": "inexpensive"
    },
    {
        "Rank": 1636,
        "Port": "atirar",
        "Eng": "shoot"
    },
    {
        "Rank": 1636,
        "Port": "atirar",
        "Eng": "throw"
    },
    {
        "Rank": 1637,
        "Port": "relativo",
        "Eng": "relative"
    },
    {
        "Rank": 1638,
        "Port": "pedaço",
        "Eng": "chunk"
    },
    {
        "Rank": 1638,
        "Port": "pedaço",
        "Eng": "piece"
    },
    {
        "Rank": 1639,
        "Port": "presidência",
        "Eng": "presidency"
    },
    {
        "Rank": 1640,
        "Port": "piano",
        "Eng": "piano"
    },
    {
        "Rank": 1641,
        "Port": "negociar",
        "Eng": "negotiate"
    },
    {
        "Rank": 1642,
        "Port": "vago",
        "Eng": "vague"
    },
    {
        "Rank": 1642,
        "Port": "vago",
        "Eng": "vacant"
    },
    {
        "Rank": 1643,
        "Port": "tratado",
        "Eng": "treaty"
    },
    {
        "Rank": 1644,
        "Port": "superfície",
        "Eng": "surface"
    },
    {
        "Rank": 1645,
        "Port": "derrubar",
        "Eng": "overthrow"
    },
    {
        "Rank": 1645,
        "Port": "derrubar",
        "Eng": "demolish"
    },
    {
        "Rank": 1646,
        "Port": "padrão",
        "Eng": "standard"
    },
    {
        "Rank": 1646,
        "Port": "padrão",
        "Eng": "pattern"
    },
    {
        "Rank": 1647,
        "Port": "operário",
        "Eng": "worker"
    },
    {
        "Rank": 1647,
        "Port": "operário",
        "Eng": "laborer"
    },
    {
        "Rank": 1647,
        "Port": "operário",
        "Eng": "operator"
    },
    {
        "Rank": 1648,
        "Port": "fórmula",
        "Eng": "formula"
    },
    {
        "Rank": 1649,
        "Port": "transportar",
        "Eng": "carry"
    },
    {
        "Rank": 1649,
        "Port": "transportar",
        "Eng": "transport"
    },
    {
        "Rank": 1650,
        "Port": "esclarecer",
        "Eng": "clear up"
    },
    {
        "Rank": 1650,
        "Port": "esclarecer",
        "Eng": "clarify"
    },
    {
        "Rank": 1651,
        "Port": "distribuição",
        "Eng": "distribution"
    },
    {
        "Rank": 1652,
        "Port": "lento",
        "Eng": "slow"
    },
    {
        "Rank": 1653,
        "Port": "concurso",
        "Eng": "contest"
    },
    {
        "Rank": 1654,
        "Port": "sobreviver",
        "Eng": "survive"
    },
    {
        "Rank": 1655,
        "Port": "freguesia",
        "Eng": "municipality"
    },
    {
        "Rank": 1655,
        "Port": "freguesia",
        "Eng": "clientele"
    },
    {
        "Rank": 1656,
        "Port": "féria",
        "Eng": "vacation"
    },
    {
        "Rank": 1656,
        "Port": "féria",
        "Eng": "holidays"
    },
    {
        "Rank": 1657,
        "Port": "arranjar",
        "Eng": "arrange"
    },
    {
        "Rank": 1657,
        "Port": "arranjar",
        "Eng": "obtain"
    },
    {
        "Rank": 1658,
        "Port": "nervoso",
        "Eng": "nervous"
    },
    {
        "Rank": 1659,
        "Port": "surpresa",
        "Eng": "surprise"
    },
    {
        "Rank": 1660,
        "Port": "introduzir",
        "Eng": "introduce"
    },
    {
        "Rank": 1661,
        "Port": "selecção",
        "Eng": "selection"
    },
    {
        "Rank": 1661,
        "Port": "selecção",
        "Eng": "team of selected players"
    },
    {
        "Rank": 1662,
        "Port": "progresso",
        "Eng": "progress"
    },
    {
        "Rank": 1663,
        "Port": "tiro",
        "Eng": "shot"
    },
    {
        "Rank": 1664,
        "Port": "exigência",
        "Eng": "demand"
    },
    {
        "Rank": 1664,
        "Port": "exigência",
        "Eng": "requirement"
    },
    {
        "Rank": 1665,
        "Port": "opção",
        "Eng": "option"
    },
    {
        "Rank": 1666,
        "Port": "vasto",
        "Eng": "wide"
    },
    {
        "Rank": 1666,
        "Port": "vasto",
        "Eng": "vast"
    },
    {
        "Rank": 1667,
        "Port": "promessa",
        "Eng": "promise"
    },
    {
        "Rank": 1668,
        "Port": "laboratório",
        "Eng": "laboratory"
    },
    {
        "Rank": 1669,
        "Port": "benefício",
        "Eng": "benefit"
    },
    {
        "Rank": 1670,
        "Port": "nascimento",
        "Eng": "birth"
    },
    {
        "Rank": 1671,
        "Port": "básico",
        "Eng": "basic"
    },
    {
        "Rank": 1672,
        "Port": "semente",
        "Eng": "seed"
    },
    {
        "Rank": 1673,
        "Port": "osso",
        "Eng": "bone"
    },
    {
        "Rank": 1674,
        "Port": "frequência",
        "Eng": "frequency"
    },
    {
        "Rank": 1674,
        "Port": "frequência",
        "Eng": "rate"
    },
    {
        "Rank": 1675,
        "Port": "conquista",
        "Eng": "conquest"
    },
    {
        "Rank": 1676,
        "Port": "arquitectura",
        "Eng": "architecture"
    },
    {
        "Rank": 1677,
        "Port": "encaminhar",
        "Eng": "direct"
    },
    {
        "Rank": 1677,
        "Port": "encaminhar",
        "Eng": "put on the right path"
    },
    {
        "Rank": 1678,
        "Port": "fiel",
        "Eng": "faithful"
    },
    {
        "Rank": 1679,
        "Port": "seguida",
        "Eng": "afterwards"
    },
    {
        "Rank": 1679,
        "Port": "seguida",
        "Eng": "then"
    },
    {
        "Rank": 1680,
        "Port": "escudo",
        "Eng": "shield"
    },
    {
        "Rank": 1680,
        "Port": "escudo",
        "Eng": "old Portuguese coin"
    },
    {
        "Rank": 1681,
        "Port": "proceder",
        "Eng": "proceed"
    },
    {
        "Rank": 1682,
        "Port": "complicado",
        "Eng": "complicated"
    },
    {
        "Rank": 1683,
        "Port": "quadrado",
        "Eng": "square"
    },
    {
        "Rank": 1683,
        "Port": "quadrado",
        "Eng": "squared"
    },
    {
        "Rank": 1684,
        "Port": "condenar",
        "Eng": "condemn"
    },
    {
        "Rank": 1685,
        "Port": "património",
        "Eng": "estate"
    },
    {
        "Rank": 1685,
        "Port": "património",
        "Eng": "heritage"
    },
    {
        "Rank": 1685,
        "Port": "património",
        "Eng": "patrimony"
    },
    {
        "Rank": 1686,
        "Port": "destinado",
        "Eng": "meant for"
    },
    {
        "Rank": 1686,
        "Port": "destinado",
        "Eng": "destined"
    },
    {
        "Rank": 1687,
        "Port": "visar",
        "Eng": "aim at"
    },
    {
        "Rank": 1687,
        "Port": "visar",
        "Eng": "have in sight"
    },
    {
        "Rank": 1687,
        "Port": "visar",
        "Eng": "drive at"
    },
    {
        "Rank": 1688,
        "Port": "transformação",
        "Eng": "transformation"
    },
    {
        "Rank": 1689,
        "Port": "interpretação",
        "Eng": "interpretation"
    },
    {
        "Rank": 1690,
        "Port": "veículo",
        "Eng": "vehicle"
    },
    {
        "Rank": 1691,
        "Port": "gato",
        "Eng": "cat"
    },
    {
        "Rank": 1692,
        "Port": "aéreo",
        "Eng": "by air"
    },
    {
        "Rank": 1692,
        "Port": "aéreo",
        "Eng": "aerial"
    },
    {
        "Rank": 1693,
        "Port": "ontem",
        "Eng": "yesterday"
    },
    {
        "Rank": 1694,
        "Port": "conferência",
        "Eng": "conference"
    },
    {
        "Rank": 1695,
        "Port": "criado",
        "Eng": "servant"
    },
    {
        "Rank": 1696,
        "Port": "tecnologia",
        "Eng": "technology"
    },
    {
        "Rank": 1697,
        "Port": "adaptar",
        "Eng": "adapt"
    },
    {
        "Rank": 1698,
        "Port": "barra",
        "Eng": "stripe"
    },
    {
        "Rank": 1698,
        "Port": "barra",
        "Eng": "bar"
    },
    {
        "Rank": 1699,
        "Port": "hotel",
        "Eng": "hotel"
    },
    {
        "Rank": 1700,
        "Port": "mistura",
        "Eng": "mixture"
    },
    {
        "Rank": 1700,
        "Port": "mistura",
        "Eng": "mix"
    },
    {
        "Rank": 1701,
        "Port": "obrigado",
        "Eng": "thank you"
    },
    {
        "Rank": 1701,
        "Port": "obrigado",
        "Eng": "obligated"
    },
    {
        "Rank": 1702,
        "Port": "teu",
        "Eng": "your"
    },
    {
        "Rank": 1702,
        "Port": "teu",
        "Eng": "yours"
    },
    {
        "Rank": 1702,
        "Port": "teu",
        "Eng": "thy"
    },
    {
        "Rank": 1702,
        "Port": "teu",
        "Eng": "thine"
    },
    {
        "Rank": 1703,
        "Port": "copa",
        "Eng": "cup"
    },
    {
        "Rank": 1703,
        "Port": "copa",
        "Eng": "tree top"
    },
    {
        "Rank": 1704,
        "Port": "romper",
        "Eng": "tear"
    },
    {
        "Rank": 1704,
        "Port": "romper",
        "Eng": "rip"
    },
    {
        "Rank": 1704,
        "Port": "romper",
        "Eng": "begin to"
    },
    {
        "Rank": 1705,
        "Port": "comandar",
        "Eng": "command"
    },
    {
        "Rank": 1705,
        "Port": "comandar",
        "Eng": "lead"
    },
    {
        "Rank": 1706,
        "Port": "respectivo",
        "Eng": "respective"
    },
    {
        "Rank": 1707,
        "Port": "avaliação",
        "Eng": "assessment"
    },
    {
        "Rank": 1708,
        "Port": "dólar",
        "Eng": "dollar"
    },
    {
        "Rank": 1709,
        "Port": "identidade",
        "Eng": "identity"
    },
    {
        "Rank": 1710,
        "Port": "salto",
        "Eng": "leap"
    },
    {
        "Rank": 1710,
        "Port": "salto",
        "Eng": "jump"
    },
    {
        "Rank": 1711,
        "Port": "centena",
        "Eng": "a hundred"
    },
    {
        "Rank": 1712,
        "Port": "interromper",
        "Eng": "interrupt"
    },
    {
        "Rank": 1713,
        "Port": "músico",
        "Eng": "musician"
    },
    {
        "Rank": 1713,
        "Port": "músico",
        "Eng": "musical"
    },
    {
        "Rank": 1714,
        "Port": "capa",
        "Eng": "cover"
    },
    {
        "Rank": 1714,
        "Port": "capa",
        "Eng": "cape"
    },
    {
        "Rank": 1714,
        "Port": "capa",
        "Eng": "cloak"
    },
    {
        "Rank": 1715,
        "Port": "roubar",
        "Eng": "steal"
    },
    {
        "Rank": 1715,
        "Port": "roubar",
        "Eng": "rob"
    },
    {
        "Rank": 1716,
        "Port": "paciente",
        "Eng": "patient"
    },
    {
        "Rank": 1717,
        "Port": "jurídico",
        "Eng": "judicial"
    },
    {
        "Rank": 1718,
        "Port": "adversário",
        "Eng": "opponent"
    },
    {
        "Rank": 1718,
        "Port": "adversário",
        "Eng": "adversary"
    },
    {
        "Rank": 1719,
        "Port": "prejuízo",
        "Eng": "damage"
    },
    {
        "Rank": 1719,
        "Port": "prejuízo",
        "Eng": "loss"
    },
    {
        "Rank": 1720,
        "Port": "capela",
        "Eng": "chapel"
    },
    {
        "Rank": 1721,
        "Port": "grego",
        "Eng": "Greek"
    },
    {
        "Rank": 1722,
        "Port": "episódio",
        "Eng": "episode"
    },
    {
        "Rank": 1723,
        "Port": "composição",
        "Eng": "composition"
    },
    {
        "Rank": 1724,
        "Port": "adiantar",
        "Eng": "put forth"
    },
    {
        "Rank": 1724,
        "Port": "adiantar",
        "Eng": "move forward"
    },
    {
        "Rank": 1725,
        "Port": "ciclo",
        "Eng": "cycle"
    },
    {
        "Rank": 1726,
        "Port": "existente",
        "Eng": "existing"
    },
    {
        "Rank": 1727,
        "Port": "excesso",
        "Eng": "excess"
    },
    {
        "Rank": 1728,
        "Port": "tecto",
        "Eng": "ceiling"
    },
    {
        "Rank": 1729,
        "Port": "rosto",
        "Eng": "face"
    },
    {
        "Rank": 1730,
        "Port": "revolucionário",
        "Eng": "revolutionary"
    },
    {
        "Rank": 1731,
        "Port": "lobo",
        "Eng": "wolf"
    },
    {
        "Rank": 1732,
        "Port": "açúcar",
        "Eng": "sugar"
    },
    {
        "Rank": 1733,
        "Port": "denunciar",
        "Eng": "denounce"
    },
    {
        "Rank": 1734,
        "Port": "associado",
        "Eng": "associated"
    },
    {
        "Rank": 1734,
        "Port": "associado",
        "Eng": "associate"
    },
    {
        "Rank": 1735,
        "Port": "organismo",
        "Eng": "organism"
    },
    {
        "Rank": 1735,
        "Port": "organismo",
        "Eng": "organization"
    },
    {
        "Rank": 1736,
        "Port": "rendimento",
        "Eng": "profit"
    },
    {
        "Rank": 1736,
        "Port": "rendimento",
        "Eng": "revenue"
    },
    {
        "Rank": 1737,
        "Port": "rumo",
        "Eng": "course of action"
    },
    {
        "Rank": 1737,
        "Port": "rumo",
        "Eng": "way"
    },
    {
        "Rank": 1737,
        "Port": "rumo",
        "Eng": "path"
    },
    {
        "Rank": 1738,
        "Port": "encerrar",
        "Eng": "close"
    },
    {
        "Rank": 1738,
        "Port": "encerrar",
        "Eng": "end"
    },
    {
        "Rank": 1739,
        "Port": "concerto",
        "Eng": "concert"
    },
    {
        "Rank": 1740,
        "Port": "electrónico",
        "Eng": "electronic"
    },
    {
        "Rank": 1741,
        "Port": "mostra",
        "Eng": "sampling"
    },
    {
        "Rank": 1741,
        "Port": "mostra",
        "Eng": "exhibition"
    },
    {
        "Rank": 1741,
        "Port": "mostra",
        "Eng": "display"
    },
    {
        "Rank": 1742,
        "Port": "trecho",
        "Eng": "excerpt"
    },
    {
        "Rank": 1742,
        "Port": "trecho",
        "Eng": "passage"
    },
    {
        "Rank": 1743,
        "Port": "intervir",
        "Eng": "intervene"
    },
    {
        "Rank": 1744,
        "Port": "ferir",
        "Eng": "wound"
    },
    {
        "Rank": 1744,
        "Port": "ferir",
        "Eng": "hurt"
    },
    {
        "Rank": 1745,
        "Port": "agência",
        "Eng": "agency"
    },
    {
        "Rank": 1746,
        "Port": "latino",
        "Eng": "relating to Latin America"
    },
    {
        "Rank": 1746,
        "Port": "latino",
        "Eng": "Latin"
    },
    {
        "Rank": 1747,
        "Port": "placa",
        "Eng": "plate"
    },
    {
        "Rank": 1747,
        "Port": "placa",
        "Eng": "plaque"
    },
    {
        "Rank": 1747,
        "Port": "placa",
        "Eng": "sign"
    },
    {
        "Rank": 1748,
        "Port": "mandato",
        "Eng": "mandate"
    },
    {
        "Rank": 1749,
        "Port": "praticamente",
        "Eng": "practically"
    },
    {
        "Rank": 1750,
        "Port": "estabelecimento",
        "Eng": "establishment"
    },
    {
        "Rank": 1751,
        "Port": "ignorar",
        "Eng": "ignore"
    },
    {
        "Rank": 1752,
        "Port": "brilhante",
        "Eng": "bright"
    },
    {
        "Rank": 1752,
        "Port": "brilhante",
        "Eng": "brilliant"
    },
    {
        "Rank": 1753,
        "Port": "ser",
        "Eng": "being"
    },
    {
        "Rank": 1754,
        "Port": "saco",
        "Eng": "bag"
    },
    {
        "Rank": 1754,
        "Port": "saco",
        "Eng": "sack"
    },
    {
        "Rank": 1754,
        "Port": "saco",
        "Eng": "pouch"
    },
    {
        "Rank": 1755,
        "Port": "departamento",
        "Eng": "department"
    },
    {
        "Rank": 1756,
        "Port": "amizade",
        "Eng": "friendship"
    },
    {
        "Rank": 1757,
        "Port": "perfil",
        "Eng": "profile"
    },
    {
        "Rank": 1758,
        "Port": "porco",
        "Eng": "pig"
    },
    {
        "Rank": 1758,
        "Port": "porco",
        "Eng": "pork"
    },
    {
        "Rank": 1759,
        "Port": "assentar",
        "Eng": "settle"
    },
    {
        "Rank": 1759,
        "Port": "assentar",
        "Eng": "rest on"
    },
    {
        "Rank": 1760,
        "Port": "produtor",
        "Eng": "producing"
    },
    {
        "Rank": 1760,
        "Port": "produtor",
        "Eng": "producer"
    },
    {
        "Rank": 1761,
        "Port": "retrato",
        "Eng": "picture"
    },
    {
        "Rank": 1761,
        "Port": "retrato",
        "Eng": "portrait"
    },
    {
        "Rank": 1762,
        "Port": "bruto",
        "Eng": "rude"
    },
    {
        "Rank": 1762,
        "Port": "bruto",
        "Eng": "gross"
    },
    {
        "Rank": 1763,
        "Port": "queimar",
        "Eng": "burn"
    },
    {
        "Rank": 1764,
        "Port": "globo",
        "Eng": "globe"
    },
    {
        "Rank": 1765,
        "Port": "bispo",
        "Eng": "bishop"
    },
    {
        "Rank": 1766,
        "Port": "mente",
        "Eng": "mind"
    },
    {
        "Rank": 1767,
        "Port": "parlamentar",
        "Eng": "parliamentary"
    },
    {
        "Rank": 1768,
        "Port": "significativo",
        "Eng": "significant"
    },
    {
        "Rank": 1768,
        "Port": "significativo",
        "Eng": "meaningful"
    },
    {
        "Rank": 1769,
        "Port": "falha",
        "Eng": "flaw"
    },
    {
        "Rank": 1769,
        "Port": "falha",
        "Eng": "fault"
    },
    {
        "Rank": 1769,
        "Port": "falha",
        "Eng": "failure"
    },
    {
        "Rank": 1770,
        "Port": "marcha",
        "Eng": "march"
    },
    {
        "Rank": 1770,
        "Port": "marcha",
        "Eng": "long walk"
    },
    {
        "Rank": 1770,
        "Port": "marcha",
        "Eng": "parade"
    },
    {
        "Rank": 1771,
        "Port": "horário",
        "Eng": "business hours"
    },
    {
        "Rank": 1771,
        "Port": "horário",
        "Eng": "schedule"
    },
    {
        "Rank": 1771,
        "Port": "horário",
        "Eng": "hours"
    },
    {
        "Rank": 1772,
        "Port": "extensão",
        "Eng": "extension"
    },
    {
        "Rank": 1772,
        "Port": "extensão",
        "Eng": "extent"
    },
    {
        "Rank": 1773,
        "Port": "vaca",
        "Eng": "cow"
    },
    {
        "Rank": 1774,
        "Port": "paulista",
        "Eng": "from São Paulo"
    },
    {
        "Rank": 1775,
        "Port": "foto",
        "Eng": "photo"
    },
    {
        "Rank": 1776,
        "Port": "aplicação",
        "Eng": "application"
    },
    {
        "Rank": 1777,
        "Port": "rato",
        "Eng": "mouse"
    },
    {
        "Rank": 1778,
        "Port": "conteúdo",
        "Eng": "content"
    },
    {
        "Rank": 1779,
        "Port": "inúmero",
        "Eng": "innumerable"
    },
    {
        "Rank": 1780,
        "Port": "constitucional",
        "Eng": "constitutional"
    },
    {
        "Rank": 1781,
        "Port": "escala",
        "Eng": "scale"
    },
    {
        "Rank": 1782,
        "Port": "negociação",
        "Eng": "negotiation"
    },
    {
        "Rank": 1783,
        "Port": "criador",
        "Eng": "creator"
    },
    {
        "Rank": 1784,
        "Port": "alegria",
        "Eng": "joy"
    },
    {
        "Rank": 1784,
        "Port": "alegria",
        "Eng": "happiness"
    },
    {
        "Rank": 1785,
        "Port": "depósito",
        "Eng": "deposit"
    },
    {
        "Rank": 1785,
        "Port": "depósito",
        "Eng": "safe"
    },
    {
        "Rank": 1785,
        "Port": "depósito",
        "Eng": "warehouse"
    },
    {
        "Rank": 1786,
        "Port": "instrução",
        "Eng": "formal schooling"
    },
    {
        "Rank": 1786,
        "Port": "instrução",
        "Eng": "instruction"
    },
    {
        "Rank": 1787,
        "Port": "comprido",
        "Eng": "long"
    },
    {
        "Rank": 1788,
        "Port": "equipa",
        "Eng": "team"
    },
    {
        "Rank": 1789,
        "Port": "dançar",
        "Eng": "dance"
    },
    {
        "Rank": 1790,
        "Port": "conservar",
        "Eng": "keep"
    },
    {
        "Rank": 1790,
        "Port": "conservar",
        "Eng": "conserve"
    },
    {
        "Rank": 1791,
        "Port": "tela",
        "Eng": "screen"
    },
    {
        "Rank": 1791,
        "Port": "tela",
        "Eng": "painting canvas"
    },
    {
        "Rank": 1792,
        "Port": "disputar",
        "Eng": "compete"
    },
    {
        "Rank": 1792,
        "Port": "disputar",
        "Eng": "dispute"
    },
    {
        "Rank": 1793,
        "Port": "adolescente",
        "Eng": "adolescent"
    },
    {
        "Rank": 1793,
        "Port": "adolescente",
        "Eng": "teenager"
    },
    {
        "Rank": 1794,
        "Port": "restante",
        "Eng": "remaining"
    },
    {
        "Rank": 1794,
        "Port": "restante",
        "Eng": "rest"
    },
    {
        "Rank": 1794,
        "Port": "restante",
        "Eng": "remainder"
    },
    {
        "Rank": 1795,
        "Port": "sólido",
        "Eng": "solid"
    },
    {
        "Rank": 1796,
        "Port": "nobre",
        "Eng": "noble"
    },
    {
        "Rank": 1796,
        "Port": "nobre",
        "Eng": "nobleman"
    },
    {
        "Rank": 1797,
        "Port": "balanço",
        "Eng": "balance"
    },
    {
        "Rank": 1798,
        "Port": "corrigir",
        "Eng": "correct"
    },
    {
        "Rank": 1799,
        "Port": "passageiro",
        "Eng": "passenger"
    },
    {
        "Rank": 1800,
        "Port": "facilidade",
        "Eng": "ease"
    },
    {
        "Rank": 1801,
        "Port": "forçar",
        "Eng": "force"
    },
    {
        "Rank": 1802,
        "Port": "mecanismo",
        "Eng": "mechanism"
    },
    {
        "Rank": 1803,
        "Port": "álcool",
        "Eng": "alcohol"
    },
    {
        "Rank": 1804,
        "Port": "convénio",
        "Eng": "agreement"
    },
    {
        "Rank": 1804,
        "Port": "convénio",
        "Eng": "accord"
    },
    {
        "Rank": 1805,
        "Port": "curva",
        "Eng": "curve"
    },
    {
        "Rank": 1806,
        "Port": "concentração",
        "Eng": "concentration"
    },
    {
        "Rank": 1807,
        "Port": "desconhecido",
        "Eng": "unknown"
    },
    {
        "Rank": 1808,
        "Port": "asa",
        "Eng": "wing"
    },
    {
        "Rank": 1808,
        "Port": "asa",
        "Eng": "wingspan"
    },
    {
        "Rank": 1809,
        "Port": "mudo",
        "Eng": "silent"
    },
    {
        "Rank": 1809,
        "Port": "mudo",
        "Eng": "mute"
    },
    {
        "Rank": 1810,
        "Port": "cama",
        "Eng": "bed"
    },
    {
        "Rank": 1811,
        "Port": "empresário",
        "Eng": "entrepreneur"
    },
    {
        "Rank": 1811,
        "Port": "empresário",
        "Eng": "business owner"
    },
    {
        "Rank": 1812,
        "Port": "distinto",
        "Eng": "distinct"
    },
    {
        "Rank": 1812,
        "Port": "distinto",
        "Eng": "distinctive"
    },
    {
        "Rank": 1813,
        "Port": "especialista",
        "Eng": "specialist"
    },
    {
        "Rank": 1814,
        "Port": "julgamento",
        "Eng": "judgment"
    },
    {
        "Rank": 1815,
        "Port": "elevar",
        "Eng": "raise"
    },
    {
        "Rank": 1815,
        "Port": "elevar",
        "Eng": "elevate"
    },
    {
        "Rank": 1816,
        "Port": "nomeadamente",
        "Eng": "namely"
    },
    {
        "Rank": 1816,
        "Port": "nomeadamente",
        "Eng": "more specifically"
    },
    {
        "Rank": 1817,
        "Port": "exibir",
        "Eng": "exhibit"
    },
    {
        "Rank": 1817,
        "Port": "exibir",
        "Eng": "display"
    },
    {
        "Rank": 1818,
        "Port": "êxito",
        "Eng": "success"
    },
    {
        "Rank": 1819,
        "Port": "internacional",
        "Eng": "international"
    },
    {
        "Rank": 1820,
        "Port": "afirmação",
        "Eng": "affirmation"
    },
    {
        "Rank": 1821,
        "Port": "fuga",
        "Eng": "escape"
    },
    {
        "Rank": 1821,
        "Port": "fuga",
        "Eng": "flight"
    },
    {
        "Rank": 1822,
        "Port": "reforçar",
        "Eng": "reinforce"
    },
    {
        "Rank": 1823,
        "Port": "engenharia",
        "Eng": "engineering"
    },
    {
        "Rank": 1824,
        "Port": "ah",
        "Eng": "ah"
    },
    {
        "Rank": 1824,
        "Port": "ah",
        "Eng": "oh"
    },
    {
        "Rank": 1825,
        "Port": "confusão",
        "Eng": "confusion"
    },
    {
        "Rank": 1826,
        "Port": "bar",
        "Eng": "bar"
    },
    {
        "Rank": 1827,
        "Port": "parente",
        "Eng": "relative"
    },
    {
        "Rank": 1827,
        "Port": "parente",
        "Eng": "extended family member"
    },
    {
        "Rank": 1828,
        "Port": "recentemente",
        "Eng": "recently"
    },
    {
        "Rank": 1829,
        "Port": "primário",
        "Eng": "primary"
    },
    {
        "Rank": 1829,
        "Port": "primário",
        "Eng": "elementary"
    },
    {
        "Rank": 1830,
        "Port": "definição",
        "Eng": "definition"
    },
    {
        "Rank": 1831,
        "Port": "gesto",
        "Eng": "gesture"
    },
    {
        "Rank": 1832,
        "Port": "liberal",
        "Eng": "liberal"
    },
    {
        "Rank": 1833,
        "Port": "licença",
        "Eng": "license"
    },
    {
        "Rank": 1833,
        "Port": "licença",
        "Eng": "permission"
    },
    {
        "Rank": 1834,
        "Port": "contemplar",
        "Eng": "contemplate"
    },
    {
        "Rank": 1835,
        "Port": "funcionamento",
        "Eng": "operation"
    },
    {
        "Rank": 1835,
        "Port": "funcionamento",
        "Eng": "functioning"
    },
    {
        "Rank": 1836,
        "Port": "correio",
        "Eng": "mail"
    },
    {
        "Rank": 1836,
        "Port": "correio",
        "Eng": "post office"
    },
    {
        "Rank": 1837,
        "Port": "fechado",
        "Eng": "closed"
    },
    {
        "Rank": 1838,
        "Port": "revolta",
        "Eng": "revolt"
    },
    {
        "Rank": 1839,
        "Port": "cumprimento",
        "Eng": "compliment"
    },
    {
        "Rank": 1839,
        "Port": "cumprimento",
        "Eng": "fulfillment"
    },
    {
        "Rank": 1840,
        "Port": "banho",
        "Eng": "bath"
    },
    {
        "Rank": 1841,
        "Port": "depressa",
        "Eng": "quickly"
    },
    {
        "Rank": 1841,
        "Port": "depressa",
        "Eng": "fast"
    },
    {
        "Rank": 1842,
        "Port": "ocupação",
        "Eng": "occupation"
    },
    {
        "Rank": 1843,
        "Port": "reflexão",
        "Eng": "reflection"
    },
    {
        "Rank": 1844,
        "Port": "percurso",
        "Eng": "route"
    },
    {
        "Rank": 1844,
        "Port": "percurso",
        "Eng": "path"
    },
    {
        "Rank": 1845,
        "Port": "orientar",
        "Eng": "direct"
    },
    {
        "Rank": 1845,
        "Port": "orientar",
        "Eng": "guide"
    },
    {
        "Rank": 1845,
        "Port": "orientar",
        "Eng": "orient"
    },
    {
        "Rank": 1846,
        "Port": "planeta",
        "Eng": "planet"
    },
    {
        "Rank": 1847,
        "Port": "voo",
        "Eng": "flight"
    },
    {
        "Rank": 1848,
        "Port": "rir",
        "Eng": "laugh"
    },
    {
        "Rank": 1849,
        "Port": "parceiro",
        "Eng": "partner"
    },
    {
        "Rank": 1849,
        "Port": "parceiro",
        "Eng": "social or game friend"
    },
    {
        "Rank": 1850,
        "Port": "homenagem",
        "Eng": "homage"
    },
    {
        "Rank": 1850,
        "Port": "homenagem",
        "Eng": "honor"
    },
    {
        "Rank": 1851,
        "Port": "fundação",
        "Eng": "foundation"
    },
    {
        "Rank": 1851,
        "Port": "fundação",
        "Eng": "founding"
    },
    {
        "Rank": 1852,
        "Port": "comprometer",
        "Eng": "commit to"
    },
    {
        "Rank": 1852,
        "Port": "comprometer",
        "Eng": "negatively compromise"
    },
    {
        "Rank": 1853,
        "Port": "mapa",
        "Eng": "map"
    },
    {
        "Rank": 1854,
        "Port": "cópia",
        "Eng": "copy"
    },
    {
        "Rank": 1855,
        "Port": "pairar",
        "Eng": "hover"
    },
    {
        "Rank": 1855,
        "Port": "pairar",
        "Eng": "hang over"
    },
    {
        "Rank": 1856,
        "Port": "secundário",
        "Eng": "secondary"
    },
    {
        "Rank": 1856,
        "Port": "secundário",
        "Eng": "supporting"
    },
    {
        "Rank": 1857,
        "Port": "sessão",
        "Eng": "session"
    },
    {
        "Rank": 1858,
        "Port": "actuação",
        "Eng": "performance"
    },
    {
        "Rank": 1858,
        "Port": "actuação",
        "Eng": "acting"
    },
    {
        "Rank": 1859,
        "Port": "consideração",
        "Eng": "consideration"
    },
    {
        "Rank": 1860,
        "Port": "ensaio",
        "Eng": "rehearsal"
    },
    {
        "Rank": 1860,
        "Port": "ensaio",
        "Eng": "practice"
    },
    {
        "Rank": 1861,
        "Port": "projectar",
        "Eng": "project"
    },
    {
        "Rank": 1861,
        "Port": "projectar",
        "Eng": "make plans"
    },
    {
        "Rank": 1862,
        "Port": "estadual",
        "Eng": "relating to the state"
    },
    {
        "Rank": 1863,
        "Port": "mal",
        "Eng": "evil"
    },
    {
        "Rank": 1864,
        "Port": "conde",
        "Eng": "count"
    },
    {
        "Rank": 1865,
        "Port": "proporção",
        "Eng": "proportion"
    },
    {
        "Rank": 1866,
        "Port": "cantor",
        "Eng": "singer"
    },
    {
        "Rank": 1867,
        "Port": "oferta",
        "Eng": "supply"
    },
    {
        "Rank": 1867,
        "Port": "oferta",
        "Eng": "offer"
    },
    {
        "Rank": 1868,
        "Port": "antigamente",
        "Eng": "used to"
    },
    {
        "Rank": 1868,
        "Port": "antigamente",
        "Eng": "anciently"
    },
    {
        "Rank": 1869,
        "Port": "debaixo",
        "Eng": "under"
    },
    {
        "Rank": 1869,
        "Port": "debaixo",
        "Eng": "beneath"
    },
    {
        "Rank": 1869,
        "Port": "debaixo",
        "Eng": "below"
    },
    {
        "Rank": 1870,
        "Port": "confiar",
        "Eng": "trust"
    },
    {
        "Rank": 1870,
        "Port": "confiar",
        "Eng": "confide"
    },
    {
        "Rank": 1871,
        "Port": "leão",
        "Eng": "lion"
    },
    {
        "Rank": 1872,
        "Port": "colaborar",
        "Eng": "collaborate"
    },
    {
        "Rank": 1873,
        "Port": "judeu",
        "Eng": "Jew"
    },
    {
        "Rank": 1874,
        "Port": "aprovar",
        "Eng": "approve"
    },
    {
        "Rank": 1874,
        "Port": "aprovar",
        "Eng": "pass a law"
    },
    {
        "Rank": 1875,
        "Port": "orientação",
        "Eng": "guidance"
    },
    {
        "Rank": 1875,
        "Port": "orientação",
        "Eng": "orientation"
    },
    {
        "Rank": 1876,
        "Port": "sexual",
        "Eng": "sexual"
    },
    {
        "Rank": 1877,
        "Port": "bomba",
        "Eng": "bomb"
    },
    {
        "Rank": 1878,
        "Port": "constar",
        "Eng": "consist of"
    },
    {
        "Rank": 1878,
        "Port": "constar",
        "Eng": "appear in"
    },
    {
        "Rank": 1879,
        "Port": "derrota",
        "Eng": "defeat"
    },
    {
        "Rank": 1880,
        "Port": "relógio",
        "Eng": "watch"
    },
    {
        "Rank": 1880,
        "Port": "relógio",
        "Eng": "clock"
    },
    {
        "Rank": 1881,
        "Port": "universitário",
        "Eng": "relating to the university"
    },
    {
        "Rank": 1882,
        "Port": "coronel",
        "Eng": "colonel"
    },
    {
        "Rank": 1883,
        "Port": "cálculo",
        "Eng": "calculation"
    },
    {
        "Rank": 1883,
        "Port": "cálculo",
        "Eng": "calculus"
    },
    {
        "Rank": 1884,
        "Port": "apreciar",
        "Eng": "appreciate"
    },
    {
        "Rank": 1885,
        "Port": "esposa",
        "Eng": "wife"
    },
    {
        "Rank": 1886,
        "Port": "coragem",
        "Eng": "courage"
    },
    {
        "Rank": 1887,
        "Port": "sentença",
        "Eng": "sentence"
    },
    {
        "Rank": 1888,
        "Port": "fruta",
        "Eng": "fruit"
    },
    {
        "Rank": 1889,
        "Port": "dom",
        "Eng": "gift"
    },
    {
        "Rank": 1889,
        "Port": "dom",
        "Eng": "honorific title"
    },
    {
        "Rank": 1890,
        "Port": "abordar",
        "Eng": "deal with"
    },
    {
        "Rank": 1890,
        "Port": "abordar",
        "Eng": "approach"
    },
    {
        "Rank": 1891,
        "Port": "terrível",
        "Eng": "terrible"
    },
    {
        "Rank": 1892,
        "Port": "contínuo",
        "Eng": "continuous"
    },
    {
        "Rank": 1893,
        "Port": "exploração",
        "Eng": "exploration"
    },
    {
        "Rank": 1893,
        "Port": "exploração",
        "Eng": "exploitation"
    },
    {
        "Rank": 1894,
        "Port": "febre",
        "Eng": "fever"
    },
    {
        "Rank": 1895,
        "Port": "competência",
        "Eng": "competence"
    },
    {
        "Rank": 1896,
        "Port": "atraso",
        "Eng": "delay"
    },
    {
        "Rank": 1897,
        "Port": "tese",
        "Eng": "thesis"
    },
    {
        "Rank": 1898,
        "Port": "consumo",
        "Eng": "use"
    },
    {
        "Rank": 1898,
        "Port": "consumo",
        "Eng": "consumption"
    },
    {
        "Rank": 1899,
        "Port": "invasão",
        "Eng": "invasion"
    },
    {
        "Rank": 1900,
        "Port": "mineiro",
        "Eng": "mining"
    },
    {
        "Rank": 1900,
        "Port": "mineiro",
        "Eng": "miner"
    },
    {
        "Rank": 1901,
        "Port": "propriamente",
        "Eng": "exactly"
    },
    {
        "Rank": 1901,
        "Port": "propriamente",
        "Eng": "properly"
    },
    {
        "Rank": 1902,
        "Port": "lago",
        "Eng": "lake"
    },
    {
        "Rank": 1903,
        "Port": "despertar",
        "Eng": "awaken"
    },
    {
        "Rank": 1904,
        "Port": "expansão",
        "Eng": "expansion"
    },
    {
        "Rank": 1904,
        "Port": "expansão",
        "Eng": "expanse"
    },
    {
        "Rank": 1905,
        "Port": "demasiado",
        "Eng": "too much"
    },
    {
        "Rank": 1906,
        "Port": "novela",
        "Eng": "soap opera"
    },
    {
        "Rank": 1907,
        "Port": "civilização",
        "Eng": "civilization"
    },
    {
        "Rank": 1908,
        "Port": "arrancar",
        "Eng": "tear away or out of"
    },
    {
        "Rank": 1909,
        "Port": "facilmente",
        "Eng": "easily"
    },
    {
        "Rank": 1910,
        "Port": "desistir",
        "Eng": "give up"
    },
    {
        "Rank": 1911,
        "Port": "gasto",
        "Eng": "expenditure"
    },
    {
        "Rank": 1912,
        "Port": "culto",
        "Eng": "worship"
    },
    {
        "Rank": 1912,
        "Port": "culto",
        "Eng": "cult"
    },
    {
        "Rank": 1912,
        "Port": "culto",
        "Eng": "learned"
    },
    {
        "Rank": 1913,
        "Port": "confundir",
        "Eng": "confuse"
    },
    {
        "Rank": 1913,
        "Port": "confundir",
        "Eng": "confound"
    },
    {
        "Rank": 1914,
        "Port": "sensível",
        "Eng": "sensitive"
    },
    {
        "Rank": 1914,
        "Port": "sensível",
        "Eng": "sensible"
    },
    {
        "Rank": 1915,
        "Port": "trigo",
        "Eng": "wheat"
    },
    {
        "Rank": 1916,
        "Port": "carecer",
        "Eng": "need"
    },
    {
        "Rank": 1916,
        "Port": "carecer",
        "Eng": "lack"
    },
    {
        "Rank": 1916,
        "Port": "carecer",
        "Eng": "do without"
    },
    {
        "Rank": 1917,
        "Port": "retornar",
        "Eng": "return to"
    },
    {
        "Rank": 1918,
        "Port": "quilo",
        "Eng": "kilo"
    },
    {
        "Rank": 1919,
        "Port": "lavar",
        "Eng": "wash"
    },
    {
        "Rank": 1919,
        "Port": "lavar",
        "Eng": "clean"
    },
    {
        "Rank": 1920,
        "Port": "obedecer",
        "Eng": "obey"
    },
    {
        "Rank": 1921,
        "Port": "liga",
        "Eng": "league"
    },
    {
        "Rank": 1921,
        "Port": "liga",
        "Eng": "union"
    },
    {
        "Rank": 1921,
        "Port": "liga",
        "Eng": "connection"
    },
    {
        "Rank": 1922,
        "Port": "pacto",
        "Eng": "agreement"
    },
    {
        "Rank": 1922,
        "Port": "pacto",
        "Eng": "pact"
    },
    {
        "Rank": 1923,
        "Port": "particularmente",
        "Eng": "particularly"
    },
    {
        "Rank": 1924,
        "Port": "novidade",
        "Eng": "news"
    },
    {
        "Rank": 1924,
        "Port": "novidade",
        "Eng": "new thing"
    },
    {
        "Rank": 1925,
        "Port": "crescente",
        "Eng": "growing"
    },
    {
        "Rank": 1925,
        "Port": "crescente",
        "Eng": "increasing"
    },
    {
        "Rank": 1926,
        "Port": "favorável",
        "Eng": "favorable"
    },
    {
        "Rank": 1926,
        "Port": "favorável",
        "Eng": "in favor"
    },
    {
        "Rank": 1927,
        "Port": "suspenso",
        "Eng": "suspended"
    },
    {
        "Rank": 1928,
        "Port": "domingo",
        "Eng": "Sunday"
    },
    {
        "Rank": 1929,
        "Port": "verso",
        "Eng": "verse"
    },
    {
        "Rank": 1930,
        "Port": "óleo",
        "Eng": "oil"
    },
    {
        "Rank": 1931,
        "Port": "fabricar",
        "Eng": "manufacture"
    },
    {
        "Rank": 1932,
        "Port": "mecânico",
        "Eng": "mechanic"
    },
    {
        "Rank": 1932,
        "Port": "mecânico",
        "Eng": "mechanical"
    },
    {
        "Rank": 1933,
        "Port": "reconhecimento",
        "Eng": "recognition"
    },
    {
        "Rank": 1934,
        "Port": "prolongar",
        "Eng": "go on"
    },
    {
        "Rank": 1934,
        "Port": "prolongar",
        "Eng": "prolong"
    },
    {
        "Rank": 1935,
        "Port": "redução",
        "Eng": "reduction"
    },
    {
        "Rank": 1936,
        "Port": "administrador",
        "Eng": "manager"
    },
    {
        "Rank": 1936,
        "Port": "administrador",
        "Eng": "administrator"
    },
    {
        "Rank": 1937,
        "Port": "leste",
        "Eng": "east"
    },
    {
        "Rank": 1938,
        "Port": "vestido",
        "Eng": "dress"
    },
    {
        "Rank": 1939,
        "Port": "perfeitamente",
        "Eng": "perfectly"
    },
    {
        "Rank": 1940,
        "Port": "compreensão",
        "Eng": "understanding"
    },
    {
        "Rank": 1940,
        "Port": "compreensão",
        "Eng": "comprehension"
    },
    {
        "Rank": 1941,
        "Port": "glória",
        "Eng": "glory"
    },
    {
        "Rank": 1942,
        "Port": "instante",
        "Eng": "instant"
    },
    {
        "Rank": 1943,
        "Port": "drama",
        "Eng": "drama"
    },
    {
        "Rank": 1944,
        "Port": "seio",
        "Eng": "center"
    },
    {
        "Rank": 1944,
        "Port": "seio",
        "Eng": "bosom"
    },
    {
        "Rank": 1944,
        "Port": "seio",
        "Eng": "breast"
    },
    {
        "Rank": 1945,
        "Port": "render",
        "Eng": "yield"
    },
    {
        "Rank": 1945,
        "Port": "render",
        "Eng": "earn"
    },
    {
        "Rank": 1946,
        "Port": "segredo",
        "Eng": "secret"
    },
    {
        "Rank": 1947,
        "Port": "chinês",
        "Eng": "Chinese"
    },
    {
        "Rank": 1948,
        "Port": "paralelo",
        "Eng": "parallel"
    },
    {
        "Rank": 1949,
        "Port": "soltar",
        "Eng": "release"
    },
    {
        "Rank": 1949,
        "Port": "soltar",
        "Eng": "unfasten"
    },
    {
        "Rank": 1950,
        "Port": "redondo",
        "Eng": "round"
    },
    {
        "Rank": 1951,
        "Port": "pedido",
        "Eng": "request"
    },
    {
        "Rank": 1951,
        "Port": "pedido",
        "Eng": "requested"
    },
    {
        "Rank": 1952,
        "Port": "idêntico",
        "Eng": "identical"
    },
    {
        "Rank": 1953,
        "Port": "cultivar",
        "Eng": "cultivate"
    },
    {
        "Rank": 1954,
        "Port": "cristão",
        "Eng": "Christian"
    },
    {
        "Rank": 1955,
        "Port": "equipe",
        "Eng": "team"
    },
    {
        "Rank": 1956,
        "Port": "muro",
        "Eng": "free-standing wall"
    },
    {
        "Rank": 1957,
        "Port": "salão",
        "Eng": "large room"
    },
    {
        "Rank": 1957,
        "Port": "salão",
        "Eng": "meeting hall"
    },
    {
        "Rank": 1957,
        "Port": "salão",
        "Eng": "salon"
    },
    {
        "Rank": 1958,
        "Port": "visível",
        "Eng": "visible"
    },
    {
        "Rank": 1959,
        "Port": "camada",
        "Eng": "layer"
    },
    {
        "Rank": 1959,
        "Port": "camada",
        "Eng": "sheet"
    },
    {
        "Rank": 1960,
        "Port": "cérebro",
        "Eng": "brain"
    },
    {
        "Rank": 1961,
        "Port": "colecção",
        "Eng": "collection"
    },
    {
        "Rank": 1962,
        "Port": "publicação",
        "Eng": "publication"
    },
    {
        "Rank": 1963,
        "Port": "romano",
        "Eng": "Roman"
    },
    {
        "Rank": 1964,
        "Port": "juízo",
        "Eng": "judgment"
    },
    {
        "Rank": 1964,
        "Port": "juízo",
        "Eng": "good sense"
    },
    {
        "Rank": 1965,
        "Port": "concelho",
        "Eng": "municipality"
    },
    {
        "Rank": 1965,
        "Port": "concelho",
        "Eng": "county"
    },
    {
        "Rank": 1965,
        "Port": "concelho",
        "Eng": "council"
    },
    {
        "Rank": 1966,
        "Port": "satisfação",
        "Eng": "satisfaction"
    },
    {
        "Rank": 1967,
        "Port": "caracterizar",
        "Eng": "characterize"
    },
    {
        "Rank": 1968,
        "Port": "primo",
        "Eng": "cousin"
    },
    {
        "Rank": 1968,
        "Port": "primo",
        "Eng": "prime"
    },
    {
        "Rank": 1969,
        "Port": "exclusivamente",
        "Eng": "exclusively"
    },
    {
        "Rank": 1970,
        "Port": "miséria",
        "Eng": "misery"
    },
    {
        "Rank": 1970,
        "Port": "miséria",
        "Eng": "poverty"
    },
    {
        "Rank": 1971,
        "Port": "execução",
        "Eng": "execution"
    },
    {
        "Rank": 1972,
        "Port": "escândalo",
        "Eng": "scandal"
    },
    {
        "Rank": 1973,
        "Port": "ordenar",
        "Eng": "command"
    },
    {
        "Rank": 1973,
        "Port": "ordenar",
        "Eng": "order"
    },
    {
        "Rank": 1974,
        "Port": "avenida",
        "Eng": "avenue"
    },
    {
        "Rank": 1975,
        "Port": "esquema",
        "Eng": "scheme"
    },
    {
        "Rank": 1976,
        "Port": "expresso",
        "Eng": "expressed"
    },
    {
        "Rank": 1977,
        "Port": "sensibilidade",
        "Eng": "sensitivity"
    },
    {
        "Rank": 1977,
        "Port": "sensibilidade",
        "Eng": "sensibility"
    },
    {
        "Rank": 1978,
        "Port": "galeria",
        "Eng": "gallery"
    },
    {
        "Rank": 1979,
        "Port": "âmbito",
        "Eng": "level"
    },
    {
        "Rank": 1979,
        "Port": "âmbito",
        "Eng": "sphere of action or work"
    },
    {
        "Rank": 1980,
        "Port": "lançamento",
        "Eng": "release"
    },
    {
        "Rank": 1980,
        "Port": "lançamento",
        "Eng": "launching"
    },
    {
        "Rank": 1981,
        "Port": "armar",
        "Eng": "arm"
    },
    {
        "Rank": 1981,
        "Port": "armar",
        "Eng": "assemble"
    },
    {
        "Rank": 1981,
        "Port": "armar",
        "Eng": "equip"
    },
    {
        "Rank": 1982,
        "Port": "estatuto",
        "Eng": "statute"
    },
    {
        "Rank": 1983,
        "Port": "sequência",
        "Eng": "sequence"
    },
    {
        "Rank": 1984,
        "Port": "composto",
        "Eng": "composed"
    },
    {
        "Rank": 1985,
        "Port": "mistério",
        "Eng": "mystery"
    },
    {
        "Rank": 1986,
        "Port": "louco",
        "Eng": "mad"
    },
    {
        "Rank": 1986,
        "Port": "louco",
        "Eng": "crazy"
    },
    {
        "Rank": 1987,
        "Port": "significado",
        "Eng": "meaning"
    },
    {
        "Rank": 1987,
        "Port": "significado",
        "Eng": "significance"
    },
    {
        "Rank": 1988,
        "Port": "pacífico",
        "Eng": "Pacific"
    },
    {
        "Rank": 1988,
        "Port": "pacífico",
        "Eng": "calm"
    },
    {
        "Rank": 1989,
        "Port": "absorver",
        "Eng": "absorb"
    },
    {
        "Rank": 1990,
        "Port": "arroz",
        "Eng": "rice"
    },
    {
        "Rank": 1991,
        "Port": "evento",
        "Eng": "event"
    },
    {
        "Rank": 1992,
        "Port": "senador",
        "Eng": "senator"
    },
    {
        "Rank": 1993,
        "Port": "estimular",
        "Eng": "stimulate"
    },
    {
        "Rank": 1994,
        "Port": "decisivo",
        "Eng": "decisive"
    },
    {
        "Rank": 1995,
        "Port": "solidariedade",
        "Eng": "solidarity"
    },
    {
        "Rank": 1995,
        "Port": "solidariedade",
        "Eng": "mutual responsibility"
    },
    {
        "Rank": 1996,
        "Port": "núcleo",
        "Eng": "nucleus"
    },
    {
        "Rank": 1996,
        "Port": "núcleo",
        "Eng": "core"
    },
    {
        "Rank": 1997,
        "Port": "optar",
        "Eng": "opt"
    },
    {
        "Rank": 1998,
        "Port": "atmosfera",
        "Eng": "air"
    },
    {
        "Rank": 1998,
        "Port": "atmosfera",
        "Eng": "atmosphere"
    },
    {
        "Rank": 1999,
        "Port": "todavia",
        "Eng": "but"
    },
    {
        "Rank": 1999,
        "Port": "todavia",
        "Eng": "still"
    },
    {
        "Rank": 2000,
        "Port": "sujeitar",
        "Eng": "subject"
    },
    {
        "Rank": 2001,
        "Port": "apurar",
        "Eng": "find out"
    },
    {
        "Rank": 2001,
        "Port": "apurar",
        "Eng": "investigate"
    },
    {
        "Rank": 2001,
        "Port": "apurar",
        "Eng": "perfect"
    },
    {
        "Rank": 2002,
        "Port": "cartão",
        "Eng": "card"
    },
    {
        "Rank": 2003,
        "Port": "triste",
        "Eng": "sad"
    },
    {
        "Rank": 2004,
        "Port": "legislativo",
        "Eng": "legislative"
    },
    {
        "Rank": 2005,
        "Port": "baseado",
        "Eng": "based"
    },
    {
        "Rank": 2005,
        "Port": "baseado",
        "Eng": "on the basis of"
    },
    {
        "Rank": 2006,
        "Port": "influenciar",
        "Eng": "influence"
    },
    {
        "Rank": 2007,
        "Port": "preparado",
        "Eng": "prepared"
    },
    {
        "Rank": 2008,
        "Port": "ganho",
        "Eng": "earnings"
    },
    {
        "Rank": 2008,
        "Port": "ganho",
        "Eng": "profits"
    },
    {
        "Rank": 2008,
        "Port": "ganho",
        "Eng": "gained"
    },
    {
        "Rank": 2009,
        "Port": "combinar",
        "Eng": "combine"
    },
    {
        "Rank": 2010,
        "Port": "emitir",
        "Eng": "emit"
    },
    {
        "Rank": 2010,
        "Port": "emitir",
        "Eng": "issue"
    },
    {
        "Rank": 2010,
        "Port": "emitir",
        "Eng": "broadcast"
    },
    {
        "Rank": 2011,
        "Port": "divulgar",
        "Eng": "make known"
    },
    {
        "Rank": 2011,
        "Port": "divulgar",
        "Eng": "publicize"
    },
    {
        "Rank": 2012,
        "Port": "claramente",
        "Eng": "clearly"
    },
    {
        "Rank": 2013,
        "Port": "mexer",
        "Eng": "touch"
    },
    {
        "Rank": 2013,
        "Port": "mexer",
        "Eng": "shake"
    },
    {
        "Rank": 2013,
        "Port": "mexer",
        "Eng": "mix"
    },
    {
        "Rank": 2013,
        "Port": "mexer",
        "Eng": "stir"
    },
    {
        "Rank": 2014,
        "Port": "senado",
        "Eng": "senate"
    },
    {
        "Rank": 2015,
        "Port": "alargar",
        "Eng": "enlarge"
    },
    {
        "Rank": 2015,
        "Port": "alargar",
        "Eng": "increase"
    },
    {
        "Rank": 2015,
        "Port": "alargar",
        "Eng": "widen"
    },
    {
        "Rank": 2016,
        "Port": "traço",
        "Eng": "trace"
    },
    {
        "Rank": 2016,
        "Port": "traço",
        "Eng": "line"
    },
    {
        "Rank": 2016,
        "Port": "traço",
        "Eng": "signal"
    },
    {
        "Rank": 2017,
        "Port": "pau",
        "Eng": "stick"
    },
    {
        "Rank": 2017,
        "Port": "pau",
        "Eng": "wood"
    },
    {
        "Rank": 2018,
        "Port": "missa",
        "Eng": "religious mass"
    },
    {
        "Rank": 2019,
        "Port": "consumir",
        "Eng": "consume"
    },
    {
        "Rank": 2020,
        "Port": "libertação",
        "Eng": "freedom"
    },
    {
        "Rank": 2020,
        "Port": "libertação",
        "Eng": "liberation"
    },
    {
        "Rank": 2021,
        "Port": "arrastar",
        "Eng": "drag"
    },
    {
        "Rank": 2022,
        "Port": "superar",
        "Eng": "overcome"
    },
    {
        "Rank": 2022,
        "Port": "superar",
        "Eng": "surpass"
    },
    {
        "Rank": 2022,
        "Port": "superar",
        "Eng": "exceed"
    },
    {
        "Rank": 2023,
        "Port": "protesto",
        "Eng": "protest"
    },
    {
        "Rank": 2024,
        "Port": "envolvido",
        "Eng": "involved"
    },
    {
        "Rank": 2025,
        "Port": "travar",
        "Eng": "take place"
    },
    {
        "Rank": 2025,
        "Port": "travar",
        "Eng": "impede"
    },
    {
        "Rank": 2025,
        "Port": "travar",
        "Eng": "stop"
    },
    {
        "Rank": 2026,
        "Port": "preferência",
        "Eng": "preference"
    },
    {
        "Rank": 2027,
        "Port": "eliminar",
        "Eng": "eliminate"
    },
    {
        "Rank": 2028,
        "Port": "federação",
        "Eng": "federation"
    },
    {
        "Rank": 2029,
        "Port": "sagrado",
        "Eng": "sacred"
    },
    {
        "Rank": 2030,
        "Port": "amanhã",
        "Eng": "tomorrow"
    },
    {
        "Rank": 2031,
        "Port": "deserto",
        "Eng": "desert"
    },
    {
        "Rank": 2031,
        "Port": "deserto",
        "Eng": "deserted"
    },
    {
        "Rank": 2032,
        "Port": "reparar",
        "Eng": "make reparations"
    },
    {
        "Rank": 2032,
        "Port": "reparar",
        "Eng": "fix"
    },
    {
        "Rank": 2032,
        "Port": "reparar",
        "Eng": "notice"
    },
    {
        "Rank": 2033,
        "Port": "petróleo",
        "Eng": "oil"
    },
    {
        "Rank": 2033,
        "Port": "petróleo",
        "Eng": "petroleum"
    },
    {
        "Rank": 2034,
        "Port": "parlamento",
        "Eng": "parliament"
    },
    {
        "Rank": 2035,
        "Port": "autêntico",
        "Eng": "authentic"
    },
    {
        "Rank": 2036,
        "Port": "fantástico",
        "Eng": "fantastic"
    },
    {
        "Rank": 2037,
        "Port": "aliado",
        "Eng": "ally"
    },
    {
        "Rank": 2037,
        "Port": "aliado",
        "Eng": "allied"
    },
    {
        "Rank": 2038,
        "Port": "competir",
        "Eng": "compete"
    },
    {
        "Rank": 2039,
        "Port": "regular",
        "Eng": "regular"
    },
    {
        "Rank": 2040,
        "Port": "erva",
        "Eng": "herb"
    },
    {
        "Rank": 2041,
        "Port": "prato",
        "Eng": "plate"
    },
    {
        "Rank": 2042,
        "Port": "masculino",
        "Eng": "masculine"
    },
    {
        "Rank": 2042,
        "Port": "masculino",
        "Eng": "male"
    },
    {
        "Rank": 2043,
        "Port": "alcance",
        "Eng": "range"
    },
    {
        "Rank": 2043,
        "Port": "alcance",
        "Eng": "reach"
    },
    {
        "Rank": 2044,
        "Port": "centímetro",
        "Eng": "centimeter"
    },
    {
        "Rank": 2045,
        "Port": "crónica",
        "Eng": "newspaper column"
    },
    {
        "Rank": 2045,
        "Port": "crónica",
        "Eng": "narrative"
    },
    {
        "Rank": 2046,
        "Port": "condenado",
        "Eng": "condemned"
    },
    {
        "Rank": 2047,
        "Port": "concorrer",
        "Eng": "compete"
    },
    {
        "Rank": 2047,
        "Port": "concorrer",
        "Eng": "apply for"
    },
    {
        "Rank": 2048,
        "Port": "tio",
        "Eng": "uncle"
    },
    {
        "Rank": 2049,
        "Port": "vigor",
        "Eng": "in effect; energy"
    },
    {
        "Rank": 2050,
        "Port": "arco",
        "Eng": "bow"
    },
    {
        "Rank": 2050,
        "Port": "arco",
        "Eng": "arch"
    },
    {
        "Rank": 2050,
        "Port": "arco",
        "Eng": "arc"
    },
    {
        "Rank": 2051,
        "Port": "ficção",
        "Eng": "fiction"
    },
    {
        "Rank": 2052,
        "Port": "sugestão",
        "Eng": "suggestion"
    },
    {
        "Rank": 2053,
        "Port": "escravo",
        "Eng": "slave"
    },
    {
        "Rank": 2054,
        "Port": "agarrar",
        "Eng": "grab"
    },
    {
        "Rank": 2054,
        "Port": "agarrar",
        "Eng": "seize"
    },
    {
        "Rank": 2054,
        "Port": "agarrar",
        "Eng": "lay hold of"
    },
    {
        "Rank": 2055,
        "Port": "proporcionar",
        "Eng": "provide"
    },
    {
        "Rank": 2055,
        "Port": "proporcionar",
        "Eng": "offer"
    },
    {
        "Rank": 2056,
        "Port": "desempenho",
        "Eng": "performance"
    },
    {
        "Rank": 2057,
        "Port": "intervalo",
        "Eng": "interval"
    },
    {
        "Rank": 2057,
        "Port": "intervalo",
        "Eng": "intermission"
    },
    {
        "Rank": 2058,
        "Port": "fundar",
        "Eng": "found"
    },
    {
        "Rank": 2059,
        "Port": "etapa",
        "Eng": "phase"
    },
    {
        "Rank": 2059,
        "Port": "etapa",
        "Eng": "stage"
    },
    {
        "Rank": 2060,
        "Port": "parada",
        "Eng": "stop"
    },
    {
        "Rank": 2060,
        "Port": "parada",
        "Eng": "break"
    },
    {
        "Rank": 2061,
        "Port": "registrar",
        "Eng": "register"
    },
    {
        "Rank": 2061,
        "Port": "registrar",
        "Eng": "record"
    },
    {
        "Rank": 2062,
        "Port": "acusação",
        "Eng": "accusation"
    },
    {
        "Rank": 2063,
        "Port": "estratégico",
        "Eng": "strategic"
    },
    {
        "Rank": 2064,
        "Port": "inteligente",
        "Eng": "intelligent"
    },
    {
        "Rank": 2065,
        "Port": "pescoço",
        "Eng": "neck"
    },
    {
        "Rank": 2066,
        "Port": "moça",
        "Eng": "young woman"
    },
    {
        "Rank": 2066,
        "Port": "moça",
        "Eng": "girl"
    },
    {
        "Rank": 2067,
        "Port": "legislação",
        "Eng": "legislation"
    },
    {
        "Rank": 2068,
        "Port": "tecido",
        "Eng": "fabric"
    },
    {
        "Rank": 2068,
        "Port": "tecido",
        "Eng": "material"
    },
    {
        "Rank": 2068,
        "Port": "tecido",
        "Eng": "tissue"
    },
    {
        "Rank": 2069,
        "Port": "boi",
        "Eng": "ox"
    },
    {
        "Rank": 2069,
        "Port": "boi",
        "Eng": "steer"
    },
    {
        "Rank": 2069,
        "Port": "boi",
        "Eng": "bull"
    },
    {
        "Rank": 2070,
        "Port": "desviar",
        "Eng": "avert"
    },
    {
        "Rank": 2070,
        "Port": "desviar",
        "Eng": "deviate"
    },
    {
        "Rank": 2070,
        "Port": "desviar",
        "Eng": "take a detour"
    },
    {
        "Rank": 2071,
        "Port": "submeter",
        "Eng": "subject"
    },
    {
        "Rank": 2071,
        "Port": "submeter",
        "Eng": "submit"
    },
    {
        "Rank": 2072,
        "Port": "dramático",
        "Eng": "dramatic"
    },
    {
        "Rank": 2073,
        "Port": "desemprego",
        "Eng": "unemployment"
    },
    {
        "Rank": 2074,
        "Port": "contribuição",
        "Eng": "contribution"
    },
    {
        "Rank": 2075,
        "Port": "dobrar",
        "Eng": "fold"
    },
    {
        "Rank": 2075,
        "Port": "dobrar",
        "Eng": "double"
    },
    {
        "Rank": 2076,
        "Port": "competição",
        "Eng": "competition"
    },
    {
        "Rank": 2077,
        "Port": "pronunciar",
        "Eng": "pronounce"
    },
    {
        "Rank": 2078,
        "Port": "comparação",
        "Eng": "comparison"
    },
    {
        "Rank": 2079,
        "Port": "animar",
        "Eng": "encourage"
    },
    {
        "Rank": 2079,
        "Port": "animar",
        "Eng": "cheer up"
    },
    {
        "Rank": 2080,
        "Port": "capitão",
        "Eng": "captain"
    },
    {
        "Rank": 2081,
        "Port": "conferir",
        "Eng": "confer"
    },
    {
        "Rank": 2081,
        "Port": "conferir",
        "Eng": "give the right to"
    },
    {
        "Rank": 2082,
        "Port": "ruim",
        "Eng": "bad"
    },
    {
        "Rank": 2082,
        "Port": "ruim",
        "Eng": "vile"
    },
    {
        "Rank": 2082,
        "Port": "ruim",
        "Eng": "wicked"
    },
    {
        "Rank": 2082,
        "Port": "ruim",
        "Eng": "rotten"
    },
    {
        "Rank": 2083,
        "Port": "aprovação",
        "Eng": "approval"
    },
    {
        "Rank": 2084,
        "Port": "pasta",
        "Eng": "folder"
    },
    {
        "Rank": 2084,
        "Port": "pasta",
        "Eng": "suitcase"
    },
    {
        "Rank": 2084,
        "Port": "pasta",
        "Eng": "paste"
    },
    {
        "Rank": 2085,
        "Port": "liderança",
        "Eng": "leadership"
    },
    {
        "Rank": 2086,
        "Port": "estabilidade",
        "Eng": "stability"
    },
    {
        "Rank": 2087,
        "Port": "campeonato",
        "Eng": "championship"
    },
    {
        "Rank": 2088,
        "Port": "conservador",
        "Eng": "conservative"
    },
    {
        "Rank": 2089,
        "Port": "converter",
        "Eng": "convert"
    },
    {
        "Rank": 2090,
        "Port": "definido",
        "Eng": "defined"
    },
    {
        "Rank": 2091,
        "Port": "absurdo",
        "Eng": "absurd"
    },
    {
        "Rank": 2091,
        "Port": "absurdo",
        "Eng": "absurdity"
    },
    {
        "Rank": 2092,
        "Port": "registar",
        "Eng": "register"
    },
    {
        "Rank": 2093,
        "Port": "beira",
        "Eng": "side"
    },
    {
        "Rank": 2093,
        "Port": "beira",
        "Eng": "edge"
    },
    {
        "Rank": 2094,
        "Port": "tragédia",
        "Eng": "tragedy"
    },
    {
        "Rank": 2095,
        "Port": "manutenção",
        "Eng": "maintenance"
    },
    {
        "Rank": 2096,
        "Port": "fortaleza",
        "Eng": "fortress"
    },
    {
        "Rank": 2096,
        "Port": "fortaleza",
        "Eng": "fort"
    },
    {
        "Rank": 2097,
        "Port": "secreto",
        "Eng": "secret"
    },
    {
        "Rank": 2098,
        "Port": "aparentemente",
        "Eng": "apparently"
    },
    {
        "Rank": 2099,
        "Port": "notável",
        "Eng": "notable"
    },
    {
        "Rank": 2099,
        "Port": "notável",
        "Eng": "noteworthy"
    },
    {
        "Rank": 2100,
        "Port": "corredor",
        "Eng": "corridor"
    },
    {
        "Rank": 2100,
        "Port": "corredor",
        "Eng": "runner"
    },
    {
        "Rank": 2101,
        "Port": "barreira",
        "Eng": "barrier"
    },
    {
        "Rank": 2102,
        "Port": "grão",
        "Eng": "grain"
    },
    {
        "Rank": 2103,
        "Port": "avanço",
        "Eng": "advance"
    },
    {
        "Rank": 2104,
        "Port": "onze",
        "Eng": "eleven"
    },
    {
        "Rank": 2105,
        "Port": "espelho",
        "Eng": "mirror"
    },
    {
        "Rank": 2106,
        "Port": "juntamente",
        "Eng": "together"
    },
    {
        "Rank": 2107,
        "Port": "brincar",
        "Eng": "play"
    },
    {
        "Rank": 2107,
        "Port": "brincar",
        "Eng": "joke"
    },
    {
        "Rank": 2108,
        "Port": "ouvido",
        "Eng": "ear"
    },
    {
        "Rank": 2108,
        "Port": "ouvido",
        "Eng": "heard"
    },
    {
        "Rank": 2109,
        "Port": "greve",
        "Eng": "strike"
    },
    {
        "Rank": 2110,
        "Port": "tecer",
        "Eng": "weave"
    },
    {
        "Rank": 2111,
        "Port": "conselheiro",
        "Eng": "counselor"
    },
    {
        "Rank": 2112,
        "Port": "pobreza",
        "Eng": "poverty"
    },
    {
        "Rank": 2113,
        "Port": "colaboração",
        "Eng": "collaboration"
    },
    {
        "Rank": 2114,
        "Port": "consumidor",
        "Eng": "consumer"
    },
    {
        "Rank": 2114,
        "Port": "consumidor",
        "Eng": "consuming"
    },
    {
        "Rank": 2115,
        "Port": "perseguir",
        "Eng": "pursue"
    },
    {
        "Rank": 2115,
        "Port": "perseguir",
        "Eng": "persecute"
    },
    {
        "Rank": 2116,
        "Port": "nu",
        "Eng": "naked"
    },
    {
        "Rank": 2116,
        "Port": "nu",
        "Eng": "nude"
    },
    {
        "Rank": 2117,
        "Port": "selvagem",
        "Eng": "wild"
    },
    {
        "Rank": 2117,
        "Port": "selvagem",
        "Eng": "savage"
    },
    {
        "Rank": 2118,
        "Port": "pesca",
        "Eng": "fishing"
    },
    {
        "Rank": 2119,
        "Port": "peito",
        "Eng": "chest"
    },
    {
        "Rank": 2119,
        "Port": "peito",
        "Eng": "breast"
    },
    {
        "Rank": 2120,
        "Port": "consulta",
        "Eng": "consultation"
    },
    {
        "Rank": 2121,
        "Port": "impacto",
        "Eng": "impact"
    },
    {
        "Rank": 2122,
        "Port": "emprestar",
        "Eng": "lend"
    },
    {
        "Rank": 2122,
        "Port": "emprestar",
        "Eng": "loan"
    },
    {
        "Rank": 2123,
        "Port": "antecipar",
        "Eng": "do earlier than planned"
    },
    {
        "Rank": 2123,
        "Port": "antecipar",
        "Eng": "anticipate"
    },
    {
        "Rank": 2124,
        "Port": "desempenhar",
        "Eng": "perform"
    },
    {
        "Rank": 2124,
        "Port": "desempenhar",
        "Eng": "act"
    },
    {
        "Rank": 2124,
        "Port": "desempenhar",
        "Eng": "fulfill"
    },
    {
        "Rank": 2125,
        "Port": "empréstimo",
        "Eng": "loan"
    },
    {
        "Rank": 2126,
        "Port": "segurar",
        "Eng": "hold"
    },
    {
        "Rank": 2126,
        "Port": "segurar",
        "Eng": "secure"
    },
    {
        "Rank": 2126,
        "Port": "segurar",
        "Eng": "make sure"
    },
    {
        "Rank": 2127,
        "Port": "pista",
        "Eng": "rink"
    },
    {
        "Rank": 2127,
        "Port": "pista",
        "Eng": "field"
    },
    {
        "Rank": 2127,
        "Port": "pista",
        "Eng": "runway"
    },
    {
        "Rank": 2127,
        "Port": "pista",
        "Eng": "lane"
    },
    {
        "Rank": 2128,
        "Port": "vestir",
        "Eng": "wear"
    },
    {
        "Rank": 2128,
        "Port": "vestir",
        "Eng": "dress"
    },
    {
        "Rank": 2129,
        "Port": "escrito",
        "Eng": "writings"
    },
    {
        "Rank": 2129,
        "Port": "escrito",
        "Eng": "written"
    },
    {
        "Rank": 2130,
        "Port": "pássaro",
        "Eng": "bird"
    },
    {
        "Rank": 2131,
        "Port": "rapariga",
        "Eng": "young girl"
    },
    {
        "Rank": 2131,
        "Port": "rapariga",
        "Eng": "prostitute"
    },
    {
        "Rank": 2132,
        "Port": "indispensável",
        "Eng": "indispensable"
    },
    {
        "Rank": 2133,
        "Port": "convocar",
        "Eng": "call"
    },
    {
        "Rank": 2133,
        "Port": "convocar",
        "Eng": "summon"
    },
    {
        "Rank": 2133,
        "Port": "convocar",
        "Eng": ""
    },
    {
        "Rank": 2134,
        "Port": "nocturno",
        "Eng": "nocturnal"
    },
    {
        "Rank": 2134,
        "Port": "nocturno",
        "Eng": "of the night"
    },
    {
        "Rank": 2135,
        "Port": "estética",
        "Eng": "aesthetics"
    },
    {
        "Rank": 2135,
        "Port": "estética",
        "Eng": "beauty"
    },
    {
        "Rank": 2136,
        "Port": "recomendar",
        "Eng": "recommend"
    },
    {
        "Rank": 2137,
        "Port": "norte-americano",
        "Eng": "North American"
    },
    {
        "Rank": 2138,
        "Port": "jantar",
        "Eng": "eat dinner"
    },
    {
        "Rank": 2139,
        "Port": "redacção",
        "Eng": "writing"
    },
    {
        "Rank": 2139,
        "Port": "redacção",
        "Eng": "editorial staff"
    },
    {
        "Rank": 2140,
        "Port": "pano",
        "Eng": "cloth"
    },
    {
        "Rank": 2141,
        "Port": "reflexo",
        "Eng": "reflection"
    },
    {
        "Rank": 2141,
        "Port": "reflexo",
        "Eng": "reflex"
    },
    {
        "Rank": 2142,
        "Port": "romântico",
        "Eng": "romantic"
    },
    {
        "Rank": 2143,
        "Port": "demanda",
        "Eng": "demand"
    },
    {
        "Rank": 2144,
        "Port": "acerca",
        "Eng": "about"
    },
    {
        "Rank": 2144,
        "Port": "acerca",
        "Eng": "near"
    },
    {
        "Rank": 2145,
        "Port": "impulso",
        "Eng": "impulse"
    },
    {
        "Rank": 2145,
        "Port": "impulso",
        "Eng": "impetus"
    },
    {
        "Rank": 2146,
        "Port": "cerimónia",
        "Eng": "ceremony"
    },
    {
        "Rank": 2147,
        "Port": "entendimento",
        "Eng": "understanding"
    },
    {
        "Rank": 2148,
        "Port": "saltar",
        "Eng": "jump"
    },
    {
        "Rank": 2148,
        "Port": "saltar",
        "Eng": "leap"
    },
    {
        "Rank": 2149,
        "Port": "temperatura",
        "Eng": "temperature"
    },
    {
        "Rank": 2150,
        "Port": "demitir",
        "Eng": "resign"
    },
    {
        "Rank": 2150,
        "Port": "demitir",
        "Eng": "quit"
    },
    {
        "Rank": 2151,
        "Port": "excessivo",
        "Eng": "excessive"
    },
    {
        "Rank": 2152,
        "Port": "componente",
        "Eng": "part"
    },
    {
        "Rank": 2152,
        "Port": "componente",
        "Eng": "component"
    },
    {
        "Rank": 2153,
        "Port": "felicidade",
        "Eng": "joy"
    },
    {
        "Rank": 2153,
        "Port": "felicidade",
        "Eng": "happiness"
    },
    {
        "Rank": 2154,
        "Port": "remédio",
        "Eng": "medicine"
    },
    {
        "Rank": 2154,
        "Port": "remédio",
        "Eng": "remedy"
    },
    {
        "Rank": 2155,
        "Port": "mancha",
        "Eng": "spot"
    },
    {
        "Rank": 2155,
        "Port": "mancha",
        "Eng": "stain"
    },
    {
        "Rank": 2156,
        "Port": "nova",
        "Eng": "news"
    },
    {
        "Rank": 2157,
        "Port": "bicho",
        "Eng": "creature"
    },
    {
        "Rank": 2157,
        "Port": "bicho",
        "Eng": "bug"
    },
    {
        "Rank": 2157,
        "Port": "bicho",
        "Eng": "beast"
    },
    {
        "Rank": 2158,
        "Port": "preparação",
        "Eng": "preparation"
    },
    {
        "Rank": 2159,
        "Port": "delegado",
        "Eng": "delegate"
    },
    {
        "Rank": 2159,
        "Port": "delegado",
        "Eng": "delegated"
    },
    {
        "Rank": 2160,
        "Port": "imaginação",
        "Eng": "imagination"
    },
    {
        "Rank": 2161,
        "Port": "ferido",
        "Eng": "wound"
    },
    {
        "Rank": 2161,
        "Port": "ferido",
        "Eng": "wounded"
    },
    {
        "Rank": 2162,
        "Port": "actriz",
        "Eng": "actress"
    },
    {
        "Rank": 2163,
        "Port": "tensão",
        "Eng": "anxiety"
    },
    {
        "Rank": 2163,
        "Port": "tensão",
        "Eng": "tension"
    },
    {
        "Rank": 2164,
        "Port": "surpreender",
        "Eng": "surprise"
    },
    {
        "Rank": 2165,
        "Port": "auxiliar",
        "Eng": "assistant"
    },
    {
        "Rank": 2165,
        "Port": "auxiliar",
        "Eng": "auxiliary"
    },
    {
        "Rank": 2166,
        "Port": "conceber",
        "Eng": "conceive"
    },
    {
        "Rank": 2167,
        "Port": "parceria",
        "Eng": "partnership"
    },
    {
        "Rank": 2168,
        "Port": "acelerar",
        "Eng": "accelerate"
    },
    {
        "Rank": 2168,
        "Port": "acelerar",
        "Eng": "speed up"
    },
    {
        "Rank": 2169,
        "Port": "concessão",
        "Eng": "concession"
    },
    {
        "Rank": 2169,
        "Port": "concessão",
        "Eng": "favor"
    },
    {
        "Rank": 2169,
        "Port": "concessão",
        "Eng": "right"
    },
    {
        "Rank": 2170,
        "Port": "radical",
        "Eng": "radical"
    },
    {
        "Rank": 2171,
        "Port": "ampliar",
        "Eng": "increase"
    },
    {
        "Rank": 2171,
        "Port": "ampliar",
        "Eng": "amplify"
    },
    {
        "Rank": 2171,
        "Port": "ampliar",
        "Eng": "enlarge"
    },
    {
        "Rank": 2172,
        "Port": "reservar",
        "Eng": "reserve"
    },
    {
        "Rank": 2173,
        "Port": "ofício",
        "Eng": "occupation"
    },
    {
        "Rank": 2173,
        "Port": "ofício",
        "Eng": "official letter"
    },
    {
        "Rank": 2174,
        "Port": "templo",
        "Eng": "temple"
    },
    {
        "Rank": 2175,
        "Port": "ligeiro",
        "Eng": "light"
    },
    {
        "Rank": 2175,
        "Port": "ligeiro",
        "Eng": "swift"
    },
    {
        "Rank": 2175,
        "Port": "ligeiro",
        "Eng": "quick"
    },
    {
        "Rank": 2175,
        "Port": "ligeiro",
        "Eng": "agile"
    },
    {
        "Rank": 2176,
        "Port": "dezena",
        "Eng": "set of ten"
    },
    {
        "Rank": 2177,
        "Port": "residir",
        "Eng": "live in"
    },
    {
        "Rank": 2177,
        "Port": "residir",
        "Eng": "reside"
    },
    {
        "Rank": 2178,
        "Port": "potencial",
        "Eng": "potential"
    },
    {
        "Rank": 2179,
        "Port": "adiante",
        "Eng": "further along"
    },
    {
        "Rank": 2179,
        "Port": "adiante",
        "Eng": "farther ahead"
    },
    {
        "Rank": 2180,
        "Port": "meta",
        "Eng": "goal"
    },
    {
        "Rank": 2181,
        "Port": "chama",
        "Eng": "flame"
    },
    {
        "Rank": 2182,
        "Port": "disputa",
        "Eng": "dispute"
    },
    {
        "Rank": 2183,
        "Port": "prestígio",
        "Eng": "prestige"
    },
    {
        "Rank": 2184,
        "Port": "passeio",
        "Eng": "walk"
    },
    {
        "Rank": 2184,
        "Port": "passeio",
        "Eng": "stroll"
    },
    {
        "Rank": 2185,
        "Port": "eixo",
        "Eng": "dividing line"
    },
    {
        "Rank": 2185,
        "Port": "eixo",
        "Eng": "axle"
    },
    {
        "Rank": 2185,
        "Port": "eixo",
        "Eng": "axis"
    },
    {
        "Rank": 2186,
        "Port": "bico",
        "Eng": "beak"
    },
    {
        "Rank": 2187,
        "Port": "utilização",
        "Eng": "use"
    },
    {
        "Rank": 2187,
        "Port": "utilização",
        "Eng": "utilization"
    },
    {
        "Rank": 2188,
        "Port": "herança",
        "Eng": "heritage"
    },
    {
        "Rank": 2188,
        "Port": "herança",
        "Eng": "inheritance"
    },
    {
        "Rank": 2189,
        "Port": "contexto",
        "Eng": "context"
    },
    {
        "Rank": 2190,
        "Port": "formal",
        "Eng": "formal"
    },
    {
        "Rank": 2191,
        "Port": "exportação",
        "Eng": "exportation"
    },
    {
        "Rank": 2192,
        "Port": "nuvem",
        "Eng": "cloud"
    },
    {
        "Rank": 2193,
        "Port": "vela",
        "Eng": "candle"
    },
    {
        "Rank": 2193,
        "Port": "vela",
        "Eng": "sail"
    },
    {
        "Rank": 2193,
        "Port": "vela",
        "Eng": "spark-plug"
    },
    {
        "Rank": 2194,
        "Port": "sessenta",
        "Eng": "sixty"
    },
    {
        "Rank": 2195,
        "Port": "especializado",
        "Eng": "specialized"
    },
    {
        "Rank": 2196,
        "Port": "situar",
        "Eng": "situate"
    },
    {
        "Rank": 2197,
        "Port": "preencher",
        "Eng": "fill"
    },
    {
        "Rank": 2198,
        "Port": "galinha",
        "Eng": "hen"
    },
    {
        "Rank": 2198,
        "Port": "galinha",
        "Eng": "chicken"
    },
    {
        "Rank": 2199,
        "Port": "pólo",
        "Eng": "pole"
    },
    {
        "Rank": 2200,
        "Port": "argentino",
        "Eng": "Argentine"
    },
    {
        "Rank": 2201,
        "Port": "rejeitar",
        "Eng": "reject"
    },
    {
        "Rank": 2202,
        "Port": "provisório",
        "Eng": "temporary"
    },
    {
        "Rank": 2202,
        "Port": "provisório",
        "Eng": "provisionary"
    },
    {
        "Rank": 2203,
        "Port": "devolver",
        "Eng": "return"
    },
    {
        "Rank": 2204,
        "Port": "orquestra",
        "Eng": "orchestra"
    },
    {
        "Rank": 2205,
        "Port": "corrupção",
        "Eng": "corruption"
    },
    {
        "Rank": 2206,
        "Port": "gordo",
        "Eng": "fat"
    },
    {
        "Rank": 2206,
        "Port": "gordo",
        "Eng": "thick"
    },
    {
        "Rank": 2207,
        "Port": "acentuar",
        "Eng": "intensify"
    },
    {
        "Rank": 2207,
        "Port": "acentuar",
        "Eng": "accentuate"
    },
    {
        "Rank": 2208,
        "Port": "explosão",
        "Eng": "explosion"
    },
    {
        "Rank": 2209,
        "Port": "terço",
        "Eng": "third"
    },
    {
        "Rank": 2209,
        "Port": "terço",
        "Eng": "a third"
    },
    {
        "Rank": 2210,
        "Port": "efectivamente",
        "Eng": "in fact"
    },
    {
        "Rank": 2210,
        "Port": "efectivamente",
        "Eng": "effectively"
    },
    {
        "Rank": 2211,
        "Port": "incêndio",
        "Eng": "fire"
    },
    {
        "Rank": 2212,
        "Port": "classificar",
        "Eng": "classify"
    },
    {
        "Rank": 2213,
        "Port": "mercadoria",
        "Eng": "merchandise"
    },
    {
        "Rank": 2214,
        "Port": "procedimento",
        "Eng": "procedure"
    },
    {
        "Rank": 2214,
        "Port": "procedimento",
        "Eng": "proceedings"
    },
    {
        "Rank": 2215,
        "Port": "relacionado",
        "Eng": "related"
    },
    {
        "Rank": 2216,
        "Port": "concorrência",
        "Eng": "competition"
    },
    {
        "Rank": 2217,
        "Port": "ocidental",
        "Eng": "Western"
    },
    {
        "Rank": 2217,
        "Port": "ocidental",
        "Eng": "occidental"
    },
    {
        "Rank": 2218,
        "Port": "apertar",
        "Eng": "shake"
    },
    {
        "Rank": 2218,
        "Port": "apertar",
        "Eng": "press"
    },
    {
        "Rank": 2218,
        "Port": "apertar",
        "Eng": "tighten"
    },
    {
        "Rank": 2218,
        "Port": "apertar",
        "Eng": "tie"
    },
    {
        "Rank": 2219,
        "Port": "queixa",
        "Eng": "complaint"
    },
    {
        "Rank": 2220,
        "Port": "tranquilo",
        "Eng": "calm"
    },
    {
        "Rank": 2220,
        "Port": "tranquilo",
        "Eng": "serene"
    },
    {
        "Rank": 2220,
        "Port": "tranquilo",
        "Eng": "tranquil"
    },
    {
        "Rank": 2221,
        "Port": "convicção",
        "Eng": "conviction"
    },
    {
        "Rank": 2222,
        "Port": "habitual",
        "Eng": "familiar"
    },
    {
        "Rank": 2222,
        "Port": "habitual",
        "Eng": "customary"
    },
    {
        "Rank": 2223,
        "Port": "previsto",
        "Eng": "foreseen"
    },
    {
        "Rank": 2224,
        "Port": "levantamento",
        "Eng": "the act of raising; uprising"
    },
    {
        "Rank": 2225,
        "Port": "trânsito",
        "Eng": "traffic"
    },
    {
        "Rank": 2226,
        "Port": "vencedor",
        "Eng": "winner"
    },
    {
        "Rank": 2227,
        "Port": "comerciante",
        "Eng": "business person"
    },
    {
        "Rank": 2227,
        "Port": "comerciante",
        "Eng": "salesper- son"
    },
    {
        "Rank": 2228,
        "Port": "rigor",
        "Eng": "rigor"
    },
    {
        "Rank": 2228,
        "Port": "rigor",
        "Eng": "worst of"
    },
    {
        "Rank": 2229,
        "Port": "correcção",
        "Eng": "correction"
    },
    {
        "Rank": 2230,
        "Port": "sonhar",
        "Eng": "dream"
    },
    {
        "Rank": 2231,
        "Port": "redor",
        "Eng": "all around"
    },
    {
        "Rank": 2232,
        "Port": "prioridade",
        "Eng": "priority"
    },
    {
        "Rank": 2233,
        "Port": "atento",
        "Eng": "alert"
    },
    {
        "Rank": 2233,
        "Port": "atento",
        "Eng": "attentive"
    },
    {
        "Rank": 2234,
        "Port": "chorar",
        "Eng": "cry"
    },
    {
        "Rank": 2235,
        "Port": "humor",
        "Eng": "humor"
    },
    {
        "Rank": 2235,
        "Port": "humor",
        "Eng": "mood"
    },
    {
        "Rank": 2236,
        "Port": "localizar",
        "Eng": "find"
    },
    {
        "Rank": 2236,
        "Port": "localizar",
        "Eng": "pin point the location"
    },
    {
        "Rank": 2237,
        "Port": "divulgação",
        "Eng": "publication"
    },
    {
        "Rank": 2237,
        "Port": "divulgação",
        "Eng": "spread"
    },
    {
        "Rank": 2237,
        "Port": "divulgação",
        "Eng": "reporting"
    },
    {
        "Rank": 2238,
        "Port": "botar",
        "Eng": "put"
    },
    {
        "Rank": 2238,
        "Port": "botar",
        "Eng": "place"
    },
    {
        "Rank": 2239,
        "Port": "farinha",
        "Eng": "flour"
    },
    {
        "Rank": 2240,
        "Port": "sapato",
        "Eng": "shoe"
    },
    {
        "Rank": 2241,
        "Port": "pastor",
        "Eng": "pastor"
    },
    {
        "Rank": 2241,
        "Port": "pastor",
        "Eng": "shepherd"
    },
    {
        "Rank": 2242,
        "Port": "oriental",
        "Eng": "Eastern"
    },
    {
        "Rank": 2242,
        "Port": "oriental",
        "Eng": "oriental"
    },
    {
        "Rank": 2243,
        "Port": "pensão",
        "Eng": "pension"
    },
    {
        "Rank": 2244,
        "Port": "mágico",
        "Eng": "magician"
    },
    {
        "Rank": 2244,
        "Port": "mágico",
        "Eng": "magical"
    },
    {
        "Rank": 2245,
        "Port": "meado",
        "Eng": "middle"
    },
    {
        "Rank": 2245,
        "Port": "meado",
        "Eng": "half-way"
    },
    {
        "Rank": 2246,
        "Port": "coroa",
        "Eng": "crown"
    },
    {
        "Rank": 2247,
        "Port": "infelizmente",
        "Eng": "unfortunately"
    },
    {
        "Rank": 2247,
        "Port": "infelizmente",
        "Eng": "sadly"
    },
    {
        "Rank": 2248,
        "Port": "erguer",
        "Eng": "erect"
    },
    {
        "Rank": 2248,
        "Port": "erguer",
        "Eng": "raise up"
    },
    {
        "Rank": 2248,
        "Port": "erguer",
        "Eng": "support"
    },
    {
        "Rank": 2249,
        "Port": "bocado",
        "Eng": "piece"
    },
    {
        "Rank": 2249,
        "Port": "bocado",
        "Eng": "portion"
    },
    {
        "Rank": 2249,
        "Port": "bocado",
        "Eng": "mouthful"
    },
    {
        "Rank": 2250,
        "Port": "falhar",
        "Eng": "fail"
    },
    {
        "Rank": 2251,
        "Port": "consistir",
        "Eng": "consist of"
    },
    {
        "Rank": 2252,
        "Port": "avisar",
        "Eng": "warn"
    },
    {
        "Rank": 2252,
        "Port": "avisar",
        "Eng": "advise"
    },
    {
        "Rank": 2252,
        "Port": "avisar",
        "Eng": "inform"
    },
    {
        "Rank": 2253,
        "Port": "zero",
        "Eng": "zero"
    },
    {
        "Rank": 2254,
        "Port": "adorar",
        "Eng": "worship"
    },
    {
        "Rank": 2254,
        "Port": "adorar",
        "Eng": "adore"
    },
    {
        "Rank": 2255,
        "Port": "transferência",
        "Eng": "transfer"
    },
    {
        "Rank": 2256,
        "Port": "curiosidade",
        "Eng": "curiosity"
    },
    {
        "Rank": 2257,
        "Port": "contrariar",
        "Eng": "contradict"
    },
    {
        "Rank": 2257,
        "Port": "contrariar",
        "Eng": "disagree"
    },
    {
        "Rank": 2258,
        "Port": "propaganda",
        "Eng": "propaganda"
    },
    {
        "Rank": 2258,
        "Port": "propaganda",
        "Eng": "advertisement"
    },
    {
        "Rank": 2259,
        "Port": "abranger",
        "Eng": "span"
    },
    {
        "Rank": 2259,
        "Port": "abranger",
        "Eng": "encompass"
    },
    {
        "Rank": 2260,
        "Port": "investigar",
        "Eng": "investigate"
    },
    {
        "Rank": 2261,
        "Port": "cruzado",
        "Eng": "old Brazilian monetary unit"
    },
    {
        "Rank": 2261,
        "Port": "cruzado",
        "Eng": "crossed"
    },
    {
        "Rank": 2262,
        "Port": "anjo",
        "Eng": "angel"
    },
    {
        "Rank": 2263,
        "Port": "cobertura",
        "Eng": "covering"
    },
    {
        "Rank": 2264,
        "Port": "oceano",
        "Eng": "ocean"
    },
    {
        "Rank": 2265,
        "Port": "atracção",
        "Eng": "attraction"
    },
    {
        "Rank": 2266,
        "Port": "habitação",
        "Eng": "home"
    },
    {
        "Rank": 2266,
        "Port": "habitação",
        "Eng": "dwelling place"
    },
    {
        "Rank": 2266,
        "Port": "habitação",
        "Eng": "habitat"
    },
    {
        "Rank": 2267,
        "Port": "psicológico",
        "Eng": "psychological"
    },
    {
        "Rank": 2268,
        "Port": "passe",
        "Eng": "assist"
    },
    {
        "Rank": 2268,
        "Port": "passe",
        "Eng": "pass"
    },
    {
        "Rank": 2269,
        "Port": "consciente",
        "Eng": "conscious"
    },
    {
        "Rank": 2269,
        "Port": "consciente",
        "Eng": "aware"
    },
    {
        "Rank": 2270,
        "Port": "postura",
        "Eng": "position"
    },
    {
        "Rank": 2270,
        "Port": "postura",
        "Eng": "posture"
    },
    {
        "Rank": 2270,
        "Port": "postura",
        "Eng": "attitude"
    },
    {
        "Rank": 2271,
        "Port": "pó",
        "Eng": "powder"
    },
    {
        "Rank": 2271,
        "Port": "pó",
        "Eng": "dust"
    },
    {
        "Rank": 2272,
        "Port": "relacionamento",
        "Eng": "relationship"
    },
    {
        "Rank": 2273,
        "Port": "substância",
        "Eng": "substance"
    },
    {
        "Rank": 2274,
        "Port": "cientista",
        "Eng": "scientist"
    },
    {
        "Rank": 2275,
        "Port": "jornalismo",
        "Eng": "journalism"
    },
    {
        "Rank": 2276,
        "Port": "devido",
        "Eng": "due to"
    },
    {
        "Rank": 2276,
        "Port": "devido",
        "Eng": "owing to"
    },
    {
        "Rank": 2277,
        "Port": "resumir",
        "Eng": "summarize"
    },
    {
        "Rank": 2277,
        "Port": "resumir",
        "Eng": "sum up"
    },
    {
        "Rank": 2278,
        "Port": "nomeado",
        "Eng": "nominee"
    },
    {
        "Rank": 2278,
        "Port": "nomeado",
        "Eng": "nominated"
    },
    {
        "Rank": 2279,
        "Port": "previsão",
        "Eng": "forecast"
    },
    {
        "Rank": 2279,
        "Port": "previsão",
        "Eng": "prediction"
    },
    {
        "Rank": 2280,
        "Port": "espiritual",
        "Eng": "spiritual"
    },
    {
        "Rank": 2281,
        "Port": "sonoro",
        "Eng": "pertaining to sound"
    },
    {
        "Rank": 2282,
        "Port": "recuar",
        "Eng": "retreat"
    },
    {
        "Rank": 2282,
        "Port": "recuar",
        "Eng": "draw back"
    },
    {
        "Rank": 2283,
        "Port": "assinado",
        "Eng": "signed"
    },
    {
        "Rank": 2284,
        "Port": "requerer",
        "Eng": "require"
    },
    {
        "Rank": 2285,
        "Port": "pregar",
        "Eng": "preach"
    },
    {
        "Rank": 2285,
        "Port": "pregar",
        "Eng": "nail"
    },
    {
        "Rank": 2286,
        "Port": "defeito",
        "Eng": "shortcoming"
    },
    {
        "Rank": 2286,
        "Port": "defeito",
        "Eng": "defect"
    },
    {
        "Rank": 2287,
        "Port": "sofrimento",
        "Eng": "suffering"
    },
    {
        "Rank": 2287,
        "Port": "sofrimento",
        "Eng": "pain"
    },
    {
        "Rank": 2288,
        "Port": "determinação",
        "Eng": "determination"
    },
    {
        "Rank": 2289,
        "Port": "efectivo",
        "Eng": "effective"
    },
    {
        "Rank": 2289,
        "Port": "efectivo",
        "Eng": "strength"
    },
    {
        "Rank": 2289,
        "Port": "efectivo",
        "Eng": "assets"
    },
    {
        "Rank": 2290,
        "Port": "global",
        "Eng": "global"
    },
    {
        "Rank": 2291,
        "Port": "marco",
        "Eng": "landmark"
    },
    {
        "Rank": 2291,
        "Port": "marco",
        "Eng": "boundary"
    },
    {
        "Rank": 2292,
        "Port": "financiamento",
        "Eng": "financing"
    },
    {
        "Rank": 2293,
        "Port": "registro",
        "Eng": "record"
    },
    {
        "Rank": 2294,
        "Port": "típico",
        "Eng": "typical"
    },
    {
        "Rank": 2294,
        "Port": "típico",
        "Eng": "characteristic"
    },
    {
        "Rank": 2295,
        "Port": "solicitar",
        "Eng": "solicit"
    },
    {
        "Rank": 2296,
        "Port": "académico",
        "Eng": "academic"
    },
    {
        "Rank": 2297,
        "Port": "compositor",
        "Eng": "composer"
    },
    {
        "Rank": 2298,
        "Port": "frequente",
        "Eng": "common"
    },
    {
        "Rank": 2298,
        "Port": "frequente",
        "Eng": "frequent"
    },
    {
        "Rank": 2299,
        "Port": "residência",
        "Eng": "residence"
    },
    {
        "Rank": 2300,
        "Port": "matriz",
        "Eng": "mould"
    },
    {
        "Rank": 2300,
        "Port": "matriz",
        "Eng": "mother"
    },
    {
        "Rank": 2301,
        "Port": "oriente",
        "Eng": "East"
    },
    {
        "Rank": 2301,
        "Port": "oriente",
        "Eng": "Orient"
    },
    {
        "Rank": 2302,
        "Port": "colorido",
        "Eng": "colored"
    },
    {
        "Rank": 2302,
        "Port": "colorido",
        "Eng": "colorful"
    },
    {
        "Rank": 2303,
        "Port": "arquivo",
        "Eng": "archive"
    },
    {
        "Rank": 2304,
        "Port": "rocha",
        "Eng": "large rock"
    },
    {
        "Rank": 2305,
        "Port": "indicação",
        "Eng": "indication"
    },
    {
        "Rank": 2306,
        "Port": "escutar",
        "Eng": "listen"
    },
    {
        "Rank": 2307,
        "Port": "agradar",
        "Eng": "please"
    },
    {
        "Rank": 2308,
        "Port": "associar",
        "Eng": "associate"
    },
    {
        "Rank": 2309,
        "Port": "continuidade",
        "Eng": "continuity"
    },
    {
        "Rank": 2310,
        "Port": "liderar",
        "Eng": "lead"
    },
    {
        "Rank": 2311,
        "Port": "obstáculo",
        "Eng": "obstacle"
    },
    {
        "Rank": 2312,
        "Port": "autorizar",
        "Eng": "authorize"
    },
    {
        "Rank": 2313,
        "Port": "destruição",
        "Eng": "destruction"
    },
    {
        "Rank": 2314,
        "Port": "britânico",
        "Eng": "British"
    },
    {
        "Rank": 2315,
        "Port": "correspondente",
        "Eng": "corresponding"
    },
    {
        "Rank": 2315,
        "Port": "correspondente",
        "Eng": "correspondent"
    },
    {
        "Rank": 2316,
        "Port": "matemática",
        "Eng": "mathematics"
    },
    {
        "Rank": 2317,
        "Port": "encarregar",
        "Eng": "put in charge of"
    },
    {
        "Rank": 2317,
        "Port": "encarregar",
        "Eng": "entrust"
    },
    {
        "Rank": 2318,
        "Port": "enganar",
        "Eng": "trick"
    },
    {
        "Rank": 2318,
        "Port": "enganar",
        "Eng": "deceive"
    },
    {
        "Rank": 2319,
        "Port": "desfazer",
        "Eng": "undo"
    },
    {
        "Rank": 2320,
        "Port": "suspender",
        "Eng": "suspend"
    },
    {
        "Rank": 2321,
        "Port": "aguardar",
        "Eng": "await"
    },
    {
        "Rank": 2322,
        "Port": "dirigente",
        "Eng": "leader"
    },
    {
        "Rank": 2322,
        "Port": "dirigente",
        "Eng": "director"
    },
    {
        "Rank": 2322,
        "Port": "dirigente",
        "Eng": "directing"
    },
    {
        "Rank": 2323,
        "Port": "rodear",
        "Eng": "surround"
    },
    {
        "Rank": 2324,
        "Port": "palmeira",
        "Eng": "palm tree"
    },
    {
        "Rank": 2325,
        "Port": "macho",
        "Eng": "male"
    },
    {
        "Rank": 2325,
        "Port": "macho",
        "Eng": "masculine"
    },
    {
        "Rank": 2326,
        "Port": "beneficiar",
        "Eng": "benefit"
    },
    {
        "Rank": 2327,
        "Port": "privilégio",
        "Eng": "privilege"
    },
    {
        "Rank": 2328,
        "Port": "inteiramente",
        "Eng": "entirely"
    },
    {
        "Rank": 2328,
        "Port": "inteiramente",
        "Eng": "wholly"
    },
    {
        "Rank": 2329,
        "Port": "destinar",
        "Eng": "be geared to"
    },
    {
        "Rank": 2329,
        "Port": "destinar",
        "Eng": "earmarked for"
    },
    {
        "Rank": 2330,
        "Port": "lidar",
        "Eng": "deal with"
    },
    {
        "Rank": 2330,
        "Port": "lidar",
        "Eng": "treat"
    },
    {
        "Rank": 2331,
        "Port": "desconhecer",
        "Eng": "not know"
    },
    {
        "Rank": 2331,
        "Port": "desconhecer",
        "Eng": "ignore"
    },
    {
        "Rank": 2332,
        "Port": "basear",
        "Eng": "base"
    },
    {
        "Rank": 2333,
        "Port": "evoluir",
        "Eng": "evolve"
    },
    {
        "Rank": 2334,
        "Port": "íntimo",
        "Eng": "intimate"
    },
    {
        "Rank": 2334,
        "Port": "íntimo",
        "Eng": "close friend"
    },
    {
        "Rank": 2335,
        "Port": "comboio",
        "Eng": "train"
    },
    {
        "Rank": 2336,
        "Port": "repente",
        "Eng": "suddenly"
    },
    {
        "Rank": 2337,
        "Port": "marcado",
        "Eng": "set"
    },
    {
        "Rank": 2337,
        "Port": "marcado",
        "Eng": "marked"
    },
    {
        "Rank": 2338,
        "Port": "rock",
        "Eng": "rock music"
    },
    {
        "Rank": 2339,
        "Port": "criminoso",
        "Eng": "criminal"
    },
    {
        "Rank": 2340,
        "Port": "candidatura",
        "Eng": "candidacy"
    },
    {
        "Rank": 2341,
        "Port": "lixo",
        "Eng": "trash"
    },
    {
        "Rank": 2341,
        "Port": "lixo",
        "Eng": "waste"
    },
    {
        "Rank": 2341,
        "Port": "lixo",
        "Eng": "garbage"
    },
    {
        "Rank": 2342,
        "Port": "cozinha",
        "Eng": "kitchen"
    },
    {
        "Rank": 2343,
        "Port": "agrário",
        "Eng": "agrarian"
    },
    {
        "Rank": 2344,
        "Port": "consultar",
        "Eng": "look up"
    },
    {
        "Rank": 2344,
        "Port": "consultar",
        "Eng": "consult"
    },
    {
        "Rank": 2345,
        "Port": "bebida",
        "Eng": "drink"
    },
    {
        "Rank": 2346,
        "Port": "carvão",
        "Eng": "coal"
    },
    {
        "Rank": 2347,
        "Port": "partidário",
        "Eng": "party member"
    },
    {
        "Rank": 2347,
        "Port": "partidário",
        "Eng": "relating to a political party"
    },
    {
        "Rank": 2348,
        "Port": "festival",
        "Eng": "festival"
    },
    {
        "Rank": 2349,
        "Port": "celebrar",
        "Eng": "celebrate"
    },
    {
        "Rank": 2350,
        "Port": "judiciário",
        "Eng": "judicial"
    },
    {
        "Rank": 2350,
        "Port": "judiciário",
        "Eng": "judiciary"
    },
    {
        "Rank": 2351,
        "Port": "arquitecto",
        "Eng": "architect"
    },
    {
        "Rank": 2352,
        "Port": "exclusivo",
        "Eng": "exclusive"
    },
    {
        "Rank": 2353,
        "Port": "numeroso",
        "Eng": "numerous"
    },
    {
        "Rank": 2354,
        "Port": "cruzar",
        "Eng": "cross"
    },
    {
        "Rank": 2355,
        "Port": "designar",
        "Eng": "designate"
    },
    {
        "Rank": 2356,
        "Port": "espectador",
        "Eng": "spectator"
    },
    {
        "Rank": 2357,
        "Port": "neve",
        "Eng": "snow"
    },
    {
        "Rank": 2358,
        "Port": "recuperação",
        "Eng": "recovery"
    },
    {
        "Rank": 2358,
        "Port": "recuperação",
        "Eng": "recuperation"
    },
    {
        "Rank": 2359,
        "Port": "ilusão",
        "Eng": "illusion"
    },
    {
        "Rank": 2360,
        "Port": "denúncia",
        "Eng": "accusation"
    },
    {
        "Rank": 2360,
        "Port": "denúncia",
        "Eng": "denunciation"
    },
    {
        "Rank": 2361,
        "Port": "destaque",
        "Eng": "prominence"
    },
    {
        "Rank": 2361,
        "Port": "destaque",
        "Eng": "distinction"
    },
    {
        "Rank": 2362,
        "Port": "planejamento",
        "Eng": "planning"
    },
    {
        "Rank": 2363,
        "Port": "campeão",
        "Eng": "champion"
    },
    {
        "Rank": 2364,
        "Port": "confessar",
        "Eng": "confess"
    },
    {
        "Rank": 2365,
        "Port": "projecção",
        "Eng": "projection"
    },
    {
        "Rank": 2366,
        "Port": "integração",
        "Eng": "integration"
    },
    {
        "Rank": 2367,
        "Port": "amiga",
        "Eng": "female friend"
    },
    {
        "Rank": 2368,
        "Port": "madrugada",
        "Eng": "early morning"
    },
    {
        "Rank": 2369,
        "Port": "avô",
        "Eng": "grandfather"
    },
    {
        "Rank": 2370,
        "Port": "restaurante",
        "Eng": "restaurant"
    },
    {
        "Rank": 2371,
        "Port": "descartar",
        "Eng": "disagree"
    },
    {
        "Rank": 2371,
        "Port": "descartar",
        "Eng": "deny"
    },
    {
        "Rank": 2371,
        "Port": "descartar",
        "Eng": "get rid of"
    },
    {
        "Rank": 2372,
        "Port": "captar",
        "Eng": "attract"
    },
    {
        "Rank": 2372,
        "Port": "captar",
        "Eng": "capture"
    },
    {
        "Rank": 2373,
        "Port": "sacrifício",
        "Eng": "sacrifice"
    },
    {
        "Rank": 2374,
        "Port": "ditadura",
        "Eng": "dictatorship"
    },
    {
        "Rank": 2375,
        "Port": "fila",
        "Eng": "line"
    },
    {
        "Rank": 2375,
        "Port": "fila",
        "Eng": "row"
    },
    {
        "Rank": 2375,
        "Port": "fila",
        "Eng": "series"
    },
    {
        "Rank": 2376,
        "Port": "largar",
        "Eng": "release"
    },
    {
        "Rank": 2376,
        "Port": "largar",
        "Eng": "get rid of"
    },
    {
        "Rank": 2377,
        "Port": "situado",
        "Eng": "located"
    },
    {
        "Rank": 2377,
        "Port": "situado",
        "Eng": "situated"
    },
    {
        "Rank": 2378,
        "Port": "imperador",
        "Eng": "emperor"
    },
    {
        "Rank": 2379,
        "Port": "reproduzir",
        "Eng": "reproduce"
    },
    {
        "Rank": 2380,
        "Port": "presidir",
        "Eng": "preside"
    },
    {
        "Rank": 2381,
        "Port": "receio",
        "Eng": "fear"
    },
    {
        "Rank": 2381,
        "Port": "receio",
        "Eng": "apprehension"
    },
    {
        "Rank": 2382,
        "Port": "poético",
        "Eng": "poetic"
    },
    {
        "Rank": 2383,
        "Port": "servidor",
        "Eng": "servant"
    },
    {
        "Rank": 2383,
        "Port": "servidor",
        "Eng": "server"
    },
    {
        "Rank": 2384,
        "Port": "primeiro-ministro",
        "Eng": "prime minister"
    },
    {
        "Rank": 2385,
        "Port": "almoço",
        "Eng": "lunch"
    },
    {
        "Rank": 2386,
        "Port": "piloto",
        "Eng": "pilot"
    },
    {
        "Rank": 2387,
        "Port": "tesouro",
        "Eng": "treasury"
    },
    {
        "Rank": 2387,
        "Port": "tesouro",
        "Eng": "treasure"
    },
    {
        "Rank": 2388,
        "Port": "fresco",
        "Eng": "cool"
    },
    {
        "Rank": 2388,
        "Port": "fresco",
        "Eng": "fresh"
    },
    {
        "Rank": 2389,
        "Port": "contraste",
        "Eng": "contrast"
    },
    {
        "Rank": 2390,
        "Port": "assinatura",
        "Eng": "signature"
    },
    {
        "Rank": 2390,
        "Port": "assinatura",
        "Eng": "subscription"
    },
    {
        "Rank": 2391,
        "Port": "álbum",
        "Eng": "album"
    },
    {
        "Rank": 2392,
        "Port": "válido",
        "Eng": "valid"
    },
    {
        "Rank": 2393,
        "Port": "apagar",
        "Eng": "turn off"
    },
    {
        "Rank": 2393,
        "Port": "apagar",
        "Eng": "erase"
    },
    {
        "Rank": 2394,
        "Port": "chá",
        "Eng": "tea"
    },
    {
        "Rank": 2395,
        "Port": "relatar",
        "Eng": "narrate"
    },
    {
        "Rank": 2395,
        "Port": "relatar",
        "Eng": "relate"
    },
    {
        "Rank": 2396,
        "Port": "agravar",
        "Eng": "worsen"
    },
    {
        "Rank": 2396,
        "Port": "agravar",
        "Eng": "aggravate"
    },
    {
        "Rank": 2397,
        "Port": "prefeito",
        "Eng": "mayor"
    },
    {
        "Rank": 2398,
        "Port": "pormenor",
        "Eng": "detail"
    },
    {
        "Rank": 2399,
        "Port": "esgotar",
        "Eng": "exhaust"
    },
    {
        "Rank": 2399,
        "Port": "esgotar",
        "Eng": "deplete"
    },
    {
        "Rank": 2400,
        "Port": "derivar",
        "Eng": "derive"
    },
    {
        "Rank": 2401,
        "Port": "correspondência",
        "Eng": "mail"
    },
    {
        "Rank": 2401,
        "Port": "correspondência",
        "Eng": "correspondence"
    },
    {
        "Rank": 2402,
        "Port": "autonomia",
        "Eng": "autonomy"
    },
    {
        "Rank": 2402,
        "Port": "autonomia",
        "Eng": "self-sufficiency"
    },
    {
        "Rank": 2403,
        "Port": "incapaz",
        "Eng": "incapable"
    },
    {
        "Rank": 2404,
        "Port": "célebre",
        "Eng": "famous"
    },
    {
        "Rank": 2404,
        "Port": "célebre",
        "Eng": "renowned"
    },
    {
        "Rank": 2404,
        "Port": "célebre",
        "Eng": "celebrity"
    },
    {
        "Rank": 2405,
        "Port": "circuito",
        "Eng": "circuit"
    },
    {
        "Rank": 2406,
        "Port": "teatral",
        "Eng": "theatrical"
    },
    {
        "Rank": 2407,
        "Port": "dispensar",
        "Eng": "dismiss"
    },
    {
        "Rank": 2407,
        "Port": "dispensar",
        "Eng": "give up"
    },
    {
        "Rank": 2407,
        "Port": "dispensar",
        "Eng": "dispense"
    },
    {
        "Rank": 2408,
        "Port": "eventual",
        "Eng": "eventual"
    },
    {
        "Rank": 2409,
        "Port": "tinta",
        "Eng": "paint"
    },
    {
        "Rank": 2409,
        "Port": "tinta",
        "Eng": "ink"
    },
    {
        "Rank": 2410,
        "Port": "aquecer",
        "Eng": "heat"
    },
    {
        "Rank": 2411,
        "Port": "cristal",
        "Eng": "crystal"
    },
    {
        "Rank": 2412,
        "Port": "bolso",
        "Eng": "pocket"
    },
    {
        "Rank": 2413,
        "Port": "examinar",
        "Eng": "examine"
    },
    {
        "Rank": 2414,
        "Port": "virgem",
        "Eng": "virgin"
    },
    {
        "Rank": 2415,
        "Port": "fama",
        "Eng": "fame"
    },
    {
        "Rank": 2415,
        "Port": "fama",
        "Eng": "reputation"
    },
    {
        "Rank": 2416,
        "Port": "semelhança",
        "Eng": "resemblance"
    },
    {
        "Rank": 2416,
        "Port": "semelhança",
        "Eng": "similarity"
    },
    {
        "Rank": 2417,
        "Port": "alimentação",
        "Eng": "nourishment"
    },
    {
        "Rank": 2417,
        "Port": "alimentação",
        "Eng": "food"
    },
    {
        "Rank": 2418,
        "Port": "confronto",
        "Eng": "confrontation"
    },
    {
        "Rank": 2419,
        "Port": "transição",
        "Eng": "transition"
    },
    {
        "Rank": 2420,
        "Port": "patrão",
        "Eng": "boss"
    },
    {
        "Rank": 2420,
        "Port": "patrão",
        "Eng": "business owner"
    },
    {
        "Rank": 2421,
        "Port": "desportivo",
        "Eng": "athletic"
    },
    {
        "Rank": 2421,
        "Port": "desportivo",
        "Eng": "sports"
    },
    {
        "Rank": 2422,
        "Port": "depositar",
        "Eng": "deposit"
    },
    {
        "Rank": 2423,
        "Port": "mérito",
        "Eng": "merit"
    },
    {
        "Rank": 2424,
        "Port": "testemunha",
        "Eng": "witness"
    },
    {
        "Rank": 2425,
        "Port": "disparar",
        "Eng": "fire"
    },
    {
        "Rank": 2426,
        "Port": "autorização",
        "Eng": "authorization"
    },
    {
        "Rank": 2427,
        "Port": "treinar",
        "Eng": "train"
    },
    {
        "Rank": 2428,
        "Port": "oeste",
        "Eng": "West"
    },
    {
        "Rank": 2429,
        "Port": "lateral",
        "Eng": "outfielder"
    },
    {
        "Rank": 2429,
        "Port": "lateral",
        "Eng": "sideline"
    },
    {
        "Rank": 2429,
        "Port": "lateral",
        "Eng": "lateral"
    },
    {
        "Rank": 2430,
        "Port": "porte",
        "Eng": "size"
    },
    {
        "Rank": 2430,
        "Port": "porte",
        "Eng": "price"
    },
    {
        "Rank": 2430,
        "Port": "porte",
        "Eng": "fare"
    },
    {
        "Rank": 2431,
        "Port": "obrigatório",
        "Eng": "mandatory"
    },
    {
        "Rank": 2431,
        "Port": "obrigatório",
        "Eng": "obligatory"
    },
    {
        "Rank": 2432,
        "Port": "vinda",
        "Eng": "arrival"
    },
    {
        "Rank": 2432,
        "Port": "vinda",
        "Eng": "coming"
    },
    {
        "Rank": 2433,
        "Port": "profundamente",
        "Eng": "profoundly"
    },
    {
        "Rank": 2433,
        "Port": "profundamente",
        "Eng": "deeply"
    },
    {
        "Rank": 2434,
        "Port": "classificação",
        "Eng": "classification"
    },
    {
        "Rank": 2435,
        "Port": "chapéu",
        "Eng": "hat"
    },
    {
        "Rank": 2436,
        "Port": "manifesto",
        "Eng": "manifesto"
    },
    {
        "Rank": 2436,
        "Port": "manifesto",
        "Eng": "manifested"
    },
    {
        "Rank": 2437,
        "Port": "são",
        "Eng": "sound"
    },
    {
        "Rank": 2437,
        "Port": "são",
        "Eng": "safe"
    },
    {
        "Rank": 2438,
        "Port": "firme",
        "Eng": "firm"
    },
    {
        "Rank": 2439,
        "Port": "cruzeiro",
        "Eng": "cruise"
    },
    {
        "Rank": 2439,
        "Port": "cruzeiro",
        "Eng": "old Brazilian coin"
    },
    {
        "Rank": 2440,
        "Port": "embaixador",
        "Eng": "ambassador"
    },
    {
        "Rank": 2441,
        "Port": "algodão",
        "Eng": "cotton"
    },
    {
        "Rank": 2442,
        "Port": "censura",
        "Eng": "censorship"
    },
    {
        "Rank": 2442,
        "Port": "censura",
        "Eng": "censure"
    },
    {
        "Rank": 2443,
        "Port": "guia",
        "Eng": "guide"
    },
    {
        "Rank": 2444,
        "Port": "oficina",
        "Eng": "shop"
    },
    {
        "Rank": 2444,
        "Port": "oficina",
        "Eng": "workshop"
    },
    {
        "Rank": 2445,
        "Port": "sucessivo",
        "Eng": "successive"
    },
    {
        "Rank": 2446,
        "Port": "cheiro",
        "Eng": "smell"
    },
    {
        "Rank": 2446,
        "Port": "cheiro",
        "Eng": "odor"
    },
    {
        "Rank": 2447,
        "Port": "plantar",
        "Eng": "plant"
    },
    {
        "Rank": 2448,
        "Port": "seminário",
        "Eng": "seminary"
    },
    {
        "Rank": 2448,
        "Port": "seminário",
        "Eng": "seminar"
    },
    {
        "Rank": 2449,
        "Port": "finanças",
        "Eng": "finance"
    },
    {
        "Rank": 2450,
        "Port": "convenção",
        "Eng": "convention"
    },
    {
        "Rank": 2451,
        "Port": "harmonia",
        "Eng": "harmony"
    },
    {
        "Rank": 2452,
        "Port": "anual",
        "Eng": "yearly"
    },
    {
        "Rank": 2452,
        "Port": "anual",
        "Eng": "annual"
    },
    {
        "Rank": 2453,
        "Port": "sobrevivência",
        "Eng": "survival"
    },
    {
        "Rank": 2454,
        "Port": "girar",
        "Eng": "spin"
    },
    {
        "Rank": 2454,
        "Port": "girar",
        "Eng": "turn"
    },
    {
        "Rank": 2454,
        "Port": "girar",
        "Eng": "rotate"
    },
    {
        "Rank": 2455,
        "Port": "violar",
        "Eng": "violate"
    },
    {
        "Rank": 2455,
        "Port": "violar",
        "Eng": "rape"
    },
    {
        "Rank": 2456,
        "Port": "tradução",
        "Eng": "translation"
    },
    {
        "Rank": 2457,
        "Port": "carteira",
        "Eng": "wallet"
    },
    {
        "Rank": 2458,
        "Port": "habitar",
        "Eng": "inhabit"
    },
    {
        "Rank": 2459,
        "Port": "adaptação",
        "Eng": "adaptation"
    },
    {
        "Rank": 2460,
        "Port": "exprimir",
        "Eng": "express"
    },
    {
        "Rank": 2461,
        "Port": "miúdo",
        "Eng": "scrawny"
    },
    {
        "Rank": 2461,
        "Port": "miúdo",
        "Eng": "small"
    },
    {
        "Rank": 2461,
        "Port": "miúdo",
        "Eng": "child"
    },
    {
        "Rank": 2462,
        "Port": "gasolina",
        "Eng": "gasoline"
    },
    {
        "Rank": 2463,
        "Port": "aço",
        "Eng": "steel"
    },
    {
        "Rank": 2464,
        "Port": "litoral",
        "Eng": "coast"
    },
    {
        "Rank": 2464,
        "Port": "litoral",
        "Eng": "coastal"
    },
    {
        "Rank": 2465,
        "Port": "acertar",
        "Eng": "be right on"
    },
    {
        "Rank": 2465,
        "Port": "acertar",
        "Eng": "hit the mark"
    },
    {
        "Rank": 2466,
        "Port": "bancário",
        "Eng": "banker"
    },
    {
        "Rank": 2467,
        "Port": "rodar",
        "Eng": "spin"
    },
    {
        "Rank": 2467,
        "Port": "rodar",
        "Eng": "turn around"
    },
    {
        "Rank": 2467,
        "Port": "rodar",
        "Eng": "roll"
    },
    {
        "Rank": 2468,
        "Port": "carnaval",
        "Eng": "carnival"
    },
    {
        "Rank": 2468,
        "Port": "carnaval",
        "Eng": "mardi gras"
    },
    {
        "Rank": 2469,
        "Port": "vergonha",
        "Eng": "shame"
    },
    {
        "Rank": 2470,
        "Port": "triunfo",
        "Eng": "victory"
    },
    {
        "Rank": 2470,
        "Port": "triunfo",
        "Eng": "triumph"
    },
    {
        "Rank": 2471,
        "Port": "aderir",
        "Eng": "adhere"
    },
    {
        "Rank": 2471,
        "Port": "aderir",
        "Eng": "join"
    },
    {
        "Rank": 2472,
        "Port": "estímulo",
        "Eng": "stimulus"
    },
    {
        "Rank": 2472,
        "Port": "estímulo",
        "Eng": "stimulant"
    },
    {
        "Rank": 2473,
        "Port": "preservar",
        "Eng": "preserve"
    },
    {
        "Rank": 2474,
        "Port": "turismo",
        "Eng": "tourism"
    },
    {
        "Rank": 2475,
        "Port": "tribo",
        "Eng": "tribe"
    },
    {
        "Rank": 2476,
        "Port": "admirar",
        "Eng": "admire"
    },
    {
        "Rank": 2477,
        "Port": "inicialmente",
        "Eng": "at first"
    },
    {
        "Rank": 2477,
        "Port": "inicialmente",
        "Eng": "initially"
    },
    {
        "Rank": 2478,
        "Port": "publicidade",
        "Eng": "advertising"
    },
    {
        "Rank": 2478,
        "Port": "publicidade",
        "Eng": "publicity"
    },
    {
        "Rank": 2479,
        "Port": "frequentemente",
        "Eng": "frequently"
    },
    {
        "Rank": 2480,
        "Port": "indicador",
        "Eng": "indicator"
    },
    {
        "Rank": 2480,
        "Port": "indicador",
        "Eng": "indicating"
    },
    {
        "Rank": 2481,
        "Port": "preocupado",
        "Eng": "worried"
    },
    {
        "Rank": 2481,
        "Port": "preocupado",
        "Eng": "preoccupied"
    },
    {
        "Rank": 2482,
        "Port": "prata",
        "Eng": "silver"
    },
    {
        "Rank": 2483,
        "Port": "eterno",
        "Eng": "eternal"
    },
    {
        "Rank": 2484,
        "Port": "variação",
        "Eng": "fluctuation"
    },
    {
        "Rank": 2484,
        "Port": "variação",
        "Eng": "variation"
    },
    {
        "Rank": 2485,
        "Port": "profundidade",
        "Eng": "depth"
    },
    {
        "Rank": 2486,
        "Port": "ovelha",
        "Eng": "sheep"
    },
    {
        "Rank": 2487,
        "Port": "delicado",
        "Eng": "delicate"
    },
    {
        "Rank": 2488,
        "Port": "enfermeiro",
        "Eng": "nurse"
    },
    {
        "Rank": 2489,
        "Port": "definitivamente",
        "Eng": "definitively"
    },
    {
        "Rank": 2490,
        "Port": "calar",
        "Eng": "be or keep quiet"
    },
    {
        "Rank": 2490,
        "Port": "calar",
        "Eng": "shut up"
    },
    {
        "Rank": 2491,
        "Port": "circulação",
        "Eng": "circulation"
    },
    {
        "Rank": 2492,
        "Port": "célula",
        "Eng": "cell"
    },
    {
        "Rank": 2493,
        "Port": "providência",
        "Eng": "providence"
    },
    {
        "Rank": 2493,
        "Port": "providência",
        "Eng": "welfare"
    },
    {
        "Rank": 2494,
        "Port": "sexto",
        "Eng": "sixth"
    },
    {
        "Rank": 2495,
        "Port": "transmissão",
        "Eng": "transmission"
    },
    {
        "Rank": 2496,
        "Port": "maravilhoso",
        "Eng": "marvelous"
    },
    {
        "Rank": 2496,
        "Port": "maravilhoso",
        "Eng": "wonderful"
    },
    {
        "Rank": 2497,
        "Port": "presidencial",
        "Eng": "presidential"
    },
    {
        "Rank": 2498,
        "Port": "legítimo",
        "Eng": "legitimate"
    },
    {
        "Rank": 2499,
        "Port": "escolar",
        "Eng": "relating to school"
    },
    {
        "Rank": 2500,
        "Port": "colónia",
        "Eng": "colony"
    },
    {
        "Rank": 2501,
        "Port": "melhoria",
        "Eng": "improvement"
    },
    {
        "Rank": 2501,
        "Port": "melhoria",
        "Eng": "benefit"
    },
    {
        "Rank": 2502,
        "Port": "eficaz",
        "Eng": "effective"
    },
    {
        "Rank": 2503,
        "Port": "bilhão",
        "Eng": "billion"
    },
    {
        "Rank": 2504,
        "Port": "sabor",
        "Eng": "taste"
    },
    {
        "Rank": 2504,
        "Port": "sabor",
        "Eng": "flavor"
    },
    {
        "Rank": 2505,
        "Port": "movimentar",
        "Eng": "move"
    },
    {
        "Rank": 2506,
        "Port": "cego",
        "Eng": "blind"
    },
    {
        "Rank": 2507,
        "Port": "reportagem",
        "Eng": "news report"
    },
    {
        "Rank": 2508,
        "Port": "batata",
        "Eng": "potato"
    },
    {
        "Rank": 2509,
        "Port": "primavera",
        "Eng": "Spring"
    },
    {
        "Rank": 2510,
        "Port": "índice",
        "Eng": "index"
    },
    {
        "Rank": 2510,
        "Port": "índice",
        "Eng": "rate"
    },
    {
        "Rank": 2511,
        "Port": "revelação",
        "Eng": "revelation"
    },
    {
        "Rank": 2511,
        "Port": "revelação",
        "Eng": "development"
    },
    {
        "Rank": 2512,
        "Port": "amante",
        "Eng": "lover"
    },
    {
        "Rank": 2512,
        "Port": "amante",
        "Eng": "mistress"
    },
    {
        "Rank": 2513,
        "Port": "pátria",
        "Eng": "homeland"
    },
    {
        "Rank": 2513,
        "Port": "pátria",
        "Eng": "native country"
    },
    {
        "Rank": 2514,
        "Port": "tabela",
        "Eng": "table"
    },
    {
        "Rank": 2514,
        "Port": "tabela",
        "Eng": "chart"
    },
    {
        "Rank": 2515,
        "Port": "demissão",
        "Eng": "dismissal"
    },
    {
        "Rank": 2515,
        "Port": "demissão",
        "Eng": "resignation"
    },
    {
        "Rank": 2516,
        "Port": "rebelde",
        "Eng": "rebel"
    },
    {
        "Rank": 2516,
        "Port": "rebelde",
        "Eng": "rebellious"
    },
    {
        "Rank": 2517,
        "Port": "gráfico",
        "Eng": "graph"
    },
    {
        "Rank": 2517,
        "Port": "gráfico",
        "Eng": "graphic"
    },
    {
        "Rank": 2518,
        "Port": "aeroporto",
        "Eng": "airport"
    },
    {
        "Rank": 2519,
        "Port": "franco",
        "Eng": "honest"
    },
    {
        "Rank": 2519,
        "Port": "franco",
        "Eng": "frank"
    },
    {
        "Rank": 2520,
        "Port": "improvisar",
        "Eng": "improvise"
    },
    {
        "Rank": 2521,
        "Port": "excluir",
        "Eng": "exclude"
    },
    {
        "Rank": 2522,
        "Port": "manual",
        "Eng": "manual"
    },
    {
        "Rank": 2523,
        "Port": "prestação",
        "Eng": "installment"
    },
    {
        "Rank": 2524,
        "Port": "aproximação",
        "Eng": "act of getting closer"
    },
    {
        "Rank": 2524,
        "Port": "aproximação",
        "Eng": "approximation"
    },
    {
        "Rank": 2525,
        "Port": "ombro",
        "Eng": "shoulder"
    },
    {
        "Rank": 2526,
        "Port": "aguentar",
        "Eng": "bear"
    },
    {
        "Rank": 2526,
        "Port": "aguentar",
        "Eng": "withstand"
    },
    {
        "Rank": 2526,
        "Port": "aguentar",
        "Eng": "stand"
    },
    {
        "Rank": 2527,
        "Port": "provável",
        "Eng": "probable"
    },
    {
        "Rank": 2528,
        "Port": "químico",
        "Eng": "chemical"
    },
    {
        "Rank": 2528,
        "Port": "químico",
        "Eng": "chemist"
    },
    {
        "Rank": 2529,
        "Port": "terror",
        "Eng": "terror"
    },
    {
        "Rank": 2530,
        "Port": "fumo",
        "Eng": "smoke"
    },
    {
        "Rank": 2531,
        "Port": "efectuar",
        "Eng": "put into action"
    },
    {
        "Rank": 2531,
        "Port": "efectuar",
        "Eng": "take place"
    },
    {
        "Rank": 2531,
        "Port": "efectuar",
        "Eng": "perform"
    },
    {
        "Rank": 2532,
        "Port": "cansado",
        "Eng": "tired"
    },
    {
        "Rank": 2533,
        "Port": "traçar",
        "Eng": "set"
    },
    {
        "Rank": 2533,
        "Port": "traçar",
        "Eng": "plan"
    },
    {
        "Rank": 2533,
        "Port": "traçar",
        "Eng": "trace"
    },
    {
        "Rank": 2534,
        "Port": "assalto",
        "Eng": "assault"
    },
    {
        "Rank": 2535,
        "Port": "feijão",
        "Eng": "bean"
    },
    {
        "Rank": 2536,
        "Port": "abalar",
        "Eng": "shake"
    },
    {
        "Rank": 2536,
        "Port": "abalar",
        "Eng": "rock back and forth"
    },
    {
        "Rank": 2537,
        "Port": "monetário",
        "Eng": "monetary"
    },
    {
        "Rank": 2538,
        "Port": "prático",
        "Eng": "practical"
    },
    {
        "Rank": 2539,
        "Port": "agradável",
        "Eng": "pleasant"
    },
    {
        "Rank": 2539,
        "Port": "agradável",
        "Eng": "pleasing"
    },
    {
        "Rank": 2540,
        "Port": "estágio",
        "Eng": "stage"
    },
    {
        "Rank": 2540,
        "Port": "estágio",
        "Eng": "internship"
    },
    {
        "Rank": 2541,
        "Port": "celular",
        "Eng": "cellular"
    },
    {
        "Rank": 2542,
        "Port": "retorno",
        "Eng": "return"
    },
    {
        "Rank": 2543,
        "Port": "favorecer",
        "Eng": "favor"
    },
    {
        "Rank": 2544,
        "Port": "divino",
        "Eng": "divine"
    },
    {
        "Rank": 2545,
        "Port": "falecer",
        "Eng": "pass away"
    },
    {
        "Rank": 2545,
        "Port": "falecer",
        "Eng": "die"
    },
    {
        "Rank": 2546,
        "Port": "guiar",
        "Eng": "guide"
    },
    {
        "Rank": 2546,
        "Port": "guiar",
        "Eng": "lead"
    },
    {
        "Rank": 2547,
        "Port": "sobrar",
        "Eng": "remain"
    },
    {
        "Rank": 2547,
        "Port": "sobrar",
        "Eng": "be left"
    },
    {
        "Rank": 2548,
        "Port": "major",
        "Eng": "major"
    },
    {
        "Rank": 2549,
        "Port": "oração",
        "Eng": "prayer"
    },
    {
        "Rank": 2549,
        "Port": "oração",
        "Eng": "clause"
    },
    {
        "Rank": 2550,
        "Port": "alegar",
        "Eng": "allege"
    },
    {
        "Rank": 2551,
        "Port": "taça",
        "Eng": "glass"
    },
    {
        "Rank": 2552,
        "Port": "motorista",
        "Eng": "driver"
    },
    {
        "Rank": 2553,
        "Port": "restrição",
        "Eng": "restriction"
    },
    {
        "Rank": 2554,
        "Port": "árabe",
        "Eng": "Arab"
    },
    {
        "Rank": 2554,
        "Port": "árabe",
        "Eng": "Arabic"
    },
    {
        "Rank": 2555,
        "Port": "modificação",
        "Eng": "change"
    },
    {
        "Rank": 2555,
        "Port": "modificação",
        "Eng": "modification"
    },
    {
        "Rank": 2556,
        "Port": "porquê",
        "Eng": "why"
    },
    {
        "Rank": 2556,
        "Port": "porquê",
        "Eng": "for what reason"
    },
    {
        "Rank": 2557,
        "Port": "inevitável",
        "Eng": "inevitable"
    },
    {
        "Rank": 2557,
        "Port": "inevitável",
        "Eng": "unavoidable"
    },
    {
        "Rank": 2558,
        "Port": "orgulho",
        "Eng": "pride"
    },
    {
        "Rank": 2559,
        "Port": "financiar",
        "Eng": "fund"
    },
    {
        "Rank": 2559,
        "Port": "financiar",
        "Eng": "finance"
    },
    {
        "Rank": 2560,
        "Port": "adesão",
        "Eng": "admission"
    },
    {
        "Rank": 2560,
        "Port": "adesão",
        "Eng": "enlistment"
    },
    {
        "Rank": 2561,
        "Port": "esquerdo",
        "Eng": "left"
    },
    {
        "Rank": 2562,
        "Port": "arriscar",
        "Eng": "risk"
    },
    {
        "Rank": 2563,
        "Port": "reduzido",
        "Eng": "reduced"
    },
    {
        "Rank": 2563,
        "Port": "reduzido",
        "Eng": "small"
    },
    {
        "Rank": 2564,
        "Port": "evidentemente",
        "Eng": "evidently"
    },
    {
        "Rank": 2565,
        "Port": "audiência",
        "Eng": "hearing"
    },
    {
        "Rank": 2565,
        "Port": "audiência",
        "Eng": "audience"
    },
    {
        "Rank": 2566,
        "Port": "editora",
        "Eng": "publishing house"
    },
    {
        "Rank": 2567,
        "Port": "cooperação",
        "Eng": "cooperation"
    },
    {
        "Rank": 2568,
        "Port": "lar",
        "Eng": "home"
    },
    {
        "Rank": 2569,
        "Port": "partilhar",
        "Eng": "share"
    },
    {
        "Rank": 2569,
        "Port": "partilhar",
        "Eng": "divide among"
    },
    {
        "Rank": 2570,
        "Port": "enterrar",
        "Eng": "bury"
    },
    {
        "Rank": 2571,
        "Port": "guerreiro",
        "Eng": "warrior"
    },
    {
        "Rank": 2572,
        "Port": "inspiração",
        "Eng": "inspiration"
    },
    {
        "Rank": 2573,
        "Port": "filósofo",
        "Eng": "philosopher"
    },
    {
        "Rank": 2574,
        "Port": "forçado",
        "Eng": "obligated"
    },
    {
        "Rank": 2574,
        "Port": "forçado",
        "Eng": "forced"
    },
    {
        "Rank": 2575,
        "Port": "resíduo",
        "Eng": "remains"
    },
    {
        "Rank": 2575,
        "Port": "resíduo",
        "Eng": "residue"
    },
    {
        "Rank": 2575,
        "Port": "resíduo",
        "Eng": "residual"
    },
    {
        "Rank": 2576,
        "Port": "temporada",
        "Eng": "season"
    },
    {
        "Rank": 2576,
        "Port": "temporada",
        "Eng": "period"
    },
    {
        "Rank": 2577,
        "Port": "expulsar",
        "Eng": "expel"
    },
    {
        "Rank": 2578,
        "Port": "empurrar",
        "Eng": "push"
    },
    {
        "Rank": 2579,
        "Port": "mergulhar",
        "Eng": "submerge"
    },
    {
        "Rank": 2579,
        "Port": "mergulhar",
        "Eng": "dive"
    },
    {
        "Rank": 2580,
        "Port": "sono",
        "Eng": "sleep"
    },
    {
        "Rank": 2581,
        "Port": "ópera",
        "Eng": "opera"
    },
    {
        "Rank": 2582,
        "Port": "entrega",
        "Eng": "delivery"
    },
    {
        "Rank": 2583,
        "Port": "apelo",
        "Eng": "appeal"
    },
    {
        "Rank": 2584,
        "Port": "magistrado",
        "Eng": "magistrate"
    },
    {
        "Rank": 2585,
        "Port": "bota",
        "Eng": "boot"
    },
    {
        "Rank": 2586,
        "Port": "aparência",
        "Eng": "appearance"
    },
    {
        "Rank": 2587,
        "Port": "suporte",
        "Eng": "support"
    },
    {
        "Rank": 2588,
        "Port": "transferir",
        "Eng": "transfer"
    },
    {
        "Rank": 2589,
        "Port": "republicano",
        "Eng": "republican"
    },
    {
        "Rank": 2590,
        "Port": "cavaleiro",
        "Eng": "rider"
    },
    {
        "Rank": 2590,
        "Port": "cavaleiro",
        "Eng": "horseman"
    },
    {
        "Rank": 2590,
        "Port": "cavaleiro",
        "Eng": "knight"
    },
    {
        "Rank": 2591,
        "Port": "quebra",
        "Eng": "decrease"
    },
    {
        "Rank": 2591,
        "Port": "quebra",
        "Eng": "break"
    },
    {
        "Rank": 2591,
        "Port": "quebra",
        "Eng": "fracture"
    },
    {
        "Rank": 2592,
        "Port": "atendimento",
        "Eng": "care"
    },
    {
        "Rank": 2592,
        "Port": "atendimento",
        "Eng": "service"
    },
    {
        "Rank": 2593,
        "Port": "parcela",
        "Eng": "portion"
    },
    {
        "Rank": 2593,
        "Port": "parcela",
        "Eng": "parcel"
    },
    {
        "Rank": 2593,
        "Port": "parcela",
        "Eng": "segment"
    },
    {
        "Rank": 2594,
        "Port": "constatar",
        "Eng": "notice"
    },
    {
        "Rank": 2594,
        "Port": "constatar",
        "Eng": "realize"
    },
    {
        "Rank": 2595,
        "Port": "gelo",
        "Eng": "ice"
    },
    {
        "Rank": 2596,
        "Port": "orelha",
        "Eng": "ear"
    },
    {
        "Rank": 2597,
        "Port": "rígido",
        "Eng": "rigid"
    },
    {
        "Rank": 2597,
        "Port": "rígido",
        "Eng": "strict"
    },
    {
        "Rank": 2598,
        "Port": "dupla",
        "Eng": "pair"
    },
    {
        "Rank": 2598,
        "Port": "dupla",
        "Eng": "set of two"
    },
    {
        "Rank": 2599,
        "Port": "marítimo",
        "Eng": "maritime"
    },
    {
        "Rank": 2600,
        "Port": "digital",
        "Eng": "digital"
    },
    {
        "Rank": 2601,
        "Port": "inflação",
        "Eng": "inflation"
    },
    {
        "Rank": 2602,
        "Port": "aconselhar",
        "Eng": "counsel"
    },
    {
        "Rank": 2603,
        "Port": "limpeza",
        "Eng": "cleaning"
    },
    {
        "Rank": 2603,
        "Port": "limpeza",
        "Eng": "cleanliness"
    },
    {
        "Rank": 2604,
        "Port": "dependência",
        "Eng": "dependency"
    },
    {
        "Rank": 2605,
        "Port": "ética",
        "Eng": "ethics"
    },
    {
        "Rank": 2606,
        "Port": "grandeza",
        "Eng": "greatness"
    },
    {
        "Rank": 2606,
        "Port": "grandeza",
        "Eng": "amplitude"
    },
    {
        "Rank": 2607,
        "Port": "intensidade",
        "Eng": "intensity"
    },
    {
        "Rank": 2608,
        "Port": "descansar",
        "Eng": "rest"
    },
    {
        "Rank": 2609,
        "Port": "incidente",
        "Eng": "incident"
    },
    {
        "Rank": 2610,
        "Port": "solar",
        "Eng": "solar"
    },
    {
        "Rank": 2610,
        "Port": "solar",
        "Eng": "sole"
    },
    {
        "Rank": 2610,
        "Port": "solar",
        "Eng": "manor house"
    },
    {
        "Rank": 2611,
        "Port": "trem",
        "Eng": "train"
    },
    {
        "Rank": 2612,
        "Port": "renovar",
        "Eng": "renew"
    },
    {
        "Rank": 2613,
        "Port": "assassinato",
        "Eng": "murder"
    },
    {
        "Rank": 2613,
        "Port": "assassinato",
        "Eng": "assassination"
    },
    {
        "Rank": 2614,
        "Port": "colonial",
        "Eng": "colonial"
    },
    {
        "Rank": 2615,
        "Port": "estrutural",
        "Eng": "structural"
    },
    {
        "Rank": 2616,
        "Port": "separação",
        "Eng": "separation"
    },
    {
        "Rank": 2617,
        "Port": "incomodar",
        "Eng": "inconvenience"
    },
    {
        "Rank": 2617,
        "Port": "incomodar",
        "Eng": "bother"
    },
    {
        "Rank": 2618,
        "Port": "decreto",
        "Eng": "decree"
    },
    {
        "Rank": 2619,
        "Port": "planeamento",
        "Eng": "planning"
    },
    {
        "Rank": 2619,
        "Port": "planeamento",
        "Eng": "scheduling"
    },
    {
        "Rank": 2620,
        "Port": "tonelada",
        "Eng": "ton"
    },
    {
        "Rank": 2621,
        "Port": "essência",
        "Eng": "essence"
    },
    {
        "Rank": 2622,
        "Port": "holandês",
        "Eng": "Dutch"
    },
    {
        "Rank": 2623,
        "Port": "lição",
        "Eng": "lesson"
    },
    {
        "Rank": 2624,
        "Port": "relacionar",
        "Eng": "equate"
    },
    {
        "Rank": 2624,
        "Port": "relacionar",
        "Eng": "relate"
    },
    {
        "Rank": 2625,
        "Port": "infinito",
        "Eng": "infinite"
    },
    {
        "Rank": 2625,
        "Port": "infinito",
        "Eng": "infinity"
    },
    {
        "Rank": 2626,
        "Port": "mosteiro",
        "Eng": "monastery"
    },
    {
        "Rank": 2627,
        "Port": "voluntário",
        "Eng": "voluntary"
    },
    {
        "Rank": 2627,
        "Port": "voluntário",
        "Eng": "volunteer"
    },
    {
        "Rank": 2628,
        "Port": "ângulo",
        "Eng": "angle"
    },
    {
        "Rank": 2629,
        "Port": "intensivo",
        "Eng": "intensive"
    },
    {
        "Rank": 2630,
        "Port": "gritar",
        "Eng": "yell"
    },
    {
        "Rank": 2630,
        "Port": "gritar",
        "Eng": "shout"
    },
    {
        "Rank": 2631,
        "Port": "auxílio",
        "Eng": "help"
    },
    {
        "Rank": 2631,
        "Port": "auxílio",
        "Eng": "aid"
    },
    {
        "Rank": 2631,
        "Port": "auxílio",
        "Eng": "service"
    },
    {
        "Rank": 2632,
        "Port": "debater",
        "Eng": "debate"
    },
    {
        "Rank": 2632,
        "Port": "debater",
        "Eng": "discuss"
    },
    {
        "Rank": 2633,
        "Port": "ribeira",
        "Eng": "stream"
    },
    {
        "Rank": 2633,
        "Port": "ribeira",
        "Eng": "riverbank"
    },
    {
        "Rank": 2634,
        "Port": "sobrinho",
        "Eng": "nephew"
    },
    {
        "Rank": 2635,
        "Port": "gigante",
        "Eng": "giant"
    },
    {
        "Rank": 2636,
        "Port": "esporte",
        "Eng": "sport"
    },
    {
        "Rank": 2637,
        "Port": "gravidade",
        "Eng": "severity"
    },
    {
        "Rank": 2637,
        "Port": "gravidade",
        "Eng": "gravity"
    },
    {
        "Rank": 2638,
        "Port": "laranja",
        "Eng": "orange"
    },
    {
        "Rank": 2639,
        "Port": "saudade",
        "Eng": "longing"
    },
    {
        "Rank": 2639,
        "Port": "saudade",
        "Eng": "nostalgia"
    },
    {
        "Rank": 2640,
        "Port": "monumento",
        "Eng": "monument"
    },
    {
        "Rank": 2641,
        "Port": "senso",
        "Eng": "sense"
    },
    {
        "Rank": 2642,
        "Port": "lindo",
        "Eng": "beautiful"
    },
    {
        "Rank": 2643,
        "Port": "apartamento",
        "Eng": "apartment"
    },
    {
        "Rank": 2644,
        "Port": "incorporar",
        "Eng": "incorporate"
    },
    {
        "Rank": 2645,
        "Port": "diabo",
        "Eng": "devil"
    },
    {
        "Rank": 2646,
        "Port": "luxo",
        "Eng": "luxury"
    },
    {
        "Rank": 2647,
        "Port": "indígena",
        "Eng": "indigenous"
    },
    {
        "Rank": 2648,
        "Port": "distinção",
        "Eng": "distinction"
    },
    {
        "Rank": 2649,
        "Port": "somar",
        "Eng": "add up"
    },
    {
        "Rank": 2649,
        "Port": "somar",
        "Eng": "sum up"
    },
    {
        "Rank": 2650,
        "Port": "visual",
        "Eng": "visual"
    },
    {
        "Rank": 2651,
        "Port": "produtivo",
        "Eng": "productive"
    },
    {
        "Rank": 2652,
        "Port": "duzentos",
        "Eng": "two hundred"
    },
    {
        "Rank": 2653,
        "Port": "comédia",
        "Eng": "comedy"
    },
    {
        "Rank": 2654,
        "Port": "inédito",
        "Eng": "unpublished"
    },
    {
        "Rank": 2655,
        "Port": "sertão",
        "Eng": "arid and remote interior region"
    },
    {
        "Rank": 2656,
        "Port": "fluxo",
        "Eng": "flux"
    },
    {
        "Rank": 2656,
        "Port": "fluxo",
        "Eng": "flow"
    },
    {
        "Rank": 2657,
        "Port": "traseiro",
        "Eng": "rear"
    },
    {
        "Rank": 2657,
        "Port": "traseiro",
        "Eng": "bottom"
    },
    {
        "Rank": 2658,
        "Port": "véspera",
        "Eng": "eve"
    },
    {
        "Rank": 2658,
        "Port": "véspera",
        "Eng": "night before"
    },
    {
        "Rank": 2659,
        "Port": "prefeitura",
        "Eng": "municipal government"
    },
    {
        "Rank": 2660,
        "Port": "planalto",
        "Eng": "plateau"
    },
    {
        "Rank": 2661,
        "Port": "anel",
        "Eng": "ring"
    },
    {
        "Rank": 2662,
        "Port": "inferno",
        "Eng": "hell"
    },
    {
        "Rank": 2662,
        "Port": "inferno",
        "Eng": "inferno"
    },
    {
        "Rank": 2663,
        "Port": "herdeiro",
        "Eng": "heir"
    },
    {
        "Rank": 2664,
        "Port": "ferramenta",
        "Eng": "tool"
    },
    {
        "Rank": 2665,
        "Port": "aquisição",
        "Eng": "acquisition"
    },
    {
        "Rank": 2666,
        "Port": "anúncio",
        "Eng": "announcement"
    },
    {
        "Rank": 2667,
        "Port": "fantasia",
        "Eng": "fantasy"
    },
    {
        "Rank": 2668,
        "Port": "localizado",
        "Eng": "located"
    },
    {
        "Rank": 2669,
        "Port": "namorado",
        "Eng": "boyfriend"
    },
    {
        "Rank": 2669,
        "Port": "namorado",
        "Eng": "girlfriend"
    },
    {
        "Rank": 2670,
        "Port": "grito",
        "Eng": "shout"
    },
    {
        "Rank": 2670,
        "Port": "grito",
        "Eng": "scream"
    },
    {
        "Rank": 2670,
        "Port": "grito",
        "Eng": "yell"
    },
    {
        "Rank": 2671,
        "Port": "queixar",
        "Eng": "complain"
    },
    {
        "Rank": 2672,
        "Port": "fundador",
        "Eng": "founder"
    },
    {
        "Rank": 2672,
        "Port": "fundador",
        "Eng": "founding"
    },
    {
        "Rank": 2673,
        "Port": "firmar",
        "Eng": "sign"
    },
    {
        "Rank": 2673,
        "Port": "firmar",
        "Eng": "settle"
    },
    {
        "Rank": 2673,
        "Port": "firmar",
        "Eng": "fix"
    },
    {
        "Rank": 2674,
        "Port": "precioso",
        "Eng": "precious"
    },
    {
        "Rank": 2675,
        "Port": "briga",
        "Eng": "fight"
    },
    {
        "Rank": 2675,
        "Port": "briga",
        "Eng": "quarrel"
    },
    {
        "Rank": 2676,
        "Port": "inquérito",
        "Eng": "survey"
    },
    {
        "Rank": 2676,
        "Port": "inquérito",
        "Eng": "inquiry"
    },
    {
        "Rank": 2677,
        "Port": "horta",
        "Eng": "vegetable garden"
    },
    {
        "Rank": 2678,
        "Port": "multiplicar",
        "Eng": "multiply"
    },
    {
        "Rank": 2679,
        "Port": "insecto",
        "Eng": "insect"
    },
    {
        "Rank": 2680,
        "Port": "inaugurar",
        "Eng": "inaugurate"
    },
    {
        "Rank": 2680,
        "Port": "inaugurar",
        "Eng": "start"
    },
    {
        "Rank": 2681,
        "Port": "ambiental",
        "Eng": "environmental"
    },
    {
        "Rank": 2682,
        "Port": "patente",
        "Eng": "obvious"
    },
    {
        "Rank": 2682,
        "Port": "patente",
        "Eng": "patent"
    },
    {
        "Rank": 2682,
        "Port": "patente",
        "Eng": "rank"
    },
    {
        "Rank": 2683,
        "Port": "viúva",
        "Eng": "widow"
    },
    {
        "Rank": 2684,
        "Port": "editor",
        "Eng": "editor"
    },
    {
        "Rank": 2685,
        "Port": "dignidade",
        "Eng": "dignity"
    },
    {
        "Rank": 2685,
        "Port": "dignidade",
        "Eng": "worthiness"
    },
    {
        "Rank": 2686,
        "Port": "elaborar",
        "Eng": "create"
    },
    {
        "Rank": 2686,
        "Port": "elaborar",
        "Eng": "elaborate"
    },
    {
        "Rank": 2687,
        "Port": "lance",
        "Eng": "glance"
    },
    {
        "Rank": 2687,
        "Port": "lance",
        "Eng": "throw"
    },
    {
        "Rank": 2687,
        "Port": "lance",
        "Eng": "slap"
    },
    {
        "Rank": 2688,
        "Port": "manobra",
        "Eng": "maneuver"
    },
    {
        "Rank": 2689,
        "Port": "tronco",
        "Eng": "trunk"
    },
    {
        "Rank": 2690,
        "Port": "apostar",
        "Eng": "bet"
    },
    {
        "Rank": 2690,
        "Port": "apostar",
        "Eng": "wager"
    },
    {
        "Rank": 2691,
        "Port": "praga",
        "Eng": "plague"
    },
    {
        "Rank": 2692,
        "Port": "escada",
        "Eng": "stair"
    },
    {
        "Rank": 2692,
        "Port": "escada",
        "Eng": "staircase"
    },
    {
        "Rank": 2693,
        "Port": "sucessão",
        "Eng": "series"
    },
    {
        "Rank": 2693,
        "Port": "sucessão",
        "Eng": "succession"
    },
    {
        "Rank": 2694,
        "Port": "fortuna",
        "Eng": "fortune"
    },
    {
        "Rank": 2695,
        "Port": "fotógrafo",
        "Eng": "photographer"
    },
    {
        "Rank": 2696,
        "Port": "segmento",
        "Eng": "segment"
    },
    {
        "Rank": 2697,
        "Port": "singular",
        "Eng": "singular"
    },
    {
        "Rank": 2698,
        "Port": "balança",
        "Eng": "scales"
    },
    {
        "Rank": 2699,
        "Port": "modalidade",
        "Eng": "way"
    },
    {
        "Rank": 2699,
        "Port": "modalidade",
        "Eng": "form"
    },
    {
        "Rank": 2700,
        "Port": "cigarro",
        "Eng": "cigarette"
    },
    {
        "Rank": 2701,
        "Port": "garoto",
        "Eng": "young boy"
    },
    {
        "Rank": 2702,
        "Port": "arranjo",
        "Eng": "arrangement"
    },
    {
        "Rank": 2703,
        "Port": "promoção",
        "Eng": "promotion"
    },
    {
        "Rank": 2703,
        "Port": "promoção",
        "Eng": "sale"
    },
    {
        "Rank": 2704,
        "Port": "garrafa",
        "Eng": "bottle"
    },
    {
        "Rank": 2705,
        "Port": "identificação",
        "Eng": "identification"
    },
    {
        "Rank": 2706,
        "Port": "bordo",
        "Eng": "aboard"
    },
    {
        "Rank": 2707,
        "Port": "calças",
        "Eng": "pants"
    },
    {
        "Rank": 2707,
        "Port": "calças",
        "Eng": "trousers"
    },
    {
        "Rank": 2708,
        "Port": "simultaneamente",
        "Eng": "simultaneously"
    },
    {
        "Rank": 2709,
        "Port": "circular",
        "Eng": "circular"
    },
    {
        "Rank": 2709,
        "Port": "circular",
        "Eng": "shuttle"
    },
    {
        "Rank": 2710,
        "Port": "estátua",
        "Eng": "statue"
    },
    {
        "Rank": 2711,
        "Port": "proximidade",
        "Eng": "nearness"
    },
    {
        "Rank": 2711,
        "Port": "proximidade",
        "Eng": "proximity"
    },
    {
        "Rank": 2712,
        "Port": "chover",
        "Eng": "rain"
    },
    {
        "Rank": 2713,
        "Port": "elaborado",
        "Eng": "planned"
    },
    {
        "Rank": 2713,
        "Port": "elaborado",
        "Eng": "created"
    },
    {
        "Rank": 2713,
        "Port": "elaborado",
        "Eng": "mapped out"
    },
    {
        "Rank": 2714,
        "Port": "desvio",
        "Eng": "detour"
    },
    {
        "Rank": 2714,
        "Port": "desvio",
        "Eng": "redirection"
    },
    {
        "Rank": 2715,
        "Port": "salientar",
        "Eng": "highlight"
    },
    {
        "Rank": 2715,
        "Port": "salientar",
        "Eng": "point out"
    },
    {
        "Rank": 2716,
        "Port": "formular",
        "Eng": "formulate"
    },
    {
        "Rank": 2717,
        "Port": "herdar",
        "Eng": "inherit"
    },
    {
        "Rank": 2718,
        "Port": "dominante",
        "Eng": "dominant"
    },
    {
        "Rank": 2719,
        "Port": "carioca",
        "Eng": "From Rio de Janeiro"
    },
    {
        "Rank": 2720,
        "Port": "passear",
        "Eng": "go for a walk or stroll"
    },
    {
        "Rank": 2721,
        "Port": "tanque",
        "Eng": "tank"
    },
    {
        "Rank": 2722,
        "Port": "autónomo",
        "Eng": "autonomous"
    },
    {
        "Rank": 2722,
        "Port": "autónomo",
        "Eng": "self-employed"
    },
    {
        "Rank": 2723,
        "Port": "comunitário",
        "Eng": "of the community"
    },
    {
        "Rank": 2724,
        "Port": "competente",
        "Eng": "competent"
    },
    {
        "Rank": 2725,
        "Port": "horror",
        "Eng": "horror"
    },
    {
        "Rank": 2725,
        "Port": "horror",
        "Eng": "fear"
    },
    {
        "Rank": 2726,
        "Port": "alheio",
        "Eng": "belonging to someone else"
    },
    {
        "Rank": 2726,
        "Port": "alheio",
        "Eng": "alien"
    },
    {
        "Rank": 2727,
        "Port": "acolher",
        "Eng": "welcome"
    },
    {
        "Rank": 2727,
        "Port": "acolher",
        "Eng": "shelter"
    },
    {
        "Rank": 2728,
        "Port": "duque",
        "Eng": "duke"
    },
    {
        "Rank": 2729,
        "Port": "entusiasmo",
        "Eng": "enthusiasm"
    },
    {
        "Rank": 2730,
        "Port": "foco",
        "Eng": "focus"
    },
    {
        "Rank": 2730,
        "Port": "foco",
        "Eng": "epicenter"
    },
    {
        "Rank": 2731,
        "Port": "cercar",
        "Eng": "surround"
    },
    {
        "Rank": 2732,
        "Port": "evidência",
        "Eng": "evidence"
    },
    {
        "Rank": 2733,
        "Port": "variedade",
        "Eng": "variety"
    },
    {
        "Rank": 2734,
        "Port": "rotina",
        "Eng": "routine"
    },
    {
        "Rank": 2735,
        "Port": "cobra",
        "Eng": "snake"
    },
    {
        "Rank": 2736,
        "Port": "militante",
        "Eng": "party member"
    },
    {
        "Rank": 2736,
        "Port": "militante",
        "Eng": "militant"
    },
    {
        "Rank": 2737,
        "Port": "subsídio",
        "Eng": "subsidy"
    },
    {
        "Rank": 2738,
        "Port": "ambição",
        "Eng": "ambition"
    },
    {
        "Rank": 2739,
        "Port": "processar",
        "Eng": "process"
    },
    {
        "Rank": 2739,
        "Port": "processar",
        "Eng": "sue"
    },
    {
        "Rank": 2740,
        "Port": "pacote",
        "Eng": "package"
    },
    {
        "Rank": 2740,
        "Port": "pacote",
        "Eng": "packet"
    },
    {
        "Rank": 2740,
        "Port": "pacote",
        "Eng": "bundle"
    },
    {
        "Rank": 2741,
        "Port": "músculo",
        "Eng": "muscle"
    },
    {
        "Rank": 2742,
        "Port": "assistente",
        "Eng": "assistant"
    },
    {
        "Rank": 2743,
        "Port": "lama",
        "Eng": "mud"
    },
    {
        "Rank": 2744,
        "Port": "primitivo",
        "Eng": "primitive"
    },
    {
        "Rank": 2745,
        "Port": "fêmea",
        "Eng": "female"
    },
    {
        "Rank": 2746,
        "Port": "razoável",
        "Eng": "reasonable"
    },
    {
        "Rank": 2747,
        "Port": "pata",
        "Eng": "hoof"
    },
    {
        "Rank": 2747,
        "Port": "pata",
        "Eng": "paw"
    },
    {
        "Rank": 2747,
        "Port": "pata",
        "Eng": "foot"
    },
    {
        "Rank": 2748,
        "Port": "raramente",
        "Eng": "seldom"
    },
    {
        "Rank": 2748,
        "Port": "raramente",
        "Eng": "rarely"
    },
    {
        "Rank": 2749,
        "Port": "vocação",
        "Eng": "vocation"
    },
    {
        "Rank": 2750,
        "Port": "coberto",
        "Eng": "covered"
    },
    {
        "Rank": 2751,
        "Port": "veia",
        "Eng": "vein"
    },
    {
        "Rank": 2752,
        "Port": "ladrão",
        "Eng": "thief"
    },
    {
        "Rank": 2753,
        "Port": "pavilhão",
        "Eng": "pavilion"
    },
    {
        "Rank": 2754,
        "Port": "chance",
        "Eng": "chance"
    },
    {
        "Rank": 2755,
        "Port": "usuário",
        "Eng": "user"
    },
    {
        "Rank": 2755,
        "Port": "usuário",
        "Eng": "consumer"
    },
    {
        "Rank": 2756,
        "Port": "camponês",
        "Eng": "peasant"
    },
    {
        "Rank": 2756,
        "Port": "camponês",
        "Eng": "field-worker"
    },
    {
        "Rank": 2756,
        "Port": "camponês",
        "Eng": "farmer"
    },
    {
        "Rank": 2756,
        "Port": "camponês",
        "Eng": "rustic"
    },
    {
        "Rank": 2757,
        "Port": "fundamento",
        "Eng": "basis"
    },
    {
        "Rank": 2757,
        "Port": "fundamento",
        "Eng": "foundation"
    },
    {
        "Rank": 2758,
        "Port": "gozar",
        "Eng": "enjoy"
    },
    {
        "Rank": 2758,
        "Port": "gozar",
        "Eng": "take pleasure"
    },
    {
        "Rank": 2759,
        "Port": "protestar",
        "Eng": "protest"
    },
    {
        "Rank": 2760,
        "Port": "topo",
        "Eng": "top"
    },
    {
        "Rank": 2761,
        "Port": "essencialmente",
        "Eng": "essentially"
    },
    {
        "Rank": 2762,
        "Port": "argumentar",
        "Eng": "argue"
    },
    {
        "Rank": 2763,
        "Port": "estádio",
        "Eng": "stadium"
    },
    {
        "Rank": 2763,
        "Port": "estádio",
        "Eng": "stage"
    },
    {
        "Rank": 2764,
        "Port": "aniversário",
        "Eng": "anniversary"
    },
    {
        "Rank": 2764,
        "Port": "aniversário",
        "Eng": "birthday"
    },
    {
        "Rank": 2765,
        "Port": "cerveja",
        "Eng": "beer"
    },
    {
        "Rank": 2766,
        "Port": "cana",
        "Eng": "sugar cane"
    },
    {
        "Rank": 2767,
        "Port": "soviético",
        "Eng": "soviet"
    },
    {
        "Rank": 2768,
        "Port": "rigoroso",
        "Eng": "rigorous"
    },
    {
        "Rank": 2769,
        "Port": "cinza",
        "Eng": "ashes"
    },
    {
        "Rank": 2770,
        "Port": "copo",
        "Eng": "glass"
    },
    {
        "Rank": 2770,
        "Port": "copo",
        "Eng": "cup"
    },
    {
        "Rank": 2771,
        "Port": "pretensão",
        "Eng": "pretense"
    },
    {
        "Rank": 2771,
        "Port": "pretensão",
        "Eng": "demand"
    },
    {
        "Rank": 2772,
        "Port": "rota",
        "Eng": "route"
    },
    {
        "Rank": 2773,
        "Port": "dependente",
        "Eng": "dependent"
    },
    {
        "Rank": 2774,
        "Port": "agricultor",
        "Eng": "farmer"
    },
    {
        "Rank": 2775,
        "Port": "morador",
        "Eng": "resident"
    },
    {
        "Rank": 2775,
        "Port": "morador",
        "Eng": "inhabitant"
    },
    {
        "Rank": 2776,
        "Port": "mentira",
        "Eng": "lie"
    },
    {
        "Rank": 2777,
        "Port": "ocidente",
        "Eng": "west"
    },
    {
        "Rank": 2778,
        "Port": "anteriormente",
        "Eng": "previously"
    },
    {
        "Rank": 2779,
        "Port": "vulgar",
        "Eng": "common"
    },
    {
        "Rank": 2779,
        "Port": "vulgar",
        "Eng": "average"
    },
    {
        "Rank": 2779,
        "Port": "vulgar",
        "Eng": "vulgar"
    },
    {
        "Rank": 2780,
        "Port": "articulação",
        "Eng": "joint"
    },
    {
        "Rank": 2780,
        "Port": "articulação",
        "Eng": "articulation"
    },
    {
        "Rank": 2781,
        "Port": "jornada",
        "Eng": "workday"
    },
    {
        "Rank": 2781,
        "Port": "jornada",
        "Eng": "journey"
    },
    {
        "Rank": 2781,
        "Port": "jornada",
        "Eng": "round"
    },
    {
        "Rank": 2782,
        "Port": "relato",
        "Eng": "account"
    },
    {
        "Rank": 2782,
        "Port": "relato",
        "Eng": "report"
    },
    {
        "Rank": 2783,
        "Port": "penetrar",
        "Eng": "penetrate"
    },
    {
        "Rank": 2784,
        "Port": "calma",
        "Eng": "peace"
    },
    {
        "Rank": 2784,
        "Port": "calma",
        "Eng": "calm"
    },
    {
        "Rank": 2785,
        "Port": "frango",
        "Eng": "young chicken"
    },
    {
        "Rank": 2785,
        "Port": "frango",
        "Eng": "cooked chicken"
    },
    {
        "Rank": 2786,
        "Port": "concretizar",
        "Eng": "come to pass"
    },
    {
        "Rank": 2786,
        "Port": "concretizar",
        "Eng": "bring about"
    },
    {
        "Rank": 2787,
        "Port": "riso",
        "Eng": "laughter"
    },
    {
        "Rank": 2788,
        "Port": "administrar",
        "Eng": "manage"
    },
    {
        "Rank": 2788,
        "Port": "administrar",
        "Eng": "administer"
    },
    {
        "Rank": 2789,
        "Port": "camisa",
        "Eng": "shirt"
    },
    {
        "Rank": 2790,
        "Port": "multidão",
        "Eng": "masses"
    },
    {
        "Rank": 2790,
        "Port": "multidão",
        "Eng": "multitude"
    },
    {
        "Rank": 2791,
        "Port": "judicial",
        "Eng": "judicial"
    },
    {
        "Rank": 2792,
        "Port": "rosa",
        "Eng": "rose"
    },
    {
        "Rank": 2792,
        "Port": "rosa",
        "Eng": "pink"
    },
    {
        "Rank": 2793,
        "Port": "compensação",
        "Eng": "compensation"
    },
    {
        "Rank": 2794,
        "Port": "filosófico",
        "Eng": "philosophical"
    },
    {
        "Rank": 2795,
        "Port": "nariz",
        "Eng": "nose"
    },
    {
        "Rank": 2796,
        "Port": "dinâmica",
        "Eng": "change"
    },
    {
        "Rank": 2796,
        "Port": "dinâmica",
        "Eng": "dynamics"
    },
    {
        "Rank": 2797,
        "Port": "palestra",
        "Eng": "lecture"
    },
    {
        "Rank": 2797,
        "Port": "palestra",
        "Eng": "discussion"
    },
    {
        "Rank": 2798,
        "Port": "milagre",
        "Eng": "miracle"
    },
    {
        "Rank": 2799,
        "Port": "joelho",
        "Eng": "knee"
    },
    {
        "Rank": 2800,
        "Port": "informática",
        "Eng": "computer science"
    },
    {
        "Rank": 2801,
        "Port": "crença",
        "Eng": "belief"
    },
    {
        "Rank": 2802,
        "Port": "conviver",
        "Eng": "spend time with"
    },
    {
        "Rank": 2802,
        "Port": "conviver",
        "Eng": "live with"
    },
    {
        "Rank": 2803,
        "Port": "amostra",
        "Eng": "sample"
    },
    {
        "Rank": 2803,
        "Port": "amostra",
        "Eng": "specimen"
    },
    {
        "Rank": 2804,
        "Port": "borracha",
        "Eng": "rubber"
    },
    {
        "Rank": 2805,
        "Port": "variado",
        "Eng": "varied"
    },
    {
        "Rank": 2806,
        "Port": "finalizar",
        "Eng": "conclude"
    },
    {
        "Rank": 2806,
        "Port": "finalizar",
        "Eng": "wrap up"
    },
    {
        "Rank": 2807,
        "Port": "reter",
        "Eng": "retain"
    },
    {
        "Rank": 2808,
        "Port": "ó",
        "Eng": "oh + N"
    },
    {
        "Rank": 2809,
        "Port": "roteiro",
        "Eng": "script"
    },
    {
        "Rank": 2809,
        "Port": "roteiro",
        "Eng": "itinerary"
    },
    {
        "Rank": 2809,
        "Port": "roteiro",
        "Eng": "route"
    },
    {
        "Rank": 2810,
        "Port": "valorizar",
        "Eng": "value"
    },
    {
        "Rank": 2811,
        "Port": "intermédio",
        "Eng": "intermediary"
    },
    {
        "Rank": 2811,
        "Port": "intermédio",
        "Eng": "intermediate"
    },
    {
        "Rank": 2812,
        "Port": "ritual",
        "Eng": "ritual"
    },
    {
        "Rank": 2813,
        "Port": "planejar",
        "Eng": "plan"
    },
    {
        "Rank": 2814,
        "Port": "pecado",
        "Eng": "sin"
    },
    {
        "Rank": 2815,
        "Port": "satisfeito",
        "Eng": "satisfied"
    },
    {
        "Rank": 2816,
        "Port": "divertir",
        "Eng": "have fun"
    },
    {
        "Rank": 2816,
        "Port": "divertir",
        "Eng": "entertain"
    },
    {
        "Rank": 2817,
        "Port": "setenta",
        "Eng": "seventy"
    },
    {
        "Rank": 2818,
        "Port": "geográfico",
        "Eng": "geographic"
    },
    {
        "Rank": 2819,
        "Port": "brilho",
        "Eng": "brightness"
    },
    {
        "Rank": 2819,
        "Port": "brilho",
        "Eng": "shine"
    },
    {
        "Rank": 2820,
        "Port": "oh",
        "Eng": "oh"
    },
    {
        "Rank": 2821,
        "Port": "suspeita",
        "Eng": "suspicion"
    },
    {
        "Rank": 2821,
        "Port": "suspeita",
        "Eng": "distrust"
    },
    {
        "Rank": 2822,
        "Port": "dourado",
        "Eng": "golden"
    },
    {
        "Rank": 2822,
        "Port": "dourado",
        "Eng": "gilded"
    },
    {
        "Rank": 2823,
        "Port": "remoto",
        "Eng": "remote"
    },
    {
        "Rank": 2824,
        "Port": "tecnológico",
        "Eng": "technological"
    },
    {
        "Rank": 2825,
        "Port": "inserir",
        "Eng": "insert"
    },
    {
        "Rank": 2825,
        "Port": "inserir",
        "Eng": "include"
    },
    {
        "Rank": 2826,
        "Port": "governamental",
        "Eng": "governmental"
    },
    {
        "Rank": 2827,
        "Port": "introdução",
        "Eng": "introduction"
    },
    {
        "Rank": 2828,
        "Port": "poço",
        "Eng": "water well"
    },
    {
        "Rank": 2829,
        "Port": "equivalente",
        "Eng": "equivalent"
    },
    {
        "Rank": 2830,
        "Port": "dever",
        "Eng": "duty"
    },
    {
        "Rank": 2831,
        "Port": "empreendimento",
        "Eng": "undertaking"
    },
    {
        "Rank": 2831,
        "Port": "empreendimento",
        "Eng": "venture"
    },
    {
        "Rank": 2832,
        "Port": "transacção",
        "Eng": "transaction"
    },
    {
        "Rank": 2833,
        "Port": "combinação",
        "Eng": "combination"
    },
    {
        "Rank": 2834,
        "Port": "avançado",
        "Eng": "advanced"
    },
    {
        "Rank": 2835,
        "Port": "prisioneiro",
        "Eng": "prisoner"
    },
    {
        "Rank": 2836,
        "Port": "registo",
        "Eng": "record"
    },
    {
        "Rank": 2836,
        "Port": "registo",
        "Eng": "ledger"
    },
    {
        "Rank": 2837,
        "Port": "clínica",
        "Eng": "clinic"
    },
    {
        "Rank": 2838,
        "Port": "protocolo",
        "Eng": "protocol"
    },
    {
        "Rank": 2839,
        "Port": "reforço",
        "Eng": "reinforcement"
    },
    {
        "Rank": 2840,
        "Port": "artificial",
        "Eng": "artificial"
    },
    {
        "Rank": 2841,
        "Port": "conduta",
        "Eng": "behavior"
    },
    {
        "Rank": 2841,
        "Port": "conduta",
        "Eng": "conduct"
    },
    {
        "Rank": 2842,
        "Port": "rebanho",
        "Eng": "flock"
    },
    {
        "Rank": 2842,
        "Port": "rebanho",
        "Eng": "herd"
    },
    {
        "Rank": 2843,
        "Port": "condutor",
        "Eng": "driver"
    },
    {
        "Rank": 2843,
        "Port": "condutor",
        "Eng": "director"
    },
    {
        "Rank": 2844,
        "Port": "atleta",
        "Eng": "athlete"
    },
    {
        "Rank": 2845,
        "Port": "emissão",
        "Eng": "emission"
    },
    {
        "Rank": 2846,
        "Port": "simpatia",
        "Eng": "sympathy"
    },
    {
        "Rank": 2846,
        "Port": "simpatia",
        "Eng": "friendliness"
    },
    {
        "Rank": 2847,
        "Port": "mito",
        "Eng": "myth"
    },
    {
        "Rank": 2848,
        "Port": "circular",
        "Eng": "circulate"
    },
    {
        "Rank": 2848,
        "Port": "circular",
        "Eng": "circle"
    },
    {
        "Rank": 2849,
        "Port": "igualdade",
        "Eng": "equality"
    },
    {
        "Rank": 2849,
        "Port": "igualdade",
        "Eng": "parity"
    },
    {
        "Rank": 2850,
        "Port": "gravação",
        "Eng": "recording"
    },
    {
        "Rank": 2851,
        "Port": "mero",
        "Eng": "mere"
    },
    {
        "Rank": 2852,
        "Port": "fortemente",
        "Eng": "strongly"
    },
    {
        "Rank": 2853,
        "Port": "emenda",
        "Eng": "amendment"
    },
    {
        "Rank": 2854,
        "Port": "convencional",
        "Eng": "conventional"
    },
    {
        "Rank": 2855,
        "Port": "esfera",
        "Eng": "sphere"
    },
    {
        "Rank": 2855,
        "Port": "esfera",
        "Eng": "area of influence"
    },
    {
        "Rank": 2856,
        "Port": "recto",
        "Eng": "straight"
    },
    {
        "Rank": 2857,
        "Port": "brincadeira",
        "Eng": "joke"
    },
    {
        "Rank": 2857,
        "Port": "brincadeira",
        "Eng": "game"
    },
    {
        "Rank": 2857,
        "Port": "brincadeira",
        "Eng": "play"
    },
    {
        "Rank": 2858,
        "Port": "programação",
        "Eng": "programming"
    },
    {
        "Rank": 2859,
        "Port": "extrair",
        "Eng": "extract"
    },
    {
        "Rank": 2860,
        "Port": "paço",
        "Eng": "palace"
    },
    {
        "Rank": 2860,
        "Port": "paço",
        "Eng": "court"
    },
    {
        "Rank": 2860,
        "Port": "paço",
        "Eng": "official building"
    },
    {
        "Rank": 2861,
        "Port": "convir",
        "Eng": "be right"
    },
    {
        "Rank": 2861,
        "Port": "convir",
        "Eng": "just"
    },
    {
        "Rank": 2861,
        "Port": "convir",
        "Eng": "be fit"
    },
    {
        "Rank": 2862,
        "Port": "câmbio",
        "Eng": "exchange"
    },
    {
        "Rank": 2863,
        "Port": "doutrina",
        "Eng": "doctrine"
    },
    {
        "Rank": 2864,
        "Port": "motivar",
        "Eng": "motivate"
    },
    {
        "Rank": 2865,
        "Port": "júnior",
        "Eng": "junior"
    },
    {
        "Rank": 2866,
        "Port": "prévio",
        "Eng": "preliminary"
    },
    {
        "Rank": 2866,
        "Port": "prévio",
        "Eng": "previous"
    },
    {
        "Rank": 2867,
        "Port": "incentivo",
        "Eng": "incentive"
    },
    {
        "Rank": 2868,
        "Port": "importação",
        "Eng": "importing"
    },
    {
        "Rank": 2869,
        "Port": "dano",
        "Eng": "damage"
    },
    {
        "Rank": 2870,
        "Port": "habilidade",
        "Eng": "skill"
    },
    {
        "Rank": 2870,
        "Port": "habilidade",
        "Eng": "ability"
    },
    {
        "Rank": 2871,
        "Port": "comprovar",
        "Eng": "substantiate"
    },
    {
        "Rank": 2871,
        "Port": "comprovar",
        "Eng": "prove"
    },
    {
        "Rank": 2872,
        "Port": "bairro",
        "Eng": "neighborhood"
    },
    {
        "Rank": 2873,
        "Port": "sumo",
        "Eng": "extreme"
    },
    {
        "Rank": 2873,
        "Port": "sumo",
        "Eng": "juice"
    },
    {
        "Rank": 2874,
        "Port": "extinto",
        "Eng": "extinct"
    },
    {
        "Rank": 2874,
        "Port": "extinto",
        "Eng": "extinguished"
    },
    {
        "Rank": 2875,
        "Port": "óbvio",
        "Eng": "obvious"
    },
    {
        "Rank": 2876,
        "Port": "modesto",
        "Eng": "modest"
    },
    {
        "Rank": 2877,
        "Port": "isolar",
        "Eng": "isolate"
    },
    {
        "Rank": 2878,
        "Port": "originar",
        "Eng": "originate"
    },
    {
        "Rank": 2879,
        "Port": "vício",
        "Eng": "addiction"
    },
    {
        "Rank": 2879,
        "Port": "vício",
        "Eng": "vice"
    },
    {
        "Rank": 2880,
        "Port": "laço",
        "Eng": "tie"
    },
    {
        "Rank": 2880,
        "Port": "laço",
        "Eng": "bond"
    },
    {
        "Rank": 2880,
        "Port": "laço",
        "Eng": "bow"
    },
    {
        "Rank": 2881,
        "Port": "lamentar",
        "Eng": "mourn"
    },
    {
        "Rank": 2881,
        "Port": "lamentar",
        "Eng": "be sorry"
    },
    {
        "Rank": 2882,
        "Port": "ruído",
        "Eng": "loud and unpleasant noise"
    },
    {
        "Rank": 2883,
        "Port": "combustível",
        "Eng": "fuel"
    },
    {
        "Rank": 2883,
        "Port": "combustível",
        "Eng": "combustible"
    },
    {
        "Rank": 2884,
        "Port": "necessariamente",
        "Eng": "necessarily"
    },
    {
        "Rank": 2885,
        "Port": "trato",
        "Eng": "dealing"
    },
    {
        "Rank": 2885,
        "Port": "trato",
        "Eng": "tract"
    },
    {
        "Rank": 2886,
        "Port": "automático",
        "Eng": "automatic"
    },
    {
        "Rank": 2887,
        "Port": "precisão",
        "Eng": "precision"
    },
    {
        "Rank": 2888,
        "Port": "porção",
        "Eng": "part"
    },
    {
        "Rank": 2888,
        "Port": "porção",
        "Eng": "portion"
    },
    {
        "Rank": 2888,
        "Port": "porção",
        "Eng": "section"
    },
    {
        "Rank": 2889,
        "Port": "titular",
        "Eng": "title holder"
    },
    {
        "Rank": 2889,
        "Port": "titular",
        "Eng": "cabinet member"
    },
    {
        "Rank": 2890,
        "Port": "vídeo",
        "Eng": "video"
    },
    {
        "Rank": 2891,
        "Port": "ausente",
        "Eng": "absent"
    },
    {
        "Rank": 2892,
        "Port": "mato",
        "Eng": "thicket"
    },
    {
        "Rank": 2892,
        "Port": "mato",
        "Eng": "brush"
    },
    {
        "Rank": 2892,
        "Port": "mato",
        "Eng": "weeds"
    },
    {
        "Rank": 2893,
        "Port": "articular",
        "Eng": "articulate"
    },
    {
        "Rank": 2894,
        "Port": "morro",
        "Eng": "hill"
    },
    {
        "Rank": 2895,
        "Port": "altamente",
        "Eng": "highly"
    },
    {
        "Rank": 2896,
        "Port": "depoimento",
        "Eng": "testimony"
    },
    {
        "Rank": 2896,
        "Port": "depoimento",
        "Eng": "affidavit"
    },
    {
        "Rank": 2897,
        "Port": "estrear",
        "Eng": "premiere"
    },
    {
        "Rank": 2897,
        "Port": "estrear",
        "Eng": "inaugurate"
    },
    {
        "Rank": 2898,
        "Port": "casca",
        "Eng": "peel"
    },
    {
        "Rank": 2898,
        "Port": "casca",
        "Eng": "skin"
    },
    {
        "Rank": 2898,
        "Port": "casca",
        "Eng": "bark"
    },
    {
        "Rank": 2899,
        "Port": "frei",
        "Eng": "friar"
    },
    {
        "Rank": 2900,
        "Port": "salgado",
        "Eng": "relating to salt"
    },
    {
        "Rank": 2901,
        "Port": "nervo",
        "Eng": "nerve"
    },
    {
        "Rank": 2902,
        "Port": "toque",
        "Eng": "touch"
    },
    {
        "Rank": 2903,
        "Port": "vertente",
        "Eng": "slope"
    },
    {
        "Rank": 2903,
        "Port": "vertente",
        "Eng": "downgrade"
    },
    {
        "Rank": 2903,
        "Port": "vertente",
        "Eng": "incline"
    },
    {
        "Rank": 2904,
        "Port": "fumar",
        "Eng": "smoke"
    },
    {
        "Rank": 2905,
        "Port": "duplo",
        "Eng": "dual"
    },
    {
        "Rank": 2905,
        "Port": "duplo",
        "Eng": "double"
    },
    {
        "Rank": 2906,
        "Port": "convento",
        "Eng": "convent"
    },
    {
        "Rank": 2907,
        "Port": "teórico",
        "Eng": "theoretical"
    },
    {
        "Rank": 2907,
        "Port": "teórico",
        "Eng": "theorist"
    },
    {
        "Rank": 2908,
        "Port": "delgado",
        "Eng": "thin"
    },
    {
        "Rank": 2908,
        "Port": "delgado",
        "Eng": "delicate"
    },
    {
        "Rank": 2908,
        "Port": "delgado",
        "Eng": "fine"
    },
    {
        "Rank": 2909,
        "Port": "invenção",
        "Eng": "invention"
    },
    {
        "Rank": 2910,
        "Port": "historiador",
        "Eng": "historian"
    },
    {
        "Rank": 2911,
        "Port": "nomear",
        "Eng": "appoint"
    },
    {
        "Rank": 2911,
        "Port": "nomear",
        "Eng": "name"
    },
    {
        "Rank": 2912,
        "Port": "loucura",
        "Eng": "insanity"
    },
    {
        "Rank": 2913,
        "Port": "expressar",
        "Eng": "express"
    },
    {
        "Rank": 2914,
        "Port": "turma",
        "Eng": "class"
    },
    {
        "Rank": 2914,
        "Port": "turma",
        "Eng": "group"
    },
    {
        "Rank": 2914,
        "Port": "turma",
        "Eng": "team"
    },
    {
        "Rank": 2915,
        "Port": "assustar",
        "Eng": "frighten"
    },
    {
        "Rank": 2915,
        "Port": "assustar",
        "Eng": "scare"
    },
    {
        "Rank": 2916,
        "Port": "urgência",
        "Eng": "urgency"
    },
    {
        "Rank": 2917,
        "Port": "cheque",
        "Eng": "check"
    },
    {
        "Rank": 2918,
        "Port": "impresso",
        "Eng": "printed"
    },
    {
        "Rank": 2919,
        "Port": "duração",
        "Eng": "duration"
    },
    {
        "Rank": 2920,
        "Port": "fantasma",
        "Eng": "ghost"
    },
    {
        "Rank": 2921,
        "Port": "pequenino",
        "Eng": "little"
    },
    {
        "Rank": 2921,
        "Port": "pequenino",
        "Eng": "very small"
    },
    {
        "Rank": 2922,
        "Port": "pessoalmente",
        "Eng": "personally"
    },
    {
        "Rank": 2923,
        "Port": "colheita",
        "Eng": "harvest"
    },
    {
        "Rank": 2924,
        "Port": "guitarra",
        "Eng": "guitar"
    },
    {
        "Rank": 2925,
        "Port": "aperceber",
        "Eng": "realize"
    },
    {
        "Rank": 2925,
        "Port": "aperceber",
        "Eng": "perceive"
    },
    {
        "Rank": 2926,
        "Port": "ruína",
        "Eng": "ruin"
    },
    {
        "Rank": 2927,
        "Port": "tortura",
        "Eng": "torture"
    },
    {
        "Rank": 2928,
        "Port": "medalha",
        "Eng": "medal"
    },
    {
        "Rank": 2929,
        "Port": "abandono",
        "Eng": "abandonment"
    },
    {
        "Rank": 2930,
        "Port": "suspensão",
        "Eng": "suspension"
    },
    {
        "Rank": 2931,
        "Port": "vírus",
        "Eng": "virus"
    },
    {
        "Rank": 2932,
        "Port": "ideológico",
        "Eng": "ideological"
    },
    {
        "Rank": 2933,
        "Port": "utilidade",
        "Eng": "usefulness"
    },
    {
        "Rank": 2933,
        "Port": "utilidade",
        "Eng": "use"
    },
    {
        "Rank": 2934,
        "Port": "eficiente",
        "Eng": "efficient"
    },
    {
        "Rank": 2935,
        "Port": "ressaltar",
        "Eng": "emphasize"
    },
    {
        "Rank": 2935,
        "Port": "ressaltar",
        "Eng": "point out"
    },
    {
        "Rank": 2936,
        "Port": "bacia",
        "Eng": "basin"
    },
    {
        "Rank": 2937,
        "Port": "finalidade",
        "Eng": "objective"
    },
    {
        "Rank": 2937,
        "Port": "finalidade",
        "Eng": "purpose"
    },
    {
        "Rank": 2937,
        "Port": "finalidade",
        "Eng": "end"
    },
    {
        "Rank": 2938,
        "Port": "abrigar",
        "Eng": "shelter"
    },
    {
        "Rank": 2938,
        "Port": "abrigar",
        "Eng": "protect"
    },
    {
        "Rank": 2939,
        "Port": "simbólico",
        "Eng": "symbolic"
    },
    {
        "Rank": 2940,
        "Port": "comité",
        "Eng": "committee"
    },
    {
        "Rank": 2941,
        "Port": "contratado",
        "Eng": "contracted"
    },
    {
        "Rank": 2942,
        "Port": "paraíso",
        "Eng": "paradise"
    },
    {
        "Rank": 2943,
        "Port": "burro",
        "Eng": "donkey"
    },
    {
        "Rank": 2943,
        "Port": "burro",
        "Eng": "ass"
    },
    {
        "Rank": 2943,
        "Port": "burro",
        "Eng": "fool"
    },
    {
        "Rank": 2944,
        "Port": "punir",
        "Eng": "punish"
    },
    {
        "Rank": 2945,
        "Port": "usado",
        "Eng": "used"
    },
    {
        "Rank": 2946,
        "Port": "pá",
        "Eng": "shovel"
    },
    {
        "Rank": 2946,
        "Port": "pá",
        "Eng": "scoop"
    },
    {
        "Rank": 2947,
        "Port": "abater",
        "Eng": "come down"
    },
    {
        "Rank": 2947,
        "Port": "abater",
        "Eng": "cut down"
    },
    {
        "Rank": 2948,
        "Port": "subida",
        "Eng": "rise"
    },
    {
        "Rank": 2948,
        "Port": "subida",
        "Eng": "ascent"
    },
    {
        "Rank": 2949,
        "Port": "Top",
        "Eng": "shot"
    },
    {
        "Rank": 2949,
        "Port": "Top",
        "Eng": "bullet"
    },
    {
        "Rank": 2949,
        "Port": "Top",
        "Eng": "candy"
    },
    {
        "Rank": 2950,
        "Port": "questionar",
        "Eng": "question"
    },
    {
        "Rank": 2951,
        "Port": "desastre",
        "Eng": "disaster"
    },
    {
        "Rank": 2952,
        "Port": "verdadeiramente",
        "Eng": "truly"
    },
    {
        "Rank": 2953,
        "Port": "separado",
        "Eng": "separated"
    },
    {
        "Rank": 2954,
        "Port": "agudo",
        "Eng": "sharp"
    },
    {
        "Rank": 2954,
        "Port": "agudo",
        "Eng": "acute"
    },
    {
        "Rank": 2955,
        "Port": "cemitério",
        "Eng": "cemetery"
    },
    {
        "Rank": 2956,
        "Port": "telefónico",
        "Eng": "telephone"
    },
    {
        "Rank": 2957,
        "Port": "baile",
        "Eng": "dance"
    },
    {
        "Rank": 2957,
        "Port": "baile",
        "Eng": "ball"
    },
    {
        "Rank": 2958,
        "Port": "desporto",
        "Eng": "sport"
    },
    {
        "Rank": 2959,
        "Port": "electricidade",
        "Eng": "electricity"
    },
    {
        "Rank": 2960,
        "Port": "castigo",
        "Eng": "punishment"
    },
    {
        "Rank": 2960,
        "Port": "castigo",
        "Eng": "curse"
    },
    {
        "Rank": 2961,
        "Port": "descrição",
        "Eng": "description"
    },
    {
        "Rank": 2962,
        "Port": "isolamento",
        "Eng": "isolation"
    },
    {
        "Rank": 2963,
        "Port": "demonstração",
        "Eng": "demonstration"
    },
    {
        "Rank": 2964,
        "Port": "integrante",
        "Eng": "integral"
    },
    {
        "Rank": 2964,
        "Port": "integrante",
        "Eng": "member"
    },
    {
        "Rank": 2965,
        "Port": "caderno",
        "Eng": "notebook"
    },
    {
        "Rank": 2966,
        "Port": "luminoso",
        "Eng": "bright"
    },
    {
        "Rank": 2966,
        "Port": "luminoso",
        "Eng": "luminous"
    },
    {
        "Rank": 2967,
        "Port": "nomeação",
        "Eng": "nomination"
    },
    {
        "Rank": 2968,
        "Port": "coligação",
        "Eng": "alliance"
    },
    {
        "Rank": 2968,
        "Port": "coligação",
        "Eng": "federation"
    },
    {
        "Rank": 2968,
        "Port": "coligação",
        "Eng": "union"
    },
    {
        "Rank": 2969,
        "Port": "comportar",
        "Eng": "behave"
    },
    {
        "Rank": 2970,
        "Port": "excepto",
        "Eng": "except"
    },
    {
        "Rank": 2971,
        "Port": "armazém",
        "Eng": "storehouse"
    },
    {
        "Rank": 2972,
        "Port": "caçador",
        "Eng": "hunter"
    },
    {
        "Rank": 2973,
        "Port": "poupar",
        "Eng": "save"
    },
    {
        "Rank": 2973,
        "Port": "poupar",
        "Eng": "spare"
    },
    {
        "Rank": 2974,
        "Port": "iluminação",
        "Eng": "lighting"
    },
    {
        "Rank": 2974,
        "Port": "iluminação",
        "Eng": "illumination"
    },
    {
        "Rank": 2975,
        "Port": "fracasso",
        "Eng": "failure"
    },
    {
        "Rank": 2976,
        "Port": "pressa",
        "Eng": "hurry"
    },
    {
        "Rank": 2976,
        "Port": "pressa",
        "Eng": "urgency"
    },
    {
        "Rank": 2977,
        "Port": "vice-presidente",
        "Eng": "vice president"
    },
    {
        "Rank": 2978,
        "Port": "tempestade",
        "Eng": "storm"
    },
    {
        "Rank": 2978,
        "Port": "tempestade",
        "Eng": "tempest"
    },
    {
        "Rank": 2979,
        "Port": "compensar",
        "Eng": "compensate"
    },
    {
        "Rank": 2980,
        "Port": "sintoma",
        "Eng": "symptom"
    },
    {
        "Rank": 2981,
        "Port": "discreto",
        "Eng": "discrete"
    },
    {
        "Rank": 2982,
        "Port": "touro",
        "Eng": "bull"
    },
    {
        "Rank": 2983,
        "Port": "agradecer",
        "Eng": "thank"
    },
    {
        "Rank": 2984,
        "Port": "recomendação",
        "Eng": "recommendation"
    },
    {
        "Rank": 2985,
        "Port": "emergência",
        "Eng": "emergency"
    },
    {
        "Rank": 2986,
        "Port": "tranquilidade",
        "Eng": "tranquility"
    },
    {
        "Rank": 2986,
        "Port": "tranquilidade",
        "Eng": "peace"
    },
    {
        "Rank": 2987,
        "Port": "caçar",
        "Eng": "hunt"
    },
    {
        "Rank": 2988,
        "Port": "substituição",
        "Eng": "substitution"
    },
    {
        "Rank": 2989,
        "Port": "vital",
        "Eng": "vital"
    },
    {
        "Rank": 2990,
        "Port": "repórter",
        "Eng": "reporter"
    },
    {
        "Rank": 2991,
        "Port": "fornecedor",
        "Eng": "provider"
    },
    {
        "Rank": 2992,
        "Port": "cauda",
        "Eng": "tail"
    },
    {
        "Rank": 2993,
        "Port": "posto",
        "Eng": "station"
    },
    {
        "Rank": 2993,
        "Port": "posto",
        "Eng": "post"
    },
    {
        "Rank": 2994,
        "Port": "ruptura",
        "Eng": "rupture"
    },
    {
        "Rank": 2994,
        "Port": "ruptura",
        "Eng": "break"
    },
    {
        "Rank": 2995,
        "Port": "paciência",
        "Eng": "patience"
    },
    {
        "Rank": 2996,
        "Port": "firma",
        "Eng": "firm"
    },
    {
        "Rank": 2997,
        "Port": "abraçar",
        "Eng": "hug"
    },
    {
        "Rank": 2997,
        "Port": "abraçar",
        "Eng": "embrace"
    },
    {
        "Rank": 2998,
        "Port": "planície",
        "Eng": "plain"
    },
    {
        "Rank": 2998,
        "Port": "planície",
        "Eng": "prairie"
    },
    {
        "Rank": 2999,
        "Port": "comprador",
        "Eng": "buyer"
    },
    {
        "Rank": 3000,
        "Port": "polémico",
        "Eng": "controversial"
    },
    {
        "Rank": 3001,
        "Port": "empresarial",
        "Eng": "related to a company"
    },
    {
        "Rank": 3002,
        "Port": "manga",
        "Eng": "sleeve"
    },
    {
        "Rank": 3002,
        "Port": "manga",
        "Eng": "mango"
    },
    {
        "Rank": 3003,
        "Port": "seda",
        "Eng": "silk"
    },
    {
        "Rank": 3004,
        "Port": "constantemente",
        "Eng": "constantly"
    },
    {
        "Rank": 3005,
        "Port": "ajuste",
        "Eng": "adjustment"
    },
    {
        "Rank": 3005,
        "Port": "ajuste",
        "Eng": "agreement"
    },
    {
        "Rank": 3006,
        "Port": "posteriormente",
        "Eng": "later"
    },
    {
        "Rank": 3006,
        "Port": "posteriormente",
        "Eng": "afterwards"
    },
    {
        "Rank": 3007,
        "Port": "realista",
        "Eng": "realist"
    },
    {
        "Rank": 3008,
        "Port": "perseguição",
        "Eng": "persecution"
    },
    {
        "Rank": 3009,
        "Port": "fusão",
        "Eng": "merger"
    },
    {
        "Rank": 3009,
        "Port": "fusão",
        "Eng": "fusion"
    },
    {
        "Rank": 3010,
        "Port": "digno",
        "Eng": "worthy"
    },
    {
        "Rank": 3011,
        "Port": "defrontar",
        "Eng": "face"
    },
    {
        "Rank": 3011,
        "Port": "defrontar",
        "Eng": "confront"
    },
    {
        "Rank": 3012,
        "Port": "abrigo",
        "Eng": "shelter"
    },
    {
        "Rank": 3012,
        "Port": "abrigo",
        "Eng": "refuge"
    },
    {
        "Rank": 3012,
        "Port": "abrigo",
        "Eng": "sanctuary"
    },
    {
        "Rank": 3013,
        "Port": "residente",
        "Eng": "resident"
    },
    {
        "Rank": 3014,
        "Port": "sujo",
        "Eng": "dirty"
    },
    {
        "Rank": 3014,
        "Port": "sujo",
        "Eng": "soiled"
    },
    {
        "Rank": 3015,
        "Port": "inútil",
        "Eng": "useless"
    },
    {
        "Rank": 3016,
        "Port": "cabra",
        "Eng": "goat"
    },
    {
        "Rank": 3017,
        "Port": "complementar",
        "Eng": "complementary"
    },
    {
        "Rank": 3017,
        "Port": "complementar",
        "Eng": "additional"
    },
    {
        "Rank": 3018,
        "Port": "proveniente",
        "Eng": "proceeding or resulting from"
    },
    {
        "Rank": 3019,
        "Port": "solitário",
        "Eng": "solitary"
    },
    {
        "Rank": 3020,
        "Port": "muralha",
        "Eng": "city wall"
    },
    {
        "Rank": 3021,
        "Port": "lenda",
        "Eng": "legend"
    },
    {
        "Rank": 3021,
        "Port": "lenda",
        "Eng": "story"
    },
    {
        "Rank": 3022,
        "Port": "jazz",
        "Eng": "jazz"
    },
    {
        "Rank": 3023,
        "Port": "medicamento",
        "Eng": "medication"
    },
    {
        "Rank": 3023,
        "Port": "medicamento",
        "Eng": "medicine"
    },
    {
        "Rank": 3024,
        "Port": "auto",
        "Eng": "legal document"
    },
    {
        "Rank": 3024,
        "Port": "auto",
        "Eng": "theatrical play"
    },
    {
        "Rank": 3025,
        "Port": "pulmão",
        "Eng": "lung"
    },
    {
        "Rank": 3026,
        "Port": "estreia",
        "Eng": "debut"
    },
    {
        "Rank": 3026,
        "Port": "estreia",
        "Eng": "premiere"
    },
    {
        "Rank": 3027,
        "Port": "catedral",
        "Eng": "cathedral"
    },
    {
        "Rank": 3028,
        "Port": "estável",
        "Eng": "stable"
    },
    {
        "Rank": 3029,
        "Port": "bebé",
        "Eng": "baby"
    },
    {
        "Rank": 3030,
        "Port": "misto",
        "Eng": "mix"
    },
    {
        "Rank": 3030,
        "Port": "misto",
        "Eng": "mixed"
    },
    {
        "Rank": 3031,
        "Port": "desencadear",
        "Eng": "unleash"
    },
    {
        "Rank": 3031,
        "Port": "desencadear",
        "Eng": "cause"
    },
    {
        "Rank": 3032,
        "Port": "estatal",
        "Eng": "relating to the state"
    },
    {
        "Rank": 3033,
        "Port": "baiano",
        "Eng": "from Bahia"
    },
    {
        "Rank": 3034,
        "Port": "assinalar",
        "Eng": "point out"
    },
    {
        "Rank": 3035,
        "Port": "excelência",
        "Eng": "excellence"
    },
    {
        "Rank": 3036,
        "Port": "abuso",
        "Eng": "abuse"
    },
    {
        "Rank": 3037,
        "Port": "física",
        "Eng": "physics"
    },
    {
        "Rank": 3038,
        "Port": "lembrança",
        "Eng": "memory"
    },
    {
        "Rank": 3038,
        "Port": "lembrança",
        "Eng": "souvenir"
    },
    {
        "Rank": 3039,
        "Port": "visitante",
        "Eng": "visitor"
    },
    {
        "Rank": 3039,
        "Port": "visitante",
        "Eng": "visiting"
    },
    {
        "Rank": 3040,
        "Port": "maré",
        "Eng": "tide"
    },
    {
        "Rank": 3040,
        "Port": "maré",
        "Eng": "flux"
    },
    {
        "Rank": 3041,
        "Port": "abastecimento",
        "Eng": "supply"
    },
    {
        "Rank": 3041,
        "Port": "abastecimento",
        "Eng": "provision"
    },
    {
        "Rank": 3041,
        "Port": "abastecimento",
        "Eng": "ration"
    },
    {
        "Rank": 3042,
        "Port": "localização",
        "Eng": "location"
    },
    {
        "Rank": 3043,
        "Port": "iluminar",
        "Eng": "illuminate"
    },
    {
        "Rank": 3044,
        "Port": "leito",
        "Eng": "bed"
    },
    {
        "Rank": 3045,
        "Port": "plataforma",
        "Eng": "platform"
    },
    {
        "Rank": 3046,
        "Port": "arrumar",
        "Eng": "organize"
    },
    {
        "Rank": 3046,
        "Port": "arrumar",
        "Eng": "arrange"
    },
    {
        "Rank": 3047,
        "Port": "independentemente",
        "Eng": "independently"
    },
    {
        "Rank": 3048,
        "Port": "pico",
        "Eng": "peak"
    },
    {
        "Rank": 3048,
        "Port": "pico",
        "Eng": "highest point"
    },
    {
        "Rank": 3048,
        "Port": "pico",
        "Eng": "insect bite"
    },
    {
        "Rank": 3049,
        "Port": "castanho",
        "Eng": "brown"
    },
    {
        "Rank": 3050,
        "Port": "colocação",
        "Eng": "placement"
    },
    {
        "Rank": 3051,
        "Port": "asiático",
        "Eng": "Asian"
    },
    {
        "Rank": 3052,
        "Port": "vaga",
        "Eng": "vacancy"
    },
    {
        "Rank": 3052,
        "Port": "vaga",
        "Eng": "opening"
    },
    {
        "Rank": 3053,
        "Port": "mortal",
        "Eng": "mortal"
    },
    {
        "Rank": 3053,
        "Port": "mortal",
        "Eng": "fatal"
    },
    {
        "Rank": 3053,
        "Port": "mortal",
        "Eng": "terminal"
    },
    {
        "Rank": 3054,
        "Port": "elaboração",
        "Eng": "preparation"
    },
    {
        "Rank": 3054,
        "Port": "elaboração",
        "Eng": "elaboration"
    },
    {
        "Rank": 3055,
        "Port": "depor",
        "Eng": "depose"
    },
    {
        "Rank": 3055,
        "Port": "depor",
        "Eng": "put down"
    },
    {
        "Rank": 3055,
        "Port": "depor",
        "Eng": "set aside"
    },
    {
        "Rank": 3056,
        "Port": "preconceito",
        "Eng": "prejudice"
    },
    {
        "Rank": 3056,
        "Port": "preconceito",
        "Eng": "preconceived notion"
    },
    {
        "Rank": 3057,
        "Port": "proibido",
        "Eng": "prohibited"
    },
    {
        "Rank": 3057,
        "Port": "proibido",
        "Eng": "forbidden"
    },
    {
        "Rank": 3058,
        "Port": "prevenção",
        "Eng": "prevention"
    },
    {
        "Rank": 3059,
        "Port": "vapor",
        "Eng": "steam"
    },
    {
        "Rank": 3059,
        "Port": "vapor",
        "Eng": "vapor"
    },
    {
        "Rank": 3060,
        "Port": "solidão",
        "Eng": "solitude"
    },
    {
        "Rank": 3061,
        "Port": "possibilitar",
        "Eng": "make possible"
    },
    {
        "Rank": 3062,
        "Port": "relevo",
        "Eng": "relief"
    },
    {
        "Rank": 3062,
        "Port": "relevo",
        "Eng": "relevance"
    },
    {
        "Rank": 3063,
        "Port": "criatura",
        "Eng": "creature"
    },
    {
        "Rank": 3064,
        "Port": "coordenação",
        "Eng": "coordination"
    },
    {
        "Rank": 3065,
        "Port": "suicídio",
        "Eng": "suicide"
    },
    {
        "Rank": 3066,
        "Port": "dispositivo",
        "Eng": "device"
    },
    {
        "Rank": 3066,
        "Port": "dispositivo",
        "Eng": "gadget"
    },
    {
        "Rank": 3067,
        "Port": "aliviar",
        "Eng": "alleviate"
    },
    {
        "Rank": 3067,
        "Port": "aliviar",
        "Eng": "relieve"
    },
    {
        "Rank": 3068,
        "Port": "cachorro",
        "Eng": "dog"
    },
    {
        "Rank": 3069,
        "Port": "bando",
        "Eng": "group"
    },
    {
        "Rank": 3069,
        "Port": "bando",
        "Eng": "band"
    },
    {
        "Rank": 3069,
        "Port": "bando",
        "Eng": "flock of birds"
    },
    {
        "Rank": 3070,
        "Port": "moinho",
        "Eng": "mill"
    },
    {
        "Rank": 3071,
        "Port": "assassino",
        "Eng": "assassin"
    },
    {
        "Rank": 3071,
        "Port": "assassino",
        "Eng": "murderer"
    },
    {
        "Rank": 3072,
        "Port": "gravura",
        "Eng": "painting"
    },
    {
        "Rank": 3072,
        "Port": "gravura",
        "Eng": "picture"
    },
    {
        "Rank": 3073,
        "Port": "trabalhista",
        "Eng": "labor party member"
    },
    {
        "Rank": 3073,
        "Port": "trabalhista",
        "Eng": "characteristic of a worker"
    },
    {
        "Rank": 3074,
        "Port": "uniforme",
        "Eng": "uniform"
    },
    {
        "Rank": 3075,
        "Port": "inesperado",
        "Eng": "unexpected"
    },
    {
        "Rank": 3076,
        "Port": "foz",
        "Eng": "mouth of a river"
    },
    {
        "Rank": 3077,
        "Port": "autoria",
        "Eng": "authorship"
    },
    {
        "Rank": 3078,
        "Port": "fiscalização",
        "Eng": "inspection"
    },
    {
        "Rank": 3079,
        "Port": "raciocínio",
        "Eng": "reasoning"
    },
    {
        "Rank": 3080,
        "Port": "diplomático",
        "Eng": "diplomatic"
    },
    {
        "Rank": 3081,
        "Port": "vaso",
        "Eng": "vessel"
    },
    {
        "Rank": 3081,
        "Port": "vaso",
        "Eng": "vase"
    },
    {
        "Rank": 3082,
        "Port": "piscina",
        "Eng": "pool"
    },
    {
        "Rank": 3083,
        "Port": "inscrição",
        "Eng": "enrollment"
    },
    {
        "Rank": 3083,
        "Port": "inscrição",
        "Eng": "inscription"
    },
    {
        "Rank": 3084,
        "Port": "calendário",
        "Eng": "calendar"
    },
    {
        "Rank": 3085,
        "Port": "cinzento",
        "Eng": "gray"
    },
    {
        "Rank": 3086,
        "Port": "time",
        "Eng": "team"
    },
    {
        "Rank": 3087,
        "Port": "angústia",
        "Eng": "anxiety"
    },
    {
        "Rank": 3087,
        "Port": "angústia",
        "Eng": "anguish"
    },
    {
        "Rank": 3088,
        "Port": "baixa",
        "Eng": "reduction"
    },
    {
        "Rank": 3089,
        "Port": "governante",
        "Eng": "governor"
    },
    {
        "Rank": 3089,
        "Port": "governante",
        "Eng": "politician"
    },
    {
        "Rank": 3089,
        "Port": "governante",
        "Eng": "governing"
    },
    {
        "Rank": 3090,
        "Port": "expressivo",
        "Eng": "expressive"
    },
    {
        "Rank": 3091,
        "Port": "limitação",
        "Eng": "limitation"
    },
    {
        "Rank": 3092,
        "Port": "barulho",
        "Eng": "noise"
    },
    {
        "Rank": 3093,
        "Port": "impressionar",
        "Eng": "impress"
    },
    {
        "Rank": 3094,
        "Port": "seleccionar",
        "Eng": "select"
    },
    {
        "Rank": 3095,
        "Port": "conversação",
        "Eng": "talk"
    },
    {
        "Rank": 3095,
        "Port": "conversação",
        "Eng": "conversation"
    },
    {
        "Rank": 3096,
        "Port": "sé",
        "Eng": "see"
    },
    {
        "Rank": 3096,
        "Port": "sé",
        "Eng": "parochial headquarters"
    },
    {
        "Rank": 3097,
        "Port": "fibra",
        "Eng": "fiber"
    },
    {
        "Rank": 3098,
        "Port": "interrogar",
        "Eng": "ask"
    },
    {
        "Rank": 3098,
        "Port": "interrogar",
        "Eng": "interrogate"
    },
    {
        "Rank": 3099,
        "Port": "renunciar",
        "Eng": "resign"
    },
    {
        "Rank": 3099,
        "Port": "renunciar",
        "Eng": "renounce"
    },
    {
        "Rank": 3100,
        "Port": "outrora",
        "Eng": "formerly"
    },
    {
        "Rank": 3100,
        "Port": "outrora",
        "Eng": "anciently"
    },
    {
        "Rank": 3101,
        "Port": "desculpa",
        "Eng": "excuse"
    },
    {
        "Rank": 3102,
        "Port": "vegetal",
        "Eng": "vegetable"
    },
    {
        "Rank": 3102,
        "Port": "vegetal",
        "Eng": "relating to vegetables"
    },
    {
        "Rank": 3103,
        "Port": "incrível",
        "Eng": "incredible"
    },
    {
        "Rank": 3104,
        "Port": "alternar",
        "Eng": "take turns"
    },
    {
        "Rank": 3104,
        "Port": "alternar",
        "Eng": "alternate"
    },
    {
        "Rank": 3105,
        "Port": "enriquecer",
        "Eng": "make or become rich"
    },
    {
        "Rank": 3105,
        "Port": "enriquecer",
        "Eng": "enrich"
    },
    {
        "Rank": 3106,
        "Port": "diminuição",
        "Eng": "decrease"
    },
    {
        "Rank": 3106,
        "Port": "diminuição",
        "Eng": "diminishing"
    },
    {
        "Rank": 3107,
        "Port": "dose",
        "Eng": "dose"
    },
    {
        "Rank": 3108,
        "Port": "narrativa",
        "Eng": "narrative"
    },
    {
        "Rank": 3109,
        "Port": "psicologia",
        "Eng": "psychology"
    },
    {
        "Rank": 3110,
        "Port": "acompanhamento",
        "Eng": "follow-up"
    },
    {
        "Rank": 3110,
        "Port": "acompanhamento",
        "Eng": "accompaniment"
    },
    {
        "Rank": 3111,
        "Port": "eficiência",
        "Eng": "efficiency"
    },
    {
        "Rank": 3112,
        "Port": "atrasado",
        "Eng": "late"
    },
    {
        "Rank": 3112,
        "Port": "atrasado",
        "Eng": "behind"
    },
    {
        "Rank": 3113,
        "Port": "diagnóstico",
        "Eng": "diagnosis"
    },
    {
        "Rank": 3113,
        "Port": "diagnóstico",
        "Eng": "diagnostic"
    },
    {
        "Rank": 3114,
        "Port": "suficientemente",
        "Eng": "sufficiently"
    },
    {
        "Rank": 3115,
        "Port": "ódio",
        "Eng": "hatred"
    },
    {
        "Rank": 3116,
        "Port": "controlo",
        "Eng": "control"
    },
    {
        "Rank": 3117,
        "Port": "resultante",
        "Eng": "resulting from"
    },
    {
        "Rank": 3117,
        "Port": "resultante",
        "Eng": "a result of"
    },
    {
        "Rank": 3118,
        "Port": "arder",
        "Eng": "burn"
    },
    {
        "Rank": 3118,
        "Port": "arder",
        "Eng": "sting"
    },
    {
        "Rank": 3119,
        "Port": "palha",
        "Eng": "straw"
    },
    {
        "Rank": 3119,
        "Port": "palha",
        "Eng": "hay"
    },
    {
        "Rank": 3120,
        "Port": "muçulmano",
        "Eng": "Muslim"
    },
    {
        "Rank": 3121,
        "Port": "piso",
        "Eng": "floor"
    },
    {
        "Rank": 3121,
        "Port": "piso",
        "Eng": "level"
    },
    {
        "Rank": 3121,
        "Port": "piso",
        "Eng": "story"
    },
    {
        "Rank": 3122,
        "Port": "andamento",
        "Eng": "progress"
    },
    {
        "Rank": 3123,
        "Port": "escasso",
        "Eng": "scarce"
    },
    {
        "Rank": 3124,
        "Port": "lata",
        "Eng": "can"
    },
    {
        "Rank": 3124,
        "Port": "lata",
        "Eng": "tin"
    },
    {
        "Rank": 3125,
        "Port": "expandir",
        "Eng": "expand"
    },
    {
        "Rank": 3126,
        "Port": "terminal",
        "Eng": "terminal"
    },
    {
        "Rank": 3126,
        "Port": "terminal",
        "Eng": "outlet"
    },
    {
        "Rank": 3127,
        "Port": "respirar",
        "Eng": "breathe"
    },
    {
        "Rank": 3128,
        "Port": "montante",
        "Eng": "amount"
    },
    {
        "Rank": 3129,
        "Port": "persistir",
        "Eng": "persist"
    },
    {
        "Rank": 3130,
        "Port": "convencido",
        "Eng": "convinced"
    },
    {
        "Rank": 3131,
        "Port": "coincidir",
        "Eng": "match up"
    },
    {
        "Rank": 3131,
        "Port": "coincidir",
        "Eng": "coincide"
    },
    {
        "Rank": 3132,
        "Port": "votação",
        "Eng": "voting"
    },
    {
        "Rank": 3132,
        "Port": "votação",
        "Eng": "election"
    },
    {
        "Rank": 3133,
        "Port": "estômago",
        "Eng": "stomach"
    },
    {
        "Rank": 3134,
        "Port": "lã",
        "Eng": "wool"
    },
    {
        "Rank": 3135,
        "Port": "empenhar",
        "Eng": "strive"
    },
    {
        "Rank": 3135,
        "Port": "empenhar",
        "Eng": "get involved"
    },
    {
        "Rank": 3136,
        "Port": "descendente",
        "Eng": "descendant"
    },
    {
        "Rank": 3137,
        "Port": "múltiplo",
        "Eng": "multiple"
    },
    {
        "Rank": 3138,
        "Port": "renovação",
        "Eng": "renovation"
    },
    {
        "Rank": 3138,
        "Port": "renovação",
        "Eng": "renewal"
    },
    {
        "Rank": 3139,
        "Port": "oficialmente",
        "Eng": "officially"
    },
    {
        "Rank": 3140,
        "Port": "transparente",
        "Eng": "clear"
    },
    {
        "Rank": 3140,
        "Port": "transparente",
        "Eng": "transparent"
    },
    {
        "Rank": 3141,
        "Port": "soma",
        "Eng": "sum"
    },
    {
        "Rank": 3142,
        "Port": "irregular",
        "Eng": "irregular"
    },
    {
        "Rank": 3143,
        "Port": "cinematográfico",
        "Eng": "cinematographic"
    },
    {
        "Rank": 3144,
        "Port": "vendedor",
        "Eng": "seller"
    },
    {
        "Rank": 3144,
        "Port": "vendedor",
        "Eng": "vendor"
    },
    {
        "Rank": 3145,
        "Port": "gaúcho",
        "Eng": "South American cowboy; from Rio Grande do Sul"
    },
    {
        "Rank": 3146,
        "Port": "ilustrar",
        "Eng": "illustrate"
    },
    {
        "Rank": 3147,
        "Port": "aparente",
        "Eng": "apparent"
    },
    {
        "Rank": 3148,
        "Port": "encomenda",
        "Eng": "order"
    },
    {
        "Rank": 3148,
        "Port": "encomenda",
        "Eng": "package"
    },
    {
        "Rank": 3149,
        "Port": "feio",
        "Eng": "ugly"
    },
    {
        "Rank": 3150,
        "Port": "barroco",
        "Eng": "baroque"
    },
    {
        "Rank": 3151,
        "Port": "imaginário",
        "Eng": "imaginary"
    },
    {
        "Rank": 3152,
        "Port": "agitar",
        "Eng": "disturb"
    },
    {
        "Rank": 3152,
        "Port": "agitar",
        "Eng": "shake up"
    },
    {
        "Rank": 3152,
        "Port": "agitar",
        "Eng": "trouble"
    },
    {
        "Rank": 3153,
        "Port": "dificilmente",
        "Eng": "hardly"
    },
    {
        "Rank": 3153,
        "Port": "dificilmente",
        "Eng": "barely"
    },
    {
        "Rank": 3154,
        "Port": "baleia",
        "Eng": "whale"
    },
    {
        "Rank": 3155,
        "Port": "restrito",
        "Eng": "restricted"
    },
    {
        "Rank": 3156,
        "Port": "queijo",
        "Eng": "cheese"
    },
    {
        "Rank": 3157,
        "Port": "ala",
        "Eng": "wing"
    },
    {
        "Rank": 3157,
        "Port": "ala",
        "Eng": "branch"
    },
    {
        "Rank": 3158,
        "Port": "produtividade",
        "Eng": "productivity"
    },
    {
        "Rank": 3159,
        "Port": "portador",
        "Eng": "carrier"
    },
    {
        "Rank": 3160,
        "Port": "mosca",
        "Eng": "fly"
    },
    {
        "Rank": 3161,
        "Port": "desafiar",
        "Eng": "challenge"
    },
    {
        "Rank": 3162,
        "Port": "adiar",
        "Eng": "put off"
    },
    {
        "Rank": 3162,
        "Port": "adiar",
        "Eng": "procrastinate"
    },
    {
        "Rank": 3163,
        "Port": "permanência",
        "Eng": "stay"
    },
    {
        "Rank": 3163,
        "Port": "permanência",
        "Eng": "permanence"
    },
    {
        "Rank": 3164,
        "Port": "estúdio",
        "Eng": "studio"
    },
    {
        "Rank": 3165,
        "Port": "aviso",
        "Eng": "warning"
    },
    {
        "Rank": 3165,
        "Port": "aviso",
        "Eng": "notice"
    },
    {
        "Rank": 3166,
        "Port": "turco",
        "Eng": "Turk"
    },
    {
        "Rank": 3167,
        "Port": "apaixonado",
        "Eng": "in love with"
    },
    {
        "Rank": 3167,
        "Port": "apaixonado",
        "Eng": "passionate"
    },
    {
        "Rank": 3168,
        "Port": "nativo",
        "Eng": "native"
    },
    {
        "Rank": 3169,
        "Port": "cura",
        "Eng": "cure"
    },
    {
        "Rank": 3169,
        "Port": "cura",
        "Eng": "curate"
    },
    {
        "Rank": 3170,
        "Port": "cessar",
        "Eng": "cease"
    },
    {
        "Rank": 3170,
        "Port": "cessar",
        "Eng": "end"
    },
    {
        "Rank": 3171,
        "Port": "rolar",
        "Eng": "roll"
    },
    {
        "Rank": 3172,
        "Port": "leilão",
        "Eng": "auction"
    },
    {
        "Rank": 3173,
        "Port": "marinho",
        "Eng": "of the sea"
    },
    {
        "Rank": 3173,
        "Port": "marinho",
        "Eng": "marine"
    },
    {
        "Rank": 3174,
        "Port": "azeite",
        "Eng": "olive oil"
    },
    {
        "Rank": 3175,
        "Port": "inovação",
        "Eng": "innovation"
    },
    {
        "Rank": 3176,
        "Port": "cobre",
        "Eng": "copper"
    },
    {
        "Rank": 3177,
        "Port": "cartaz",
        "Eng": "poster"
    },
    {
        "Rank": 3178,
        "Port": "apelar",
        "Eng": "appeal"
    },
    {
        "Rank": 3179,
        "Port": "directoria",
        "Eng": "directors"
    },
    {
        "Rank": 3180,
        "Port": "posterior",
        "Eng": "later"
    },
    {
        "Rank": 3180,
        "Port": "posterior",
        "Eng": "posterior"
    },
    {
        "Rank": 3181,
        "Port": "botão",
        "Eng": "button"
    },
    {
        "Rank": 3182,
        "Port": "atentado",
        "Eng": "criminal attempt"
    },
    {
        "Rank": 3183,
        "Port": "circo",
        "Eng": "circus"
    },
    {
        "Rank": 3184,
        "Port": "cimento",
        "Eng": "cement"
    },
    {
        "Rank": 3185,
        "Port": "interferir",
        "Eng": "interfere"
    },
    {
        "Rank": 3186,
        "Port": "milímetro",
        "Eng": "millimeter"
    },
    {
        "Rank": 3187,
        "Port": "nuclear",
        "Eng": "nuclear"
    },
    {
        "Rank": 3188,
        "Port": "expedição",
        "Eng": "expedition"
    },
    {
        "Rank": 3189,
        "Port": "intermediário",
        "Eng": "intermediate"
    },
    {
        "Rank": 3189,
        "Port": "intermediário",
        "Eng": "intermediary"
    },
    {
        "Rank": 3190,
        "Port": "deficit",
        "Eng": "deficiency"
    },
    {
        "Rank": 3190,
        "Port": "deficit",
        "Eng": "deficit"
    },
    {
        "Rank": 3191,
        "Port": "lentamente",
        "Eng": "slowly"
    },
    {
        "Rank": 3192,
        "Port": "dúzia",
        "Eng": "dozen"
    },
    {
        "Rank": 3193,
        "Port": "duvidar",
        "Eng": "doubt"
    },
    {
        "Rank": 3194,
        "Port": "enrolar",
        "Eng": "roll up"
    },
    {
        "Rank": 3194,
        "Port": "enrolar",
        "Eng": "complicate"
    },
    {
        "Rank": 3195,
        "Port": "figurar",
        "Eng": "represent"
    },
    {
        "Rank": 3195,
        "Port": "figurar",
        "Eng": "look like"
    },
    {
        "Rank": 3196,
        "Port": "esquina",
        "Eng": "corner"
    },
    {
        "Rank": 3197,
        "Port": "bolo",
        "Eng": "cake"
    },
    {
        "Rank": 3198,
        "Port": "exílio",
        "Eng": "exile"
    },
    {
        "Rank": 3199,
        "Port": "caminhão",
        "Eng": "freight truck"
    },
    {
        "Rank": 3200,
        "Port": "observador",
        "Eng": "observer"
    },
    {
        "Rank": 3200,
        "Port": "observador",
        "Eng": "observant"
    },
    {
        "Rank": 3201,
        "Port": "contrair",
        "Eng": "contract"
    },
    {
        "Rank": 3202,
        "Port": "consolidar",
        "Eng": "consolidate"
    },
    {
        "Rank": 3203,
        "Port": "reitor",
        "Eng": "dean"
    },
    {
        "Rank": 3204,
        "Port": "molde",
        "Eng": "mold"
    },
    {
        "Rank": 3205,
        "Port": "oitenta",
        "Eng": "eighty"
    },
    {
        "Rank": 3206,
        "Port": "ideologia",
        "Eng": "ideology"
    },
    {
        "Rank": 3207,
        "Port": "respiração",
        "Eng": "breathing"
    },
    {
        "Rank": 3207,
        "Port": "respiração",
        "Eng": "respiration"
    },
    {
        "Rank": 3208,
        "Port": "desenvolvido",
        "Eng": "developed"
    },
    {
        "Rank": 3209,
        "Port": "abstracto",
        "Eng": "abstract"
    },
    {
        "Rank": 3210,
        "Port": "amador",
        "Eng": "amateur"
    },
    {
        "Rank": 3210,
        "Port": "amador",
        "Eng": "enthusiast"
    },
    {
        "Rank": 3210,
        "Port": "amador",
        "Eng": "lover"
    },
    {
        "Rank": 3211,
        "Port": "semear",
        "Eng": "sow"
    },
    {
        "Rank": 3212,
        "Port": "referente",
        "Eng": "pertaining to"
    },
    {
        "Rank": 3212,
        "Port": "referente",
        "Eng": "relating to"
    },
    {
        "Rank": 3213,
        "Port": "felizmente",
        "Eng": "fortunately"
    },
    {
        "Rank": 3213,
        "Port": "felizmente",
        "Eng": "happily"
    },
    {
        "Rank": 3214,
        "Port": "referendo",
        "Eng": "referendum"
    },
    {
        "Rank": 3215,
        "Port": "gordura",
        "Eng": "fat"
    },
    {
        "Rank": 3215,
        "Port": "gordura",
        "Eng": "grease"
    },
    {
        "Rank": 3216,
        "Port": "contradição",
        "Eng": "contradiction"
    },
    {
        "Rank": 3217,
        "Port": "lavoura",
        "Eng": "farming"
    },
    {
        "Rank": 3217,
        "Port": "lavoura",
        "Eng": "agriculture"
    },
    {
        "Rank": 3218,
        "Port": "severo",
        "Eng": "harsh"
    },
    {
        "Rank": 3218,
        "Port": "severo",
        "Eng": "severe"
    },
    {
        "Rank": 3219,
        "Port": "perdoar",
        "Eng": "forgive"
    },
    {
        "Rank": 3220,
        "Port": "fraqueza",
        "Eng": "weakness"
    },
    {
        "Rank": 3221,
        "Port": "cirurgia",
        "Eng": "surgery"
    },
    {
        "Rank": 3222,
        "Port": "guerrilheiro",
        "Eng": "guerilla"
    },
    {
        "Rank": 3223,
        "Port": "esfregar",
        "Eng": "rub"
    },
    {
        "Rank": 3223,
        "Port": "esfregar",
        "Eng": "scrub"
    },
    {
        "Rank": 3224,
        "Port": "eleitor",
        "Eng": "voter"
    },
    {
        "Rank": 3225,
        "Port": "atribuição",
        "Eng": "awarding"
    },
    {
        "Rank": 3225,
        "Port": "atribuição",
        "Eng": "assignment"
    },
    {
        "Rank": 3226,
        "Port": "escultura",
        "Eng": "sculpture"
    },
    {
        "Rank": 3227,
        "Port": "orgânico",
        "Eng": "organic"
    },
    {
        "Rank": 3228,
        "Port": "recepção",
        "Eng": "reception"
    },
    {
        "Rank": 3229,
        "Port": "basicamente",
        "Eng": "basically"
    },
    {
        "Rank": 3230,
        "Port": "privilegiado",
        "Eng": "privileged"
    },
    {
        "Rank": 3231,
        "Port": "potência",
        "Eng": "power"
    },
    {
        "Rank": 3231,
        "Port": "potência",
        "Eng": "potency"
    },
    {
        "Rank": 3232,
        "Port": "mediante",
        "Eng": "through"
    },
    {
        "Rank": 3232,
        "Port": "mediante",
        "Eng": "by means of"
    },
    {
        "Rank": 3233,
        "Port": "trágico",
        "Eng": "tragic"
    },
    {
        "Rank": 3234,
        "Port": "anónimo",
        "Eng": "anonymous"
    },
    {
        "Rank": 3235,
        "Port": "dicionário",
        "Eng": "dictionary"
    },
    {
        "Rank": 3236,
        "Port": "desgraça",
        "Eng": "disgrace"
    },
    {
        "Rank": 3237,
        "Port": "participante",
        "Eng": "participant"
    },
    {
        "Rank": 3238,
        "Port": "treinamento",
        "Eng": "training"
    },
    {
        "Rank": 3239,
        "Port": "ingressar",
        "Eng": "enlist"
    },
    {
        "Rank": 3239,
        "Port": "ingressar",
        "Eng": "enter"
    },
    {
        "Rank": 3239,
        "Port": "ingressar",
        "Eng": "be admitted"
    },
    {
        "Rank": 3240,
        "Port": "bravo",
        "Eng": "mad"
    },
    {
        "Rank": 3240,
        "Port": "bravo",
        "Eng": "angry"
    },
    {
        "Rank": 3240,
        "Port": "bravo",
        "Eng": "wild"
    },
    {
        "Rank": 3241,
        "Port": "vista",
        "Eng": "sight"
    },
    {
        "Rank": 3241,
        "Port": "vista",
        "Eng": "view"
    },
    {
        "Rank": 3242,
        "Port": "dificultar",
        "Eng": "make difficult"
    },
    {
        "Rank": 3243,
        "Port": "bilhete",
        "Eng": "note"
    },
    {
        "Rank": 3243,
        "Port": "bilhete",
        "Eng": "ticket"
    },
    {
        "Rank": 3243,
        "Port": "bilhete",
        "Eng": "pamphlet"
    },
    {
        "Rank": 3244,
        "Port": "colaborador",
        "Eng": "collaborator"
    },
    {
        "Rank": 3244,
        "Port": "colaborador",
        "Eng": "collaborative"
    },
    {
        "Rank": 3245,
        "Port": "linho",
        "Eng": "flax"
    },
    {
        "Rank": 3245,
        "Port": "linho",
        "Eng": "linen"
    },
    {
        "Rank": 3246,
        "Port": "trajectória",
        "Eng": "trajectory"
    },
    {
        "Rank": 3247,
        "Port": "barba",
        "Eng": "beard"
    },
    {
        "Rank": 3248,
        "Port": "prestes",
        "Eng": "about to"
    },
    {
        "Rank": 3248,
        "Port": "prestes",
        "Eng": "ready to"
    },
    {
        "Rank": 3249,
        "Port": "selo",
        "Eng": "seal"
    },
    {
        "Rank": 3249,
        "Port": "selo",
        "Eng": "stamp"
    },
    {
        "Rank": 3250,
        "Port": "espada",
        "Eng": "sword"
    },
    {
        "Rank": 3251,
        "Port": "burguês",
        "Eng": "bourgeois"
    },
    {
        "Rank": 3252,
        "Port": "hierarquia",
        "Eng": "hierarchy"
    },
    {
        "Rank": 3253,
        "Port": "representativo",
        "Eng": "representative"
    },
    {
        "Rank": 3254,
        "Port": "lágrima",
        "Eng": "tear"
    },
    {
        "Rank": 3255,
        "Port": "nave",
        "Eng": "spaceship"
    },
    {
        "Rank": 3255,
        "Port": "nave",
        "Eng": "vessel"
    },
    {
        "Rank": 3256,
        "Port": "cadáver",
        "Eng": "corpse"
    },
    {
        "Rank": 3256,
        "Port": "cadáver",
        "Eng": "cadaver"
    },
    {
        "Rank": 3257,
        "Port": "operador",
        "Eng": "operator"
    },
    {
        "Rank": 3258,
        "Port": "passivo",
        "Eng": "passive"
    },
    {
        "Rank": 3258,
        "Port": "passivo",
        "Eng": "lethargic"
    },
    {
        "Rank": 3259,
        "Port": "implantar",
        "Eng": "initiate"
    },
    {
        "Rank": 3259,
        "Port": "implantar",
        "Eng": "implant"
    },
    {
        "Rank": 3260,
        "Port": "raiva",
        "Eng": "anger"
    },
    {
        "Rank": 3260,
        "Port": "raiva",
        "Eng": "rabies"
    },
    {
        "Rank": 3261,
        "Port": "culpado",
        "Eng": "guilty"
    },
    {
        "Rank": 3261,
        "Port": "culpado",
        "Eng": "guilty party"
    },
    {
        "Rank": 3262,
        "Port": "item",
        "Eng": "item"
    },
    {
        "Rank": 3263,
        "Port": "prevenir",
        "Eng": "prevent"
    },
    {
        "Rank": 3264,
        "Port": "formato",
        "Eng": "format"
    },
    {
        "Rank": 3264,
        "Port": "formato",
        "Eng": "form"
    },
    {
        "Rank": 3265,
        "Port": "metálico",
        "Eng": "metallic"
    },
    {
        "Rank": 3266,
        "Port": "envolvimento",
        "Eng": "involvement"
    },
    {
        "Rank": 3267,
        "Port": "agressivo",
        "Eng": "aggressive"
    },
    {
        "Rank": 3268,
        "Port": "extinção",
        "Eng": "extinction"
    },
    {
        "Rank": 3269,
        "Port": "dezoito",
        "Eng": "eighteen"
    },
    {
        "Rank": 3270,
        "Port": "engenho",
        "Eng": "engine"
    },
    {
        "Rank": 3270,
        "Port": "engenho",
        "Eng": "sugar mill"
    },
    {
        "Rank": 3271,
        "Port": "despedir",
        "Eng": "say goodbye"
    },
    {
        "Rank": 3271,
        "Port": "despedir",
        "Eng": "fire"
    },
    {
        "Rank": 3272,
        "Port": "ocultar",
        "Eng": "conceal"
    },
    {
        "Rank": 3272,
        "Port": "ocultar",
        "Eng": "hide"
    },
    {
        "Rank": 3273,
        "Port": "cerco",
        "Eng": "siege"
    },
    {
        "Rank": 3273,
        "Port": "cerco",
        "Eng": "blockade"
    },
    {
        "Rank": 3273,
        "Port": "cerco",
        "Eng": "enclosing"
    },
    {
        "Rank": 3274,
        "Port": "montagem",
        "Eng": "assembly"
    },
    {
        "Rank": 3274,
        "Port": "montagem",
        "Eng": "editing"
    },
    {
        "Rank": 3275,
        "Port": "tropical",
        "Eng": "tropical"
    },
    {
        "Rank": 3276,
        "Port": "extenso",
        "Eng": "extensive"
    },
    {
        "Rank": 3277,
        "Port": "misericórdia",
        "Eng": "mercy"
    },
    {
        "Rank": 3278,
        "Port": "terrestre",
        "Eng": "of the earth"
    },
    {
        "Rank": 3278,
        "Port": "terrestre",
        "Eng": "earthly"
    },
    {
        "Rank": 3279,
        "Port": "portão",
        "Eng": "gate"
    },
    {
        "Rank": 3280,
        "Port": "einreichen",
        "Eng": "dust"
    },
    {
        "Rank": 3281,
        "Port": "capitalismo",
        "Eng": "capitalism"
    },
    {
        "Rank": 3282,
        "Port": "multa",
        "Eng": "fine"
    },
    {
        "Rank": 3283,
        "Port": "explodir",
        "Eng": "explode"
    },
    {
        "Rank": 3284,
        "Port": "urgente",
        "Eng": "urgent"
    },
    {
        "Rank": 3285,
        "Port": "vosso",
        "Eng": "your"
    },
    {
        "Rank": 3286,
        "Port": "aborto",
        "Eng": "abortion"
    },
    {
        "Rank": 3287,
        "Port": "faca",
        "Eng": "knife"
    },
    {
        "Rank": 3288,
        "Port": "concha",
        "Eng": "shell"
    },
    {
        "Rank": 3289,
        "Port": "marginal",
        "Eng": "delinquent"
    },
    {
        "Rank": 3289,
        "Port": "marginal",
        "Eng": "lawless"
    },
    {
        "Rank": 3289,
        "Port": "marginal",
        "Eng": "marginal"
    },
    {
        "Rank": 3290,
        "Port": "relevante",
        "Eng": "relevant"
    },
    {
        "Rank": 3291,
        "Port": "couro",
        "Eng": "leather"
    },
    {
        "Rank": 3292,
        "Port": "aposta",
        "Eng": "bet"
    },
    {
        "Rank": 3293,
        "Port": "espontâneo",
        "Eng": "spontaneous"
    },
    {
        "Rank": 3294,
        "Port": "gratuito",
        "Eng": "free of charge"
    },
    {
        "Rank": 3295,
        "Port": "conservação",
        "Eng": "conservation"
    },
    {
        "Rank": 3296,
        "Port": "exposto",
        "Eng": "displayed"
    },
    {
        "Rank": 3296,
        "Port": "exposto",
        "Eng": "exposed"
    },
    {
        "Rank": 3297,
        "Port": "guerrilha",
        "Eng": "guerilla warfare"
    },
    {
        "Rank": 3298,
        "Port": "imitar",
        "Eng": "imitate"
    },
    {
        "Rank": 3299,
        "Port": "diamante",
        "Eng": "diamond"
    },
    {
        "Rank": 3300,
        "Port": "ocorrência",
        "Eng": "occurrence"
    },
    {
        "Rank": 3301,
        "Port": "telhado",
        "Eng": "roof"
    },
    {
        "Rank": 3302,
        "Port": "pescador",
        "Eng": "fisherman"
    },
    {
        "Rank": 3302,
        "Port": "pescador",
        "Eng": "fishing"
    },
    {
        "Rank": 3303,
        "Port": "aparecimento",
        "Eng": "appearance"
    },
    {
        "Rank": 3304,
        "Port": "desempregado",
        "Eng": "unemployed"
    },
    {
        "Rank": 3305,
        "Port": "ordinário",
        "Eng": "usual"
    },
    {
        "Rank": 3305,
        "Port": "ordinário",
        "Eng": "customary"
    },
    {
        "Rank": 3305,
        "Port": "ordinário",
        "Eng": "vulgar"
    },
    {
        "Rank": 3306,
        "Port": "anular",
        "Eng": "revoke"
    },
    {
        "Rank": 3306,
        "Port": "anular",
        "Eng": "annul"
    },
    {
        "Rank": 3307,
        "Port": "quotidiano",
        "Eng": "day-to-day"
    },
    {
        "Rank": 3308,
        "Port": "apropriado",
        "Eng": "appropriate"
    },
    {
        "Rank": 3309,
        "Port": "poluição",
        "Eng": "pollution"
    },
    {
        "Rank": 3310,
        "Port": "dotar",
        "Eng": "endow"
    },
    {
        "Rank": 3310,
        "Port": "dotar",
        "Eng": "provide"
    },
    {
        "Rank": 3311,
        "Port": "liceu",
        "Eng": "high school"
    },
    {
        "Rank": 3312,
        "Port": "lagoa",
        "Eng": "large lake"
    },
    {
        "Rank": 3313,
        "Port": "credor",
        "Eng": "creditor"
    },
    {
        "Rank": 3314,
        "Port": "abordagem",
        "Eng": "approach"
    },
    {
        "Rank": 3315,
        "Port": "antena",
        "Eng": "antenna"
    },
    {
        "Rank": 3316,
        "Port": "capturar",
        "Eng": "capture"
    },
    {
        "Rank": 3317,
        "Port": "vingança",
        "Eng": "vengeance"
    },
    {
        "Rank": 3317,
        "Port": "vingança",
        "Eng": "revenge"
    },
    {
        "Rank": 3318,
        "Port": "tapar",
        "Eng": "cover"
    },
    {
        "Rank": 3318,
        "Port": "tapar",
        "Eng": "close"
    },
    {
        "Rank": 3319,
        "Port": "tábua",
        "Eng": "board"
    },
    {
        "Rank": 3319,
        "Port": "tábua",
        "Eng": "tablet"
    },
    {
        "Rank": 3319,
        "Port": "tábua",
        "Eng": "plank"
    },
    {
        "Rank": 3320,
        "Port": "mel",
        "Eng": "honey"
    },
    {
        "Rank": 3321,
        "Port": "encargo",
        "Eng": "responsibility"
    },
    {
        "Rank": 3321,
        "Port": "encargo",
        "Eng": "duty"
    },
    {
        "Rank": 3321,
        "Port": "encargo",
        "Eng": "job"
    },
    {
        "Rank": 3322,
        "Port": "condução",
        "Eng": "driving"
    },
    {
        "Rank": 3323,
        "Port": "táctico",
        "Eng": "tactical"
    },
    {
        "Rank": 3323,
        "Port": "táctico",
        "Eng": "tactician"
    },
    {
        "Rank": 3324,
        "Port": "barriga",
        "Eng": "belly"
    },
    {
        "Rank": 3324,
        "Port": "barriga",
        "Eng": "stomach"
    },
    {
        "Rank": 3325,
        "Port": "prosa",
        "Eng": "prose"
    },
    {
        "Rank": 3326,
        "Port": "percepção",
        "Eng": "perception"
    },
    {
        "Rank": 3327,
        "Port": "diariamente",
        "Eng": "daily"
    },
    {
        "Rank": 3328,
        "Port": "nacionalista",
        "Eng": "nationalist"
    },
    {
        "Rank": 3329,
        "Port": "investigador",
        "Eng": "researcher"
    },
    {
        "Rank": 3329,
        "Port": "investigador",
        "Eng": "investigator"
    },
    {
        "Rank": 3330,
        "Port": "salvação",
        "Eng": "salvation"
    },
    {
        "Rank": 3331,
        "Port": "tubo",
        "Eng": "tube"
    },
    {
        "Rank": 3332,
        "Port": "instância",
        "Eng": "occurrence"
    },
    {
        "Rank": 3332,
        "Port": "instância",
        "Eng": "instance"
    },
    {
        "Rank": 3333,
        "Port": "endereço",
        "Eng": "address"
    },
    {
        "Rank": 3334,
        "Port": "frágil",
        "Eng": "fragile"
    },
    {
        "Rank": 3335,
        "Port": "eficácia",
        "Eng": "effectiveness"
    },
    {
        "Rank": 3336,
        "Port": "revestir",
        "Eng": "cover"
    },
    {
        "Rank": 3336,
        "Port": "revestir",
        "Eng": "line"
    },
    {
        "Rank": 3337,
        "Port": "amoroso",
        "Eng": "loving"
    },
    {
        "Rank": 3337,
        "Port": "amoroso",
        "Eng": "tender"
    },
    {
        "Rank": 3337,
        "Port": "amoroso",
        "Eng": "sweet"
    },
    {
        "Rank": 3338,
        "Port": "implantação",
        "Eng": "implementation"
    },
    {
        "Rank": 3338,
        "Port": "implantação",
        "Eng": "implantation"
    },
    {
        "Rank": 3339,
        "Port": "privatização",
        "Eng": "privatization"
    },
    {
        "Rank": 3340,
        "Port": "parcial",
        "Eng": "partial"
    },
    {
        "Rank": 3341,
        "Port": "monge",
        "Eng": "monk"
    },
    {
        "Rank": 3342,
        "Port": "grama",
        "Eng": "grass"
    },
    {
        "Rank": 3343,
        "Port": "temporal",
        "Eng": "temporal"
    },
    {
        "Rank": 3343,
        "Port": "temporal",
        "Eng": "tempest"
    },
    {
        "Rank": 3344,
        "Port": "aproximadamente",
        "Eng": "approximately"
    },
    {
        "Rank": 3345,
        "Port": "convidado",
        "Eng": "guest"
    },
    {
        "Rank": 3345,
        "Port": "convidado",
        "Eng": "invited"
    },
    {
        "Rank": 3346,
        "Port": "regulamento",
        "Eng": "regulation"
    },
    {
        "Rank": 3347,
        "Port": "simpático",
        "Eng": "friendly"
    },
    {
        "Rank": 3347,
        "Port": "simpático",
        "Eng": "nice"
    },
    {
        "Rank": 3348,
        "Port": "idoso",
        "Eng": "elderly"
    },
    {
        "Rank": 3348,
        "Port": "idoso",
        "Eng": "aged"
    },
    {
        "Rank": 3349,
        "Port": "mensal",
        "Eng": "monthly"
    },
    {
        "Rank": 3350,
        "Port": "ansiedade",
        "Eng": "anxiety"
    },
    {
        "Rank": 3351,
        "Port": "disponibilidade",
        "Eng": "availability"
    },
    {
        "Rank": 3352,
        "Port": "povoação",
        "Eng": "settlement"
    },
    {
        "Rank": 3353,
        "Port": "afastamento",
        "Eng": "separation"
    },
    {
        "Rank": 3353,
        "Port": "afastamento",
        "Eng": "dismissal"
    },
    {
        "Rank": 3353,
        "Port": "afastamento",
        "Eng": "removal"
    },
    {
        "Rank": 3354,
        "Port": "minério",
        "Eng": "mineral"
    },
    {
        "Rank": 3355,
        "Port": "biológico",
        "Eng": "biological"
    },
    {
        "Rank": 3356,
        "Port": "intimidade",
        "Eng": "familiarity"
    },
    {
        "Rank": 3356,
        "Port": "intimidade",
        "Eng": "intimacy"
    },
    {
        "Rank": 3357,
        "Port": "cardeal",
        "Eng": "cardinal"
    },
    {
        "Rank": 3358,
        "Port": "rival",
        "Eng": "rival"
    },
    {
        "Rank": 3359,
        "Port": "instalado",
        "Eng": "installed"
    },
    {
        "Rank": 3360,
        "Port": "comemorar",
        "Eng": "commemorate"
    },
    {
        "Rank": 3361,
        "Port": "comprimento",
        "Eng": "length"
    },
    {
        "Rank": 3362,
        "Port": "roubo",
        "Eng": "theft"
    },
    {
        "Rank": 3362,
        "Port": "roubo",
        "Eng": "robbery"
    },
    {
        "Rank": 3363,
        "Port": "respectivamente",
        "Eng": "respectively"
    },
    {
        "Rank": 3364,
        "Port": "banca",
        "Eng": "news stand"
    },
    {
        "Rank": 3364,
        "Port": "banca",
        "Eng": "bench"
    },
    {
        "Rank": 3364,
        "Port": "banca",
        "Eng": "board"
    },
    {
        "Rank": 3365,
        "Port": "aceitação",
        "Eng": "acceptance"
    },
    {
        "Rank": 3366,
        "Port": "ida",
        "Eng": "trip"
    },
    {
        "Rank": 3366,
        "Port": "ida",
        "Eng": "departure"
    },
    {
        "Rank": 3367,
        "Port": "deparar",
        "Eng": "run into"
    },
    {
        "Rank": 3367,
        "Port": "deparar",
        "Eng": "come across"
    },
    {
        "Rank": 3368,
        "Port": "génio",
        "Eng": "temperament"
    },
    {
        "Rank": 3369,
        "Port": "denominar",
        "Eng": "call"
    },
    {
        "Rank": 3369,
        "Port": "denominar",
        "Eng": "name"
    },
    {
        "Rank": 3370,
        "Port": "editar",
        "Eng": "edit"
    },
    {
        "Rank": 3370,
        "Port": "editar",
        "Eng": "publish"
    },
    {
        "Rank": 3371,
        "Port": "cobrança",
        "Eng": "money collecting"
    },
    {
        "Rank": 3372,
        "Port": "esmagar",
        "Eng": "crush"
    },
    {
        "Rank": 3372,
        "Port": "esmagar",
        "Eng": "smash"
    },
    {
        "Rank": 3373,
        "Port": "colono",
        "Eng": "colonist"
    },
    {
        "Rank": 3373,
        "Port": "colono",
        "Eng": "colonizer"
    },
    {
        "Rank": 3374,
        "Port": "fachada",
        "Eng": "facade"
    },
    {
        "Rank": 3374,
        "Port": "fachada",
        "Eng": "appearance"
    },
    {
        "Rank": 3375,
        "Port": "túnel",
        "Eng": "tunnel"
    },
    {
        "Rank": 3376,
        "Port": "ousar",
        "Eng": "dare"
    },
    {
        "Rank": 3377,
        "Port": "chumbo",
        "Eng": "lead"
    },
    {
        "Rank": 3378,
        "Port": "magro",
        "Eng": "thin"
    },
    {
        "Rank": 3379,
        "Port": "treino",
        "Eng": "training"
    },
    {
        "Rank": 3380,
        "Port": "jóia",
        "Eng": "jewel"
    },
    {
        "Rank": 3381,
        "Port": "vós",
        "Eng": "you"
    },
    {
        "Rank": 3382,
        "Port": "popularidade",
        "Eng": "popularity"
    },
    {
        "Rank": 3383,
        "Port": "satélite",
        "Eng": "satellite"
    },
    {
        "Rank": 3384,
        "Port": "coordenador",
        "Eng": "coordinator"
    },
    {
        "Rank": 3384,
        "Port": "coordenador",
        "Eng": "coordinating"
    },
    {
        "Rank": 3385,
        "Port": "quartel",
        "Eng": "barracks"
    },
    {
        "Rank": 3385,
        "Port": "quartel",
        "Eng": "quarters"
    },
    {
        "Rank": 3386,
        "Port": "desconfiar",
        "Eng": "suspect"
    },
    {
        "Rank": 3386,
        "Port": "desconfiar",
        "Eng": "distrust"
    },
    {
        "Rank": 3387,
        "Port": "invés",
        "Eng": "instead"
    },
    {
        "Rank": 3388,
        "Port": "derrotar",
        "Eng": "defeat"
    },
    {
        "Rank": 3388,
        "Port": "derrotar",
        "Eng": "overthrow"
    },
    {
        "Rank": 3389,
        "Port": "golo",
        "Eng": "goal"
    },
    {
        "Rank": 3390,
        "Port": "fado",
        "Eng": "Portuguese musical genre"
    },
    {
        "Rank": 3390,
        "Port": "fado",
        "Eng": "fate"
    },
    {
        "Rank": 3391,
        "Port": "sorriso",
        "Eng": "smile"
    },
    {
        "Rank": 3392,
        "Port": "ilegal",
        "Eng": "illegal"
    },
    {
        "Rank": 3393,
        "Port": "vereador",
        "Eng": "city council member"
    },
    {
        "Rank": 3394,
        "Port": "pertencente",
        "Eng": "pertaining to"
    },
    {
        "Rank": 3394,
        "Port": "pertencente",
        "Eng": "belonging to"
    },
    {
        "Rank": 3395,
        "Port": "extinguir",
        "Eng": "extinguish"
    },
    {
        "Rank": 3396,
        "Port": "calhar",
        "Eng": "happen"
    },
    {
        "Rank": 3397,
        "Port": "acender",
        "Eng": "light"
    },
    {
        "Rank": 3397,
        "Port": "acender",
        "Eng": "turn on the lights"
    },
    {
        "Rank": 3398,
        "Port": "desprezar",
        "Eng": "despise"
    },
    {
        "Rank": 3398,
        "Port": "desprezar",
        "Eng": "ignore"
    },
    {
        "Rank": 3399,
        "Port": "encomendar",
        "Eng": "order"
    },
    {
        "Rank": 3399,
        "Port": "encomendar",
        "Eng": "commission"
    },
    {
        "Rank": 3400,
        "Port": "complicar",
        "Eng": "complicate"
    },
    {
        "Rank": 3401,
        "Port": "unha",
        "Eng": "fingernail"
    },
    {
        "Rank": 3401,
        "Port": "unha",
        "Eng": "toenail"
    },
    {
        "Rank": 3402,
        "Port": "indiano",
        "Eng": "Indian"
    },
    {
        "Rank": 3403,
        "Port": "bronze",
        "Eng": "bronze"
    },
    {
        "Rank": 3404,
        "Port": "navegação",
        "Eng": "navigation"
    },
    {
        "Rank": 3405,
        "Port": "macaco",
        "Eng": "monkey"
    },
    {
        "Rank": 3406,
        "Port": "adepto",
        "Eng": "adept"
    },
    {
        "Rank": 3406,
        "Port": "adepto",
        "Eng": "supporter"
    },
    {
        "Rank": 3406,
        "Port": "adepto",
        "Eng": "fan"
    },
    {
        "Rank": 3407,
        "Port": "ocupado",
        "Eng": "busy"
    },
    {
        "Rank": 3407,
        "Port": "ocupado",
        "Eng": "occupied"
    },
    {
        "Rank": 3408,
        "Port": "apreensão",
        "Eng": "capture"
    },
    {
        "Rank": 3408,
        "Port": "apreensão",
        "Eng": "apprehension"
    },
    {
        "Rank": 3409,
        "Port": "frota",
        "Eng": "fleet"
    },
    {
        "Rank": 3410,
        "Port": "fotográfico",
        "Eng": "photographic"
    },
    {
        "Rank": 3411,
        "Port": "livrar",
        "Eng": "free"
    },
    {
        "Rank": 3412,
        "Port": "protagonista",
        "Eng": "protagonist"
    },
    {
        "Rank": 3412,
        "Port": "protagonista",
        "Eng": "main character"
    },
    {
        "Rank": 3413,
        "Port": "contestar",
        "Eng": "contest"
    },
    {
        "Rank": 3413,
        "Port": "contestar",
        "Eng": "appeal"
    },
    {
        "Rank": 3414,
        "Port": "demora",
        "Eng": "delay"
    },
    {
        "Rank": 3414,
        "Port": "demora",
        "Eng": "wait"
    },
    {
        "Rank": 3415,
        "Port": "embarcar",
        "Eng": "embark"
    },
    {
        "Rank": 3416,
        "Port": "ignorância",
        "Eng": "ignorance"
    },
    {
        "Rank": 3417,
        "Port": "cooperativa",
        "Eng": "cooperative"
    },
    {
        "Rank": 3418,
        "Port": "jurar",
        "Eng": "swear"
    },
    {
        "Rank": 3419,
        "Port": "metropolitano",
        "Eng": "metropolitan"
    },
    {
        "Rank": 3419,
        "Port": "metropolitano",
        "Eng": "region"
    },
    {
        "Rank": 3420,
        "Port": "conforto",
        "Eng": "comfort"
    },
    {
        "Rank": 3421,
        "Port": "devidamente",
        "Eng": "duly"
    },
    {
        "Rank": 3421,
        "Port": "devidamente",
        "Eng": "rightfully"
    },
    {
        "Rank": 3422,
        "Port": "revisor",
        "Eng": "proofreader"
    },
    {
        "Rank": 3422,
        "Port": "revisor",
        "Eng": "examining"
    },
    {
        "Rank": 3423,
        "Port": "inocente",
        "Eng": "innocent"
    },
    {
        "Rank": 3424,
        "Port": "deficiência",
        "Eng": "deficiency"
    },
    {
        "Rank": 3425,
        "Port": "reprodução",
        "Eng": "reproduction"
    },
    {
        "Rank": 3426,
        "Port": "telefonar",
        "Eng": "call"
    },
    {
        "Rank": 3426,
        "Port": "telefonar",
        "Eng": "telephone"
    },
    {
        "Rank": 3427,
        "Port": "colo",
        "Eng": "lap"
    },
    {
        "Rank": 3428,
        "Port": "quadra",
        "Eng": "block"
    },
    {
        "Rank": 3428,
        "Port": "quadra",
        "Eng": "court"
    },
    {
        "Rank": 3429,
        "Port": "torcer",
        "Eng": "twist"
    },
    {
        "Rank": 3429,
        "Port": "torcer",
        "Eng": "root"
    },
    {
        "Rank": 3430,
        "Port": "mútuo",
        "Eng": "mutual"
    },
    {
        "Rank": 3431,
        "Port": "verba",
        "Eng": "funding"
    },
    {
        "Rank": 3431,
        "Port": "verba",
        "Eng": "amount"
    },
    {
        "Rank": 3432,
        "Port": "monstro",
        "Eng": "monster"
    },
    {
        "Rank": 3433,
        "Port": "súbito",
        "Eng": "sudden"
    },
    {
        "Rank": 3433,
        "Port": "súbito",
        "Eng": "unexpected"
    },
    {
        "Rank": 3434,
        "Port": "minoria",
        "Eng": "minority"
    },
    {
        "Rank": 3435,
        "Port": "elogio",
        "Eng": "compliment"
    },
    {
        "Rank": 3435,
        "Port": "elogio",
        "Eng": "praise"
    },
    {
        "Rank": 3436,
        "Port": "bárbaro",
        "Eng": "barbaric"
    },
    {
        "Rank": 3436,
        "Port": "bárbaro",
        "Eng": "barbarian"
    },
    {
        "Rank": 3437,
        "Port": "brinquedo",
        "Eng": "toy"
    },
    {
        "Rank": 3438,
        "Port": "aprofundar",
        "Eng": "go into depth"
    },
    {
        "Rank": 3438,
        "Port": "aprofundar",
        "Eng": "deepen"
    },
    {
        "Rank": 3439,
        "Port": "suave",
        "Eng": "soft"
    },
    {
        "Rank": 3439,
        "Port": "suave",
        "Eng": "pleasing"
    },
    {
        "Rank": 3439,
        "Port": "suave",
        "Eng": "smooth"
    },
    {
        "Rank": 3440,
        "Port": "leal",
        "Eng": "loyal"
    },
    {
        "Rank": 3441,
        "Port": "perturbar",
        "Eng": "disturb"
    },
    {
        "Rank": 3441,
        "Port": "perturbar",
        "Eng": "trouble"
    },
    {
        "Rank": 3441,
        "Port": "perturbar",
        "Eng": "annoy"
    },
    {
        "Rank": 3442,
        "Port": "inconsciente",
        "Eng": "unconscious"
    },
    {
        "Rank": 3443,
        "Port": "desespero",
        "Eng": "despair"
    },
    {
        "Rank": 3444,
        "Port": "piorar",
        "Eng": "get worse"
    },
    {
        "Rank": 3445,
        "Port": "chapa",
        "Eng": "plate"
    },
    {
        "Rank": 3445,
        "Port": "chapa",
        "Eng": "sheet"
    },
    {
        "Rank": 3446,
        "Port": "precipitar",
        "Eng": "come to a head"
    },
    {
        "Rank": 3446,
        "Port": "precipitar",
        "Eng": "rush"
    },
    {
        "Rank": 3446,
        "Port": "precipitar",
        "Eng": "act rashly"
    },
    {
        "Rank": 3447,
        "Port": "coro",
        "Eng": "choir"
    },
    {
        "Rank": 3447,
        "Port": "coro",
        "Eng": "chorus"
    },
    {
        "Rank": 3448,
        "Port": "safra",
        "Eng": "harvest"
    },
    {
        "Rank": 3449,
        "Port": "repressão",
        "Eng": "repression"
    },
    {
        "Rank": 3450,
        "Port": "mole",
        "Eng": "soft"
    },
    {
        "Rank": 3450,
        "Port": "mole",
        "Eng": "weak"
    },
    {
        "Rank": 3451,
        "Port": "adolescência",
        "Eng": "adolescence"
    },
    {
        "Rank": 3452,
        "Port": "convivência",
        "Eng": "coexistence"
    },
    {
        "Rank": 3452,
        "Port": "convivência",
        "Eng": "socializing"
    },
    {
        "Rank": 3453,
        "Port": "camarada",
        "Eng": "comrade"
    },
    {
        "Rank": 3453,
        "Port": "camarada",
        "Eng": "friend"
    },
    {
        "Rank": 3453,
        "Port": "camarada",
        "Eng": "guy"
    },
    {
        "Rank": 3454,
        "Port": "testar",
        "Eng": "test"
    },
    {
        "Rank": 3455,
        "Port": "maduro",
        "Eng": "ripe"
    },
    {
        "Rank": 3455,
        "Port": "maduro",
        "Eng": "mature"
    },
    {
        "Rank": 3456,
        "Port": "fragmento",
        "Eng": "fragment"
    },
    {
        "Rank": 3457,
        "Port": "obviamente",
        "Eng": "obviously"
    },
    {
        "Rank": 3458,
        "Port": "insuportável",
        "Eng": "unbearable"
    },
    {
        "Rank": 3459,
        "Port": "maravilha",
        "Eng": "wonder"
    },
    {
        "Rank": 3459,
        "Port": "maravilha",
        "Eng": "marvel"
    },
    {
        "Rank": 3460,
        "Port": "refugiar",
        "Eng": "take refuge"
    },
    {
        "Rank": 3461,
        "Port": "borda",
        "Eng": "bank"
    },
    {
        "Rank": 3461,
        "Port": "borda",
        "Eng": "edge"
    },
    {
        "Rank": 3461,
        "Port": "borda",
        "Eng": "margin"
    },
    {
        "Rank": 3462,
        "Port": "concorrente",
        "Eng": "competitor"
    },
    {
        "Rank": 3462,
        "Port": "concorrente",
        "Eng": "contestant"
    },
    {
        "Rank": 3463,
        "Port": "indício",
        "Eng": "hint"
    },
    {
        "Rank": 3463,
        "Port": "indício",
        "Eng": "sign"
    },
    {
        "Rank": 3463,
        "Port": "indício",
        "Eng": "trace"
    },
    {
        "Rank": 3464,
        "Port": "ridículo",
        "Eng": "ridiculous"
    },
    {
        "Rank": 3465,
        "Port": "sanitário",
        "Eng": "sanitary"
    },
    {
        "Rank": 3465,
        "Port": "sanitário",
        "Eng": "health"
    },
    {
        "Rank": 3466,
        "Port": "remeter",
        "Eng": "send"
    },
    {
        "Rank": 3466,
        "Port": "remeter",
        "Eng": "remit"
    },
    {
        "Rank": 3466,
        "Port": "remeter",
        "Eng": "forward"
    },
    {
        "Rank": 3467,
        "Port": "desligar",
        "Eng": "turn off"
    },
    {
        "Rank": 3467,
        "Port": "desligar",
        "Eng": "disconnect"
    },
    {
        "Rank": 3468,
        "Port": "pêlo",
        "Eng": "fur"
    },
    {
        "Rank": 3469,
        "Port": "consenso",
        "Eng": "consensus"
    },
    {
        "Rank": 3470,
        "Port": "experimental",
        "Eng": "experimental"
    },
    {
        "Rank": 3471,
        "Port": "cúpula",
        "Eng": "dome"
    },
    {
        "Rank": 3471,
        "Port": "cúpula",
        "Eng": "cupola"
    },
    {
        "Rank": 3472,
        "Port": "diploma",
        "Eng": "diploma"
    },
    {
        "Rank": 3473,
        "Port": "recado",
        "Eng": "message"
    },
    {
        "Rank": 3473,
        "Port": "recado",
        "Eng": "note"
    },
    {
        "Rank": 3474,
        "Port": "gerir",
        "Eng": "manage"
    },
    {
        "Rank": 3474,
        "Port": "gerir",
        "Eng": "direct"
    },
    {
        "Rank": 3474,
        "Port": "gerir",
        "Eng": "guide"
    },
    {
        "Rank": 3475,
        "Port": "pretexto",
        "Eng": "pretext"
    },
    {
        "Rank": 3476,
        "Port": "software",
        "Eng": "software"
    },
    {
        "Rank": 3477,
        "Port": "refeição",
        "Eng": "meal"
    },
    {
        "Rank": 3478,
        "Port": "proclamar",
        "Eng": "proclaim"
    },
    {
        "Rank": 3479,
        "Port": "telecomunicações",
        "Eng": "telecommunications"
    },
    {
        "Rank": 3480,
        "Port": "esquisito",
        "Eng": "strange"
    },
    {
        "Rank": 3481,
        "Port": "gota",
        "Eng": "drop"
    },
    {
        "Rank": 3482,
        "Port": "sexta-feira",
        "Eng": "Friday"
    },
    {
        "Rank": 3483,
        "Port": "diversidade",
        "Eng": "diversity"
    },
    {
        "Rank": 3484,
        "Port": "samba",
        "Eng": "samba"
    },
    {
        "Rank": 3485,
        "Port": "tabaco",
        "Eng": "tobacco"
    },
    {
        "Rank": 3486,
        "Port": "noventa",
        "Eng": "ninety"
    },
    {
        "Rank": 3487,
        "Port": "óptimo",
        "Eng": "excellent"
    },
    {
        "Rank": 3487,
        "Port": "óptimo",
        "Eng": "optimal"
    },
    {
        "Rank": 3488,
        "Port": "adopção",
        "Eng": "adoption"
    },
    {
        "Rank": 3489,
        "Port": "ascensão",
        "Eng": "rise"
    },
    {
        "Rank": 3489,
        "Port": "ascensão",
        "Eng": "ascension"
    },
    {
        "Rank": 3490,
        "Port": "parto",
        "Eng": "childbirth"
    },
    {
        "Rank": 3491,
        "Port": "medieval",
        "Eng": "medieval"
    },
    {
        "Rank": 3492,
        "Port": "contente",
        "Eng": "glad"
    },
    {
        "Rank": 3492,
        "Port": "contente",
        "Eng": "satisfied"
    },
    {
        "Rank": 3492,
        "Port": "contente",
        "Eng": "content"
    },
    {
        "Rank": 3493,
        "Port": "resumo",
        "Eng": "summary"
    },
    {
        "Rank": 3494,
        "Port": "variável",
        "Eng": "varying"
    },
    {
        "Rank": 3494,
        "Port": "variável",
        "Eng": "variable"
    },
    {
        "Rank": 3495,
        "Port": "confederação",
        "Eng": "confederation"
    },
    {
        "Rank": 3496,
        "Port": "ironia",
        "Eng": "irony"
    },
    {
        "Rank": 3497,
        "Port": "liso",
        "Eng": "straight"
    },
    {
        "Rank": 3497,
        "Port": "liso",
        "Eng": "smooth"
    },
    {
        "Rank": 3498,
        "Port": "eventualmente",
        "Eng": "eventually"
    },
    {
        "Rank": 3499,
        "Port": "lente",
        "Eng": "lens"
    },
    {
        "Rank": 3500,
        "Port": "habitualmente",
        "Eng": "usually"
    },
    {
        "Rank": 3500,
        "Port": "habitualmente",
        "Eng": "habitually"
    },
    {
        "Rank": 3501,
        "Port": "feição",
        "Eng": "feature"
    },
    {
        "Rank": 3501,
        "Port": "feição",
        "Eng": "appearance"
    },
    {
        "Rank": 3502,
        "Port": "calçada",
        "Eng": "sidewalk"
    },
    {
        "Rank": 3503,
        "Port": "óptico",
        "Eng": "optic"
    },
    {
        "Rank": 3503,
        "Port": "óptico",
        "Eng": "optical"
    },
    {
        "Rank": 3504,
        "Port": "previdência",
        "Eng": "precaution"
    },
    {
        "Rank": 3504,
        "Port": "previdência",
        "Eng": "welfare"
    },
    {
        "Rank": 3505,
        "Port": "defensor",
        "Eng": "defender"
    },
    {
        "Rank": 3505,
        "Port": "defensor",
        "Eng": "defending"
    },
    {
        "Rank": 3506,
        "Port": "determinante",
        "Eng": "determining"
    },
    {
        "Rank": 3506,
        "Port": "determinante",
        "Eng": "decisive"
    },
    {
        "Rank": 3507,
        "Port": "secar",
        "Eng": "dry"
    },
    {
        "Rank": 3508,
        "Port": "espacial",
        "Eng": "space"
    },
    {
        "Rank": 3508,
        "Port": "espacial",
        "Eng": "spatial"
    },
    {
        "Rank": 3509,
        "Port": "saia",
        "Eng": "skirt"
    },
    {
        "Rank": 3510,
        "Port": "barragem",
        "Eng": "dam"
    },
    {
        "Rank": 3510,
        "Port": "barragem",
        "Eng": "barrier"
    },
    {
        "Rank": 3511,
        "Port": "genético",
        "Eng": "genetic"
    },
    {
        "Rank": 3512,
        "Port": "economista",
        "Eng": "economist"
    },
    {
        "Rank": 3513,
        "Port": "designação",
        "Eng": "designation"
    },
    {
        "Rank": 3513,
        "Port": "designação",
        "Eng": "assignment"
    },
    {
        "Rank": 3514,
        "Port": "candidatar",
        "Eng": "become a candidate"
    },
    {
        "Rank": 3515,
        "Port": "vanguarda",
        "Eng": "avant-garde"
    },
    {
        "Rank": 3515,
        "Port": "vanguarda",
        "Eng": "front line"
    },
    {
        "Rank": 3516,
        "Port": "chocar",
        "Eng": "shock"
    },
    {
        "Rank": 3517,
        "Port": "provir",
        "Eng": "come from"
    },
    {
        "Rank": 3517,
        "Port": "provir",
        "Eng": "proceed from"
    },
    {
        "Rank": 3518,
        "Port": "verbo",
        "Eng": "verb"
    },
    {
        "Rank": 3519,
        "Port": "balcão",
        "Eng": "counter"
    },
    {
        "Rank": 3519,
        "Port": "balcão",
        "Eng": "balcony"
    },
    {
        "Rank": 3520,
        "Port": "painel",
        "Eng": "panel"
    },
    {
        "Rank": 3521,
        "Port": "perdido",
        "Eng": "lost"
    },
    {
        "Rank": 3522,
        "Port": "brigar",
        "Eng": "fight"
    },
    {
        "Rank": 3522,
        "Port": "brigar",
        "Eng": "argue"
    },
    {
        "Rank": 3523,
        "Port": "engraçado",
        "Eng": "funny"
    },
    {
        "Rank": 3524,
        "Port": "oriundo",
        "Eng": "originating from"
    },
    {
        "Rank": 3525,
        "Port": "cercado",
        "Eng": "enclosure"
    },
    {
        "Rank": 3525,
        "Port": "cercado",
        "Eng": "fenced"
    },
    {
        "Rank": 3526,
        "Port": "pulso",
        "Eng": "pulse"
    },
    {
        "Rank": 3526,
        "Port": "pulso",
        "Eng": "wrist"
    },
    {
        "Rank": 3527,
        "Port": "contorno",
        "Eng": "contour"
    },
    {
        "Rank": 3527,
        "Port": "contorno",
        "Eng": "outline"
    },
    {
        "Rank": 3528,
        "Port": "divergência",
        "Eng": "disagreement"
    },
    {
        "Rank": 3528,
        "Port": "divergência",
        "Eng": "divergence"
    },
    {
        "Rank": 3529,
        "Port": "bombeiro",
        "Eng": "fireman"
    },
    {
        "Rank": 3530,
        "Port": "possivelmente",
        "Eng": "possibly"
    },
    {
        "Rank": 3531,
        "Port": "comparecer",
        "Eng": "attend"
    },
    {
        "Rank": 3531,
        "Port": "comparecer",
        "Eng": "appear at"
    },
    {
        "Rank": 3532,
        "Port": "reinar",
        "Eng": "rule"
    },
    {
        "Rank": 3532,
        "Port": "reinar",
        "Eng": "reign"
    },
    {
        "Rank": 3533,
        "Port": "máscara",
        "Eng": "mask"
    },
    {
        "Rank": 3534,
        "Port": "dissolver",
        "Eng": "dissolve"
    },
    {
        "Rank": 3535,
        "Port": "pânico",
        "Eng": "panic"
    },
    {
        "Rank": 3536,
        "Port": "bíblia",
        "Eng": "Bible"
    },
    {
        "Rank": 3537,
        "Port": "pinheiro",
        "Eng": "pine tree"
    },
    {
        "Rank": 3538,
        "Port": "embarcação",
        "Eng": "boat"
    },
    {
        "Rank": 3538,
        "Port": "embarcação",
        "Eng": "ship"
    },
    {
        "Rank": 3539,
        "Port": "educativo",
        "Eng": "educational"
    },
    {
        "Rank": 3540,
        "Port": "simplicidade",
        "Eng": "simplicity"
    },
    {
        "Rank": 3541,
        "Port": "paralelamente",
        "Eng": "at the same time"
    },
    {
        "Rank": 3541,
        "Port": "paralelamente",
        "Eng": "concurrently"
    },
    {
        "Rank": 3542,
        "Port": "rapidez",
        "Eng": "speed"
    },
    {
        "Rank": 3543,
        "Port": "hesitar",
        "Eng": "hesitate"
    },
    {
        "Rank": 3544,
        "Port": "sintético",
        "Eng": "synthetic"
    },
    {
        "Rank": 3545,
        "Port": "elite",
        "Eng": "elite"
    },
    {
        "Rank": 3546,
        "Port": "vulnerável",
        "Eng": "vulnerable"
    },
    {
        "Rank": 3547,
        "Port": "pátio",
        "Eng": "courtyard"
    },
    {
        "Rank": 3547,
        "Port": "pátio",
        "Eng": "patio"
    },
    {
        "Rank": 3547,
        "Port": "pátio",
        "Eng": "atrium"
    },
    {
        "Rank": 3548,
        "Port": "irritar",
        "Eng": "irritate"
    },
    {
        "Rank": 3549,
        "Port": "apressar",
        "Eng": "hurry"
    },
    {
        "Rank": 3549,
        "Port": "apressar",
        "Eng": "hasten"
    },
    {
        "Rank": 3550,
        "Port": "lesão",
        "Eng": "lesion"
    },
    {
        "Rank": 3551,
        "Port": "avó",
        "Eng": "grandmother"
    },
    {
        "Rank": 3552,
        "Port": "copiar",
        "Eng": "copy"
    },
    {
        "Rank": 3553,
        "Port": "capitalista",
        "Eng": "capitalist"
    },
    {
        "Rank": 3554,
        "Port": "túmulo",
        "Eng": "tomb"
    },
    {
        "Rank": 3555,
        "Port": "movimentação",
        "Eng": "movement"
    },
    {
        "Rank": 3556,
        "Port": "cintura",
        "Eng": "waist"
    },
    {
        "Rank": 3557,
        "Port": "ligeiramente",
        "Eng": "slightly"
    },
    {
        "Rank": 3557,
        "Port": "ligeiramente",
        "Eng": "lightly"
    },
    {
        "Rank": 3558,
        "Port": "olímpico",
        "Eng": "Olympic"
    },
    {
        "Rank": 3559,
        "Port": "politicamente",
        "Eng": "politically"
    },
    {
        "Rank": 3560,
        "Port": "juvenil",
        "Eng": "youthful"
    },
    {
        "Rank": 3560,
        "Port": "juvenil",
        "Eng": "juvenile"
    },
    {
        "Rank": 3561,
        "Port": "proibição",
        "Eng": "prohibition"
    },
    {
        "Rank": 3562,
        "Port": "galo",
        "Eng": "rooster"
    },
    {
        "Rank": 3563,
        "Port": "induzir",
        "Eng": "induce"
    },
    {
        "Rank": 3563,
        "Port": "induzir",
        "Eng": "incite"
    },
    {
        "Rank": 3564,
        "Port": "órbita",
        "Eng": "orbit"
    },
    {
        "Rank": 3565,
        "Port": "sublinhar",
        "Eng": "emphasize"
    },
    {
        "Rank": 3565,
        "Port": "sublinhar",
        "Eng": "underline"
    },
    {
        "Rank": 3565,
        "Port": "sublinhar",
        "Eng": "stress"
    },
    {
        "Rank": 3566,
        "Port": "totalidade",
        "Eng": "totality"
    },
    {
        "Rank": 3566,
        "Port": "totalidade",
        "Eng": "fullness"
    },
    {
        "Rank": 3567,
        "Port": "vigilância",
        "Eng": "surveillance"
    },
    {
        "Rank": 3567,
        "Port": "vigilância",
        "Eng": "vigilance"
    },
    {
        "Rank": 3568,
        "Port": "geografia",
        "Eng": "geography"
    },
    {
        "Rank": 3569,
        "Port": "gravidez",
        "Eng": "pregnancy"
    },
    {
        "Rank": 3570,
        "Port": "bancada",
        "Eng": "bench"
    },
    {
        "Rank": 3570,
        "Port": "bancada",
        "Eng": "parliamentary group"
    },
    {
        "Rank": 3571,
        "Port": "poupança",
        "Eng": "savings"
    },
    {
        "Rank": 3572,
        "Port": "indiferente",
        "Eng": "indifferent"
    },
    {
        "Rank": 3573,
        "Port": "óculos",
        "Eng": "glasses"
    },
    {
        "Rank": 3573,
        "Port": "óculos",
        "Eng": "spectacles"
    },
    {
        "Rank": 3574,
        "Port": "descanso",
        "Eng": "rest"
    },
    {
        "Rank": 3575,
        "Port": "confissão",
        "Eng": "confession"
    },
    {
        "Rank": 3576,
        "Port": "fundir",
        "Eng": "smelt"
    },
    {
        "Rank": 3576,
        "Port": "fundir",
        "Eng": "melt"
    },
    {
        "Rank": 3577,
        "Port": "admiração",
        "Eng": "admiration"
    },
    {
        "Rank": 3578,
        "Port": "rebentar",
        "Eng": "burst"
    },
    {
        "Rank": 3578,
        "Port": "rebentar",
        "Eng": "explode"
    },
    {
        "Rank": 3579,
        "Port": "substituto",
        "Eng": "substitute"
    },
    {
        "Rank": 3580,
        "Port": "pesquisador",
        "Eng": "researcher"
    },
    {
        "Rank": 3580,
        "Port": "pesquisador",
        "Eng": "investigator"
    },
    {
        "Rank": 3581,
        "Port": "temático",
        "Eng": "thematic"
    },
    {
        "Rank": 3582,
        "Port": "sacerdote",
        "Eng": "priest"
    },
    {
        "Rank": 3583,
        "Port": "animação",
        "Eng": "encouragement"
    },
    {
        "Rank": 3584,
        "Port": "excepcional",
        "Eng": "exceptional"
    },
    {
        "Rank": 3585,
        "Port": "engolir",
        "Eng": "swallow"
    },
    {
        "Rank": 3586,
        "Port": "garganta",
        "Eng": "throat"
    },
    {
        "Rank": 3587,
        "Port": "biografia",
        "Eng": "biography"
    },
    {
        "Rank": 3588,
        "Port": "intuito",
        "Eng": "motive"
    },
    {
        "Rank": 3588,
        "Port": "intuito",
        "Eng": "design"
    },
    {
        "Rank": 3588,
        "Port": "intuito",
        "Eng": "goal"
    },
    {
        "Rank": 3589,
        "Port": "emergir",
        "Eng": "emerge"
    },
    {
        "Rank": 3590,
        "Port": "eco",
        "Eng": "echo"
    },
    {
        "Rank": 3591,
        "Port": "sábio",
        "Eng": "wise"
    },
    {
        "Rank": 3592,
        "Port": "retórica",
        "Eng": "rhetoric"
    },
    {
        "Rank": 3593,
        "Port": "testemunho",
        "Eng": "witness"
    },
    {
        "Rank": 3593,
        "Port": "testemunho",
        "Eng": "testimony"
    },
    {
        "Rank": 3594,
        "Port": "sumir",
        "Eng": "disappear"
    },
    {
        "Rank": 3595,
        "Port": "doloroso",
        "Eng": "painful"
    },
    {
        "Rank": 3595,
        "Port": "doloroso",
        "Eng": "hurtful"
    },
    {
        "Rank": 3596,
        "Port": "fabricante",
        "Eng": "manufacturer"
    },
    {
        "Rank": 3596,
        "Port": "fabricante",
        "Eng": "producer"
    },
    {
        "Rank": 3597,
        "Port": "errado",
        "Eng": "wrong"
    },
    {
        "Rank": 3598,
        "Port": "mencionar",
        "Eng": "mention"
    },
    {
        "Rank": 3599,
        "Port": "exemplar",
        "Eng": "copy"
    },
    {
        "Rank": 3599,
        "Port": "exemplar",
        "Eng": "exemplary"
    },
    {
        "Rank": 3600,
        "Port": "injustiça",
        "Eng": "unfairness"
    },
    {
        "Rank": 3600,
        "Port": "injustiça",
        "Eng": "injustice"
    },
    {
        "Rank": 3601,
        "Port": "institucional",
        "Eng": "institutional"
    },
    {
        "Rank": 3602,
        "Port": "localidade",
        "Eng": "place"
    },
    {
        "Rank": 3602,
        "Port": "localidade",
        "Eng": "location"
    },
    {
        "Rank": 3603,
        "Port": "molho",
        "Eng": "sauce"
    },
    {
        "Rank": 3604,
        "Port": "engano",
        "Eng": "mistake"
    },
    {
        "Rank": 3604,
        "Port": "engano",
        "Eng": "error"
    },
    {
        "Rank": 3604,
        "Port": "engano",
        "Eng": "deceit"
    },
    {
        "Rank": 3605,
        "Port": "agulha",
        "Eng": "needle"
    },
    {
        "Rank": 3606,
        "Port": "turno",
        "Eng": "round"
    },
    {
        "Rank": 3606,
        "Port": "turno",
        "Eng": "shift"
    },
    {
        "Rank": 3607,
        "Port": "surdo",
        "Eng": "deaf"
    },
    {
        "Rank": 3608,
        "Port": "descida",
        "Eng": "drop"
    },
    {
        "Rank": 3608,
        "Port": "descida",
        "Eng": "decrease"
    },
    {
        "Rank": 3609,
        "Port": "recorde",
        "Eng": "record"
    },
    {
        "Rank": 3610,
        "Port": "calda",
        "Eng": "syrup"
    },
    {
        "Rank": 3611,
        "Port": "dotado",
        "Eng": "endowed"
    },
    {
        "Rank": 3611,
        "Port": "dotado",
        "Eng": "gifted"
    },
    {
        "Rank": 3612,
        "Port": "naval",
        "Eng": "naval"
    },
    {
        "Rank": 3613,
        "Port": "proferir",
        "Eng": "utter"
    },
    {
        "Rank": 3613,
        "Port": "proferir",
        "Eng": "state"
    },
    {
        "Rank": 3613,
        "Port": "proferir",
        "Eng": "say"
    },
    {
        "Rank": 3614,
        "Port": "mármore",
        "Eng": "marble"
    },
    {
        "Rank": 3615,
        "Port": "especialidade",
        "Eng": "specialty"
    },
    {
        "Rank": 3616,
        "Port": "gerente",
        "Eng": "manager"
    },
    {
        "Rank": 3617,
        "Port": "detectar",
        "Eng": "detect"
    },
    {
        "Rank": 3618,
        "Port": "picar",
        "Eng": "sting"
    },
    {
        "Rank": 3618,
        "Port": "picar",
        "Eng": "prick"
    },
    {
        "Rank": 3618,
        "Port": "picar",
        "Eng": "bite"
    },
    {
        "Rank": 3619,
        "Port": "infra-estrutura",
        "Eng": "infrastructure"
    },
    {
        "Rank": 3620,
        "Port": "sindical",
        "Eng": "relating to a trade union"
    },
    {
        "Rank": 3621,
        "Port": "trono",
        "Eng": "throne"
    },
    {
        "Rank": 3622,
        "Port": "accionista",
        "Eng": "stockholder"
    },
    {
        "Rank": 3623,
        "Port": "reeleição",
        "Eng": "re-election"
    },
    {
        "Rank": 3624,
        "Port": "socorro",
        "Eng": "aid"
    },
    {
        "Rank": 3624,
        "Port": "socorro",
        "Eng": "help"
    },
    {
        "Rank": 3624,
        "Port": "socorro",
        "Eng": "relief"
    },
    {
        "Rank": 3625,
        "Port": "turístico",
        "Eng": "tourist"
    },
    {
        "Rank": 3626,
        "Port": "agitação",
        "Eng": "commotion"
    },
    {
        "Rank": 3626,
        "Port": "agitação",
        "Eng": "agitation"
    },
    {
        "Rank": 3627,
        "Port": "elegante",
        "Eng": "elegant"
    },
    {
        "Rank": 3628,
        "Port": "sucedido",
        "Eng": "successful"
    },
    {
        "Rank": 3628,
        "Port": "sucedido",
        "Eng": "succeeded"
    },
    {
        "Rank": 3629,
        "Port": "magnífico",
        "Eng": "magnificent"
    },
    {
        "Rank": 3630,
        "Port": "irregularidade",
        "Eng": "irregularity"
    },
    {
        "Rank": 3631,
        "Port": "tráfico",
        "Eng": "trafficking"
    },
    {
        "Rank": 3632,
        "Port": "equilibrado",
        "Eng": "balanced"
    },
    {
        "Rank": 3633,
        "Port": "lógico",
        "Eng": "logical"
    },
    {
        "Rank": 3634,
        "Port": "decorrente",
        "Eng": "resulting from"
    },
    {
        "Rank": 3635,
        "Port": "integral",
        "Eng": "full"
    },
    {
        "Rank": 3635,
        "Port": "integral",
        "Eng": "whole"
    },
    {
        "Rank": 3635,
        "Port": "integral",
        "Eng": "integral"
    },
    {
        "Rank": 3636,
        "Port": "consequentemente",
        "Eng": "consequently"
    },
    {
        "Rank": 3636,
        "Port": "consequentemente",
        "Eng": "as a result"
    },
    {
        "Rank": 3637,
        "Port": "intérprete",
        "Eng": "performer"
    },
    {
        "Rank": 3637,
        "Port": "intérprete",
        "Eng": "Interpreter"
    },
    {
        "Rank": 3638,
        "Port": "educado",
        "Eng": "polite"
    },
    {
        "Rank": 3638,
        "Port": "educado",
        "Eng": "well-mannered"
    },
    {
        "Rank": 3638,
        "Port": "educado",
        "Eng": "educated"
    },
    {
        "Rank": 3639,
        "Port": "recordação",
        "Eng": "recollection"
    },
    {
        "Rank": 3639,
        "Port": "recordação",
        "Eng": "souvenir"
    },
    {
        "Rank": 3640,
        "Port": "inspector",
        "Eng": "inspector"
    },
    {
        "Rank": 3641,
        "Port": "instituir",
        "Eng": "institute"
    },
    {
        "Rank": 3642,
        "Port": "leque",
        "Eng": "range"
    },
    {
        "Rank": 3642,
        "Port": "leque",
        "Eng": "scope"
    },
    {
        "Rank": 3642,
        "Port": "leque",
        "Eng": "hand-fan"
    },
    {
        "Rank": 3643,
        "Port": "filmar",
        "Eng": "film"
    },
    {
        "Rank": 3644,
        "Port": "estatístico",
        "Eng": "statistical"
    },
    {
        "Rank": 3644,
        "Port": "estatístico",
        "Eng": "statistician"
    },
    {
        "Rank": 3645,
        "Port": "estabelecido",
        "Eng": "established"
    },
    {
        "Rank": 3646,
        "Port": "denso",
        "Eng": "dense"
    },
    {
        "Rank": 3646,
        "Port": "denso",
        "Eng": "thick"
    },
    {
        "Rank": 3647,
        "Port": "convívio",
        "Eng": "act of living together"
    },
    {
        "Rank": 3648,
        "Port": "fígado",
        "Eng": "liver"
    },
    {
        "Rank": 3649,
        "Port": "habituado",
        "Eng": "used to"
    },
    {
        "Rank": 3649,
        "Port": "habituado",
        "Eng": "accustomed"
    },
    {
        "Rank": 3650,
        "Port": "mala",
        "Eng": "suitcase"
    },
    {
        "Rank": 3650,
        "Port": "mala",
        "Eng": "luggage"
    },
    {
        "Rank": 3651,
        "Port": "repouso",
        "Eng": "rest"
    },
    {
        "Rank": 3651,
        "Port": "repouso",
        "Eng": "repose"
    },
    {
        "Rank": 3652,
        "Port": "batalhão",
        "Eng": "battalion"
    },
    {
        "Rank": 3653,
        "Port": "dispersar",
        "Eng": "disperse"
    },
    {
        "Rank": 3653,
        "Port": "dispersar",
        "Eng": "scatter"
    },
    {
        "Rank": 3654,
        "Port": "apaixonar",
        "Eng": "fall in love with"
    },
    {
        "Rank": 3655,
        "Port": "vegetação",
        "Eng": "vegetation"
    },
    {
        "Rank": 3656,
        "Port": "procurador",
        "Eng": "proxy"
    },
    {
        "Rank": 3656,
        "Port": "procurador",
        "Eng": "attorney"
    },
    {
        "Rank": 3657,
        "Port": "tristeza",
        "Eng": "sadness"
    },
    {
        "Rank": 3658,
        "Port": "enfiar",
        "Eng": "put into or through"
    },
    {
        "Rank": 3659,
        "Port": "afundar",
        "Eng": "sink"
    },
    {
        "Rank": 3660,
        "Port": "exibição",
        "Eng": "showing"
    },
    {
        "Rank": 3660,
        "Port": "exibição",
        "Eng": "exhibition"
    },
    {
        "Rank": 3661,
        "Port": "espantar",
        "Eng": "surprise"
    },
    {
        "Rank": 3662,
        "Port": "gigantesco",
        "Eng": "gigantic"
    },
    {
        "Rank": 3663,
        "Port": "cordão",
        "Eng": "cord"
    },
    {
        "Rank": 3663,
        "Port": "cordão",
        "Eng": "string"
    },
    {
        "Rank": 3663,
        "Port": "cordão",
        "Eng": "necklace"
    },
    {
        "Rank": 3664,
        "Port": "rezar",
        "Eng": "say a rote prayer"
    },
    {
        "Rank": 3665,
        "Port": "exportar",
        "Eng": "export"
    },
    {
        "Rank": 3666,
        "Port": "precário",
        "Eng": "precarious"
    },
    {
        "Rank": 3667,
        "Port": "tentação",
        "Eng": "temptation"
    },
    {
        "Rank": 3668,
        "Port": "sino",
        "Eng": "bell"
    },
    {
        "Rank": 3669,
        "Port": "decorar",
        "Eng": "memorize"
    },
    {
        "Rank": 3669,
        "Port": "decorar",
        "Eng": "decorate"
    },
    {
        "Rank": 3670,
        "Port": "suspeito",
        "Eng": "suspect"
    },
    {
        "Rank": 3670,
        "Port": "suspeito",
        "Eng": "suspected"
    },
    {
        "Rank": 3671,
        "Port": "saldo",
        "Eng": "balance"
    },
    {
        "Rank": 3671,
        "Port": "saldo",
        "Eng": "remainder"
    },
    {
        "Rank": 3672,
        "Port": "síntese",
        "Eng": "synthesis"
    },
    {
        "Rank": 3673,
        "Port": "malha",
        "Eng": "knit clothing"
    },
    {
        "Rank": 3673,
        "Port": "malha",
        "Eng": "net"
    },
    {
        "Rank": 3673,
        "Port": "malha",
        "Eng": "club"
    },
    {
        "Rank": 3674,
        "Port": "coelho",
        "Eng": "rabbit"
    },
    {
        "Rank": 3675,
        "Port": "soar",
        "Eng": "sound"
    },
    {
        "Rank": 3676,
        "Port": "atrapalhar",
        "Eng": "get in the way of"
    },
    {
        "Rank": 3676,
        "Port": "atrapalhar",
        "Eng": "frustrate"
    },
    {
        "Rank": 3677,
        "Port": "falência",
        "Eng": "bankruptcy"
    },
    {
        "Rank": 3677,
        "Port": "falência",
        "Eng": "failure"
    },
    {
        "Rank": 3678,
        "Port": "imposição",
        "Eng": "imposition"
    },
    {
        "Rank": 3679,
        "Port": "recusa",
        "Eng": "refusal"
    },
    {
        "Rank": 3680,
        "Port": "navegar",
        "Eng": "navigate"
    },
    {
        "Rank": 3681,
        "Port": "inspecção",
        "Eng": "inspection"
    },
    {
        "Rank": 3682,
        "Port": "embaixada",
        "Eng": "embassy"
    },
    {
        "Rank": 3683,
        "Port": "moço",
        "Eng": "young man"
    },
    {
        "Rank": 3683,
        "Port": "moço",
        "Eng": "boy"
    },
    {
        "Rank": 3684,
        "Port": "conversão",
        "Eng": "conversion"
    },
    {
        "Rank": 3685,
        "Port": "ânimo",
        "Eng": "spirit"
    },
    {
        "Rank": 3685,
        "Port": "ânimo",
        "Eng": "courage"
    },
    {
        "Rank": 3685,
        "Port": "ânimo",
        "Eng": "excitement"
    },
    {
        "Rank": 3686,
        "Port": "acampamento",
        "Eng": "camp"
    },
    {
        "Rank": 3686,
        "Port": "acampamento",
        "Eng": "encampment"
    },
    {
        "Rank": 3687,
        "Port": "altar",
        "Eng": "altar"
    },
    {
        "Rank": 3688,
        "Port": "assento",
        "Eng": "seat"
    },
    {
        "Rank": 3688,
        "Port": "assento",
        "Eng": "spot"
    },
    {
        "Rank": 3689,
        "Port": "doar",
        "Eng": "bequeath"
    },
    {
        "Rank": 3689,
        "Port": "doar",
        "Eng": "donate"
    },
    {
        "Rank": 3690,
        "Port": "coordenar",
        "Eng": "manage"
    },
    {
        "Rank": 3690,
        "Port": "coordenar",
        "Eng": "coordinate"
    },
    {
        "Rank": 3691,
        "Port": "vestuário",
        "Eng": "clothing"
    },
    {
        "Rank": 3691,
        "Port": "vestuário",
        "Eng": "garment"
    },
    {
        "Rank": 3692,
        "Port": "desagradável",
        "Eng": "unpleasant"
    },
    {
        "Rank": 3693,
        "Port": "treze",
        "Eng": "thirteen"
    },
    {
        "Rank": 3694,
        "Port": "ácido",
        "Eng": "acid"
    },
    {
        "Rank": 3695,
        "Port": "saudável",
        "Eng": "healthy"
    },
    {
        "Rank": 3696,
        "Port": "lençol",
        "Eng": "sheet"
    },
    {
        "Rank": 3697,
        "Port": "decadência",
        "Eng": "decline"
    },
    {
        "Rank": 3697,
        "Port": "decadência",
        "Eng": "decadence"
    },
    {
        "Rank": 3698,
        "Port": "uva",
        "Eng": "grape"
    },
    {
        "Rank": 3699,
        "Port": "retirada",
        "Eng": "retreat"
    },
    {
        "Rank": 3699,
        "Port": "retirada",
        "Eng": "withdrawal"
    },
    {
        "Rank": 3700,
        "Port": "tapete",
        "Eng": "rug"
    },
    {
        "Rank": 3700,
        "Port": "tapete",
        "Eng": "carpet"
    },
    {
        "Rank": 3701,
        "Port": "fiar",
        "Eng": "spin"
    },
    {
        "Rank": 3701,
        "Port": "fiar",
        "Eng": "trust"
    },
    {
        "Rank": 3702,
        "Port": "generoso",
        "Eng": "generous"
    },
    {
        "Rank": 3703,
        "Port": "penal",
        "Eng": "penal"
    },
    {
        "Rank": 3704,
        "Port": "incentivar",
        "Eng": "encourage"
    },
    {
        "Rank": 3704,
        "Port": "incentivar",
        "Eng": "motivate"
    },
    {
        "Rank": 3705,
        "Port": "foguete",
        "Eng": "firework"
    },
    {
        "Rank": 3705,
        "Port": "foguete",
        "Eng": "rocket"
    },
    {
        "Rank": 3705,
        "Port": "foguete",
        "Eng": "fast train"
    },
    {
        "Rank": 3706,
        "Port": "barraca",
        "Eng": "shack"
    },
    {
        "Rank": 3706,
        "Port": "barraca",
        "Eng": "tent"
    },
    {
        "Rank": 3706,
        "Port": "barraca",
        "Eng": "hut"
    },
    {
        "Rank": 3707,
        "Port": "soberania",
        "Eng": "sovereignty"
    },
    {
        "Rank": 3707,
        "Port": "soberania",
        "Eng": "authority"
    },
    {
        "Rank": 3708,
        "Port": "curar",
        "Eng": "cure"
    },
    {
        "Rank": 3708,
        "Port": "curar",
        "Eng": "heal"
    },
    {
        "Rank": 3709,
        "Port": "cerrado",
        "Eng": "closed"
    },
    {
        "Rank": 3710,
        "Port": "repertório",
        "Eng": "repertoire"
    },
    {
        "Rank": 3711,
        "Port": "árbitro",
        "Eng": "referee"
    },
    {
        "Rank": 3712,
        "Port": "maciço",
        "Eng": "in mass"
    },
    {
        "Rank": 3712,
        "Port": "maciço",
        "Eng": "solid"
    },
    {
        "Rank": 3712,
        "Port": "maciço",
        "Eng": "mountain range"
    },
    {
        "Rank": 3713,
        "Port": "fatal",
        "Eng": "fatal"
    },
    {
        "Rank": 3714,
        "Port": "desesperado",
        "Eng": "in despair"
    },
    {
        "Rank": 3714,
        "Port": "desesperado",
        "Eng": "without hope"
    },
    {
        "Rank": 3715,
        "Port": "terreiro",
        "Eng": "yard"
    },
    {
        "Rank": 3715,
        "Port": "terreiro",
        "Eng": "spacious outdoor area"
    },
    {
        "Rank": 3716,
        "Port": "imperial",
        "Eng": "imperial"
    },
    {
        "Rank": 3717,
        "Port": "cineasta",
        "Eng": "film producer"
    },
    {
        "Rank": 3718,
        "Port": "percentagem",
        "Eng": "percentage"
    },
    {
        "Rank": 3719,
        "Port": "escalar",
        "Eng": "scale"
    },
    {
        "Rank": 3719,
        "Port": "escalar",
        "Eng": "climb"
    },
    {
        "Rank": 3720,
        "Port": "ficha",
        "Eng": "form"
    },
    {
        "Rank": 3720,
        "Port": "ficha",
        "Eng": "slip"
    },
    {
        "Rank": 3720,
        "Port": "ficha",
        "Eng": "card"
    },
    {
        "Rank": 3721,
        "Port": "fingir",
        "Eng": "pretend"
    },
    {
        "Rank": 3721,
        "Port": "fingir",
        "Eng": "fake"
    },
    {
        "Rank": 3722,
        "Port": "ofensiva",
        "Eng": "offensive"
    },
    {
        "Rank": 3723,
        "Port": "oscilar",
        "Eng": "shake"
    },
    {
        "Rank": 3723,
        "Port": "oscilar",
        "Eng": "oscillate"
    },
    {
        "Rank": 3724,
        "Port": "enxergar",
        "Eng": "catch sight of"
    },
    {
        "Rank": 3724,
        "Port": "enxergar",
        "Eng": "make out"
    },
    {
        "Rank": 3725,
        "Port": "desequilíbrio",
        "Eng": "imbalance"
    },
    {
        "Rank": 3726,
        "Port": "pop",
        "Eng": "pop"
    },
    {
        "Rank": 3727,
        "Port": "detrás",
        "Eng": "behind"
    },
    {
        "Rank": 3728,
        "Port": "vestígio",
        "Eng": "remains"
    },
    {
        "Rank": 3728,
        "Port": "vestígio",
        "Eng": "vestige"
    },
    {
        "Rank": 3729,
        "Port": "vaidade",
        "Eng": "vanity"
    },
    {
        "Rank": 3730,
        "Port": "pressionar",
        "Eng": "pressure"
    },
    {
        "Rank": 3730,
        "Port": "pressionar",
        "Eng": "press"
    },
    {
        "Rank": 3731,
        "Port": "bailarina",
        "Eng": "ballerina"
    },
    {
        "Rank": 3732,
        "Port": "fórum",
        "Eng": "court"
    },
    {
        "Rank": 3732,
        "Port": "fórum",
        "Eng": "forum"
    },
    {
        "Rank": 3733,
        "Port": "bicicleta",
        "Eng": "bicycle"
    },
    {
        "Rank": 3734,
        "Port": "consagrar",
        "Eng": "be recognized for"
    },
    {
        "Rank": 3734,
        "Port": "consagrar",
        "Eng": "consecrate"
    },
    {
        "Rank": 3735,
        "Port": "torneio",
        "Eng": "tournament"
    },
    {
        "Rank": 3736,
        "Port": "realismo",
        "Eng": "realism"
    },
    {
        "Rank": 3737,
        "Port": "aranha",
        "Eng": "spider"
    },
    {
        "Rank": 3738,
        "Port": "quinhentos",
        "Eng": "five hundred"
    },
    {
        "Rank": 3739,
        "Port": "brilhar",
        "Eng": "sparkle"
    },
    {
        "Rank": 3739,
        "Port": "brilhar",
        "Eng": "shine"
    },
    {
        "Rank": 3740,
        "Port": "delegação",
        "Eng": "delegation"
    },
    {
        "Rank": 3741,
        "Port": "aquando",
        "Eng": "at the time"
    },
    {
        "Rank": 3742,
        "Port": "fotografar",
        "Eng": "photograph"
    },
    {
        "Rank": 3743,
        "Port": "morder",
        "Eng": "bite"
    },
    {
        "Rank": 3744,
        "Port": "bateria",
        "Eng": "battery"
    },
    {
        "Rank": 3744,
        "Port": "bateria",
        "Eng": "percussion"
    },
    {
        "Rank": 3745,
        "Port": "restringir",
        "Eng": "restrict"
    },
    {
        "Rank": 3746,
        "Port": "cofre",
        "Eng": "safe"
    },
    {
        "Rank": 3746,
        "Port": "cofre",
        "Eng": "coffer"
    },
    {
        "Rank": 3746,
        "Port": "cofre",
        "Eng": "chest"
    },
    {
        "Rank": 3747,
        "Port": "sudeste",
        "Eng": "Southeast"
    },
    {
        "Rank": 3748,
        "Port": "lírico",
        "Eng": "lyric"
    },
    {
        "Rank": 3749,
        "Port": "invisível",
        "Eng": "invisible"
    },
    {
        "Rank": 3750,
        "Port": "místico",
        "Eng": "mystic"
    },
    {
        "Rank": 3750,
        "Port": "místico",
        "Eng": "mystical"
    },
    {
        "Rank": 3751,
        "Port": "reger",
        "Eng": "rule"
    },
    {
        "Rank": 3751,
        "Port": "reger",
        "Eng": "manage"
    },
    {
        "Rank": 3751,
        "Port": "reger",
        "Eng": "conduct"
    },
    {
        "Rank": 3752,
        "Port": "culpar",
        "Eng": "blame"
    },
    {
        "Rank": 3752,
        "Port": "culpar",
        "Eng": "place guilt"
    },
    {
        "Rank": 3753,
        "Port": "automaticamente",
        "Eng": "automatically"
    },
    {
        "Rank": 3754,
        "Port": "clareza",
        "Eng": "clarity"
    },
    {
        "Rank": 3755,
        "Port": "elevação",
        "Eng": "rising"
    },
    {
        "Rank": 3755,
        "Port": "elevação",
        "Eng": "elevation"
    },
    {
        "Rank": 3755,
        "Port": "elevação",
        "Eng": "ascent"
    },
    {
        "Rank": 3756,
        "Port": "transparência",
        "Eng": "clarity"
    },
    {
        "Rank": 3756,
        "Port": "transparência",
        "Eng": "transparency"
    },
    {
        "Rank": 3757,
        "Port": "cansar",
        "Eng": "get tired"
    },
    {
        "Rank": 3758,
        "Port": "cunhado",
        "Eng": "brother or sister-in-law"
    },
    {
        "Rank": 3759,
        "Port": "vencimento",
        "Eng": "expiration"
    },
    {
        "Rank": 3759,
        "Port": "vencimento",
        "Eng": "salary"
    },
    {
        "Rank": 3760,
        "Port": "liberado",
        "Eng": "free"
    },
    {
        "Rank": 3760,
        "Port": "liberado",
        "Eng": "available"
    },
    {
        "Rank": 3761,
        "Port": "doação",
        "Eng": "donation"
    },
    {
        "Rank": 3762,
        "Port": "nascente",
        "Eng": "source"
    },
    {
        "Rank": 3762,
        "Port": "nascente",
        "Eng": "East"
    },
    {
        "Rank": 3762,
        "Port": "nascente",
        "Eng": "emerging"
    },
    {
        "Rank": 3763,
        "Port": "consagrado",
        "Eng": "consecrated"
    },
    {
        "Rank": 3764,
        "Port": "ingénuo",
        "Eng": "naive"
    },
    {
        "Rank": 3764,
        "Port": "ingénuo",
        "Eng": "innocent"
    },
    {
        "Rank": 3765,
        "Port": "sopa",
        "Eng": "soup"
    },
    {
        "Rank": 3766,
        "Port": "livraria",
        "Eng": "bookstore"
    },
    {
        "Rank": 3767,
        "Port": "panorama",
        "Eng": "scene"
    },
    {
        "Rank": 3767,
        "Port": "panorama",
        "Eng": "panorama"
    },
    {
        "Rank": 3767,
        "Port": "panorama",
        "Eng": "view"
    },
    {
        "Rank": 3768,
        "Port": "treinador",
        "Eng": "coach"
    },
    {
        "Rank": 3768,
        "Port": "treinador",
        "Eng": "trainer"
    },
    {
        "Rank": 3769,
        "Port": "segunda-feira",
        "Eng": "Monday"
    },
    {
        "Rank": 3770,
        "Port": "lâmpada",
        "Eng": "lamp"
    },
    {
        "Rank": 3770,
        "Port": "lâmpada",
        "Eng": "light bulb"
    },
    {
        "Rank": 3771,
        "Port": "namens",
        "Eng": "seated"
    },
    {
        "Rank": 3772,
        "Port": "turista",
        "Eng": "tourist"
    },
    {
        "Rank": 3773,
        "Port": "alugar",
        "Eng": "rent"
    },
    {
        "Rank": 3774,
        "Port": "ante",
        "Eng": "before"
    },
    {
        "Rank": 3775,
        "Port": "cheirar",
        "Eng": "smell"
    },
    {
        "Rank": 3776,
        "Port": "altitude",
        "Eng": "altitude"
    },
    {
        "Rank": 3777,
        "Port": "limpo",
        "Eng": "clear"
    },
    {
        "Rank": 3777,
        "Port": "limpo",
        "Eng": "clean"
    },
    {
        "Rank": 3778,
        "Port": "generalizado",
        "Eng": "widespread"
    },
    {
        "Rank": 3778,
        "Port": "generalizado",
        "Eng": "generalized"
    },
    {
        "Rank": 3779,
        "Port": "seca",
        "Eng": "drought"
    },
    {
        "Rank": 3780,
        "Port": "escultor",
        "Eng": "sculptor"
    },
    {
        "Rank": 3781,
        "Port": "parágrafo",
        "Eng": "paragraph"
    },
    {
        "Rank": 3782,
        "Port": "humilde",
        "Eng": "humble"
    },
    {
        "Rank": 3783,
        "Port": "desconfiança",
        "Eng": "distrust"
    },
    {
        "Rank": 3784,
        "Port": "considerável",
        "Eng": "considerable"
    },
    {
        "Rank": 3785,
        "Port": "marcação",
        "Eng": "measuring"
    },
    {
        "Rank": 3785,
        "Port": "marcação",
        "Eng": "marking"
    },
    {
        "Rank": 3786,
        "Port": "galego",
        "Eng": "Galician"
    },
    {
        "Rank": 3787,
        "Port": "favela",
        "Eng": "slum"
    },
    {
        "Rank": 3787,
        "Port": "favela",
        "Eng": "ghetto"
    },
    {
        "Rank": 3788,
        "Port": "vingar",
        "Eng": "avenge"
    },
    {
        "Rank": 3788,
        "Port": "vingar",
        "Eng": "take revenge"
    },
    {
        "Rank": 3789,
        "Port": "farmácia",
        "Eng": "pharmacy"
    },
    {
        "Rank": 3790,
        "Port": "quintal",
        "Eng": "yard"
    },
    {
        "Rank": 3791,
        "Port": "agressão",
        "Eng": "aggression"
    },
    {
        "Rank": 3792,
        "Port": "ônibus",
        "Eng": "bus"
    },
    {
        "Rank": 3793,
        "Port": "angolano",
        "Eng": "Angolan"
    },
    {
        "Rank": 3794,
        "Port": "superficial",
        "Eng": "superficial"
    },
    {
        "Rank": 3795,
        "Port": "nacionalidade",
        "Eng": "nationality"
    },
    {
        "Rank": 3796,
        "Port": "instinto",
        "Eng": "instinct"
    },
    {
        "Rank": 3797,
        "Port": "nítido",
        "Eng": "clear"
    },
    {
        "Rank": 3797,
        "Port": "nítido",
        "Eng": "explicit"
    },
    {
        "Rank": 3797,
        "Port": "nítido",
        "Eng": "sharp"
    },
    {
        "Rank": 3798,
        "Port": "tia",
        "Eng": "aunt"
    },
    {
        "Rank": 3799,
        "Port": "autoritário",
        "Eng": "authoritarian"
    },
    {
        "Rank": 3800,
        "Port": "contrapartida",
        "Eng": "on the other hand"
    },
    {
        "Rank": 3801,
        "Port": "continuação",
        "Eng": "continuation"
    },
    {
        "Rank": 3802,
        "Port": "bandido",
        "Eng": "outlaw"
    },
    {
        "Rank": 3802,
        "Port": "bandido",
        "Eng": "bandit"
    },
    {
        "Rank": 3803,
        "Port": "tráfego",
        "Eng": "traffic"
    },
    {
        "Rank": 3804,
        "Port": "recomeçar",
        "Eng": "resume"
    },
    {
        "Rank": 3804,
        "Port": "recomeçar",
        "Eng": "restart"
    },
    {
        "Rank": 3805,
        "Port": "reivindicação",
        "Eng": "demand"
    },
    {
        "Rank": 3805,
        "Port": "reivindicação",
        "Eng": "formal complaint"
    },
    {
        "Rank": 3806,
        "Port": "quantia",
        "Eng": "sum"
    },
    {
        "Rank": 3806,
        "Port": "quantia",
        "Eng": "portion"
    },
    {
        "Rank": 3806,
        "Port": "quantia",
        "Eng": "amount"
    },
    {
        "Rank": 3807,
        "Port": "misterioso",
        "Eng": "mysterious"
    },
    {
        "Rank": 3808,
        "Port": "têxtil",
        "Eng": "textile"
    },
    {
        "Rank": 3809,
        "Port": "subterrâneo",
        "Eng": "underground"
    },
    {
        "Rank": 3810,
        "Port": "disfarçar",
        "Eng": "disguise"
    },
    {
        "Rank": 3810,
        "Port": "disfarçar",
        "Eng": "pretend"
    },
    {
        "Rank": 3811,
        "Port": "sucessor",
        "Eng": "successor"
    },
    {
        "Rank": 3812,
        "Port": "apetecer",
        "Eng": "appeal"
    },
    {
        "Rank": 3813,
        "Port": "étnico",
        "Eng": "ethnic"
    },
    {
        "Rank": 3814,
        "Port": "noiva",
        "Eng": "bride"
    },
    {
        "Rank": 3814,
        "Port": "noiva",
        "Eng": "fiancée"
    },
    {
        "Rank": 3815,
        "Port": "fundado",
        "Eng": "based"
    },
    {
        "Rank": 3815,
        "Port": "fundado",
        "Eng": "founded"
    },
    {
        "Rank": 3816,
        "Port": "demónio",
        "Eng": "devil"
    },
    {
        "Rank": 3816,
        "Port": "demónio",
        "Eng": "demon"
    },
    {
        "Rank": 3817,
        "Port": "honesto",
        "Eng": "honest"
    },
    {
        "Rank": 3818,
        "Port": "desculpar",
        "Eng": "forgive"
    },
    {
        "Rank": 3818,
        "Port": "desculpar",
        "Eng": "excuse"
    },
    {
        "Rank": 3819,
        "Port": "ultimamente",
        "Eng": "lately"
    },
    {
        "Rank": 3820,
        "Port": "degradação",
        "Eng": "degradation"
    },
    {
        "Rank": 3821,
        "Port": "cruzamento",
        "Eng": "mixing"
    },
    {
        "Rank": 3821,
        "Port": "cruzamento",
        "Eng": "intersection"
    },
    {
        "Rank": 3822,
        "Port": "acarretar",
        "Eng": "cause"
    },
    {
        "Rank": 3822,
        "Port": "acarretar",
        "Eng": "provoke"
    },
    {
        "Rank": 3823,
        "Port": "verbal",
        "Eng": "verbal"
    },
    {
        "Rank": 3824,
        "Port": "nobreza",
        "Eng": "nobility"
    },
    {
        "Rank": 3825,
        "Port": "pernambucano",
        "Eng": "from Pernambuco"
    },
    {
        "Rank": 3826,
        "Port": "estatística",
        "Eng": "statistic"
    },
    {
        "Rank": 3826,
        "Port": "estatística",
        "Eng": "statistics"
    },
    {
        "Rank": 3827,
        "Port": "táxi",
        "Eng": "taxi"
    },
    {
        "Rank": 3828,
        "Port": "preservação",
        "Eng": "preservation"
    },
    {
        "Rank": 3829,
        "Port": "operacional",
        "Eng": "operational"
    },
    {
        "Rank": 3829,
        "Port": "operacional",
        "Eng": "operating"
    },
    {
        "Rank": 3830,
        "Port": "manteiga",
        "Eng": "butter"
    },
    {
        "Rank": 3831,
        "Port": "emocional",
        "Eng": "emotional"
    },
    {
        "Rank": 3832,
        "Port": "perdão",
        "Eng": "forgiveness"
    },
    {
        "Rank": 3833,
        "Port": "rasgar",
        "Eng": "tear"
    },
    {
        "Rank": 3833,
        "Port": "rasgar",
        "Eng": "rip"
    },
    {
        "Rank": 3834,
        "Port": "gene",
        "Eng": "gene"
    },
    {
        "Rank": 3835,
        "Port": "vertical",
        "Eng": "vertical"
    },
    {
        "Rank": 3836,
        "Port": "lenha",
        "Eng": "firewood"
    },
    {
        "Rank": 3837,
        "Port": "mentalidade",
        "Eng": "mentality"
    },
    {
        "Rank": 3838,
        "Port": "terrorista",
        "Eng": "terrorist"
    },
    {
        "Rank": 3839,
        "Port": "perturbação",
        "Eng": "disturbance"
    },
    {
        "Rank": 3839,
        "Port": "perturbação",
        "Eng": "commotion"
    },
    {
        "Rank": 3840,
        "Port": "lote",
        "Eng": "lot"
    },
    {
        "Rank": 3841,
        "Port": "bem-estar",
        "Eng": "well-being"
    },
    {
        "Rank": 3842,
        "Port": "cruel",
        "Eng": "cruel"
    },
    {
        "Rank": 3843,
        "Port": "rondar",
        "Eng": "be approximately"
    },
    {
        "Rank": 3843,
        "Port": "rondar",
        "Eng": "circle"
    },
    {
        "Rank": 3844,
        "Port": "reclamação",
        "Eng": "complaint"
    },
    {
        "Rank": 3844,
        "Port": "reclamação",
        "Eng": "grievance"
    },
    {
        "Rank": 3845,
        "Port": "infeliz",
        "Eng": "unhappy"
    },
    {
        "Rank": 3846,
        "Port": "ténis",
        "Eng": "tennis"
    },
    {
        "Rank": 3846,
        "Port": "ténis",
        "Eng": "tennis shoes"
    },
    {
        "Rank": 3847,
        "Port": "manso",
        "Eng": "meek"
    },
    {
        "Rank": 3847,
        "Port": "manso",
        "Eng": "tame"
    },
    {
        "Rank": 3847,
        "Port": "manso",
        "Eng": "gentle"
    },
    {
        "Rank": 3848,
        "Port": "confuso",
        "Eng": "confused"
    },
    {
        "Rank": 3848,
        "Port": "confuso",
        "Eng": "confusing"
    },
    {
        "Rank": 3849,
        "Port": "oposto",
        "Eng": "opposite"
    },
    {
        "Rank": 3849,
        "Port": "oposto",
        "Eng": "opposing"
    },
    {
        "Rank": 3850,
        "Port": "degrau",
        "Eng": "step"
    },
    {
        "Rank": 3851,
        "Port": "camisola",
        "Eng": "nightgown"
    },
    {
        "Rank": 3852,
        "Port": "magnético",
        "Eng": "magnetic"
    },
    {
        "Rank": 3853,
        "Port": "hectare",
        "Eng": "hectare"
    },
    {
        "Rank": 3854,
        "Port": "beijo",
        "Eng": "kiss"
    },
    {
        "Rank": 3855,
        "Port": "reformar",
        "Eng": "reform"
    },
    {
        "Rank": 3856,
        "Port": "revoltar",
        "Eng": "revolt"
    },
    {
        "Rank": 3856,
        "Port": "revoltar",
        "Eng": "disgust"
    },
    {
        "Rank": 3857,
        "Port": "ascender",
        "Eng": "go up"
    },
    {
        "Rank": 3857,
        "Port": "ascender",
        "Eng": "ascend"
    },
    {
        "Rank": 3858,
        "Port": "interlocutor",
        "Eng": "spokesperson"
    },
    {
        "Rank": 3858,
        "Port": "interlocutor",
        "Eng": "speaker"
    },
    {
        "Rank": 3859,
        "Port": "centrar",
        "Eng": "center"
    },
    {
        "Rank": 3859,
        "Port": "centrar",
        "Eng": "focus"
    },
    {
        "Rank": 3860,
        "Port": "mobilizar",
        "Eng": "mobilize"
    },
    {
        "Rank": 3861,
        "Port": "incerteza",
        "Eng": "uncertainty"
    },
    {
        "Rank": 3862,
        "Port": "testamento",
        "Eng": "will"
    },
    {
        "Rank": 3862,
        "Port": "testamento",
        "Eng": "testament"
    },
    {
        "Rank": 3863,
        "Port": "feroz",
        "Eng": "fierce"
    },
    {
        "Rank": 3863,
        "Port": "feroz",
        "Eng": "ferocious"
    },
    {
        "Rank": 3864,
        "Port": "comercialização",
        "Eng": "marketing"
    },
    {
        "Rank": 3864,
        "Port": "comercialização",
        "Eng": "commercialization"
    },
    {
        "Rank": 3865,
        "Port": "despejar",
        "Eng": "pour"
    },
    {
        "Rank": 3865,
        "Port": "despejar",
        "Eng": "evict"
    },
    {
        "Rank": 3866,
        "Port": "encosta",
        "Eng": "slope"
    },
    {
        "Rank": 3866,
        "Port": "encosta",
        "Eng": "hillside"
    },
    {
        "Rank": 3867,
        "Port": "aprendizagem",
        "Eng": "learning"
    },
    {
        "Rank": 3868,
        "Port": "aproveitamento",
        "Eng": "act of taking advantage"
    },
    {
        "Rank": 3869,
        "Port": "escritura",
        "Eng": "scriptures"
    },
    {
        "Rank": 3869,
        "Port": "escritura",
        "Eng": "legal document"
    },
    {
        "Rank": 3870,
        "Port": "pancada",
        "Eng": "blow"
    },
    {
        "Rank": 3870,
        "Port": "pancada",
        "Eng": "hit"
    },
    {
        "Rank": 3871,
        "Port": "cansaço",
        "Eng": "fatigue"
    },
    {
        "Rank": 3871,
        "Port": "cansaço",
        "Eng": "exhaustion"
    },
    {
        "Rank": 3872,
        "Port": "racional",
        "Eng": "rational"
    },
    {
        "Rank": 3873,
        "Port": "virtual",
        "Eng": "virtual"
    },
    {
        "Rank": 3874,
        "Port": "disciplinar",
        "Eng": "discipline"
    },
    {
        "Rank": 3875,
        "Port": "fornecimento",
        "Eng": "supply"
    },
    {
        "Rank": 3875,
        "Port": "fornecimento",
        "Eng": "furnishing"
    },
    {
        "Rank": 3876,
        "Port": "monarquia",
        "Eng": "monarchy"
    },
    {
        "Rank": 3877,
        "Port": "esforçar",
        "Eng": "strive"
    },
    {
        "Rank": 3877,
        "Port": "esforçar",
        "Eng": "try hard"
    },
    {
        "Rank": 3878,
        "Port": "repartição",
        "Eng": "office"
    },
    {
        "Rank": 3878,
        "Port": "repartição",
        "Eng": "distribution"
    },
    {
        "Rank": 3879,
        "Port": "indirecto",
        "Eng": "indirect"
    },
    {
        "Rank": 3880,
        "Port": "característico",
        "Eng": "characteristic"
    },
    {
        "Rank": 3881,
        "Port": "quinta-feira",
        "Eng": "Thursday"
    },
    {
        "Rank": 3882,
        "Port": "fraude",
        "Eng": "fraud"
    },
    {
        "Rank": 3883,
        "Port": "especificamente",
        "Eng": "specifically"
    },
    {
        "Rank": 3884,
        "Port": "júri",
        "Eng": "jury"
    },
    {
        "Rank": 3885,
        "Port": "sétimo",
        "Eng": "seventh"
    },
    {
        "Rank": 3886,
        "Port": "cela",
        "Eng": "cell"
    },
    {
        "Rank": 3887,
        "Port": "pousar",
        "Eng": "land"
    },
    {
        "Rank": 3887,
        "Port": "pousar",
        "Eng": "rest"
    },
    {
        "Rank": 3888,
        "Port": "sanção",
        "Eng": "sanction"
    },
    {
        "Rank": 3889,
        "Port": "empenho",
        "Eng": "dedication"
    },
    {
        "Rank": 3889,
        "Port": "empenho",
        "Eng": "effort"
    },
    {
        "Rank": 3889,
        "Port": "empenho",
        "Eng": "focus"
    },
    {
        "Rank": 3890,
        "Port": "afastado",
        "Eng": "cut off"
    },
    {
        "Rank": 3890,
        "Port": "afastado",
        "Eng": "removed"
    },
    {
        "Rank": 3890,
        "Port": "afastado",
        "Eng": "distanced"
    },
    {
        "Rank": 3891,
        "Port": "decoração",
        "Eng": "decoration"
    },
    {
        "Rank": 3891,
        "Port": "decoração",
        "Eng": "embellishment"
    },
    {
        "Rank": 3892,
        "Port": "ponderar",
        "Eng": "ponder"
    },
    {
        "Rank": 3893,
        "Port": "fogueira",
        "Eng": "bonfire"
    },
    {
        "Rank": 3894,
        "Port": "exceder",
        "Eng": "exceed"
    },
    {
        "Rank": 3895,
        "Port": "mentir",
        "Eng": "lie"
    },
    {
        "Rank": 3896,
        "Port": "polémica",
        "Eng": "controversy"
    },
    {
        "Rank": 3897,
        "Port": "acessível",
        "Eng": "accessible"
    },
    {
        "Rank": 3898,
        "Port": "confrontar",
        "Eng": "confront"
    },
    {
        "Rank": 3899,
        "Port": "secretário-geral",
        "Eng": "secretary-general"
    },
    {
        "Rank": 3900,
        "Port": "requisito",
        "Eng": "requirement"
    },
    {
        "Rank": 3900,
        "Port": "requisito",
        "Eng": "requisite"
    },
    {
        "Rank": 3901,
        "Port": "valorização",
        "Eng": "appreciation"
    },
    {
        "Rank": 3902,
        "Port": "litro",
        "Eng": "liter"
    },
    {
        "Rank": 3903,
        "Port": "criativo",
        "Eng": "creative"
    },
    {
        "Rank": 3904,
        "Port": "constituinte",
        "Eng": "constituent"
    },
    {
        "Rank": 3904,
        "Port": "constituinte",
        "Eng": "part of"
    },
    {
        "Rank": 3904,
        "Port": "constituinte",
        "Eng": "member"
    },
    {
        "Rank": 3905,
        "Port": "silencioso",
        "Eng": "silent"
    },
    {
        "Rank": 3906,
        "Port": "salarial",
        "Eng": "relating to salary"
    },
    {
        "Rank": 3907,
        "Port": "adjunto",
        "Eng": "assistant"
    },
    {
        "Rank": 3907,
        "Port": "adjunto",
        "Eng": "adjunct"
    },
    {
        "Rank": 3908,
        "Port": "caridade",
        "Eng": "charity"
    },
    {
        "Rank": 3909,
        "Port": "carência",
        "Eng": "lack of"
    },
    {
        "Rank": 3909,
        "Port": "carência",
        "Eng": "need"
    },
    {
        "Rank": 3910,
        "Port": "grávida",
        "Eng": "pregnant"
    },
    {
        "Rank": 3911,
        "Port": "sofisticado",
        "Eng": "sophisticated"
    },
    {
        "Rank": 3912,
        "Port": "mobiliário",
        "Eng": "furniture"
    },
    {
        "Rank": 3913,
        "Port": "repetição",
        "Eng": "repetition"
    },
    {
        "Rank": 3914,
        "Port": "discípulo",
        "Eng": "disciple"
    },
    {
        "Rank": 3915,
        "Port": "trezentos",
        "Eng": "three hundred"
    },
    {
        "Rank": 3916,
        "Port": "pianista",
        "Eng": "pianist"
    },
    {
        "Rank": 3917,
        "Port": "alastrar",
        "Eng": "spread"
    },
    {
        "Rank": 3918,
        "Port": "restaurar",
        "Eng": "restore"
    },
    {
        "Rank": 3919,
        "Port": "teia",
        "Eng": "web"
    },
    {
        "Rank": 3920,
        "Port": "cova",
        "Eng": "opening"
    },
    {
        "Rank": 3920,
        "Port": "cova",
        "Eng": "cave"
    },
    {
        "Rank": 3920,
        "Port": "cova",
        "Eng": "hole"
    },
    {
        "Rank": 3921,
        "Port": "tarifa",
        "Eng": "tariff"
    },
    {
        "Rank": 3922,
        "Port": "carregado",
        "Eng": "loaded with"
    },
    {
        "Rank": 3922,
        "Port": "carregado",
        "Eng": "carried"
    },
    {
        "Rank": 3923,
        "Port": "decretar",
        "Eng": "decree"
    },
    {
        "Rank": 3924,
        "Port": "intuição",
        "Eng": "intuition"
    },
    {
        "Rank": 3925,
        "Port": "imigrante",
        "Eng": "immigrant"
    },
    {
        "Rank": 3926,
        "Port": "ditar",
        "Eng": "dictate"
    },
    {
        "Rank": 3927,
        "Port": "infecção",
        "Eng": "infection"
    },
    {
        "Rank": 3928,
        "Port": "sabedoria",
        "Eng": "wisdom"
    },
    {
        "Rank": 3929,
        "Port": "pioneiro",
        "Eng": "pioneer"
    },
    {
        "Rank": 3929,
        "Port": "pioneiro",
        "Eng": "pioneering"
    },
    {
        "Rank": 3930,
        "Port": "banhar",
        "Eng": "bathe"
    },
    {
        "Rank": 3931,
        "Port": "tremendo",
        "Eng": "tremendous"
    },
    {
        "Rank": 3932,
        "Port": "aparição",
        "Eng": "apparition"
    },
    {
        "Rank": 3932,
        "Port": "aparição",
        "Eng": "appearance"
    },
    {
        "Rank": 3933,
        "Port": "devagar",
        "Eng": "slowly"
    },
    {
        "Rank": 3933,
        "Port": "devagar",
        "Eng": "slow"
    },
    {
        "Rank": 3934,
        "Port": "libra",
        "Eng": "pound"
    },
    {
        "Rank": 3935,
        "Port": "acalmar",
        "Eng": "calm down"
    },
    {
        "Rank": 3935,
        "Port": "acalmar",
        "Eng": "appease"
    },
    {
        "Rank": 3936,
        "Port": "esclarecimento",
        "Eng": "clarification"
    },
    {
        "Rank": 3936,
        "Port": "esclarecimento",
        "Eng": "explanation"
    },
    {
        "Rank": 3937,
        "Port": "proveito",
        "Eng": "profit"
    },
    {
        "Rank": 3937,
        "Port": "proveito",
        "Eng": "taking advantage"
    },
    {
        "Rank": 3938,
        "Port": "comemoração",
        "Eng": "commemoration"
    },
    {
        "Rank": 3939,
        "Port": "desembarcar",
        "Eng": "get out"
    },
    {
        "Rank": 3939,
        "Port": "desembarcar",
        "Eng": "disembark"
    },
    {
        "Rank": 3940,
        "Port": "adeus",
        "Eng": "goodbye"
    },
    {
        "Rank": 3940,
        "Port": "adeus",
        "Eng": "farewell"
    },
    {
        "Rank": 3940,
        "Port": "adeus",
        "Eng": "adieu"
    },
    {
        "Rank": 3941,
        "Port": "massacre",
        "Eng": "massacre"
    },
    {
        "Rank": 3942,
        "Port": "retratar",
        "Eng": "portray"
    },
    {
        "Rank": 3943,
        "Port": "injusto",
        "Eng": "unjust"
    },
    {
        "Rank": 3943,
        "Port": "injusto",
        "Eng": "unfair"
    },
    {
        "Rank": 3944,
        "Port": "limitado",
        "Eng": "limited"
    },
    {
        "Rank": 3945,
        "Port": "tributário",
        "Eng": "related to taxes"
    },
    {
        "Rank": 3945,
        "Port": "tributário",
        "Eng": "tributary"
    },
    {
        "Rank": 3946,
        "Port": "florestal",
        "Eng": "relating to the forest"
    },
    {
        "Rank": 3947,
        "Port": "sociologia",
        "Eng": "sociology"
    },
    {
        "Rank": 3948,
        "Port": "fidelidade",
        "Eng": "loyalty"
    },
    {
        "Rank": 3948,
        "Port": "fidelidade",
        "Eng": "faithfulness"
    },
    {
        "Rank": 3948,
        "Port": "fidelidade",
        "Eng": "fidelity"
    },
    {
        "Rank": 3949,
        "Port": "contraditório",
        "Eng": "contradictory"
    },
    {
        "Rank": 3950,
        "Port": "mineral",
        "Eng": "mineral"
    },
    {
        "Rank": 3951,
        "Port": "partícula",
        "Eng": "particle"
    },
    {
        "Rank": 3952,
        "Port": "auxiliar",
        "Eng": "help"
    },
    {
        "Rank": 3952,
        "Port": "auxiliar",
        "Eng": "aid"
    },
    {
        "Rank": 3953,
        "Port": "estoque",
        "Eng": "stock"
    },
    {
        "Rank": 3954,
        "Port": "probabilidade",
        "Eng": "probability"
    },
    {
        "Rank": 3955,
        "Port": "gol",
        "Eng": "soccer goal"
    },
    {
        "Rank": 3956,
        "Port": "coral",
        "Eng": "coral"
    },
    {
        "Rank": 3956,
        "Port": "coral",
        "Eng": "choral"
    },
    {
        "Rank": 3957,
        "Port": "contratar",
        "Eng": "contract"
    },
    {
        "Rank": 3958,
        "Port": "cambial",
        "Eng": "related to exchange rates"
    },
    {
        "Rank": 3959,
        "Port": "coxa",
        "Eng": "thigh"
    },
    {
        "Rank": 3960,
        "Port": "progressivo",
        "Eng": "progressive"
    },
    {
        "Rank": 3961,
        "Port": "reivindicar",
        "Eng": "demand"
    },
    {
        "Rank": 3961,
        "Port": "reivindicar",
        "Eng": "ask for"
    },
    {
        "Rank": 3962,
        "Port": "narrar",
        "Eng": "narrate"
    },
    {
        "Rank": 3963,
        "Port": "obtenção",
        "Eng": "getting"
    },
    {
        "Rank": 3963,
        "Port": "obtenção",
        "Eng": "obtaining"
    },
    {
        "Rank": 3964,
        "Port": "eh",
        "Eng": "hey"
    },
    {
        "Rank": 3965,
        "Port": "península",
        "Eng": "peninsula"
    },
    {
        "Rank": 3966,
        "Port": "actualidade",
        "Eng": "the present"
    },
    {
        "Rank": 3966,
        "Port": "actualidade",
        "Eng": "modern times"
    },
    {
        "Rank": 3967,
        "Port": "vibração",
        "Eng": "vibration"
    },
    {
        "Rank": 3968,
        "Port": "fabrico",
        "Eng": "production"
    },
    {
        "Rank": 3969,
        "Port": "moderado",
        "Eng": "moderate"
    },
    {
        "Rank": 3970,
        "Port": "esportivo",
        "Eng": "sporting"
    },
    {
        "Rank": 3970,
        "Port": "esportivo",
        "Eng": "sport"
    },
    {
        "Rank": 3971,
        "Port": "ênfase",
        "Eng": "emphasis"
    },
    {
        "Rank": 3972,
        "Port": "contabilidade",
        "Eng": "bookkeeping"
    },
    {
        "Rank": 3972,
        "Port": "contabilidade",
        "Eng": "accounting"
    },
    {
        "Rank": 3973,
        "Port": "cerebral",
        "Eng": "cerebral"
    },
    {
        "Rank": 3973,
        "Port": "cerebral",
        "Eng": "of the brain"
    },
    {
        "Rank": 3974,
        "Port": "marinheiro",
        "Eng": "sailor"
    },
    {
        "Rank": 3974,
        "Port": "marinheiro",
        "Eng": "seaman"
    },
    {
        "Rank": 3975,
        "Port": "publicamente",
        "Eng": "publicly"
    },
    {
        "Rank": 3976,
        "Port": "provedor",
        "Eng": "provider"
    },
    {
        "Rank": 3977,
        "Port": "reprimir",
        "Eng": "repress"
    },
    {
        "Rank": 3977,
        "Port": "reprimir",
        "Eng": "control"
    },
    {
        "Rank": 3978,
        "Port": "subordinado",
        "Eng": "subordinate"
    },
    {
        "Rank": 3978,
        "Port": "subordinado",
        "Eng": "subordinated"
    },
    {
        "Rank": 3979,
        "Port": "abundante",
        "Eng": "abundant"
    },
    {
        "Rank": 3980,
        "Port": "ilustre",
        "Eng": "illustrious"
    },
    {
        "Rank": 3981,
        "Port": "socialismo",
        "Eng": "socialism"
    },
    {
        "Rank": 3982,
        "Port": "atómico",
        "Eng": "atomic"
    },
    {
        "Rank": 3983,
        "Port": "delegacia",
        "Eng": "police station"
    },
    {
        "Rank": 3983,
        "Port": "delegacia",
        "Eng": "office"
    },
    {
        "Rank": 3984,
        "Port": "assemelhar",
        "Eng": "be similar to"
    },
    {
        "Rank": 3985,
        "Port": "desenrolar",
        "Eng": "take place"
    },
    {
        "Rank": 3985,
        "Port": "desenrolar",
        "Eng": "unfold"
    },
    {
        "Rank": 3986,
        "Port": "tijolo",
        "Eng": "brick"
    },
    {
        "Rank": 3987,
        "Port": "sacudir",
        "Eng": "shake"
    },
    {
        "Rank": 3988,
        "Port": "parecido",
        "Eng": "similar to"
    },
    {
        "Rank": 3988,
        "Port": "parecido",
        "Eng": "resembling"
    },
    {
        "Rank": 3989,
        "Port": "propagar",
        "Eng": "spread"
    },
    {
        "Rank": 3989,
        "Port": "propagar",
        "Eng": "propagate"
    },
    {
        "Rank": 3990,
        "Port": "funcional",
        "Eng": "functional"
    },
    {
        "Rank": 3991,
        "Port": "perfeição",
        "Eng": "perfection"
    },
    {
        "Rank": 3992,
        "Port": "coitado",
        "Eng": "poor thing"
    },
    {
        "Rank": 3992,
        "Port": "coitado",
        "Eng": "pitiful"
    },
    {
        "Rank": 3992,
        "Port": "coitado",
        "Eng": "wretch"
    },
    {
        "Rank": 3993,
        "Port": "habituar",
        "Eng": "get used to"
    },
    {
        "Rank": 3994,
        "Port": "rebelião",
        "Eng": "rebellion"
    },
    {
        "Rank": 3995,
        "Port": "pisar",
        "Eng": "step"
    },
    {
        "Rank": 3996,
        "Port": "niederländisch",
        "Eng": "neighborhood"
    },
    {
        "Rank": 3997,
        "Port": "motivação",
        "Eng": "motivation"
    },
    {
        "Rank": 3998,
        "Port": "educar",
        "Eng": "educate"
    },
    {
        "Rank": 3999,
        "Port": "inveja",
        "Eng": "envy"
    },
    {
        "Rank": 4000,
        "Port": "equilibrar",
        "Eng": "balance"
    },
    {
        "Rank": 4001,
        "Port": "pesquisar",
        "Eng": "research"
    },
    {
        "Rank": 4001,
        "Port": "pesquisar",
        "Eng": "investigate"
    },
    {
        "Rank": 4002,
        "Port": "emissora",
        "Eng": "network"
    },
    {
        "Rank": 4003,
        "Port": "réu",
        "Eng": "defendant"
    },
    {
        "Rank": 4003,
        "Port": "réu",
        "Eng": "accused"
    },
    {
        "Rank": 4004,
        "Port": "debruçar",
        "Eng": "bend or lean over"
    },
    {
        "Rank": 4005,
        "Port": "autarquia",
        "Eng": "self-governing unit"
    },
    {
        "Rank": 4006,
        "Port": "bezerro",
        "Eng": "calf"
    },
    {
        "Rank": 4007,
        "Port": "depressão",
        "Eng": "depression"
    },
    {
        "Rank": 4008,
        "Port": "idioma",
        "Eng": "language"
    },
    {
        "Rank": 4009,
        "Port": "rumor",
        "Eng": "rumor"
    },
    {
        "Rank": 4010,
        "Port": "gruta",
        "Eng": "cave"
    },
    {
        "Rank": 4010,
        "Port": "gruta",
        "Eng": "grotto"
    },
    {
        "Rank": 4011,
        "Port": "marcante",
        "Eng": "memorable"
    },
    {
        "Rank": 4011,
        "Port": "marcante",
        "Eng": "noteworthy"
    },
    {
        "Rank": 4012,
        "Port": "espectro",
        "Eng": "spectrum"
    },
    {
        "Rank": 4012,
        "Port": "espectro",
        "Eng": "specter"
    },
    {
        "Rank": 4013,
        "Port": "plantação",
        "Eng": "plantation"
    },
    {
        "Rank": 4014,
        "Port": "soberano",
        "Eng": "sovereign"
    },
    {
        "Rank": 4015,
        "Port": "cláusula",
        "Eng": "clause"
    },
    {
        "Rank": 4016,
        "Port": "controvérsia",
        "Eng": "controversy"
    },
    {
        "Rank": 4017,
        "Port": "pedagógico",
        "Eng": "pedagogical"
    },
    {
        "Rank": 4017,
        "Port": "pedagógico",
        "Eng": "teaching"
    },
    {
        "Rank": 4018,
        "Port": "descarga",
        "Eng": "discharge"
    },
    {
        "Rank": 4018,
        "Port": "descarga",
        "Eng": "flush"
    },
    {
        "Rank": 4019,
        "Port": "carinho",
        "Eng": "endearment"
    },
    {
        "Rank": 4019,
        "Port": "carinho",
        "Eng": "tenderness"
    },
    {
        "Rank": 4020,
        "Port": "estimativa",
        "Eng": "estimate"
    },
    {
        "Rank": 4021,
        "Port": "advertir",
        "Eng": "warn"
    },
    {
        "Rank": 4022,
        "Port": "borboleta",
        "Eng": "butterfly"
    },
    {
        "Rank": 4023,
        "Port": "volante",
        "Eng": "steering wheel"
    },
    {
        "Rank": 4024,
        "Port": "extremidade",
        "Eng": "end"
    },
    {
        "Rank": 4024,
        "Port": "extremidade",
        "Eng": "edge"
    },
    {
        "Rank": 4025,
        "Port": "bloqueio",
        "Eng": "blockade"
    },
    {
        "Rank": 4025,
        "Port": "bloqueio",
        "Eng": "siege"
    },
    {
        "Rank": 4025,
        "Port": "bloqueio",
        "Eng": "obstruction"
    },
    {
        "Rank": 4026,
        "Port": "promotor",
        "Eng": "promoting"
    },
    {
        "Rank": 4026,
        "Port": "promotor",
        "Eng": "promoter"
    },
    {
        "Rank": 4027,
        "Port": "rabo",
        "Eng": "tail"
    },
    {
        "Rank": 4027,
        "Port": "rabo",
        "Eng": "buttocks"
    },
    {
        "Rank": 4028,
        "Port": "semestre",
        "Eng": "semester"
    },
    {
        "Rank": 4029,
        "Port": "décimo",
        "Eng": "units of ten"
    },
    {
        "Rank": 4029,
        "Port": "décimo",
        "Eng": "tenth"
    },
    {
        "Rank": 4030,
        "Port": "democrata",
        "Eng": "democrat"
    },
    {
        "Rank": 4031,
        "Port": "cigano",
        "Eng": "gypsy"
    },
    {
        "Rank": 4032,
        "Port": "ventre",
        "Eng": "womb"
    },
    {
        "Rank": 4033,
        "Port": "embalagem",
        "Eng": "container"
    },
    {
        "Rank": 4033,
        "Port": "embalagem",
        "Eng": "wrapping"
    },
    {
        "Rank": 4034,
        "Port": "almoçar",
        "Eng": "eat lunch"
    },
    {
        "Rank": 4035,
        "Port": "pular",
        "Eng": "jump"
    },
    {
        "Rank": 4035,
        "Port": "pular",
        "Eng": "skip"
    },
    {
        "Rank": 4036,
        "Port": "inclinar",
        "Eng": "lean"
    },
    {
        "Rank": 4036,
        "Port": "inclinar",
        "Eng": "incline"
    },
    {
        "Rank": 4037,
        "Port": "lavrador",
        "Eng": "farmer"
    },
    {
        "Rank": 4037,
        "Port": "lavrador",
        "Eng": "peasant"
    },
    {
        "Rank": 4038,
        "Port": "biologia",
        "Eng": "biology"
    },
    {
        "Rank": 4039,
        "Port": "quota",
        "Eng": "amount"
    },
    {
        "Rank": 4039,
        "Port": "quota",
        "Eng": "quota"
    },
    {
        "Rank": 4040,
        "Port": "estético",
        "Eng": "aesthetic"
    },
    {
        "Rank": 4040,
        "Port": "estético",
        "Eng": "elegant"
    },
    {
        "Rank": 4041,
        "Port": "repleto",
        "Eng": "full"
    },
    {
        "Rank": 4041,
        "Port": "repleto",
        "Eng": "overrunning"
    },
    {
        "Rank": 4042,
        "Port": "cesta",
        "Eng": "basket"
    },
    {
        "Rank": 4042,
        "Port": "cesta",
        "Eng": "trash can"
    },
    {
        "Rank": 4042,
        "Port": "cesta",
        "Eng": "hoop"
    },
    {
        "Rank": 4043,
        "Port": "genro",
        "Eng": "son-in-law"
    },
    {
        "Rank": 4044,
        "Port": "macio",
        "Eng": "soft"
    },
    {
        "Rank": 4045,
        "Port": "clandestino",
        "Eng": "illegal"
    },
    {
        "Rank": 4045,
        "Port": "clandestino",
        "Eng": "clandestino"
    },
    {
        "Rank": 4046,
        "Port": "aspiração",
        "Eng": "aspiration"
    },
    {
        "Rank": 4046,
        "Port": "aspiração",
        "Eng": "desire"
    },
    {
        "Rank": 4047,
        "Port": "sereno",
        "Eng": "serene"
    },
    {
        "Rank": 4047,
        "Port": "sereno",
        "Eng": "calm"
    },
    {
        "Rank": 4047,
        "Port": "sereno",
        "Eng": "peaceful"
    },
    {
        "Rank": 4048,
        "Port": "lume",
        "Eng": "light"
    },
    {
        "Rank": 4048,
        "Port": "lume",
        "Eng": "fire"
    },
    {
        "Rank": 4049,
        "Port": "injecção",
        "Eng": "injection"
    },
    {
        "Rank": 4050,
        "Port": "sistemático",
        "Eng": "systematic"
    },
    {
        "Rank": 4051,
        "Port": "atrasar",
        "Eng": "delay"
    },
    {
        "Rank": 4051,
        "Port": "atrasar",
        "Eng": "make late"
    },
    {
        "Rank": 4052,
        "Port": "latim",
        "Eng": "Latin"
    },
    {
        "Rank": 4053,
        "Port": "tenente",
        "Eng": "lieutenant"
    },
    {
        "Rank": 4054,
        "Port": "santuário",
        "Eng": "sanctuary"
    },
    {
        "Rank": 4055,
        "Port": "solidário",
        "Eng": "fully supportive"
    },
    {
        "Rank": 4056,
        "Port": "cívico",
        "Eng": "civic"
    },
    {
        "Rank": 4057,
        "Port": "cano",
        "Eng": "pipe"
    },
    {
        "Rank": 4057,
        "Port": "cano",
        "Eng": "tube"
    },
    {
        "Rank": 4057,
        "Port": "cano",
        "Eng": "gun barrel"
    },
    {
        "Rank": 4058,
        "Port": "violão",
        "Eng": "acoustic guitar"
    },
    {
        "Rank": 4059,
        "Port": "dama",
        "Eng": "lady"
    },
    {
        "Rank": 4059,
        "Port": "dama",
        "Eng": "dame"
    },
    {
        "Rank": 4060,
        "Port": "sacrificar",
        "Eng": "sacrifice"
    },
    {
        "Rank": 4061,
        "Port": "ciúme",
        "Eng": "jealousy"
    },
    {
        "Rank": 4062,
        "Port": "cómodo",
        "Eng": "comfort"
    },
    {
        "Rank": 4062,
        "Port": "cómodo",
        "Eng": "room"
    },
    {
        "Rank": 4062,
        "Port": "cómodo",
        "Eng": "comfortable"
    },
    {
        "Rank": 4063,
        "Port": "supermercado",
        "Eng": "supermarket"
    },
    {
        "Rank": 4064,
        "Port": "lábio",
        "Eng": "lip"
    },
    {
        "Rank": 4065,
        "Port": "aliar",
        "Eng": "join"
    },
    {
        "Rank": 4065,
        "Port": "aliar",
        "Eng": "ally oneself with"
    },
    {
        "Rank": 4066,
        "Port": "romancista",
        "Eng": "novelist"
    },
    {
        "Rank": 4067,
        "Port": "periferia",
        "Eng": "outskirts"
    },
    {
        "Rank": 4067,
        "Port": "periferia",
        "Eng": "periphery"
    },
    {
        "Rank": 4068,
        "Port": "fundamentalmente",
        "Eng": "basically"
    },
    {
        "Rank": 4068,
        "Port": "fundamentalmente",
        "Eng": "fundamentally"
    },
    {
        "Rank": 4069,
        "Port": "coincidência",
        "Eng": "coincidence"
    },
    {
        "Rank": 4070,
        "Port": "obstante",
        "Eng": "notwithstanding"
    },
    {
        "Rank": 4070,
        "Port": "obstante",
        "Eng": "nevertheless"
    },
    {
        "Rank": 4071,
        "Port": "alinhar",
        "Eng": "align"
    },
    {
        "Rank": 4071,
        "Port": "alinhar",
        "Eng": "line up with"
    },
    {
        "Rank": 4072,
        "Port": "parâmetro",
        "Eng": "parameter"
    },
    {
        "Rank": 4073,
        "Port": "corno",
        "Eng": "horn"
    },
    {
        "Rank": 4074,
        "Port": "variante",
        "Eng": "variant"
    },
    {
        "Rank": 4074,
        "Port": "variante",
        "Eng": "variable"
    },
    {
        "Rank": 4075,
        "Port": "elenco",
        "Eng": "troupe"
    },
    {
        "Rank": 4075,
        "Port": "elenco",
        "Eng": "list"
    },
    {
        "Rank": 4075,
        "Port": "elenco",
        "Eng": "index"
    },
    {
        "Rank": 4076,
        "Port": "tardar",
        "Eng": "delay"
    },
    {
        "Rank": 4076,
        "Port": "tardar",
        "Eng": "be late"
    },
    {
        "Rank": 4077,
        "Port": "horrível",
        "Eng": "horrible"
    },
    {
        "Rank": 4078,
        "Port": "viajante",
        "Eng": "traveler"
    },
    {
        "Rank": 4079,
        "Port": "meio-dia",
        "Eng": "noon"
    },
    {
        "Rank": 4079,
        "Port": "meio-dia",
        "Eng": "mid-day"
    },
    {
        "Rank": 4080,
        "Port": "investidor",
        "Eng": "investor"
    },
    {
        "Rank": 4081,
        "Port": "agenda",
        "Eng": "agenda"
    },
    {
        "Rank": 4081,
        "Port": "agenda",
        "Eng": "schedule"
    },
    {
        "Rank": 4082,
        "Port": "condenação",
        "Eng": "sentence"
    },
    {
        "Rank": 4082,
        "Port": "condenação",
        "Eng": "condemnation"
    },
    {
        "Rank": 4083,
        "Port": "afecto",
        "Eng": "affection"
    },
    {
        "Rank": 4084,
        "Port": "remuneração",
        "Eng": "payment"
    },
    {
        "Rank": 4084,
        "Port": "remuneração",
        "Eng": "salary"
    },
    {
        "Rank": 4085,
        "Port": "auge",
        "Eng": "apex"
    },
    {
        "Rank": 4085,
        "Port": "auge",
        "Eng": "climax"
    },
    {
        "Rank": 4085,
        "Port": "auge",
        "Eng": "pinnacle"
    },
    {
        "Rank": 4086,
        "Port": "punição",
        "Eng": "punishment"
    },
    {
        "Rank": 4087,
        "Port": "muscular",
        "Eng": "muscular"
    },
    {
        "Rank": 4088,
        "Port": "selva",
        "Eng": "jungle"
    },
    {
        "Rank": 4089,
        "Port": "similar",
        "Eng": "similar"
    },
    {
        "Rank": 4090,
        "Port": "noivo",
        "Eng": "fiancé"
    },
    {
        "Rank": 4090,
        "Port": "noivo",
        "Eng": "bridegroom"
    },
    {
        "Rank": 4091,
        "Port": "gramática",
        "Eng": "grammar"
    },
    {
        "Rank": 4092,
        "Port": "varrer",
        "Eng": "sweep"
    },
    {
        "Rank": 4093,
        "Port": "veneno",
        "Eng": "poison"
    },
    {
        "Rank": 4093,
        "Port": "veneno",
        "Eng": "venom"
    },
    {
        "Rank": 4094,
        "Port": "longínquo",
        "Eng": "far away"
    },
    {
        "Rank": 4094,
        "Port": "longínquo",
        "Eng": "distant"
    },
    {
        "Rank": 4095,
        "Port": "encerramento",
        "Eng": "closing"
    },
    {
        "Rank": 4096,
        "Port": "conformar",
        "Eng": "accept"
    },
    {
        "Rank": 4096,
        "Port": "conformar",
        "Eng": "conform"
    },
    {
        "Rank": 4096,
        "Port": "conformar",
        "Eng": "adapt"
    },
    {
        "Rank": 4097,
        "Port": "higiene",
        "Eng": "hygiene"
    },
    {
        "Rank": 4098,
        "Port": "comissário",
        "Eng": "commissioner"
    },
    {
        "Rank": 4098,
        "Port": "comissário",
        "Eng": "superinten- dent"
    },
    {
        "Rank": 4099,
        "Port": "hospitalar",
        "Eng": "relating to a hospital"
    },
    {
        "Rank": 4100,
        "Port": "incidir",
        "Eng": "focus on"
    },
    {
        "Rank": 4100,
        "Port": "incidir",
        "Eng": "fall upon"
    },
    {
        "Rank": 4100,
        "Port": "incidir",
        "Eng": "occur"
    },
    {
        "Rank": 4101,
        "Port": "elementar",
        "Eng": "elementary"
    },
    {
        "Rank": 4102,
        "Port": "evidenciar",
        "Eng": "become noted for"
    },
    {
        "Rank": 4102,
        "Port": "evidenciar",
        "Eng": "stand out"
    },
    {
        "Rank": 4103,
        "Port": "susto",
        "Eng": "fright"
    },
    {
        "Rank": 4103,
        "Port": "susto",
        "Eng": "shock"
    },
    {
        "Rank": 4103,
        "Port": "susto",
        "Eng": "scare"
    },
    {
        "Rank": 4104,
        "Port": "temor",
        "Eng": "fear"
    },
    {
        "Rank": 4104,
        "Port": "temor",
        "Eng": "apprehension"
    },
    {
        "Rank": 4104,
        "Port": "temor",
        "Eng": "concern"
    },
    {
        "Rank": 4105,
        "Port": "materno",
        "Eng": "maternal"
    },
    {
        "Rank": 4105,
        "Port": "materno",
        "Eng": "motherly"
    },
    {
        "Rank": 4106,
        "Port": "genérico",
        "Eng": "generic"
    },
    {
        "Rank": 4107,
        "Port": "cidadania",
        "Eng": "citizenship"
    },
    {
        "Rank": 4108,
        "Port": "regionalização",
        "Eng": "regionalization"
    },
    {
        "Rank": 4109,
        "Port": "cautela",
        "Eng": "caution"
    },
    {
        "Rank": 4109,
        "Port": "cautela",
        "Eng": "care"
    },
    {
        "Rank": 4110,
        "Port": "liberar",
        "Eng": "release"
    },
    {
        "Rank": 4110,
        "Port": "liberar",
        "Eng": "liberate"
    },
    {
        "Rank": 4111,
        "Port": "arredor",
        "Eng": "outskirts"
    },
    {
        "Rank": 4111,
        "Port": "arredor",
        "Eng": "suburbs"
    },
    {
        "Rank": 4112,
        "Port": "sueco",
        "Eng": "Swedish"
    },
    {
        "Rank": 4113,
        "Port": "regular",
        "Eng": "regulate"
    },
    {
        "Rank": 4114,
        "Port": "largura",
        "Eng": "width"
    },
    {
        "Rank": 4115,
        "Port": "pendente",
        "Eng": "waiting for"
    },
    {
        "Rank": 4115,
        "Port": "pendente",
        "Eng": "pending"
    },
    {
        "Rank": 4115,
        "Port": "pendente",
        "Eng": "hanging"
    },
    {
        "Rank": 4116,
        "Port": "testa",
        "Eng": "forehead"
    },
    {
        "Rank": 4116,
        "Port": "testa",
        "Eng": "forefront"
    },
    {
        "Rank": 4117,
        "Port": "renúncia",
        "Eng": "renunciation"
    },
    {
        "Rank": 4117,
        "Port": "renúncia",
        "Eng": "resignation"
    },
    {
        "Rank": 4118,
        "Port": "renascimento",
        "Eng": "renaissance"
    },
    {
        "Rank": 4118,
        "Port": "renascimento",
        "Eng": "rebirth"
    },
    {
        "Rank": 4119,
        "Port": "visconde",
        "Eng": "viscount"
    },
    {
        "Rank": 4120,
        "Port": "suspeitar",
        "Eng": "suspect"
    },
    {
        "Rank": 4121,
        "Port": "simultâneo",
        "Eng": "simultaneous"
    },
    {
        "Rank": 4122,
        "Port": "beijar",
        "Eng": "kiss"
    },
    {
        "Rank": 4123,
        "Port": "favorito",
        "Eng": "favorite"
    },
    {
        "Rank": 4123,
        "Port": "favorito",
        "Eng": "preferred"
    },
    {
        "Rank": 4124,
        "Port": "brigada",
        "Eng": "brigade"
    },
    {
        "Rank": 4125,
        "Port": "sorrir",
        "Eng": "smile"
    },
    {
        "Rank": 4126,
        "Port": "culminar",
        "Eng": "culminate"
    },
    {
        "Rank": 4127,
        "Port": "bacalhau",
        "Eng": "cod fish"
    },
    {
        "Rank": 4128,
        "Port": "alga",
        "Eng": "algae"
    },
    {
        "Rank": 4128,
        "Port": "alga",
        "Eng": "seaweed"
    },
    {
        "Rank": 4129,
        "Port": "especulação",
        "Eng": "speculation"
    },
    {
        "Rank": 4130,
        "Port": "alertar",
        "Eng": "advise"
    },
    {
        "Rank": 4130,
        "Port": "alertar",
        "Eng": "warn"
    },
    {
        "Rank": 4131,
        "Port": "desprezo",
        "Eng": "contempt"
    },
    {
        "Rank": 4131,
        "Port": "desprezo",
        "Eng": "disdain"
    },
    {
        "Rank": 4132,
        "Port": "vitorioso",
        "Eng": "victorious"
    },
    {
        "Rank": 4133,
        "Port": "ansioso",
        "Eng": "anxious"
    },
    {
        "Rank": 4134,
        "Port": "expulsão",
        "Eng": "expulsion"
    },
    {
        "Rank": 4135,
        "Port": "caos",
        "Eng": "chaos"
    },
    {
        "Rank": 4136,
        "Port": "baptizar",
        "Eng": "baptize"
    },
    {
        "Rank": 4137,
        "Port": "apreciação",
        "Eng": "appreciation"
    },
    {
        "Rank": 4138,
        "Port": "avistar",
        "Eng": "catch sight of"
    },
    {
        "Rank": 4138,
        "Port": "avistar",
        "Eng": "see in the distance"
    },
    {
        "Rank": 4139,
        "Port": "artéria",
        "Eng": "artery"
    },
    {
        "Rank": 4140,
        "Port": "intitulado",
        "Eng": "entitled"
    },
    {
        "Rank": 4141,
        "Port": "imprimir",
        "Eng": "print"
    },
    {
        "Rank": 4141,
        "Port": "imprimir",
        "Eng": "mark"
    },
    {
        "Rank": 4141,
        "Port": "imprimir",
        "Eng": "influence"
    },
    {
        "Rank": 4142,
        "Port": "temporário",
        "Eng": "temporary"
    },
    {
        "Rank": 4143,
        "Port": "restauração",
        "Eng": "restoration"
    },
    {
        "Rank": 4144,
        "Port": "desaparecimento",
        "Eng": "disappearance"
    },
    {
        "Rank": 4144,
        "Port": "desaparecimento",
        "Eng": "vanishing"
    },
    {
        "Rank": 4145,
        "Port": "emigrante",
        "Eng": "emigrant"
    },
    {
        "Rank": 4146,
        "Port": "territorial",
        "Eng": "territorial"
    },
    {
        "Rank": 4147,
        "Port": "realizador",
        "Eng": "producer"
    },
    {
        "Rank": 4148,
        "Port": "arrecadação",
        "Eng": "collection"
    },
    {
        "Rank": 4148,
        "Port": "arrecadação",
        "Eng": "saving"
    },
    {
        "Rank": 4149,
        "Port": "deficiente",
        "Eng": "deficient"
    },
    {
        "Rank": 4150,
        "Port": "padrinho",
        "Eng": "godfather"
    },
    {
        "Rank": 4150,
        "Port": "padrinho",
        "Eng": "godparents"
    },
    {
        "Rank": 4151,
        "Port": "elogiar",
        "Eng": "praise"
    },
    {
        "Rank": 4151,
        "Port": "elogiar",
        "Eng": "compliment"
    },
    {
        "Rank": 4152,
        "Port": "casaco",
        "Eng": "coat"
    },
    {
        "Rank": 4153,
        "Port": "lâmina",
        "Eng": "blade"
    },
    {
        "Rank": 4154,
        "Port": "alongar",
        "Eng": "extend"
    },
    {
        "Rank": 4154,
        "Port": "alongar",
        "Eng": "lengthen"
    },
    {
        "Rank": 4155,
        "Port": "doido",
        "Eng": "crazy"
    },
    {
        "Rank": 4156,
        "Port": "golfo",
        "Eng": "gulf"
    },
    {
        "Rank": 4157,
        "Port": "prevalecer",
        "Eng": "prevail"
    },
    {
        "Rank": 4158,
        "Port": "esgoto",
        "Eng": "sewage"
    },
    {
        "Rank": 4159,
        "Port": "povoado",
        "Eng": "settlement"
    },
    {
        "Rank": 4159,
        "Port": "povoado",
        "Eng": "small village"
    },
    {
        "Rank": 4160,
        "Port": "traição",
        "Eng": "betrayal"
    },
    {
        "Rank": 4160,
        "Port": "traição",
        "Eng": "treason"
    },
    {
        "Rank": 4161,
        "Port": "plenário",
        "Eng": "general assembly"
    },
    {
        "Rank": 4162,
        "Port": "encanto",
        "Eng": "enchantment"
    },
    {
        "Rank": 4162,
        "Port": "encanto",
        "Eng": "fascination"
    },
    {
        "Rank": 4163,
        "Port": "sombrio",
        "Eng": "somber"
    },
    {
        "Rank": 4163,
        "Port": "sombrio",
        "Eng": "dark"
    },
    {
        "Rank": 4163,
        "Port": "sombrio",
        "Eng": "melancholy"
    },
    {
        "Rank": 4164,
        "Port": "complexidade",
        "Eng": "complexity"
    },
    {
        "Rank": 4165,
        "Port": "acerto",
        "Eng": "success"
    },
    {
        "Rank": 4165,
        "Port": "acerto",
        "Eng": "agreement"
    },
    {
        "Rank": 4166,
        "Port": "licitação",
        "Eng": "bidding"
    },
    {
        "Rank": 4166,
        "Port": "licitação",
        "Eng": "auction"
    },
    {
        "Rank": 4167,
        "Port": "molhado",
        "Eng": "wet"
    },
    {
        "Rank": 4168,
        "Port": "colina",
        "Eng": "hill"
    },
    {
        "Rank": 4169,
        "Port": "querido",
        "Eng": "dear"
    },
    {
        "Rank": 4169,
        "Port": "querido",
        "Eng": "beloved"
    },
    {
        "Rank": 4170,
        "Port": "adormecer",
        "Eng": "fall asleep"
    },
    {
        "Rank": 4171,
        "Port": "abundância",
        "Eng": "abundance"
    },
    {
        "Rank": 4172,
        "Port": "pescar",
        "Eng": "fish"
    },
    {
        "Rank": 4173,
        "Port": "recear",
        "Eng": "fear"
    },
    {
        "Rank": 4174,
        "Port": "insuficiente",
        "Eng": "insufficient"
    },
    {
        "Rank": 4175,
        "Port": "conveniente",
        "Eng": "convenient"
    },
    {
        "Rank": 4176,
        "Port": "patamar",
        "Eng": "level"
    },
    {
        "Rank": 4176,
        "Port": "patamar",
        "Eng": "threshold"
    },
    {
        "Rank": 4176,
        "Port": "patamar",
        "Eng": "degree"
    },
    {
        "Rank": 4177,
        "Port": "fortalecer",
        "Eng": "strengthen"
    },
    {
        "Rank": 4178,
        "Port": "banana",
        "Eng": "banana"
    },
    {
        "Rank": 4179,
        "Port": "furar",
        "Eng": "penetrate"
    },
    {
        "Rank": 4179,
        "Port": "furar",
        "Eng": "make a hole"
    },
    {
        "Rank": 4180,
        "Port": "antiguidade",
        "Eng": "antiquity"
    },
    {
        "Rank": 4180,
        "Port": "antiguidade",
        "Eng": "ancient times"
    },
    {
        "Rank": 4181,
        "Port": "consórcio",
        "Eng": "consortium"
    },
    {
        "Rank": 4181,
        "Port": "consórcio",
        "Eng": "union"
    },
    {
        "Rank": 4182,
        "Port": "ecológico",
        "Eng": "ecological"
    },
    {
        "Rank": 4183,
        "Port": "signo",
        "Eng": "sign of the zodiac"
    },
    {
        "Rank": 4184,
        "Port": "formiga",
        "Eng": "ant"
    },
    {
        "Rank": 4185,
        "Port": "sondagem",
        "Eng": "analysis"
    },
    {
        "Rank": 4185,
        "Port": "sondagem",
        "Eng": "poll"
    },
    {
        "Rank": 4185,
        "Port": "sondagem",
        "Eng": "investigation"
    },
    {
        "Rank": 4186,
        "Port": "resistente",
        "Eng": "resistant"
    },
    {
        "Rank": 4187,
        "Port": "repassar",
        "Eng": "go back and forth"
    },
    {
        "Rank": 4187,
        "Port": "repassar",
        "Eng": "revise"
    },
    {
        "Rank": 4188,
        "Port": "devedor",
        "Eng": "debtor"
    },
    {
        "Rank": 4189,
        "Port": "assente",
        "Eng": "settled"
    },
    {
        "Rank": 4189,
        "Port": "assente",
        "Eng": "established"
    },
    {
        "Rank": 4190,
        "Port": "optimista",
        "Eng": "optimist"
    },
    {
        "Rank": 4191,
        "Port": "trilha",
        "Eng": "trail"
    },
    {
        "Rank": 4192,
        "Port": "incapacidade",
        "Eng": "inability"
    },
    {
        "Rank": 4193,
        "Port": "inverter",
        "Eng": "reverse"
    },
    {
        "Rank": 4193,
        "Port": "inverter",
        "Eng": "invert"
    },
    {
        "Rank": 4194,
        "Port": "olhar",
        "Eng": "look"
    },
    {
        "Rank": 4195,
        "Port": "mexicano",
        "Eng": "Mexican"
    },
    {
        "Rank": 4196,
        "Port": "agrupamento",
        "Eng": "collection"
    },
    {
        "Rank": 4197,
        "Port": "diferenciado",
        "Eng": "different"
    },
    {
        "Rank": 4198,
        "Port": "inscrito",
        "Eng": "enrolled"
    },
    {
        "Rank": 4198,
        "Port": "inscrito",
        "Eng": "inscribed"
    },
    {
        "Rank": 4199,
        "Port": "consoante",
        "Eng": "according to"
    },
    {
        "Rank": 4199,
        "Port": "consoante",
        "Eng": "in conformity with"
    },
    {
        "Rank": 4200,
        "Port": "cubano",
        "Eng": "Cuban"
    },
    {
        "Rank": 4201,
        "Port": "monopólio",
        "Eng": "monopoly"
    },
    {
        "Rank": 4202,
        "Port": "espanto",
        "Eng": "surprise"
    },
    {
        "Rank": 4202,
        "Port": "espanto",
        "Eng": "wonder"
    },
    {
        "Rank": 4202,
        "Port": "espanto",
        "Eng": "fright"
    },
    {
        "Rank": 4203,
        "Port": "pilha",
        "Eng": "battery"
    },
    {
        "Rank": 4203,
        "Port": "pilha",
        "Eng": "pile"
    },
    {
        "Rank": 4204,
        "Port": "mesquita",
        "Eng": "mosque"
    },
    {
        "Rank": 4205,
        "Port": "ampliação",
        "Eng": "enlargement"
    },
    {
        "Rank": 4205,
        "Port": "ampliação",
        "Eng": "amplification"
    },
    {
        "Rank": 4206,
        "Port": "farmacêutico",
        "Eng": "pharmaceutical"
    },
    {
        "Rank": 4206,
        "Port": "farmacêutico",
        "Eng": "pharmacist"
    },
    {
        "Rank": 4207,
        "Port": "repercussão",
        "Eng": "repercussion"
    },
    {
        "Rank": 4208,
        "Port": "fiscalizar",
        "Eng": "regulate"
    },
    {
        "Rank": 4208,
        "Port": "fiscalizar",
        "Eng": "inspect"
    },
    {
        "Rank": 4208,
        "Port": "fiscalizar",
        "Eng": "supervise"
    },
    {
        "Rank": 4209,
        "Port": "baía",
        "Eng": "bay"
    },
    {
        "Rank": 4209,
        "Port": "baía",
        "Eng": "harbor"
    },
    {
        "Rank": 4210,
        "Port": "invocar",
        "Eng": "invoke"
    },
    {
        "Rank": 4210,
        "Port": "invocar",
        "Eng": "call upon"
    },
    {
        "Rank": 4211,
        "Port": "nadar",
        "Eng": "swim"
    },
    {
        "Rank": 4212,
        "Port": "soprar",
        "Eng": "blow"
    },
    {
        "Rank": 4213,
        "Port": "pauta",
        "Eng": "agenda"
    },
    {
        "Rank": 4213,
        "Port": "pauta",
        "Eng": "guideline"
    },
    {
        "Rank": 4214,
        "Port": "erudito",
        "Eng": "educated"
    },
    {
        "Rank": 4214,
        "Port": "erudito",
        "Eng": "scholarly"
    },
    {
        "Rank": 4214,
        "Port": "erudito",
        "Eng": "learned"
    },
    {
        "Rank": 4215,
        "Port": "interacção",
        "Eng": "interaction"
    },
    {
        "Rank": 4216,
        "Port": "fumaça",
        "Eng": "smoke"
    },
    {
        "Rank": 4217,
        "Port": "matéria-prima",
        "Eng": "raw material"
    },
    {
        "Rank": 4218,
        "Port": "docente",
        "Eng": "faculty member"
    },
    {
        "Rank": 4218,
        "Port": "docente",
        "Eng": "educator"
    },
    {
        "Rank": 4219,
        "Port": "ordenado",
        "Eng": "salary"
    },
    {
        "Rank": 4219,
        "Port": "ordenado",
        "Eng": "ordained"
    },
    {
        "Rank": 4219,
        "Port": "ordenado",
        "Eng": "ordered"
    },
    {
        "Rank": 4220,
        "Port": "seara",
        "Eng": "field of grains"
    },
    {
        "Rank": 4221,
        "Port": "aposentadoria",
        "Eng": "retirement"
    },
    {
        "Rank": 4221,
        "Port": "aposentadoria",
        "Eng": "retirement fund"
    },
    {
        "Rank": 4222,
        "Port": "anteceder",
        "Eng": "precede"
    },
    {
        "Rank": 4222,
        "Port": "anteceder",
        "Eng": "take place before"
    },
    {
        "Rank": 4223,
        "Port": "magia",
        "Eng": "magic"
    },
    {
        "Rank": 4224,
        "Port": "tutela",
        "Eng": "auspices"
    },
    {
        "Rank": 4224,
        "Port": "tutela",
        "Eng": "tutelage"
    },
    {
        "Rank": 4225,
        "Port": "outono",
        "Eng": "fall"
    },
    {
        "Rank": 4225,
        "Port": "outono",
        "Eng": "autumn"
    },
    {
        "Rank": 4226,
        "Port": "impossibilidade",
        "Eng": "impossibility"
    },
    {
        "Rank": 4227,
        "Port": "namorar",
        "Eng": "date steadily"
    },
    {
        "Rank": 4228,
        "Port": "acostumado",
        "Eng": "used to"
    },
    {
        "Rank": 4228,
        "Port": "acostumado",
        "Eng": "accustomed"
    },
    {
        "Rank": 4229,
        "Port": "seguido",
        "Eng": "followed"
    },
    {
        "Rank": 4230,
        "Port": "caseiro",
        "Eng": "household"
    },
    {
        "Rank": 4230,
        "Port": "caseiro",
        "Eng": "homemade"
    },
    {
        "Rank": 4231,
        "Port": "diplomata",
        "Eng": "diplomat"
    },
    {
        "Rank": 4232,
        "Port": "flexível",
        "Eng": "flexible"
    },
    {
        "Rank": 4233,
        "Port": "utilizador",
        "Eng": "user"
    },
    {
        "Rank": 4234,
        "Port": "dedicação",
        "Eng": "dedication"
    },
    {
        "Rank": 4235,
        "Port": "bagagem",
        "Eng": "luggage"
    },
    {
        "Rank": 4235,
        "Port": "bagagem",
        "Eng": "baggage"
    },
    {
        "Rank": 4236,
        "Port": "qualificação",
        "Eng": "qualification"
    },
    {
        "Rank": 4237,
        "Port": "surpreendente",
        "Eng": "surprising"
    },
    {
        "Rank": 4237,
        "Port": "surpreendente",
        "Eng": "admirable"
    },
    {
        "Rank": 4237,
        "Port": "surpreendente",
        "Eng": "amazing"
    },
    {
        "Rank": 4238,
        "Port": "integrado",
        "Eng": "made up of"
    },
    {
        "Rank": 4238,
        "Port": "integrado",
        "Eng": "integrated"
    },
    {
        "Rank": 4239,
        "Port": "doer",
        "Eng": "hurt"
    },
    {
        "Rank": 4240,
        "Port": "periódico",
        "Eng": "newspaper"
    },
    {
        "Rank": 4240,
        "Port": "periódico",
        "Eng": "periodic"
    },
    {
        "Rank": 4241,
        "Port": "alargamento",
        "Eng": "expansion"
    },
    {
        "Rank": 4241,
        "Port": "alargamento",
        "Eng": "widening"
    },
    {
        "Rank": 4242,
        "Port": "madame",
        "Eng": "madam"
    },
    {
        "Rank": 4243,
        "Port": "gelado",
        "Eng": "cold"
    },
    {
        "Rank": 4243,
        "Port": "gelado",
        "Eng": "chilled"
    },
    {
        "Rank": 4243,
        "Port": "gelado",
        "Eng": "ice-cream"
    },
    {
        "Rank": 4244,
        "Port": "trair",
        "Eng": "betray"
    },
    {
        "Rank": 4245,
        "Port": "distrair",
        "Eng": "distract"
    },
    {
        "Rank": 4246,
        "Port": "grade",
        "Eng": "bars"
    },
    {
        "Rank": 4246,
        "Port": "grade",
        "Eng": "railing"
    },
    {
        "Rank": 4247,
        "Port": "cancro",
        "Eng": "cancer"
    },
    {
        "Rank": 4248,
        "Port": "húmido",
        "Eng": "humid"
    },
    {
        "Rank": 4248,
        "Port": "húmido",
        "Eng": "moist"
    },
    {
        "Rank": 4249,
        "Port": "corporação",
        "Eng": "corporation"
    },
    {
        "Rank": 4250,
        "Port": "diferenciar",
        "Eng": "differentiate"
    },
    {
        "Rank": 4251,
        "Port": "lazer",
        "Eng": "leisure"
    },
    {
        "Rank": 4251,
        "Port": "lazer",
        "Eng": "relaxation"
    },
    {
        "Rank": 4252,
        "Port": "citação",
        "Eng": "citation"
    },
    {
        "Rank": 4253,
        "Port": "andar",
        "Eng": "floor"
    },
    {
        "Rank": 4253,
        "Port": "andar",
        "Eng": "level"
    },
    {
        "Rank": 4253,
        "Port": "andar",
        "Eng": "walk"
    },
    {
        "Rank": 4254,
        "Port": "resgatar",
        "Eng": "rescue"
    },
    {
        "Rank": 4254,
        "Port": "resgatar",
        "Eng": "save"
    },
    {
        "Rank": 4255,
        "Port": "vibrar",
        "Eng": "vibrate"
    },
    {
        "Rank": 4256,
        "Port": "terça-feira",
        "Eng": "Tuesday"
    },
    {
        "Rank": 4257,
        "Port": "vão",
        "Eng": "vain"
    },
    {
        "Rank": 4258,
        "Port": "mania",
        "Eng": "craze"
    },
    {
        "Rank": 4258,
        "Port": "mania",
        "Eng": "habit"
    },
    {
        "Rank": 4259,
        "Port": "imobiliário",
        "Eng": "relating to real-estate"
    },
    {
        "Rank": 4260,
        "Port": "seita",
        "Eng": "sect"
    },
    {
        "Rank": 4260,
        "Port": "seita",
        "Eng": "religion"
    },
    {
        "Rank": 4261,
        "Port": "boneca",
        "Eng": "doll"
    },
    {
        "Rank": 4262,
        "Port": "piedade",
        "Eng": "compassion"
    },
    {
        "Rank": 4262,
        "Port": "piedade",
        "Eng": "piety"
    },
    {
        "Rank": 4262,
        "Port": "piedade",
        "Eng": "pity"
    },
    {
        "Rank": 4263,
        "Port": "diligência",
        "Eng": "diligence"
    },
    {
        "Rank": 4264,
        "Port": "alumínio",
        "Eng": "aluminum"
    },
    {
        "Rank": 4265,
        "Port": "solene",
        "Eng": "solemn"
    },
    {
        "Rank": 4265,
        "Port": "solene",
        "Eng": "official"
    },
    {
        "Rank": 4265,
        "Port": "solene",
        "Eng": "formal"
    },
    {
        "Rank": 4266,
        "Port": "elefante",
        "Eng": "elephant"
    },
    {
        "Rank": 4267,
        "Port": "ofender",
        "Eng": "offend"
    },
    {
        "Rank": 4268,
        "Port": "varanda",
        "Eng": "balcony"
    },
    {
        "Rank": 4268,
        "Port": "varanda",
        "Eng": "porch"
    },
    {
        "Rank": 4269,
        "Port": "responsabilizar",
        "Eng": "take responsibility for"
    },
    {
        "Rank": 4270,
        "Port": "discordar",
        "Eng": "disagree"
    },
    {
        "Rank": 4271,
        "Port": "documentário",
        "Eng": "documentary"
    },
    {
        "Rank": 4272,
        "Port": "plateia",
        "Eng": "audience"
    },
    {
        "Rank": 4273,
        "Port": "alívio",
        "Eng": "relief"
    },
    {
        "Rank": 4274,
        "Port": "fazendeiro",
        "Eng": "farmer"
    },
    {
        "Rank": 4275,
        "Port": "abelha",
        "Eng": "bee"
    },
    {
        "Rank": 4276,
        "Port": "aceso",
        "Eng": "lit"
    },
    {
        "Rank": 4277,
        "Port": "reconstrução",
        "Eng": "reconstruction"
    },
    {
        "Rank": 4278,
        "Port": "teor",
        "Eng": "content"
    },
    {
        "Rank": 4278,
        "Port": "teor",
        "Eng": "nature of"
    },
    {
        "Rank": 4279,
        "Port": "manta",
        "Eng": "blanket"
    },
    {
        "Rank": 4279,
        "Port": "manta",
        "Eng": "bedspread"
    },
    {
        "Rank": 4280,
        "Port": "judaico",
        "Eng": "Jewish"
    },
    {
        "Rank": 4281,
        "Port": "paragem",
        "Eng": "stop"
    },
    {
        "Rank": 4282,
        "Port": "refém",
        "Eng": "hostage"
    },
    {
        "Rank": 4283,
        "Port": "sentimental",
        "Eng": "sentimental"
    },
    {
        "Rank": 4284,
        "Port": "austríaco",
        "Eng": "Austrian"
    },
    {
        "Rank": 4285,
        "Port": "encostar",
        "Eng": "rest"
    },
    {
        "Rank": 4285,
        "Port": "encostar",
        "Eng": "lean"
    },
    {
        "Rank": 4285,
        "Port": "encostar",
        "Eng": "place against"
    },
    {
        "Rank": 4286,
        "Port": "portaria",
        "Eng": "reception or information desk"
    },
    {
        "Rank": 4287,
        "Port": "evocar",
        "Eng": "evoke"
    },
    {
        "Rank": 4288,
        "Port": "modernização",
        "Eng": "modernization"
    },
    {
        "Rank": 4289,
        "Port": "interferência",
        "Eng": "interference"
    },
    {
        "Rank": 4290,
        "Port": "exagerado",
        "Eng": "exaggerated"
    },
    {
        "Rank": 4291,
        "Port": "gémeo",
        "Eng": "twin"
    },
    {
        "Rank": 4292,
        "Port": "garota",
        "Eng": "girl"
    },
    {
        "Rank": 4293,
        "Port": "competitivo",
        "Eng": "competitive"
    },
    {
        "Rank": 4294,
        "Port": "cru",
        "Eng": "raw"
    },
    {
        "Rank": 4295,
        "Port": "ferver",
        "Eng": "boil"
    },
    {
        "Rank": 4296,
        "Port": "cesto",
        "Eng": "basket"
    },
    {
        "Rank": 4297,
        "Port": "saneamento",
        "Eng": "sanitation"
    },
    {
        "Rank": 4298,
        "Port": "cais",
        "Eng": "dock"
    },
    {
        "Rank": 4298,
        "Port": "cais",
        "Eng": "pier"
    },
    {
        "Rank": 4299,
        "Port": "escondido",
        "Eng": "hidden"
    },
    {
        "Rank": 4300,
        "Port": "anualmente",
        "Eng": "annually"
    },
    {
        "Rank": 4300,
        "Port": "anualmente",
        "Eng": "yearly"
    },
    {
        "Rank": 4301,
        "Port": "fósforo",
        "Eng": "match"
    },
    {
        "Rank": 4302,
        "Port": "eliminação",
        "Eng": "elimination"
    },
    {
        "Rank": 4303,
        "Port": "quarta-feira",
        "Eng": "Wednesday"
    },
    {
        "Rank": 4304,
        "Port": "química",
        "Eng": "chemistry"
    },
    {
        "Rank": 4305,
        "Port": "marchar",
        "Eng": "march"
    },
    {
        "Rank": 4306,
        "Port": "pensador",
        "Eng": "intellectual"
    },
    {
        "Rank": 4306,
        "Port": "pensador",
        "Eng": "thinker"
    },
    {
        "Rank": 4307,
        "Port": "metrópole",
        "Eng": "large city"
    },
    {
        "Rank": 4307,
        "Port": "metrópole",
        "Eng": "metropolis"
    },
    {
        "Rank": 4308,
        "Port": "arqueológico",
        "Eng": "archeological"
    },
    {
        "Rank": 4309,
        "Port": "perfume",
        "Eng": "perfume"
    },
    {
        "Rank": 4310,
        "Port": "gaveta",
        "Eng": "drawer"
    },
    {
        "Rank": 4311,
        "Port": "conveniência",
        "Eng": "convenience"
    },
    {
        "Rank": 4312,
        "Port": "criminal",
        "Eng": "criminal"
    },
    {
        "Rank": 4313,
        "Port": "ingresso",
        "Eng": "admission"
    },
    {
        "Rank": 4313,
        "Port": "ingresso",
        "Eng": "entrance"
    },
    {
        "Rank": 4314,
        "Port": "delírio",
        "Eng": "delirium"
    },
    {
        "Rank": 4315,
        "Port": "desigualdade",
        "Eng": "inequality"
    },
    {
        "Rank": 4316,
        "Port": "largamente",
        "Eng": "widely"
    },
    {
        "Rank": 4316,
        "Port": "largamente",
        "Eng": "extensively"
    },
    {
        "Rank": 4317,
        "Port": "tímido",
        "Eng": "shy"
    },
    {
        "Rank": 4317,
        "Port": "tímido",
        "Eng": "timid"
    },
    {
        "Rank": 4318,
        "Port": "crista",
        "Eng": "crest"
    },
    {
        "Rank": 4318,
        "Port": "crista",
        "Eng": "plume"
    },
    {
        "Rank": 4319,
        "Port": "tributo",
        "Eng": "tribute"
    },
    {
        "Rank": 4320,
        "Port": "regulamentação",
        "Eng": "regulation"
    },
    {
        "Rank": 4321,
        "Port": "câmera",
        "Eng": "camera"
    },
    {
        "Rank": 4322,
        "Port": "irónico",
        "Eng": "ironic"
    },
    {
        "Rank": 4322,
        "Port": "irónico",
        "Eng": "sarcastic"
    },
    {
        "Rank": 4323,
        "Port": "agredir",
        "Eng": "attack"
    },
    {
        "Rank": 4323,
        "Port": "agredir",
        "Eng": "assault"
    },
    {
        "Rank": 4324,
        "Port": "fluido",
        "Eng": "fluid"
    },
    {
        "Rank": 4325,
        "Port": "redigir",
        "Eng": "write"
    },
    {
        "Rank": 4325,
        "Port": "redigir",
        "Eng": "handwrite"
    },
    {
        "Rank": 4326,
        "Port": "restabelecer",
        "Eng": "reestablish"
    },
    {
        "Rank": 4327,
        "Port": "oral",
        "Eng": "oral"
    },
    {
        "Rank": 4328,
        "Port": "calmo",
        "Eng": "calm"
    },
    {
        "Rank": 4329,
        "Port": "impressionante",
        "Eng": "impressive"
    },
    {
        "Rank": 4329,
        "Port": "impressionante",
        "Eng": "impressing"
    },
    {
        "Rank": 4330,
        "Port": "arrecadar",
        "Eng": "collect"
    },
    {
        "Rank": 4330,
        "Port": "arrecadar",
        "Eng": "store"
    },
    {
        "Rank": 4330,
        "Port": "arrecadar",
        "Eng": "save"
    },
    {
        "Rank": 4331,
        "Port": "interrupção",
        "Eng": "interruption"
    },
    {
        "Rank": 4332,
        "Port": "fabricação",
        "Eng": "manufacturing"
    },
    {
        "Rank": 4332,
        "Port": "fabricação",
        "Eng": "production"
    },
    {
        "Rank": 4333,
        "Port": "convergência",
        "Eng": "convergence"
    },
    {
        "Rank": 4334,
        "Port": "contactar",
        "Eng": "contact"
    },
    {
        "Rank": 4335,
        "Port": "missionário",
        "Eng": "missionary"
    },
    {
        "Rank": 4336,
        "Port": "auditório",
        "Eng": "auditorium"
    },
    {
        "Rank": 4336,
        "Port": "auditório",
        "Eng": "audible"
    },
    {
        "Rank": 4337,
        "Port": "ambicioso",
        "Eng": "ambitious"
    },
    {
        "Rank": 4338,
        "Port": "comunismo",
        "Eng": "communism"
    },
    {
        "Rank": 4339,
        "Port": "cardíaco",
        "Eng": "cardiac"
    },
    {
        "Rank": 4339,
        "Port": "cardíaco",
        "Eng": "of the heart"
    },
    {
        "Rank": 4340,
        "Port": "hostilidade",
        "Eng": "hostility"
    },
    {
        "Rank": 4341,
        "Port": "diversão",
        "Eng": "amusement"
    },
    {
        "Rank": 4341,
        "Port": "diversão",
        "Eng": "diversion"
    },
    {
        "Rank": 4342,
        "Port": "moer",
        "Eng": "grind"
    },
    {
        "Rank": 4342,
        "Port": "moer",
        "Eng": "crush"
    },
    {
        "Rank": 4343,
        "Port": "partilha",
        "Eng": "division"
    },
    {
        "Rank": 4343,
        "Port": "partilha",
        "Eng": "distribution"
    },
    {
        "Rank": 4344,
        "Port": "puramente",
        "Eng": "purely"
    },
    {
        "Rank": 4344,
        "Port": "puramente",
        "Eng": "only"
    },
    {
        "Rank": 4345,
        "Port": "enterro",
        "Eng": "burial"
    },
    {
        "Rank": 4346,
        "Port": "amargo",
        "Eng": "bitter"
    },
    {
        "Rank": 4347,
        "Port": "pureza",
        "Eng": "purity"
    },
    {
        "Rank": 4348,
        "Port": "mídia",
        "Eng": "media"
    },
    {
        "Rank": 4349,
        "Port": "colar",
        "Eng": "glue"
    },
    {
        "Rank": 4349,
        "Port": "colar",
        "Eng": "stick"
    },
    {
        "Rank": 4350,
        "Port": "cacho",
        "Eng": "bunch"
    },
    {
        "Rank": 4350,
        "Port": "cacho",
        "Eng": "cluster"
    },
    {
        "Rank": 4351,
        "Port": "adivinhar",
        "Eng": "predict"
    },
    {
        "Rank": 4351,
        "Port": "adivinhar",
        "Eng": "guess"
    },
    {
        "Rank": 4352,
        "Port": "canoa",
        "Eng": "canoe"
    },
    {
        "Rank": 4353,
        "Port": "moreno",
        "Eng": "brown"
    },
    {
        "Rank": 4353,
        "Port": "moreno",
        "Eng": "dark-skinned"
    },
    {
        "Rank": 4354,
        "Port": "divórcio",
        "Eng": "divorce"
    },
    {
        "Rank": 4355,
        "Port": "foro",
        "Eng": "court"
    },
    {
        "Rank": 4356,
        "Port": "casco",
        "Eng": "hull"
    },
    {
        "Rank": 4356,
        "Port": "casco",
        "Eng": "hoof"
    },
    {
        "Rank": 4357,
        "Port": "cimeira",
        "Eng": "summit"
    },
    {
        "Rank": 4358,
        "Port": "formoso",
        "Eng": "beautiful"
    },
    {
        "Rank": 4358,
        "Port": "formoso",
        "Eng": "attractive"
    },
    {
        "Rank": 4359,
        "Port": "caminhada",
        "Eng": "walk"
    },
    {
        "Rank": 4359,
        "Port": "caminhada",
        "Eng": "trek"
    },
    {
        "Rank": 4360,
        "Port": "banqueiro",
        "Eng": "banker"
    },
    {
        "Rank": 4361,
        "Port": "colecta",
        "Eng": "collection"
    },
    {
        "Rank": 4361,
        "Port": "colecta",
        "Eng": "tax"
    },
    {
        "Rank": 4361,
        "Port": "colecta",
        "Eng": "fee"
    },
    {
        "Rank": 4362,
        "Port": "besta",
        "Eng": "beast"
    },
    {
        "Rank": 4363,
        "Port": "noroeste",
        "Eng": "northwest"
    },
    {
        "Rank": 4364,
        "Port": "vácuo",
        "Eng": "vacuum"
    },
    {
        "Rank": 4364,
        "Port": "vácuo",
        "Eng": "without air"
    },
    {
        "Rank": 4365,
        "Port": "amarrar",
        "Eng": "bind"
    },
    {
        "Rank": 4365,
        "Port": "amarrar",
        "Eng": "tie down"
    },
    {
        "Rank": 4366,
        "Port": "jornalístico",
        "Eng": "journalistic"
    },
    {
        "Rank": 4367,
        "Port": "complicação",
        "Eng": "complication"
    },
    {
        "Rank": 4368,
        "Port": "odiar",
        "Eng": "hate"
    },
    {
        "Rank": 4369,
        "Port": "exclusão",
        "Eng": "exclusion"
    },
    {
        "Rank": 4370,
        "Port": "parado",
        "Eng": "stopped"
    },
    {
        "Rank": 4370,
        "Port": "parado",
        "Eng": "parked"
    },
    {
        "Rank": 4371,
        "Port": "dobro",
        "Eng": "twice the amount"
    },
    {
        "Rank": 4371,
        "Port": "dobro",
        "Eng": "double"
    },
    {
        "Rank": 4372,
        "Port": "ira",
        "Eng": "anger"
    },
    {
        "Rank": 4372,
        "Port": "ira",
        "Eng": "indignation"
    },
    {
        "Rank": 4372,
        "Port": "ira",
        "Eng": "wrath"
    },
    {
        "Rank": 4373,
        "Port": "fidalgo",
        "Eng": "noble"
    },
    {
        "Rank": 4373,
        "Port": "fidalgo",
        "Eng": "rich"
    },
    {
        "Rank": 4373,
        "Port": "fidalgo",
        "Eng": "aristocratic"
    },
    {
        "Rank": 4374,
        "Port": "anexo",
        "Eng": "attached"
    },
    {
        "Rank": 4374,
        "Port": "anexo",
        "Eng": "attachment"
    },
    {
        "Rank": 4375,
        "Port": "cólera",
        "Eng": "anger"
    },
    {
        "Rank": 4375,
        "Port": "cólera",
        "Eng": "irritation"
    },
    {
        "Rank": 4376,
        "Port": "ensinamento",
        "Eng": "teaching"
    },
    {
        "Rank": 4377,
        "Port": "infante",
        "Eng": "non-heir son of a king"
    },
    {
        "Rank": 4377,
        "Port": "infante",
        "Eng": "infant"
    },
    {
        "Rank": 4378,
        "Port": "débito",
        "Eng": "debit"
    },
    {
        "Rank": 4378,
        "Port": "débito",
        "Eng": "debt"
    },
    {
        "Rank": 4379,
        "Port": "ronda",
        "Eng": "round"
    },
    {
        "Rank": 4379,
        "Port": "ronda",
        "Eng": "surveillance"
    },
    {
        "Rank": 4380,
        "Port": "consecutivo",
        "Eng": "consecutive"
    },
    {
        "Rank": 4381,
        "Port": "cozido",
        "Eng": "cooked"
    },
    {
        "Rank": 4381,
        "Port": "cozido",
        "Eng": "stew"
    },
    {
        "Rank": 4382,
        "Port": "virado",
        "Eng": "facing"
    },
    {
        "Rank": 4382,
        "Port": "virado",
        "Eng": "turned to"
    },
    {
        "Rank": 4382,
        "Port": "virado",
        "Eng": "aimed at"
    },
    {
        "Rank": 4383,
        "Port": "fileira",
        "Eng": "row"
    },
    {
        "Rank": 4383,
        "Port": "fileira",
        "Eng": "rank"
    },
    {
        "Rank": 4384,
        "Port": "submarino",
        "Eng": "submarine"
    },
    {
        "Rank": 4384,
        "Port": "submarino",
        "Eng": "under-water"
    },
    {
        "Rank": 4385,
        "Port": "arame",
        "Eng": "wire"
    },
    {
        "Rank": 4386,
        "Port": "flutuar",
        "Eng": "float"
    },
    {
        "Rank": 4386,
        "Port": "flutuar",
        "Eng": "fluctuate"
    },
    {
        "Rank": 4387,
        "Port": "atractivo",
        "Eng": "attractive"
    },
    {
        "Rank": 4388,
        "Port": "luva",
        "Eng": "glove"
    },
    {
        "Rank": 4389,
        "Port": "vinculado",
        "Eng": "linked"
    },
    {
        "Rank": 4389,
        "Port": "vinculado",
        "Eng": "bound"
    },
    {
        "Rank": 4389,
        "Port": "vinculado",
        "Eng": "connected to"
    },
    {
        "Rank": 4390,
        "Port": "propício",
        "Eng": "favorable"
    },
    {
        "Rank": 4390,
        "Port": "propício",
        "Eng": "propitious"
    },
    {
        "Rank": 4391,
        "Port": "envelhecer",
        "Eng": "grow old"
    },
    {
        "Rank": 4392,
        "Port": "dia-a-dia",
        "Eng": "everyday life"
    },
    {
        "Rank": 4392,
        "Port": "dia-a-dia",
        "Eng": "day to day"
    },
    {
        "Rank": 4393,
        "Port": "casto",
        "Eng": "chaste"
    },
    {
        "Rank": 4394,
        "Port": "cera",
        "Eng": "wax"
    },
    {
        "Rank": 4395,
        "Port": "reverter",
        "Eng": "reverse"
    },
    {
        "Rank": 4395,
        "Port": "reverter",
        "Eng": "revert"
    },
    {
        "Rank": 4396,
        "Port": "obscuro",
        "Eng": "obscure"
    },
    {
        "Rank": 4397,
        "Port": "continental",
        "Eng": "continental"
    },
    {
        "Rank": 4398,
        "Port": "publicitário",
        "Eng": "advertising"
    },
    {
        "Rank": 4399,
        "Port": "choro",
        "Eng": "weeping"
    },
    {
        "Rank": 4400,
        "Port": "explosivo",
        "Eng": "explosive"
    },
    {
        "Rank": 4401,
        "Port": "protector",
        "Eng": "protector"
    },
    {
        "Rank": 4401,
        "Port": "protector",
        "Eng": "protective"
    },
    {
        "Rank": 4401,
        "Port": "protector",
        "Eng": "patron"
    },
    {
        "Rank": 4402,
        "Port": "marketing",
        "Eng": "marketing"
    },
    {
        "Rank": 4403,
        "Port": "sincero",
        "Eng": "sincere"
    },
    {
        "Rank": 4404,
        "Port": "sargento",
        "Eng": "sergeant"
    },
    {
        "Rank": 4405,
        "Port": "oitavo",
        "Eng": "eighth"
    },
    {
        "Rank": 4406,
        "Port": "troço",
        "Eng": "something"
    },
    {
        "Rank": 4406,
        "Port": "troço",
        "Eng": "stuff"
    },
    {
        "Rank": 4406,
        "Port": "troço",
        "Eng": "section of road"
    },
    {
        "Rank": 4407,
        "Port": "livremente",
        "Eng": "freely"
    },
    {
        "Rank": 4408,
        "Port": "difusão",
        "Eng": "spreading"
    },
    {
        "Rank": 4408,
        "Port": "difusão",
        "Eng": "diffusion"
    },
    {
        "Rank": 4409,
        "Port": "defensivo",
        "Eng": "defensive"
    },
    {
        "Rank": 4410,
        "Port": "atar",
        "Eng": "bind"
    },
    {
        "Rank": 4410,
        "Port": "atar",
        "Eng": "tie up"
    },
    {
        "Rank": 4411,
        "Port": "indiferença",
        "Eng": "indifference"
    },
    {
        "Rank": 4412,
        "Port": "acervo",
        "Eng": "large collection"
    },
    {
        "Rank": 4413,
        "Port": "concentrado",
        "Eng": "concentrated"
    },
    {
        "Rank": 4414,
        "Port": "distanciar",
        "Eng": "distance"
    },
    {
        "Rank": 4415,
        "Port": "credibilidade",
        "Eng": "credibility"
    },
    {
        "Rank": 4416,
        "Port": "acusado",
        "Eng": "accused"
    },
    {
        "Rank": 4417,
        "Port": "rolo",
        "Eng": "scroll"
    },
    {
        "Rank": 4417,
        "Port": "rolo",
        "Eng": "roll"
    },
    {
        "Rank": 4418,
        "Port": "usina",
        "Eng": "plant"
    },
    {
        "Rank": 4418,
        "Port": "usina",
        "Eng": "factory"
    },
    {
        "Rank": 4419,
        "Port": "previamente",
        "Eng": "previously"
    },
    {
        "Rank": 4420,
        "Port": "violação",
        "Eng": "violation"
    },
    {
        "Rank": 4421,
        "Port": "arquipélago",
        "Eng": "archipelago"
    },
    {
        "Rank": 4422,
        "Port": "especificar",
        "Eng": "specify"
    },
    {
        "Rank": 4423,
        "Port": "pasto",
        "Eng": "animal feed"
    },
    {
        "Rank": 4423,
        "Port": "pasto",
        "Eng": "pasture"
    },
    {
        "Rank": 4423,
        "Port": "pasto",
        "Eng": "food"
    },
    {
        "Rank": 4424,
        "Port": "feixe",
        "Eng": "shaft"
    },
    {
        "Rank": 4424,
        "Port": "feixe",
        "Eng": "ray"
    },
    {
        "Rank": 4424,
        "Port": "feixe",
        "Eng": "bundle"
    },
    {
        "Rank": 4425,
        "Port": "discriminação",
        "Eng": "discrimination"
    },
    {
        "Rank": 4426,
        "Port": "fértil",
        "Eng": "fertile"
    },
    {
        "Rank": 4427,
        "Port": "marcador",
        "Eng": "goal scorer"
    },
    {
        "Rank": 4427,
        "Port": "marcador",
        "Eng": "marker"
    },
    {
        "Rank": 4428,
        "Port": "vector",
        "Eng": "vector"
    },
    {
        "Rank": 4429,
        "Port": "parcialmente",
        "Eng": "partially"
    },
    {
        "Rank": 4430,
        "Port": "compacto",
        "Eng": "compact"
    },
    {
        "Rank": 4431,
        "Port": "aluguel",
        "Eng": "rent"
    },
    {
        "Rank": 4432,
        "Port": "remover",
        "Eng": "remove"
    },
    {
        "Rank": 4433,
        "Port": "instabilidade",
        "Eng": "instability"
    },
    {
        "Rank": 4434,
        "Port": "superioridade",
        "Eng": "superiority"
    },
    {
        "Rank": 4435,
        "Port": "suposto",
        "Eng": "supposed"
    },
    {
        "Rank": 4436,
        "Port": "miserável",
        "Eng": "miserable"
    },
    {
        "Rank": 4437,
        "Port": "mouro",
        "Eng": "moor"
    },
    {
        "Rank": 4438,
        "Port": "cumprimentar",
        "Eng": "greet"
    },
    {
        "Rank": 4439,
        "Port": "coerente",
        "Eng": "coherent"
    },
    {
        "Rank": 4440,
        "Port": "aperfeiçoar",
        "Eng": "perfect"
    },
    {
        "Rank": 4441,
        "Port": "unicamente",
        "Eng": "solely"
    },
    {
        "Rank": 4441,
        "Port": "unicamente",
        "Eng": "only"
    },
    {
        "Rank": 4442,
        "Port": "boletim",
        "Eng": "bulletin"
    },
    {
        "Rank": 4443,
        "Port": "tabuleiro",
        "Eng": "board"
    },
    {
        "Rank": 4443,
        "Port": "tabuleiro",
        "Eng": "tray"
    },
    {
        "Rank": 4444,
        "Port": "didáctico",
        "Eng": "teaching"
    },
    {
        "Rank": 4444,
        "Port": "didáctico",
        "Eng": "didactic"
    },
    {
        "Rank": 4445,
        "Port": "assaltar",
        "Eng": "rob"
    },
    {
        "Rank": 4445,
        "Port": "assaltar",
        "Eng": "assault"
    },
    {
        "Rank": 4445,
        "Port": "assaltar",
        "Eng": "mug"
    },
    {
        "Rank": 4446,
        "Port": "toca",
        "Eng": "den"
    },
    {
        "Rank": 4446,
        "Port": "toca",
        "Eng": "burrow"
    },
    {
        "Rank": 4446,
        "Port": "toca",
        "Eng": "lair"
    },
    {
        "Rank": 4447,
        "Port": "contribuinte",
        "Eng": "taxpayer"
    },
    {
        "Rank": 4447,
        "Port": "contribuinte",
        "Eng": "contributor"
    },
    {
        "Rank": 4448,
        "Port": "consequente",
        "Eng": "subsequent"
    },
    {
        "Rank": 4449,
        "Port": "desmentir",
        "Eng": "deny"
    },
    {
        "Rank": 4449,
        "Port": "desmentir",
        "Eng": "contradict"
    },
    {
        "Rank": 4449,
        "Port": "desmentir",
        "Eng": "expose"
    },
    {
        "Rank": 4450,
        "Port": "indenização",
        "Eng": "reparation"
    },
    {
        "Rank": 4451,
        "Port": "intensificar",
        "Eng": "intensify"
    },
    {
        "Rank": 4452,
        "Port": "ente",
        "Eng": "being"
    },
    {
        "Rank": 4452,
        "Port": "ente",
        "Eng": "person"
    },
    {
        "Rank": 4453,
        "Port": "analista",
        "Eng": "analyst"
    },
    {
        "Rank": 4454,
        "Port": "embaixo",
        "Eng": "under"
    },
    {
        "Rank": 4454,
        "Port": "embaixo",
        "Eng": "beneath"
    },
    {
        "Rank": 4454,
        "Port": "embaixo",
        "Eng": "underneath"
    },
    {
        "Rank": 4455,
        "Port": "explícito",
        "Eng": "explicit"
    },
    {
        "Rank": 4456,
        "Port": "compartilhar",
        "Eng": "share"
    },
    {
        "Rank": 4457,
        "Port": "ibérico",
        "Eng": "Iberian"
    },
    {
        "Rank": 4458,
        "Port": "inocência",
        "Eng": "innocence"
    },
    {
        "Rank": 4459,
        "Port": "horizontal",
        "Eng": "horizontal"
    },
    {
        "Rank": 4460,
        "Port": "brando",
        "Eng": "gentle"
    },
    {
        "Rank": 4460,
        "Port": "brando",
        "Eng": "tender"
    },
    {
        "Rank": 4460,
        "Port": "brando",
        "Eng": "soft"
    },
    {
        "Rank": 4461,
        "Port": "nó",
        "Eng": "knot"
    },
    {
        "Rank": 4462,
        "Port": "estrago",
        "Eng": "damage"
    },
    {
        "Rank": 4462,
        "Port": "estrago",
        "Eng": "destruction"
    },
    {
        "Rank": 4463,
        "Port": "espantoso",
        "Eng": "shocking"
    },
    {
        "Rank": 4463,
        "Port": "espantoso",
        "Eng": "surprising"
    },
    {
        "Rank": 4464,
        "Port": "inscrever",
        "Eng": "enroll"
    },
    {
        "Rank": 4464,
        "Port": "inscrever",
        "Eng": "register"
    },
    {
        "Rank": 4465,
        "Port": "frade",
        "Eng": "friar"
    },
    {
        "Rank": 4466,
        "Port": "antecedente",
        "Eng": "previous"
    },
    {
        "Rank": 4467,
        "Port": "ginásio",
        "Eng": "middle school"
    },
    {
        "Rank": 4467,
        "Port": "ginásio",
        "Eng": "gymnasium"
    },
    {
        "Rank": 4468,
        "Port": "vigiar",
        "Eng": "keep an eye on"
    },
    {
        "Rank": 4468,
        "Port": "vigiar",
        "Eng": "watch over"
    },
    {
        "Rank": 4469,
        "Port": "aviação",
        "Eng": "aviation"
    },
    {
        "Rank": 4470,
        "Port": "galho",
        "Eng": "branch"
    },
    {
        "Rank": 4471,
        "Port": "suíço",
        "Eng": "Swiss"
    },
    {
        "Rank": 4472,
        "Port": "inovador",
        "Eng": "innovative"
    },
    {
        "Rank": 4472,
        "Port": "inovador",
        "Eng": "innovator"
    },
    {
        "Rank": 4473,
        "Port": "brutal",
        "Eng": "brutal"
    },
    {
        "Rank": 4474,
        "Port": "ilustração",
        "Eng": "illustration"
    },
    {
        "Rank": 4475,
        "Port": "homossexual",
        "Eng": "homosexual"
    },
    {
        "Rank": 4476,
        "Port": "roxo",
        "Eng": "dark red"
    },
    {
        "Rank": 4476,
        "Port": "roxo",
        "Eng": "scarlet"
    },
    {
        "Rank": 4477,
        "Port": "difundir",
        "Eng": "spread"
    },
    {
        "Rank": 4477,
        "Port": "difundir",
        "Eng": "broadcast"
    },
    {
        "Rank": 4478,
        "Port": "recolha",
        "Eng": "compilation"
    },
    {
        "Rank": 4478,
        "Port": "recolha",
        "Eng": "collection"
    },
    {
        "Rank": 4479,
        "Port": "despacho",
        "Eng": "decree"
    },
    {
        "Rank": 4479,
        "Port": "despacho",
        "Eng": "resolution"
    },
    {
        "Rank": 4479,
        "Port": "despacho",
        "Eng": "decision"
    },
    {
        "Rank": 4480,
        "Port": "cortina",
        "Eng": "curtain"
    },
    {
        "Rank": 4481,
        "Port": "solicitação",
        "Eng": "request"
    },
    {
        "Rank": 4481,
        "Port": "solicitação",
        "Eng": "solicitation"
    },
    {
        "Rank": 4482,
        "Port": "agregado",
        "Eng": "aggregate"
    },
    {
        "Rank": 4482,
        "Port": "agregado",
        "Eng": "mixture"
    },
    {
        "Rank": 4483,
        "Port": "inundação",
        "Eng": "flood"
    },
    {
        "Rank": 4484,
        "Port": "exigente",
        "Eng": "demanding"
    },
    {
        "Rank": 4485,
        "Port": "fatia",
        "Eng": "share"
    },
    {
        "Rank": 4485,
        "Port": "fatia",
        "Eng": "slice"
    },
    {
        "Rank": 4485,
        "Port": "fatia",
        "Eng": "piece"
    },
    {
        "Rank": 4486,
        "Port": "espreitar",
        "Eng": "peek"
    },
    {
        "Rank": 4486,
        "Port": "espreitar",
        "Eng": "pry"
    },
    {
        "Rank": 4487,
        "Port": "cota",
        "Eng": "quota"
    },
    {
        "Rank": 4488,
        "Port": "enquadrar",
        "Eng": "fall into"
    },
    {
        "Rank": 4488,
        "Port": "enquadrar",
        "Eng": "fit into"
    },
    {
        "Rank": 4488,
        "Port": "enquadrar",
        "Eng": "abide by"
    },
    {
        "Rank": 4489,
        "Port": "melodia",
        "Eng": "melody"
    },
    {
        "Rank": 4490,
        "Port": "porventura",
        "Eng": "perhaps"
    },
    {
        "Rank": 4490,
        "Port": "porventura",
        "Eng": "maybe"
    },
    {
        "Rank": 4490,
        "Port": "porventura",
        "Eng": "by chance"
    },
    {
        "Rank": 4491,
        "Port": "sufocar",
        "Eng": "suffocate"
    },
    {
        "Rank": 4492,
        "Port": "helicóptero",
        "Eng": "helicopter"
    },
    {
        "Rank": 4493,
        "Port": "especialização",
        "Eng": "specialization"
    },
    {
        "Rank": 4494,
        "Port": "antepassado",
        "Eng": "ancestor"
    },
    {
        "Rank": 4495,
        "Port": "equivaler",
        "Eng": "be equivalent"
    },
    {
        "Rank": 4496,
        "Port": "englobar",
        "Eng": "encompass"
    },
    {
        "Rank": 4497,
        "Port": "grilo",
        "Eng": "cricket"
    },
    {
        "Rank": 4498,
        "Port": "oportuno",
        "Eng": "opportune"
    },
    {
        "Rank": 4498,
        "Port": "oportuno",
        "Eng": "timely"
    },
    {
        "Rank": 4499,
        "Port": "arrepender",
        "Eng": "repent"
    },
    {
        "Rank": 4499,
        "Port": "arrepender",
        "Eng": "regret"
    },
    {
        "Rank": 4500,
        "Port": "energético",
        "Eng": "of energy"
    },
    {
        "Rank": 4500,
        "Port": "energético",
        "Eng": "energetic"
    },
    {
        "Rank": 4501,
        "Port": "pendurado",
        "Eng": "hung"
    },
    {
        "Rank": 4502,
        "Port": "inclusão",
        "Eng": "inclusion"
    },
    {
        "Rank": 4503,
        "Port": "berço",
        "Eng": "crib"
    },
    {
        "Rank": 4503,
        "Port": "berço",
        "Eng": "cradle"
    },
    {
        "Rank": 4503,
        "Port": "berço",
        "Eng": "homeland"
    },
    {
        "Rank": 4504,
        "Port": "revólver",
        "Eng": "revolver"
    },
    {
        "Rank": 4505,
        "Port": "pálido",
        "Eng": "pale"
    },
    {
        "Rank": 4506,
        "Port": "fecho",
        "Eng": "bolt"
    },
    {
        "Rank": 4506,
        "Port": "fecho",
        "Eng": "latch"
    },
    {
        "Rank": 4506,
        "Port": "fecho",
        "Eng": "clasp"
    },
    {
        "Rank": 4506,
        "Port": "fecho",
        "Eng": "lock"
    },
    {
        "Rank": 4507,
        "Port": "cómico",
        "Eng": "comic"
    },
    {
        "Rank": 4507,
        "Port": "cómico",
        "Eng": "comedian"
    },
    {
        "Rank": 4508,
        "Port": "vivência",
        "Eng": "life"
    },
    {
        "Rank": 4508,
        "Port": "vivência",
        "Eng": "life experience"
    },
    {
        "Rank": 4509,
        "Port": "omissão",
        "Eng": "omission"
    },
    {
        "Rank": 4510,
        "Port": "empreender",
        "Eng": "undertake"
    },
    {
        "Rank": 4511,
        "Port": "adicional",
        "Eng": "additional"
    },
    {
        "Rank": 4512,
        "Port": "oxigénio",
        "Eng": "oxygen"
    },
    {
        "Rank": 4513,
        "Port": "sigilo",
        "Eng": "secrecy"
    },
    {
        "Rank": 4514,
        "Port": "suor",
        "Eng": "sweat"
    },
    {
        "Rank": 4515,
        "Port": "espesso",
        "Eng": "thick"
    },
    {
        "Rank": 4515,
        "Port": "espesso",
        "Eng": "dense"
    },
    {
        "Rank": 4515,
        "Port": "espesso",
        "Eng": "opaque"
    },
    {
        "Rank": 4516,
        "Port": "inverso",
        "Eng": "opposite"
    },
    {
        "Rank": 4516,
        "Port": "inverso",
        "Eng": "inverse"
    },
    {
        "Rank": 4516,
        "Port": "inverso",
        "Eng": "contrary"
    },
    {
        "Rank": 4517,
        "Port": "toalha",
        "Eng": "towel"
    },
    {
        "Rank": 4518,
        "Port": "incerto",
        "Eng": "uncertain"
    },
    {
        "Rank": 4518,
        "Port": "incerto",
        "Eng": "unsure"
    },
    {
        "Rank": 4519,
        "Port": "densidade",
        "Eng": "density"
    },
    {
        "Rank": 4520,
        "Port": "hino",
        "Eng": "anthem"
    },
    {
        "Rank": 4520,
        "Port": "hino",
        "Eng": "hymn"
    },
    {
        "Rank": 4521,
        "Port": "preceito",
        "Eng": "precept"
    },
    {
        "Rank": 4521,
        "Port": "preceito",
        "Eng": "order"
    },
    {
        "Rank": 4521,
        "Port": "preceito",
        "Eng": "norm"
    },
    {
        "Rank": 4522,
        "Port": "bruxa",
        "Eng": "witch"
    },
    {
        "Rank": 4523,
        "Port": "sobrepor",
        "Eng": "take precedence over"
    },
    {
        "Rank": 4523,
        "Port": "sobrepor",
        "Eng": "surpass"
    },
    {
        "Rank": 4524,
        "Port": "sobrinha",
        "Eng": "niece"
    },
    {
        "Rank": 4525,
        "Port": "cultivo",
        "Eng": "act of planting"
    },
    {
        "Rank": 4526,
        "Port": "desvantagem",
        "Eng": "disadvantage"
    },
    {
        "Rank": 4527,
        "Port": "punho",
        "Eng": "fist"
    },
    {
        "Rank": 4527,
        "Port": "punho",
        "Eng": "wrist"
    },
    {
        "Rank": 4527,
        "Port": "punho",
        "Eng": "cuff"
    },
    {
        "Rank": 4528,
        "Port": "bosque",
        "Eng": "grove"
    },
    {
        "Rank": 4529,
        "Port": "cotação",
        "Eng": "value"
    },
    {
        "Rank": 4529,
        "Port": "cotação",
        "Eng": "estimate"
    },
    {
        "Rank": 4530,
        "Port": "luto",
        "Eng": "mourning"
    },
    {
        "Rank": 4531,
        "Port": "precedente",
        "Eng": "precedent"
    },
    {
        "Rank": 4532,
        "Port": "pontapé",
        "Eng": "kick"
    },
    {
        "Rank": 4533,
        "Port": "varejo",
        "Eng": "retail"
    },
    {
        "Rank": 4534,
        "Port": "espectacular",
        "Eng": "spectacular"
    },
    {
        "Rank": 4535,
        "Port": "esquecimento",
        "Eng": "forgetfulness"
    },
    {
        "Rank": 4536,
        "Port": "aplicado",
        "Eng": "applied"
    },
    {
        "Rank": 4537,
        "Port": "documentação",
        "Eng": "documentation"
    },
    {
        "Rank": 4538,
        "Port": "inauguração",
        "Eng": "inauguration"
    },
    {
        "Rank": 4539,
        "Port": "satisfatório",
        "Eng": "satisfactory"
    },
    {
        "Rank": 4540,
        "Port": "gótico",
        "Eng": "gothic"
    },
    {
        "Rank": 4541,
        "Port": "sumário",
        "Eng": "brief"
    },
    {
        "Rank": 4541,
        "Port": "sumário",
        "Eng": "summary"
    },
    {
        "Rank": 4542,
        "Port": "politécnico",
        "Eng": "polytechnic"
    },
    {
        "Rank": 4543,
        "Port": "nulo",
        "Eng": "void"
    },
    {
        "Rank": 4543,
        "Port": "nulo",
        "Eng": "null"
    },
    {
        "Rank": 4544,
        "Port": "protestante",
        "Eng": "protestant"
    },
    {
        "Rank": 4545,
        "Port": "autárquico",
        "Eng": "non-governmental"
    },
    {
        "Rank": 4546,
        "Port": "interrogação",
        "Eng": "question"
    },
    {
        "Rank": 4546,
        "Port": "interrogação",
        "Eng": "interrogation"
    },
    {
        "Rank": 4547,
        "Port": "terapia",
        "Eng": "therapy"
    },
    {
        "Rank": 4547,
        "Port": "terapia",
        "Eng": "treatment"
    },
    {
        "Rank": 4548,
        "Port": "temperamento",
        "Eng": "temperament"
    },
    {
        "Rank": 4549,
        "Port": "arbitragem",
        "Eng": "arbitration"
    },
    {
        "Rank": 4550,
        "Port": "falecido",
        "Eng": "deceased"
    },
    {
        "Rank": 4551,
        "Port": "furioso",
        "Eng": "sharp"
    },
    {
        "Rank": 4551,
        "Port": "furioso",
        "Eng": "furious"
    },
    {
        "Rank": 4552,
        "Port": "proteína",
        "Eng": "protein"
    },
    {
        "Rank": 4553,
        "Port": "extra",
        "Eng": "extra"
    },
    {
        "Rank": 4553,
        "Port": "extra",
        "Eng": "added"
    },
    {
        "Rank": 4554,
        "Port": "bordado",
        "Eng": "embroidered"
    },
    {
        "Rank": 4554,
        "Port": "bordado",
        "Eng": "embroidery"
    },
    {
        "Rank": 4555,
        "Port": "coesão",
        "Eng": "cohesion"
    },
    {
        "Rank": 4556,
        "Port": "colonização",
        "Eng": "colonization"
    },
    {
        "Rank": 4556,
        "Port": "colonização",
        "Eng": "colonizing"
    },
    {
        "Rank": 4557,
        "Port": "proporcional",
        "Eng": "proportional"
    },
    {
        "Rank": 4558,
        "Port": "psicólogo",
        "Eng": "psychologist"
    },
    {
        "Rank": 4559,
        "Port": "accionar",
        "Eng": "activate"
    },
    {
        "Rank": 4559,
        "Port": "accionar",
        "Eng": "put into action"
    },
    {
        "Rank": 4560,
        "Port": "recair",
        "Eng": "go back to"
    },
    {
        "Rank": 4560,
        "Port": "recair",
        "Eng": "revert to"
    },
    {
        "Rank": 4561,
        "Port": "equívoco",
        "Eng": "error"
    },
    {
        "Rank": 4561,
        "Port": "equívoco",
        "Eng": "equivocal"
    },
    {
        "Rank": 4562,
        "Port": "desconto",
        "Eng": "discount"
    },
    {
        "Rank": 4563,
        "Port": "espinho",
        "Eng": "thorn"
    },
    {
        "Rank": 4564,
        "Port": "espinha",
        "Eng": "spine"
    },
    {
        "Rank": 4564,
        "Port": "espinha",
        "Eng": "fish bone"
    },
    {
        "Rank": 4564,
        "Port": "espinha",
        "Eng": "acne"
    },
    {
        "Rank": 4565,
        "Port": "esqueleto",
        "Eng": "skeleton"
    },
    {
        "Rank": 4566,
        "Port": "metodologia",
        "Eng": "methodology"
    },
    {
        "Rank": 4567,
        "Port": "teclado",
        "Eng": "keyboard"
    },
    {
        "Rank": 4568,
        "Port": "condicionado",
        "Eng": "conditioned"
    },
    {
        "Rank": 4569,
        "Port": "corrector",
        "Eng": "broker"
    },
    {
        "Rank": 4569,
        "Port": "corrector",
        "Eng": "agent"
    },
    {
        "Rank": 4570,
        "Port": "artilharia",
        "Eng": "artillery"
    },
    {
        "Rank": 4571,
        "Port": "tubarão",
        "Eng": "shark"
    },
    {
        "Rank": 4572,
        "Port": "comunhão",
        "Eng": "communion"
    },
    {
        "Rank": 4573,
        "Port": "abraço",
        "Eng": "embrace"
    },
    {
        "Rank": 4573,
        "Port": "abraço",
        "Eng": "hug"
    },
    {
        "Rank": 4574,
        "Port": "rim",
        "Eng": "kidney"
    },
    {
        "Rank": 4575,
        "Port": "fracção",
        "Eng": "fraction"
    },
    {
        "Rank": 4575,
        "Port": "fracção",
        "Eng": "bit"
    },
    {
        "Rank": 4576,
        "Port": "editorial",
        "Eng": "editorial"
    },
    {
        "Rank": 4577,
        "Port": "resgate",
        "Eng": "rescue"
    },
    {
        "Rank": 4577,
        "Port": "resgate",
        "Eng": "ransom"
    },
    {
        "Rank": 4578,
        "Port": "mercê",
        "Eng": "at the mercy of"
    },
    {
        "Rank": 4579,
        "Port": "marechal",
        "Eng": "marshal"
    },
    {
        "Rank": 4580,
        "Port": "balão",
        "Eng": "balloon"
    },
    {
        "Rank": 4581,
        "Port": "tremer",
        "Eng": "shake"
    },
    {
        "Rank": 4581,
        "Port": "tremer",
        "Eng": "tremble"
    },
    {
        "Rank": 4582,
        "Port": "milénio",
        "Eng": "millennium"
    },
    {
        "Rank": 4583,
        "Port": "reajuste",
        "Eng": "adjustment"
    },
    {
        "Rank": 4584,
        "Port": "aquecimento",
        "Eng": "warming"
    },
    {
        "Rank": 4584,
        "Port": "aquecimento",
        "Eng": "heating"
    },
    {
        "Rank": 4585,
        "Port": "torturar",
        "Eng": "torture"
    },
    {
        "Rank": 4586,
        "Port": "afogar",
        "Eng": "drown"
    },
    {
        "Rank": 4586,
        "Port": "afogar",
        "Eng": "flood"
    },
    {
        "Rank": 4587,
        "Port": "estragar",
        "Eng": "ruin"
    },
    {
        "Rank": 4587,
        "Port": "estragar",
        "Eng": "go bad"
    },
    {
        "Rank": 4587,
        "Port": "estragar",
        "Eng": "spoil"
    },
    {
        "Rank": 4588,
        "Port": "alinhado",
        "Eng": "aligned"
    },
    {
        "Rank": 4589,
        "Port": "charuto",
        "Eng": "cigar"
    },
    {
        "Rank": 4590,
        "Port": "nora",
        "Eng": "daughter-in-law"
    },
    {
        "Rank": 4591,
        "Port": "aposentado",
        "Eng": "retired"
    },
    {
        "Rank": 4592,
        "Port": "consentir",
        "Eng": "approve"
    },
    {
        "Rank": 4592,
        "Port": "consentir",
        "Eng": "grant"
    },
    {
        "Rank": 4593,
        "Port": "romantismo",
        "Eng": "romanticism"
    },
    {
        "Rank": 4594,
        "Port": "agrupar",
        "Eng": "group"
    },
    {
        "Rank": 4595,
        "Port": "narrativo",
        "Eng": "narrative"
    },
    {
        "Rank": 4596,
        "Port": "canhão",
        "Eng": "cannon"
    },
    {
        "Rank": 4597,
        "Port": "sanguíneo",
        "Eng": "relating to blood"
    },
    {
        "Rank": 4598,
        "Port": "bandeirante",
        "Eng": "bandeirante"
    },
    {
        "Rank": 4599,
        "Port": "atlântico",
        "Eng": "Atlantic"
    },
    {
        "Rank": 4600,
        "Port": "reputação",
        "Eng": "reputation"
    },
    {
        "Rank": 4601,
        "Port": "saudar",
        "Eng": "greet"
    },
    {
        "Rank": 4601,
        "Port": "saudar",
        "Eng": "salute"
    },
    {
        "Rank": 4602,
        "Port": "traçado",
        "Eng": "drawn out"
    },
    {
        "Rank": 4602,
        "Port": "traçado",
        "Eng": "planned"
    },
    {
        "Rank": 4602,
        "Port": "traçado",
        "Eng": "plan"
    },
    {
        "Rank": 4603,
        "Port": "preliminar",
        "Eng": "preliminary"
    },
    {
        "Rank": 4604,
        "Port": "confirmação",
        "Eng": "confirmation"
    },
    {
        "Rank": 4605,
        "Port": "abdicar",
        "Eng": "abdicate"
    },
    {
        "Rank": 4606,
        "Port": "diferir",
        "Eng": "differ"
    },
    {
        "Rank": 4606,
        "Port": "diferir",
        "Eng": "be different from"
    },
    {
        "Rank": 4607,
        "Port": "escocês",
        "Eng": "Scottish"
    },
    {
        "Rank": 4608,
        "Port": "curiosamente",
        "Eng": "curiously"
    },
    {
        "Rank": 4609,
        "Port": "ídolo",
        "Eng": "idol"
    },
    {
        "Rank": 4610,
        "Port": "grosseiro",
        "Eng": "crude"
    },
    {
        "Rank": 4610,
        "Port": "grosseiro",
        "Eng": "coarse"
    },
    {
        "Rank": 4610,
        "Port": "grosseiro",
        "Eng": "rude"
    },
    {
        "Rank": 4611,
        "Port": "serpente",
        "Eng": "snake"
    },
    {
        "Rank": 4611,
        "Port": "serpente",
        "Eng": "serpent"
    },
    {
        "Rank": 4612,
        "Port": "tomada",
        "Eng": "seizure"
    },
    {
        "Rank": 4612,
        "Port": "tomada",
        "Eng": "socket"
    },
    {
        "Rank": 4613,
        "Port": "terno",
        "Eng": "suit"
    },
    {
        "Rank": 4614,
        "Port": "civilizado",
        "Eng": "civilized"
    },
    {
        "Rank": 4615,
        "Port": "anotar",
        "Eng": "write down"
    },
    {
        "Rank": 4615,
        "Port": "anotar",
        "Eng": "annotate"
    },
    {
        "Rank": 4616,
        "Port": "desdobrar",
        "Eng": "unfold"
    },
    {
        "Rank": 4617,
        "Port": "progredir",
        "Eng": "progress"
    },
    {
        "Rank": 4618,
        "Port": "montado",
        "Eng": "mounted"
    },
    {
        "Rank": 4618,
        "Port": "montado",
        "Eng": "assembled"
    },
    {
        "Rank": 4619,
        "Port": "furtar",
        "Eng": "steal"
    },
    {
        "Rank": 4620,
        "Port": "contributo",
        "Eng": "contribution"
    },
    {
        "Rank": 4621,
        "Port": "precoce",
        "Eng": "premature"
    },
    {
        "Rank": 4621,
        "Port": "precoce",
        "Eng": "precocious"
    },
    {
        "Rank": 4622,
        "Port": "pardo",
        "Eng": "mulatto"
    },
    {
        "Rank": 4622,
        "Port": "pardo",
        "Eng": "off-white"
    },
    {
        "Rank": 4623,
        "Port": "poste",
        "Eng": "pole"
    },
    {
        "Rank": 4623,
        "Port": "poste",
        "Eng": "vertical post"
    },
    {
        "Rank": 4624,
        "Port": "milionário",
        "Eng": "millionaire"
    },
    {
        "Rank": 4625,
        "Port": "panela",
        "Eng": "pot"
    },
    {
        "Rank": 4625,
        "Port": "panela",
        "Eng": "pan"
    },
    {
        "Rank": 4626,
        "Port": "hostil",
        "Eng": "hostile"
    },
    {
        "Rank": 4627,
        "Port": "sudoeste",
        "Eng": "southwest"
    },
    {
        "Rank": 4628,
        "Port": "cavalaria",
        "Eng": "cavalry"
    },
    {
        "Rank": 4629,
        "Port": "bonde",
        "Eng": "streetcar"
    },
    {
        "Rank": 4630,
        "Port": "exagero",
        "Eng": "exaggeration"
    },
    {
        "Rank": 4631,
        "Port": "liquidar",
        "Eng": "pay off"
    },
    {
        "Rank": 4631,
        "Port": "liquidar",
        "Eng": "liquidate"
    },
    {
        "Rank": 4632,
        "Port": "rosado",
        "Eng": "rosy"
    },
    {
        "Rank": 4632,
        "Port": "rosado",
        "Eng": "pink"
    },
    {
        "Rank": 4633,
        "Port": "referido",
        "Eng": "above-mentioned"
    },
    {
        "Rank": 4633,
        "Port": "referido",
        "Eng": "referred"
    },
    {
        "Rank": 4634,
        "Port": "brisa",
        "Eng": "breeze"
    },
    {
        "Rank": 4635,
        "Port": "internado",
        "Eng": "hospitalized"
    },
    {
        "Rank": 4636,
        "Port": "convocação",
        "Eng": "convocation"
    },
    {
        "Rank": 4636,
        "Port": "convocação",
        "Eng": "gathering"
    },
    {
        "Rank": 4637,
        "Port": "adjectivo",
        "Eng": "adjective"
    },
    {
        "Rank": 4638,
        "Port": "trajecto",
        "Eng": "route"
    },
    {
        "Rank": 4638,
        "Port": "trajecto",
        "Eng": "distance"
    },
    {
        "Rank": 4638,
        "Port": "trajecto",
        "Eng": "stretch"
    },
    {
        "Rank": 4639,
        "Port": "chefiar",
        "Eng": "head"
    },
    {
        "Rank": 4639,
        "Port": "chefiar",
        "Eng": "lead"
    },
    {
        "Rank": 4640,
        "Port": "vocabulário",
        "Eng": "vocabulary"
    },
    {
        "Rank": 4641,
        "Port": "coerência",
        "Eng": "coherence"
    },
    {
        "Rank": 4642,
        "Port": "aspirar",
        "Eng": "aspire to"
    },
    {
        "Rank": 4642,
        "Port": "aspirar",
        "Eng": "inhale"
    },
    {
        "Rank": 4643,
        "Port": "multinacional",
        "Eng": "international corporation"
    },
    {
        "Rank": 4644,
        "Port": "homenagear",
        "Eng": "pay homage to"
    },
    {
        "Rank": 4644,
        "Port": "homenagear",
        "Eng": "honor"
    },
    {
        "Rank": 4645,
        "Port": "asilo",
        "Eng": "asylum"
    },
    {
        "Rank": 4646,
        "Port": "ceia",
        "Eng": "supper"
    },
    {
        "Rank": 4646,
        "Port": "ceia",
        "Eng": "dinner"
    },
    {
        "Rank": 4647,
        "Port": "moradia",
        "Eng": "dwelling"
    },
    {
        "Rank": 4648,
        "Port": "nordestino",
        "Eng": "northeastern"
    },
    {
        "Rank": 4648,
        "Port": "nordestino",
        "Eng": "northeasterner"
    },
    {
        "Rank": 4649,
        "Port": "apelido",
        "Eng": "nickname"
    },
    {
        "Rank": 4650,
        "Port": "celeste",
        "Eng": "heavenly"
    },
    {
        "Rank": 4651,
        "Port": "abismo",
        "Eng": "abyss"
    },
    {
        "Rank": 4652,
        "Port": "aprendizado",
        "Eng": "the act of learning"
    },
    {
        "Rank": 4653,
        "Port": "contestação",
        "Eng": "challenge"
    },
    {
        "Rank": 4654,
        "Port": "liberação",
        "Eng": "disbursement"
    },
    {
        "Rank": 4654,
        "Port": "liberação",
        "Eng": "absolving"
    },
    {
        "Rank": 4655,
        "Port": "homicídio",
        "Eng": "murder"
    },
    {
        "Rank": 4655,
        "Port": "homicídio",
        "Eng": "homicide"
    },
    {
        "Rank": 4656,
        "Port": "terraço",
        "Eng": "porch"
    },
    {
        "Rank": 4656,
        "Port": "terraço",
        "Eng": "patio"
    },
    {
        "Rank": 4656,
        "Port": "terraço",
        "Eng": "balcony"
    },
    {
        "Rank": 4657,
        "Port": "fixação",
        "Eng": "stability"
    },
    {
        "Rank": 4657,
        "Port": "fixação",
        "Eng": "settlement"
    },
    {
        "Rank": 4658,
        "Port": "heróico",
        "Eng": "heroic"
    },
    {
        "Rank": 4659,
        "Port": "ensaiar",
        "Eng": "practice"
    },
    {
        "Rank": 4659,
        "Port": "ensaiar",
        "Eng": "rehearse"
    },
    {
        "Rank": 4660,
        "Port": "pontual",
        "Eng": "punctual"
    },
    {
        "Rank": 4661,
        "Port": "manto",
        "Eng": "mantle"
    },
    {
        "Rank": 4661,
        "Port": "manto",
        "Eng": "robe"
    },
    {
        "Rank": 4662,
        "Port": "globalização",
        "Eng": "globalization"
    },
    {
        "Rank": 4663,
        "Port": "catálogo",
        "Eng": "index"
    },
    {
        "Rank": 4663,
        "Port": "catálogo",
        "Eng": "catalog"
    },
    {
        "Rank": 4664,
        "Port": "socorrer",
        "Eng": "aid"
    },
    {
        "Rank": 4664,
        "Port": "socorrer",
        "Eng": "relieve"
    },
    {
        "Rank": 4665,
        "Port": "colocado",
        "Eng": "positioned"
    },
    {
        "Rank": 4665,
        "Port": "colocado",
        "Eng": "placed"
    },
    {
        "Rank": 4666,
        "Port": "desgaste",
        "Eng": "deterioration"
    },
    {
        "Rank": 4667,
        "Port": "cantiga",
        "Eng": "song"
    },
    {
        "Rank": 4667,
        "Port": "cantiga",
        "Eng": "ballad"
    },
    {
        "Rank": 4668,
        "Port": "recinto",
        "Eng": "chamber"
    },
    {
        "Rank": 4668,
        "Port": "recinto",
        "Eng": "enclosure"
    },
    {
        "Rank": 4669,
        "Port": "qualificado",
        "Eng": "qualified"
    },
    {
        "Rank": 4670,
        "Port": "encaixar",
        "Eng": "fit"
    },
    {
        "Rank": 4670,
        "Port": "encaixar",
        "Eng": "belong"
    },
    {
        "Rank": 4671,
        "Port": "preceder",
        "Eng": "precede"
    },
    {
        "Rank": 4671,
        "Port": "preceder",
        "Eng": "predate"
    },
    {
        "Rank": 4672,
        "Port": "ditador",
        "Eng": "dictator"
    },
    {
        "Rank": 4673,
        "Port": "nominal",
        "Eng": "nominal"
    },
    {
        "Rank": 4674,
        "Port": "maturidade",
        "Eng": "maturity"
    },
    {
        "Rank": 4675,
        "Port": "inquietação",
        "Eng": "unrest"
    },
    {
        "Rank": 4675,
        "Port": "inquietação",
        "Eng": "disturbance"
    },
    {
        "Rank": 4676,
        "Port": "cerâmica",
        "Eng": "pottery"
    },
    {
        "Rank": 4676,
        "Port": "cerâmica",
        "Eng": "ceramics"
    },
    {
        "Rank": 4677,
        "Port": "mecânica",
        "Eng": "mechanics"
    },
    {
        "Rank": 4678,
        "Port": "sacar",
        "Eng": "take out"
    },
    {
        "Rank": 4678,
        "Port": "sacar",
        "Eng": "pull out"
    },
    {
        "Rank": 4679,
        "Port": "porta-voz",
        "Eng": "spokesperson"
    },
    {
        "Rank": 4680,
        "Port": "mão-de-obra",
        "Eng": "labor"
    },
    {
        "Rank": 4680,
        "Port": "mão-de-obra",
        "Eng": "workers"
    },
    {
        "Rank": 4681,
        "Port": "distrital",
        "Eng": "relating to a district"
    },
    {
        "Rank": 4682,
        "Port": "burocrático",
        "Eng": "bureaucratic"
    },
    {
        "Rank": 4683,
        "Port": "extracção",
        "Eng": "extraction"
    },
    {
        "Rank": 4684,
        "Port": "descobrimento",
        "Eng": "discovery"
    },
    {
        "Rank": 4685,
        "Port": "complemento",
        "Eng": "object"
    },
    {
        "Rank": 4685,
        "Port": "complemento",
        "Eng": "addition"
    },
    {
        "Rank": 4686,
        "Port": "desordem",
        "Eng": "disorder"
    },
    {
        "Rank": 4686,
        "Port": "desordem",
        "Eng": "quarrel"
    },
    {
        "Rank": 4686,
        "Port": "desordem",
        "Eng": "fight"
    },
    {
        "Rank": 4687,
        "Port": "ex-presidente",
        "Eng": "ex-president"
    },
    {
        "Rank": 4688,
        "Port": "influente",
        "Eng": "influential"
    },
    {
        "Rank": 4689,
        "Port": "postal",
        "Eng": "postal"
    },
    {
        "Rank": 4690,
        "Port": "queima",
        "Eng": "burning"
    },
    {
        "Rank": 4691,
        "Port": "suprir",
        "Eng": "supply"
    },
    {
        "Rank": 4692,
        "Port": "clínico",
        "Eng": "clinical"
    },
    {
        "Rank": 4693,
        "Port": "alinhamento",
        "Eng": "alignment"
    },
    {
        "Rank": 4693,
        "Port": "alinhamento",
        "Eng": "line up"
    },
    {
        "Rank": 4694,
        "Port": "desembargador",
        "Eng": "judge of a court of appeals"
    },
    {
        "Rank": 4695,
        "Port": "catástrofe",
        "Eng": "catastrophe"
    },
    {
        "Rank": 4696,
        "Port": "deslocamento",
        "Eng": "movement"
    },
    {
        "Rank": 4696,
        "Port": "deslocamento",
        "Eng": "travel"
    },
    {
        "Rank": 4697,
        "Port": "indagar",
        "Eng": "inquire"
    },
    {
        "Rank": 4697,
        "Port": "indagar",
        "Eng": "question"
    },
    {
        "Rank": 4698,
        "Port": "mobilização",
        "Eng": "mobilization"
    },
    {
        "Rank": 4699,
        "Port": "seduzir",
        "Eng": "seduce"
    },
    {
        "Rank": 4699,
        "Port": "seduzir",
        "Eng": "entice"
    },
    {
        "Rank": 4700,
        "Port": "equipado",
        "Eng": "equipped"
    },
    {
        "Rank": 4701,
        "Port": "atrever",
        "Eng": "dare"
    },
    {
        "Rank": 4702,
        "Port": "regimento",
        "Eng": "regiment"
    },
    {
        "Rank": 4703,
        "Port": "estranhar",
        "Eng": "find strange"
    },
    {
        "Rank": 4703,
        "Port": "estranhar",
        "Eng": "be surprised"
    },
    {
        "Rank": 4704,
        "Port": "vulto",
        "Eng": "notable person"
    },
    {
        "Rank": 4704,
        "Port": "vulto",
        "Eng": "appearance"
    },
    {
        "Rank": 4705,
        "Port": "legião",
        "Eng": "legion"
    },
    {
        "Rank": 4706,
        "Port": "elevador",
        "Eng": "elevator"
    },
    {
        "Rank": 4707,
        "Port": "afinidade",
        "Eng": "affinity"
    },
    {
        "Rank": 4708,
        "Port": "corporal",
        "Eng": "relating to the body"
    },
    {
        "Rank": 4708,
        "Port": "corporal",
        "Eng": "bodily"
    },
    {
        "Rank": 4709,
        "Port": "inerente",
        "Eng": "inherent"
    },
    {
        "Rank": 4710,
        "Port": "suscitar",
        "Eng": "rouse"
    },
    {
        "Rank": 4710,
        "Port": "suscitar",
        "Eng": "excite"
    },
    {
        "Rank": 4711,
        "Port": "conciliar",
        "Eng": "reconcile"
    },
    {
        "Rank": 4711,
        "Port": "conciliar",
        "Eng": "harmonize"
    },
    {
        "Rank": 4712,
        "Port": "estabilização",
        "Eng": "stability"
    },
    {
        "Rank": 4712,
        "Port": "estabilização",
        "Eng": "stabilization"
    },
    {
        "Rank": 4713,
        "Port": "intriga",
        "Eng": "intrigue"
    },
    {
        "Rank": 4714,
        "Port": "facção",
        "Eng": "faction"
    },
    {
        "Rank": 4715,
        "Port": "indignação",
        "Eng": "indignation"
    },
    {
        "Rank": 4716,
        "Port": "lavagem",
        "Eng": "washing"
    },
    {
        "Rank": 4717,
        "Port": "tigre",
        "Eng": "tiger"
    },
    {
        "Rank": 4718,
        "Port": "humidade",
        "Eng": "humidity"
    },
    {
        "Rank": 4718,
        "Port": "humidade",
        "Eng": "moisture"
    },
    {
        "Rank": 4719,
        "Port": "cumplicidade",
        "Eng": "complicity"
    },
    {
        "Rank": 4720,
        "Port": "travessia",
        "Eng": "crossing"
    },
    {
        "Rank": 4721,
        "Port": "gerador",
        "Eng": "generator"
    },
    {
        "Rank": 4722,
        "Port": "arraial",
        "Eng": "encampment"
    },
    {
        "Rank": 4722,
        "Port": "arraial",
        "Eng": "fair"
    },
    {
        "Rank": 4723,
        "Port": "assessor",
        "Eng": "assistant"
    },
    {
        "Rank": 4723,
        "Port": "assessor",
        "Eng": "advisor"
    },
    {
        "Rank": 4723,
        "Port": "assessor",
        "Eng": "consultant"
    },
    {
        "Rank": 4724,
        "Port": "saca",
        "Eng": "bag"
    },
    {
        "Rank": 4724,
        "Port": "saca",
        "Eng": "sack"
    },
    {
        "Rank": 4725,
        "Port": "prosperidade",
        "Eng": "prosperity"
    },
    {
        "Rank": 4726,
        "Port": "simular",
        "Eng": "simulate"
    },
    {
        "Rank": 4727,
        "Port": "estacionar",
        "Eng": "park"
    },
    {
        "Rank": 4728,
        "Port": "peculiar",
        "Eng": "peculiar"
    },
    {
        "Rank": 4729,
        "Port": "competitividade",
        "Eng": "competitiveness"
    },
    {
        "Rank": 4730,
        "Port": "torcida",
        "Eng": "fans"
    },
    {
        "Rank": 4730,
        "Port": "torcida",
        "Eng": "fan club"
    },
    {
        "Rank": 4731,
        "Port": "predominar",
        "Eng": "predominate"
    },
    {
        "Rank": 4731,
        "Port": "predominar",
        "Eng": "prevail"
    },
    {
        "Rank": 4732,
        "Port": "concretização",
        "Eng": "realization"
    },
    {
        "Rank": 4733,
        "Port": "chato",
        "Eng": "bothersome"
    },
    {
        "Rank": 4733,
        "Port": "chato",
        "Eng": "unpleasant"
    },
    {
        "Rank": 4733,
        "Port": "chato",
        "Eng": "boring"
    },
    {
        "Rank": 4734,
        "Port": "insistência",
        "Eng": "insistence"
    },
    {
        "Rank": 4735,
        "Port": "problemática",
        "Eng": "issue"
    },
    {
        "Rank": 4735,
        "Port": "problemática",
        "Eng": "problem"
    },
    {
        "Rank": 4736,
        "Port": "semanal",
        "Eng": "weekly"
    },
    {
        "Rank": 4737,
        "Port": "atributo",
        "Eng": "attribute"
    },
    {
        "Rank": 4737,
        "Port": "atributo",
        "Eng": "trait"
    },
    {
        "Rank": 4738,
        "Port": "educacional",
        "Eng": "educational"
    },
    {
        "Rank": 4739,
        "Port": "acrescido",
        "Eng": "increased"
    },
    {
        "Rank": 4739,
        "Port": "acrescido",
        "Eng": "addition"
    },
    {
        "Rank": 4740,
        "Port": "tona",
        "Eng": "the surface"
    },
    {
        "Rank": 4741,
        "Port": "sonda",
        "Eng": "probe"
    },
    {
        "Rank": 4741,
        "Port": "sonda",
        "Eng": "catheter"
    },
    {
        "Rank": 4742,
        "Port": "australiano",
        "Eng": "Australian"
    },
    {
        "Rank": 4743,
        "Port": "ferimento",
        "Eng": "wound"
    },
    {
        "Rank": 4744,
        "Port": "câncer",
        "Eng": "cancer"
    },
    {
        "Rank": 4745,
        "Port": "subúrbio",
        "Eng": "suburbs"
    },
    {
        "Rank": 4746,
        "Port": "neutro",
        "Eng": "neutral"
    },
    {
        "Rank": 4747,
        "Port": "estudioso",
        "Eng": "studious"
    },
    {
        "Rank": 4747,
        "Port": "estudioso",
        "Eng": "scholar"
    },
    {
        "Rank": 4747,
        "Port": "estudioso",
        "Eng": "erudite"
    },
    {
        "Rank": 4748,
        "Port": "flagrante",
        "Eng": "flagrant"
    },
    {
        "Rank": 4749,
        "Port": "pneu",
        "Eng": "tire"
    },
    {
        "Rank": 4750,
        "Port": "tostão",
        "Eng": "an old coin worth 0 cents"
    },
    {
        "Rank": 4751,
        "Port": "acta",
        "Eng": "minutes"
    },
    {
        "Rank": 4752,
        "Port": "naturalidade",
        "Eng": "ease"
    },
    {
        "Rank": 4752,
        "Port": "naturalidade",
        "Eng": "place of birth"
    },
    {
        "Rank": 4753,
        "Port": "rodada",
        "Eng": "round"
    },
    {
        "Rank": 4753,
        "Port": "rodada",
        "Eng": "rotation"
    },
    {
        "Rank": 4754,
        "Port": "procissão",
        "Eng": "procession"
    },
    {
        "Rank": 4755,
        "Port": "chocolate",
        "Eng": "chocolate"
    },
    {
        "Rank": 4756,
        "Port": "vice",
        "Eng": "vice"
    },
    {
        "Rank": 4757,
        "Port": "alimentar",
        "Eng": "relating to food"
    },
    {
        "Rank": 4758,
        "Port": "robusto",
        "Eng": "strong"
    },
    {
        "Rank": 4758,
        "Port": "robusto",
        "Eng": "robust"
    },
    {
        "Rank": 4759,
        "Port": "contaminado",
        "Eng": "contaminated"
    },
    {
        "Rank": 4760,
        "Port": "detalhado",
        "Eng": "detailed"
    },
    {
        "Rank": 4761,
        "Port": "pausa",
        "Eng": "pause"
    },
    {
        "Rank": 4761,
        "Port": "pausa",
        "Eng": "rest"
    },
    {
        "Rank": 4762,
        "Port": "marfim",
        "Eng": "ivory"
    },
    {
        "Rank": 4763,
        "Port": "rejeição",
        "Eng": "rejection"
    },
    {
        "Rank": 4764,
        "Port": "acomodar",
        "Eng": "make comfortable"
    },
    {
        "Rank": 4764,
        "Port": "acomodar",
        "Eng": "accommodate"
    },
    {
        "Rank": 4765,
        "Port": "mocidade",
        "Eng": "youth"
    },
    {
        "Rank": 4766,
        "Port": "fúria",
        "Eng": "fury"
    },
    {
        "Rank": 4766,
        "Port": "fúria",
        "Eng": "wrath"
    },
    {
        "Rank": 4767,
        "Port": "incompleto",
        "Eng": "incomplete"
    },
    {
        "Rank": 4768,
        "Port": "implementação",
        "Eng": "implementation"
    },
    {
        "Rank": 4769,
        "Port": "actualizar",
        "Eng": "update"
    },
    {
        "Rank": 4769,
        "Port": "actualizar",
        "Eng": "modernize"
    },
    {
        "Rank": 4770,
        "Port": "sábado",
        "Eng": "Saturday"
    },
    {
        "Rank": 4771,
        "Port": "crente",
        "Eng": "believer"
    },
    {
        "Rank": 4771,
        "Port": "crente",
        "Eng": "believing"
    },
    {
        "Rank": 4772,
        "Port": "preventivo",
        "Eng": "preventative"
    },
    {
        "Rank": 4773,
        "Port": "papai",
        "Eng": "daddy"
    },
    {
        "Rank": 4774,
        "Port": "meia-noite",
        "Eng": "midnight"
    },
    {
        "Rank": 4775,
        "Port": "risca",
        "Eng": "stripe"
    },
    {
        "Rank": 4775,
        "Port": "risca",
        "Eng": "line"
    },
    {
        "Rank": 4776,
        "Port": "sardinha",
        "Eng": "sardine"
    },
    {
        "Rank": 4777,
        "Port": "espingarda",
        "Eng": "shotgun"
    },
    {
        "Rank": 4778,
        "Port": "profeta",
        "Eng": "prophet"
    },
    {
        "Rank": 4779,
        "Port": "emigrar",
        "Eng": "emigrate"
    },
    {
        "Rank": 4780,
        "Port": "eleitorado",
        "Eng": "electorate"
    },
    {
        "Rank": 4781,
        "Port": "progressivamente",
        "Eng": "gradually"
    },
    {
        "Rank": 4781,
        "Port": "progressivamente",
        "Eng": "progres- sively"
    },
    {
        "Rank": 4782,
        "Port": "cavar",
        "Eng": "dig"
    },
    {
        "Rank": 4783,
        "Port": "contagem",
        "Eng": "counting"
    },
    {
        "Rank": 4784,
        "Port": "pesadelo",
        "Eng": "nightmare"
    },
    {
        "Rank": 4785,
        "Port": "honrar",
        "Eng": "honor"
    },
    {
        "Rank": 4786,
        "Port": "susceptível",
        "Eng": "susceptible"
    },
    {
        "Rank": 4787,
        "Port": "cereal",
        "Eng": "grain"
    },
    {
        "Rank": 4787,
        "Port": "cereal",
        "Eng": "cereal"
    },
    {
        "Rank": 4788,
        "Port": "renascer",
        "Eng": "be reborn"
    },
    {
        "Rank": 4789,
        "Port": "apetite",
        "Eng": "appetite"
    },
    {
        "Rank": 4790,
        "Port": "sertanejo",
        "Eng": "from rural Northeastern Brazil"
    },
    {
        "Rank": 4791,
        "Port": "cartório",
        "Eng": "records office"
    },
    {
        "Rank": 4792,
        "Port": "privilegiar",
        "Eng": "favor"
    },
    {
        "Rank": 4792,
        "Port": "privilegiar",
        "Eng": "choose"
    },
    {
        "Rank": 4793,
        "Port": "fracassar",
        "Eng": "fail"
    },
    {
        "Rank": 4794,
        "Port": "sinistro",
        "Eng": "sinister"
    },
    {
        "Rank": 4795,
        "Port": "atenuar",
        "Eng": "reduce"
    },
    {
        "Rank": 4795,
        "Port": "atenuar",
        "Eng": "die down"
    },
    {
        "Rank": 4795,
        "Port": "atenuar",
        "Eng": "ease"
    },
    {
        "Rank": 4796,
        "Port": "catorze",
        "Eng": "fourteen"
    },
    {
        "Rank": 4797,
        "Port": "marxista",
        "Eng": "Marxist"
    },
    {
        "Rank": 4798,
        "Port": "substancial",
        "Eng": "substantial"
    },
    {
        "Rank": 4799,
        "Port": "bloquear",
        "Eng": "block"
    },
    {
        "Rank": 4800,
        "Port": "refazer",
        "Eng": "redo"
    },
    {
        "Rank": 4801,
        "Port": "reafirmar",
        "Eng": "reaffirm"
    },
    {
        "Rank": 4802,
        "Port": "flanco",
        "Eng": "flank"
    },
    {
        "Rank": 4803,
        "Port": "estacionamento",
        "Eng": "parking"
    },
    {
        "Rank": 4804,
        "Port": "equação",
        "Eng": "equation"
    },
    {
        "Rank": 4805,
        "Port": "secular",
        "Eng": "secular"
    },
    {
        "Rank": 4806,
        "Port": "acostumar",
        "Eng": "get used to"
    },
    {
        "Rank": 4807,
        "Port": "velar",
        "Eng": "watch over"
    },
    {
        "Rank": 4807,
        "Port": "velar",
        "Eng": "care for"
    },
    {
        "Rank": 4808,
        "Port": "derramar",
        "Eng": "shed"
    },
    {
        "Rank": 4808,
        "Port": "derramar",
        "Eng": "spill"
    },
    {
        "Rank": 4809,
        "Port": "encarnado",
        "Eng": "incarnate"
    },
    {
        "Rank": 4809,
        "Port": "encarnado",
        "Eng": "in the flesh"
    },
    {
        "Rank": 4810,
        "Port": "ternura",
        "Eng": "tenderness"
    },
    {
        "Rank": 4810,
        "Port": "ternura",
        "Eng": "kindness"
    },
    {
        "Rank": 4811,
        "Port": "condicionar",
        "Eng": "condition"
    },
    {
        "Rank": 4811,
        "Port": "condicionar",
        "Eng": "subject"
    },
    {
        "Rank": 4812,
        "Port": "fluir",
        "Eng": "flow"
    },
    {
        "Rank": 4813,
        "Port": "poça",
        "Eng": "puddle"
    },
    {
        "Rank": 4814,
        "Port": "irritação",
        "Eng": "irritation"
    },
    {
        "Rank": 4815,
        "Port": "esvaziar",
        "Eng": "empty"
    },
    {
        "Rank": 4816,
        "Port": "giro",
        "Eng": "walk"
    },
    {
        "Rank": 4816,
        "Port": "giro",
        "Eng": "return"
    },
    {
        "Rank": 4817,
        "Port": "providenciar",
        "Eng": "provide"
    },
    {
        "Rank": 4818,
        "Port": "encostado",
        "Eng": "leaning"
    },
    {
        "Rank": 4818,
        "Port": "encostado",
        "Eng": "resting on"
    },
    {
        "Rank": 4819,
        "Port": "perpétuo",
        "Eng": "perpetual"
    },
    {
        "Rank": 4819,
        "Port": "perpétuo",
        "Eng": "permanent"
    },
    {
        "Rank": 4820,
        "Port": "declínio",
        "Eng": "decline"
    },
    {
        "Rank": 4821,
        "Port": "prematuro",
        "Eng": "premature"
    },
    {
        "Rank": 4822,
        "Port": "amplitude",
        "Eng": "range"
    },
    {
        "Rank": 4822,
        "Port": "amplitude",
        "Eng": "amplitude"
    },
    {
        "Rank": 4823,
        "Port": "piada",
        "Eng": "joke"
    },
    {
        "Rank": 4824,
        "Port": "rancho",
        "Eng": "ranch"
    },
    {
        "Rank": 4825,
        "Port": "despeito",
        "Eng": "despite"
    },
    {
        "Rank": 4825,
        "Port": "despeito",
        "Eng": "in spite of"
    },
    {
        "Rank": 4826,
        "Port": "acréscimo",
        "Eng": "growth"
    },
    {
        "Rank": 4826,
        "Port": "acréscimo",
        "Eng": "addition"
    },
    {
        "Rank": 4826,
        "Port": "acréscimo",
        "Eng": "increase"
    },
    {
        "Rank": 4827,
        "Port": "fôlego",
        "Eng": "breath"
    },
    {
        "Rank": 4828,
        "Port": "derivado",
        "Eng": "derivative"
    },
    {
        "Rank": 4828,
        "Port": "derivado",
        "Eng": "derived"
    },
    {
        "Rank": 4829,
        "Port": "armamento",
        "Eng": "weaponry"
    },
    {
        "Rank": 4830,
        "Port": "deduzir",
        "Eng": "figure out"
    },
    {
        "Rank": 4830,
        "Port": "deduzir",
        "Eng": "deduce"
    },
    {
        "Rank": 4831,
        "Port": "confortável",
        "Eng": "comfortable"
    },
    {
        "Rank": 4832,
        "Port": "cauteloso",
        "Eng": "cautious"
    },
    {
        "Rank": 4832,
        "Port": "cauteloso",
        "Eng": "prudent"
    },
    {
        "Rank": 4833,
        "Port": "televisivo",
        "Eng": "related to TV"
    },
    {
        "Rank": 4834,
        "Port": "sul-africano",
        "Eng": "South African"
    },
    {
        "Rank": 4835,
        "Port": "boneco",
        "Eng": "toy"
    },
    {
        "Rank": 4835,
        "Port": "boneco",
        "Eng": "action figure"
    },
    {
        "Rank": 4836,
        "Port": "castigar",
        "Eng": "punish"
    },
    {
        "Rank": 4837,
        "Port": "curvar",
        "Eng": "bow"
    },
    {
        "Rank": 4838,
        "Port": "gestor",
        "Eng": "manager"
    },
    {
        "Rank": 4839,
        "Port": "caldo",
        "Eng": "broth"
    },
    {
        "Rank": 4839,
        "Port": "caldo",
        "Eng": "soup"
    },
    {
        "Rank": 4840,
        "Port": "tolerância",
        "Eng": "tolerance"
    },
    {
        "Rank": 4841,
        "Port": "feto",
        "Eng": "fetus"
    },
    {
        "Rank": 4842,
        "Port": "valioso",
        "Eng": "valuable"
    },
    {
        "Rank": 4843,
        "Port": "fauna",
        "Eng": "fauna"
    },
    {
        "Rank": 4844,
        "Port": "detestar",
        "Eng": "detest"
    },
    {
        "Rank": 4845,
        "Port": "cirúrgico",
        "Eng": "surgical"
    },
    {
        "Rank": 4846,
        "Port": "cavalheiro",
        "Eng": "gentleman"
    },
    {
        "Rank": 4847,
        "Port": "astro",
        "Eng": "any heavenly body"
    },
    {
        "Rank": 4847,
        "Port": "astro",
        "Eng": "pop-star"
    },
    {
        "Rank": 4848,
        "Port": "presumir",
        "Eng": "presume"
    },
    {
        "Rank": 4848,
        "Port": "presumir",
        "Eng": "suspect"
    },
    {
        "Rank": 4849,
        "Port": "ofensa",
        "Eng": "offense"
    },
    {
        "Rank": 4850,
        "Port": "queimado",
        "Eng": "burned"
    },
    {
        "Rank": 4851,
        "Port": "atraente",
        "Eng": "attractive"
    },
    {
        "Rank": 4852,
        "Port": "viável",
        "Eng": "viable"
    },
    {
        "Rank": 4853,
        "Port": "afectivo",
        "Eng": "affectionate"
    },
    {
        "Rank": 4853,
        "Port": "afectivo",
        "Eng": "caring"
    },
    {
        "Rank": 4854,
        "Port": "configuração",
        "Eng": "configuration"
    },
    {
        "Rank": 4855,
        "Port": "gravata",
        "Eng": "necktie"
    },
    {
        "Rank": 4856,
        "Port": "influir",
        "Eng": "influence"
    },
    {
        "Rank": 4856,
        "Port": "influir",
        "Eng": "dominate"
    },
    {
        "Rank": 4857,
        "Port": "chefia",
        "Eng": "leadership"
    },
    {
        "Rank": 4857,
        "Port": "chefia",
        "Eng": "command"
    },
    {
        "Rank": 4858,
        "Port": "subitamente",
        "Eng": "suddenly"
    },
    {
        "Rank": 4859,
        "Port": "burocracia",
        "Eng": "bureaucracy"
    },
    {
        "Rank": 4860,
        "Port": "correctamente",
        "Eng": "accurately"
    },
    {
        "Rank": 4860,
        "Port": "correctamente",
        "Eng": "correctly"
    },
    {
        "Rank": 4861,
        "Port": "criatividade",
        "Eng": "creativity"
    },
    {
        "Rank": 4862,
        "Port": "parasita",
        "Eng": "parasite"
    },
    {
        "Rank": 4862,
        "Port": "parasita",
        "Eng": "parasitic"
    },
    {
        "Rank": 4863,
        "Port": "findar",
        "Eng": "end"
    },
    {
        "Rank": 4863,
        "Port": "findar",
        "Eng": "finalize"
    },
    {
        "Rank": 4864,
        "Port": "comunicado",
        "Eng": "press release"
    },
    {
        "Rank": 4864,
        "Port": "comunicado",
        "Eng": "announcement"
    },
    {
        "Rank": 4865,
        "Port": "fera",
        "Eng": "wild animal"
    },
    {
        "Rank": 4865,
        "Port": "fera",
        "Eng": "beast"
    },
    {
        "Rank": 4866,
        "Port": "rosário",
        "Eng": "rosary"
    },
    {
        "Rank": 4867,
        "Port": "solucionar",
        "Eng": "solve"
    },
    {
        "Rank": 4867,
        "Port": "solucionar",
        "Eng": "find a solution"
    },
    {
        "Rank": 4868,
        "Port": "rito",
        "Eng": "rite"
    },
    {
        "Rank": 4869,
        "Port": "tradicionalmente",
        "Eng": "traditionally"
    },
    {
        "Rank": 4870,
        "Port": "embrião",
        "Eng": "embryo"
    },
    {
        "Rank": 4871,
        "Port": "contido",
        "Eng": "contained"
    },
    {
        "Rank": 4872,
        "Port": "sopro",
        "Eng": "puff"
    },
    {
        "Rank": 4872,
        "Port": "sopro",
        "Eng": "breath"
    },
    {
        "Rank": 4873,
        "Port": "vulcão",
        "Eng": "volcano"
    },
    {
        "Rank": 4874,
        "Port": "site",
        "Eng": "website"
    },
    {
        "Rank": 4875,
        "Port": "índex",
        "Eng": "index"
    },
    {
        "Rank": 4876,
        "Port": "escorrer",
        "Eng": "trickle"
    },
    {
        "Rank": 4876,
        "Port": "escorrer",
        "Eng": "drain"
    },
    {
        "Rank": 4876,
        "Port": "escorrer",
        "Eng": "drip"
    },
    {
        "Rank": 4877,
        "Port": "decerto",
        "Eng": "certainly"
    },
    {
        "Rank": 4878,
        "Port": "detective",
        "Eng": "detective"
    },
    {
        "Rank": 4879,
        "Port": "lavrar",
        "Eng": "cultivate"
    },
    {
        "Rank": 4880,
        "Port": "calado",
        "Eng": "quiet"
    },
    {
        "Rank": 4881,
        "Port": "gozo",
        "Eng": "pleasure"
    },
    {
        "Rank": 4881,
        "Port": "gozo",
        "Eng": "joy"
    },
    {
        "Rank": 4881,
        "Port": "gozo",
        "Eng": "enjoyment"
    },
    {
        "Rank": 4882,
        "Port": "isento",
        "Eng": "exempt"
    },
    {
        "Rank": 4882,
        "Port": "isento",
        "Eng": "free"
    },
    {
        "Rank": 4883,
        "Port": "prudente",
        "Eng": "prudent"
    },
    {
        "Rank": 4884,
        "Port": "envergonhar",
        "Eng": "be ashamed"
    },
    {
        "Rank": 4885,
        "Port": "confiante",
        "Eng": "confident"
    },
    {
        "Rank": 4886,
        "Port": "seriedade",
        "Eng": "seriousness"
    },
    {
        "Rank": 4887,
        "Port": "perito",
        "Eng": "expert"
    },
    {
        "Rank": 4888,
        "Port": "peste",
        "Eng": "plague"
    },
    {
        "Rank": 4888,
        "Port": "peste",
        "Eng": "pest"
    },
    {
        "Rank": 4889,
        "Port": "obsessão",
        "Eng": "obsession"
    },
    {
        "Rank": 4890,
        "Port": "cozinhar",
        "Eng": "cook"
    },
    {
        "Rank": 4891,
        "Port": "cristianismo",
        "Eng": "Christianity"
    },
    {
        "Rank": 4892,
        "Port": "convicto",
        "Eng": "die-hard"
    },
    {
        "Rank": 4892,
        "Port": "convicto",
        "Eng": "convinced"
    },
    {
        "Rank": 4893,
        "Port": "detenção",
        "Eng": "arrest"
    },
    {
        "Rank": 4893,
        "Port": "detenção",
        "Eng": "detention"
    },
    {
        "Rank": 4894,
        "Port": "despedida",
        "Eng": "farewell"
    },
    {
        "Rank": 4895,
        "Port": "perverso",
        "Eng": "pervert"
    },
    {
        "Rank": 4895,
        "Port": "perverso",
        "Eng": "perverse"
    },
    {
        "Rank": 4896,
        "Port": "experimento",
        "Eng": "experiment"
    },
    {
        "Rank": 4897,
        "Port": "mandado",
        "Eng": "warrant"
    },
    {
        "Rank": 4897,
        "Port": "mandado",
        "Eng": "mandate"
    },
    {
        "Rank": 4898,
        "Port": "reservado",
        "Eng": "reserved"
    },
    {
        "Rank": 4899,
        "Port": "tenso",
        "Eng": "tense"
    },
    {
        "Rank": 4900,
        "Port": "telefonema",
        "Eng": "phone call"
    },
    {
        "Rank": 4901,
        "Port": "devorar",
        "Eng": "devour"
    },
    {
        "Rank": 4902,
        "Port": "mola",
        "Eng": "spring"
    },
    {
        "Rank": 4902,
        "Port": "mola",
        "Eng": "driving force"
    },
    {
        "Rank": 4903,
        "Port": "voltado",
        "Eng": "intended for"
    },
    {
        "Rank": 4903,
        "Port": "voltado",
        "Eng": "turned towards"
    },
    {
        "Rank": 4904,
        "Port": "descalço",
        "Eng": "barefoot"
    },
    {
        "Rank": 4904,
        "Port": "descalço",
        "Eng": "shoeless"
    },
    {
        "Rank": 4905,
        "Port": "caixão",
        "Eng": "coffin"
    },
    {
        "Rank": 4905,
        "Port": "caixão",
        "Eng": "casket"
    },
    {
        "Rank": 4905,
        "Port": "caixão",
        "Eng": "big box"
    },
    {
        "Rank": 4906,
        "Port": "construtor",
        "Eng": "builder"
    },
    {
        "Rank": 4907,
        "Port": "azeitona",
        "Eng": "olive"
    },
    {
        "Rank": 4908,
        "Port": "violino",
        "Eng": "violin"
    },
    {
        "Rank": 4909,
        "Port": "apreender",
        "Eng": "comprehend"
    },
    {
        "Rank": 4909,
        "Port": "apreender",
        "Eng": "apprehend"
    },
    {
        "Rank": 4910,
        "Port": "brusco",
        "Eng": "abrupt"
    },
    {
        "Rank": 4910,
        "Port": "brusco",
        "Eng": "sudden"
    },
    {
        "Rank": 4910,
        "Port": "brusco",
        "Eng": "brusque"
    },
    {
        "Rank": 4911,
        "Port": "tripa",
        "Eng": "intestine"
    },
    {
        "Rank": 4911,
        "Port": "tripa",
        "Eng": "tripe"
    },
    {
        "Rank": 4912,
        "Port": "ginástica",
        "Eng": "gymnastics"
    },
    {
        "Rank": 4913,
        "Port": "povoar",
        "Eng": "people"
    },
    {
        "Rank": 4914,
        "Port": "reparação",
        "Eng": "redress"
    },
    {
        "Rank": 4914,
        "Port": "reparação",
        "Eng": "reparation"
    },
    {
        "Rank": 4915,
        "Port": "advogar",
        "Eng": "advocate"
    },
    {
        "Rank": 4916,
        "Port": "justificação",
        "Eng": "justification"
    },
    {
        "Rank": 4917,
        "Port": "firmeza",
        "Eng": "steadiness"
    },
    {
        "Rank": 4917,
        "Port": "firmeza",
        "Eng": "firmness"
    },
    {
        "Rank": 4918,
        "Port": "originário",
        "Eng": "originally from"
    },
    {
        "Rank": 4919,
        "Port": "peregrinação",
        "Eng": "pilgrimage"
    },
    {
        "Rank": 4919,
        "Port": "peregrinação",
        "Eng": "journey"
    },
    {
        "Rank": 4920,
        "Port": "validade",
        "Eng": "validity"
    },
    {
        "Rank": 4921,
        "Port": "proposição",
        "Eng": "proposition"
    },
    {
        "Rank": 4922,
        "Port": "flauta",
        "Eng": "flute"
    },
    {
        "Rank": 4923,
        "Port": "suicidar",
        "Eng": "commit suicide"
    },
    {
        "Rank": 4924,
        "Port": "roer",
        "Eng": "gnaw"
    },
    {
        "Rank": 4924,
        "Port": "roer",
        "Eng": "bite"
    },
    {
        "Rank": 4925,
        "Port": "incómodo",
        "Eng": "inconvenient"
    },
    {
        "Rank": 4925,
        "Port": "incómodo",
        "Eng": "difficulty"
    },
    {
        "Rank": 4926,
        "Port": "edificação",
        "Eng": "building"
    },
    {
        "Rank": 4926,
        "Port": "edificação",
        "Eng": "construction"
    },
    {
        "Rank": 4926,
        "Port": "edificação",
        "Eng": "edification"
    },
    {
        "Rank": 4927,
        "Port": "regularmente",
        "Eng": "regularly"
    },
    {
        "Rank": 4928,
        "Port": "opositor",
        "Eng": "opponent"
    },
    {
        "Rank": 4928,
        "Port": "opositor",
        "Eng": "opposing"
    },
    {
        "Rank": 4929,
        "Port": "fóssil",
        "Eng": "fossil"
    },
    {
        "Rank": 4930,
        "Port": "esquecido",
        "Eng": "forgotten"
    },
    {
        "Rank": 4931,
        "Port": "arbusto",
        "Eng": "bush"
    },
    {
        "Rank": 4932,
        "Port": "celebração",
        "Eng": "celebration"
    },
    {
        "Rank": 4933,
        "Port": "encarregado",
        "Eng": "in charge"
    },
    {
        "Rank": 4933,
        "Port": "encarregado",
        "Eng": "responsible"
    },
    {
        "Rank": 4934,
        "Port": "cal",
        "Eng": "limestone"
    },
    {
        "Rank": 4935,
        "Port": "ninho",
        "Eng": "nest"
    },
    {
        "Rank": 4936,
        "Port": "encantar",
        "Eng": "fascinate"
    },
    {
        "Rank": 4936,
        "Port": "encantar",
        "Eng": "cast a spell on"
    },
    {
        "Rank": 4937,
        "Port": "linear",
        "Eng": "linear"
    },
    {
        "Rank": 4938,
        "Port": "colete",
        "Eng": "vest"
    },
    {
        "Rank": 4938,
        "Port": "colete",
        "Eng": "straight jacket"
    },
    {
        "Rank": 4939,
        "Port": "manipulação",
        "Eng": "manipulation"
    },
    {
        "Rank": 4940,
        "Port": "potencialidade",
        "Eng": "potential"
    },
    {
        "Rank": 4941,
        "Port": "pia",
        "Eng": "sink"
    },
    {
        "Rank": 4941,
        "Port": "pia",
        "Eng": "font"
    },
    {
        "Rank": 4942,
        "Port": "namoro",
        "Eng": "affair"
    },
    {
        "Rank": 4943,
        "Port": "divisa",
        "Eng": "border"
    },
    {
        "Rank": 4943,
        "Port": "divisa",
        "Eng": "money"
    },
    {
        "Rank": 4944,
        "Port": "compatível",
        "Eng": "compatible"
    },
    {
        "Rank": 4945,
        "Port": "deslocação",
        "Eng": "travel"
    },
    {
        "Rank": 4945,
        "Port": "deslocação",
        "Eng": "displacement"
    },
    {
        "Rank": 4946,
        "Port": "amarrado",
        "Eng": "fastened"
    },
    {
        "Rank": 4946,
        "Port": "amarrado",
        "Eng": "tied"
    },
    {
        "Rank": 4947,
        "Port": "paralisar",
        "Eng": "paralyze"
    },
    {
        "Rank": 4948,
        "Port": "casado",
        "Eng": "married"
    },
    {
        "Rank": 4949,
        "Port": "violeta",
        "Eng": "violet"
    },
    {
        "Rank": 4950,
        "Port": "paradigma",
        "Eng": "paradigm"
    },
    {
        "Rank": 4951,
        "Port": "encolher",
        "Eng": "shrug"
    },
    {
        "Rank": 4952,
        "Port": "decorativo",
        "Eng": "decorative"
    },
    {
        "Rank": 4953,
        "Port": "vigente",
        "Eng": "current"
    },
    {
        "Rank": 4953,
        "Port": "vigente",
        "Eng": "currently in force"
    },
    {
        "Rank": 4954,
        "Port": "artifício",
        "Eng": "device"
    },
    {
        "Rank": 4954,
        "Port": "artifício",
        "Eng": "tool"
    },
    {
        "Rank": 4955,
        "Port": "club",
        "Eng": "club"
    },
    {
        "Rank": 4956,
        "Port": "expediente",
        "Eng": "business hours"
    },
    {
        "Rank": 4956,
        "Port": "expediente",
        "Eng": "escape from problem"
    },
    {
        "Rank": 4957,
        "Port": "armadilha",
        "Eng": "trap"
    },
    {
        "Rank": 4958,
        "Port": "gripe",
        "Eng": "cold"
    },
    {
        "Rank": 4958,
        "Port": "gripe",
        "Eng": "flu"
    },
    {
        "Rank": 4959,
        "Port": "contornar",
        "Eng": "go around"
    },
    {
        "Rank": 4959,
        "Port": "contornar",
        "Eng": "bypass"
    },
    {
        "Rank": 4960,
        "Port": "epidemia",
        "Eng": "epidemic"
    },
    {
        "Rank": 4961,
        "Port": "aceitável",
        "Eng": "acceptable"
    },
    {
        "Rank": 4962,
        "Port": "vocal",
        "Eng": "vocal"
    },
    {
        "Rank": 4963,
        "Port": "candeeiro",
        "Eng": "lamp"
    },
    {
        "Rank": 4963,
        "Port": "candeeiro",
        "Eng": "light fixture"
    },
    {
        "Rank": 4964,
        "Port": "cancelar",
        "Eng": "revoke"
    },
    {
        "Rank": 4964,
        "Port": "cancelar",
        "Eng": "cancel"
    },
    {
        "Rank": 4965,
        "Port": "cimo",
        "Eng": "peak"
    },
    {
        "Rank": 4965,
        "Port": "cimo",
        "Eng": "top"
    },
    {
        "Rank": 4966,
        "Port": "bengala",
        "Eng": "walking stick"
    },
    {
        "Rank": 4966,
        "Port": "bengala",
        "Eng": "cane"
    },
    {
        "Rank": 4967,
        "Port": "consultor",
        "Eng": "consultant"
    },
    {
        "Rank": 4968,
        "Port": "velhice",
        "Eng": "old age"
    },
    {
        "Rank": 4969,
        "Port": "escuridão",
        "Eng": "darkness"
    },
    {
        "Rank": 4970,
        "Port": "telegrama",
        "Eng": "telegram"
    },
    {
        "Rank": 4971,
        "Port": "carneiro",
        "Eng": "ram"
    },
    {
        "Rank": 4972,
        "Port": "iludir",
        "Eng": "deceive"
    },
    {
        "Rank": 4972,
        "Port": "iludir",
        "Eng": "delude"
    },
    {
        "Rank": 4973,
        "Port": "barca",
        "Eng": "small boat"
    },
    {
        "Rank": 4974,
        "Port": "armazenar",
        "Eng": "store"
    },
    {
        "Rank": 4975,
        "Port": "incidência",
        "Eng": "incidence"
    },
    {
        "Rank": 4976,
        "Port": "bondade",
        "Eng": "kindness"
    },
    {
        "Rank": 4976,
        "Port": "bondade",
        "Eng": "goodness"
    },
    {
        "Rank": 4977,
        "Port": "carroça",
        "Eng": "cart"
    },
    {
        "Rank": 4977,
        "Port": "carroça",
        "Eng": "wagon"
    },
    {
        "Rank": 4978,
        "Port": "afectado",
        "Eng": "affected"
    },
    {
        "Rank": 4979,
        "Port": "edificar",
        "Eng": "build"
    },
    {
        "Rank": 4979,
        "Port": "edificar",
        "Eng": "edify"
    },
    {
        "Rank": 4980,
        "Port": "edital",
        "Eng": "public notice"
    },
    {
        "Rank": 4980,
        "Port": "edital",
        "Eng": "relating to editing"
    },
    {
        "Rank": 4981,
        "Port": "estúpido",
        "Eng": "stupid"
    },
    {
        "Rank": 4981,
        "Port": "estúpido",
        "Eng": "brute"
    },
    {
        "Rank": 4982,
        "Port": "idiota",
        "Eng": "idiot"
    },
    {
        "Rank": 4982,
        "Port": "idiota",
        "Eng": "idiotic"
    },
    {
        "Rank": 4983,
        "Port": "insegurança",
        "Eng": "insecurity"
    },
    {
        "Rank": 4984,
        "Port": "despir",
        "Eng": "take off"
    },
    {
        "Rank": 4984,
        "Port": "despir",
        "Eng": "undress"
    },
    {
        "Rank": 4985,
        "Port": "ancestral",
        "Eng": "ancestor"
    },
    {
        "Rank": 4985,
        "Port": "ancestral",
        "Eng": "ancestral"
    },
    {
        "Rank": 4986,
        "Port": "rótulo",
        "Eng": "label"
    },
    {
        "Rank": 4987,
        "Port": "vigoroso",
        "Eng": "vigorous"
    },
    {
        "Rank": 4987,
        "Port": "vigoroso",
        "Eng": "strong"
    },
    {
        "Rank": 4988,
        "Port": "louro",
        "Eng": "blond"
    },
    {
        "Rank": 4989,
        "Port": "deslizar",
        "Eng": "slide"
    },
    {
        "Rank": 4989,
        "Port": "deslizar",
        "Eng": "glide"
    },
    {
        "Rank": 4989,
        "Port": "deslizar",
        "Eng": "slip"
    },
    {
        "Rank": 4990,
        "Port": "módulo",
        "Eng": "module"
    },
    {
        "Rank": 4991,
        "Port": "ministrar",
        "Eng": "administer"
    },
    {
        "Rank": 4992,
        "Port": "mendigo",
        "Eng": "beggar"
    },
    {
        "Rank": 4993,
        "Port": "serenidade",
        "Eng": "serenity"
    },
    {
        "Rank": 4994,
        "Port": "informativo",
        "Eng": "informative"
    },
    {
        "Rank": 4995,
        "Port": "crânio",
        "Eng": "skull"
    },
    {
        "Rank": 4996,
        "Port": "feitio",
        "Eng": "personality"
    },
    {
        "Rank": 4996,
        "Port": "feitio",
        "Eng": "temperament"
    },
    {
        "Rank": 4996,
        "Port": "feitio",
        "Eng": "feature"
    },
    {
        "Rank": 4997,
        "Port": "lápis",
        "Eng": "pencil"
    },
    {
        "Rank": 4998,
        "Port": "penetração",
        "Eng": "penetration"
    },
    {
        "Rank": 4999,
        "Port": "mamífero",
        "Eng": "mammal"
    },
    {
        "Rank": 5000,
        "Port": "sul-americano",
        "Eng": "South-American"
    }
].map(entry => Object.assign(entry, {Eng: entry.Eng.trim().toLowerCase()}));

export default lookup;
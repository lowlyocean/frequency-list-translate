// loosely convert a slider representing lexical coverage to a word frequency rank

const lexCoverageSliderVals =[
    .3, //~14 words
    .4, //~36 words,
    .5, //~88 words
    .55, //~138 words
    .6, //~216 words
    .7, //~531 words
    .8, //~1302 words
    .9, //~3193 words
    .95 //~4999 words
];

// Assuming 5k words == 95% coverage, and using WolframAlpha combined with formula described
// https://stackoverflow.com/questions/55518957/how-to-calculate-the-optimal-zipf-distribution-of-word-frequencies-in-a-text
const rankThresh = lexCoverageFraction => Math.ceil(Math.exp(lexCoverageFraction*(Math.log(4397) + .576613)));

export { lexCoverageSliderVals, rankThresh };
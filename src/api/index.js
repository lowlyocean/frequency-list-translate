import lookup from './en_to_pt';
import conjugate from 'conjugate';

// courtesy of
// https://www.freecodecamp.org/news/understanding-memoize-in-javascript-51d07d19430e/
// a simple memoize function that takes in a function
// and returns a memoized function
const memoize = (fn) => {
  let cache = {};
  return (...args) => {
    let n = args.join('');
    if (n in cache) {
      return cache[n];
    }
    else {
      let result = fn(...args);
      cache[n] = result;
      return result;
    }
  }
};

const memoizedConjugate = memoize(conjugate);

export const doLookup = (English, RankThresh) => {
  const Portuguese = lookup.filter( o => o.Rank <= RankThresh &&
    (o.Eng === English ||
      // check for conjugations too... this is hacky (is there a better library?)
      ['I', 'he', 'she', 'it'].map(pronoun => memoizedConjugate(pronoun, o.Eng))
      .concat([ 
        /*jump -> jumping*/ o.Eng + 'ing', 
        /*integrate -> integrating*/ o.Eng.length > 1 ? o.Eng.slice(0, -2) + 'ing' : undefined,
        /*fan -> fanning*/ o.Eng + o.Eng.slice(-1) + 'ing'
      ])
      .concat([
        /* jump -> jumped */ o.Eng + 'ed',
        /* fare -> fared */ o.Eng.length > 1 ? o.Eng.slice(0, -2) + 'ed' : undefined,
        // doesn't handle irregular past participle .. e.g. catch -> caught, eat -> ate, sell -> sold
      ])
      .includes(English)
    )
  );
  if (Portuguese && Portuguese.length && Portuguese[Portuguese.length-1].Port) {
    return({ ok: true, data: Portuguese[Portuguese.length-1].Port } );
  }
  else {
    return( {ok: false} );
  }
};

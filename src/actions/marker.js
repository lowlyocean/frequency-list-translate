export const SET_ENABLED = 'SET_ENABLED';
export const SET_LEX_COVERAGE = 'SET_LEX_COVERAGE';

export const setEnabled = data => ({
  type: SET_ENABLED, data
});

export const setLexCoverage = data => ({
  type: SET_LEX_COVERAGE, data
});

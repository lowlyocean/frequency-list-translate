export const SET_BOLD = 'SET_BOLD';

export const setBold = data => ({
  type: SET_BOLD, data
});
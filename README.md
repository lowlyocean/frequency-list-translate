# frequency-list-translate

A Chrome extension meant to help English readers learn Portguese. It translates words on a page; the user can adjust how many of the most frequently used words get translated, to match their pace of language learning. At the final setting, it translates the 5000 most common words which should cover 95% of text. At this level of lexical coverage, missing vocabulary can often be inferred from context.

Instructions: `npm ci` followed by `npm run build`. Then in Chrome Extensions, enable Developer Mode and select the generated *build* folder from *Load Unpacked*